﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IdentificationList
    {
        public int Id { get; set; }
        public string ColumnName { get; set; }
        public bool Deleted { get; set; }
    }
}
