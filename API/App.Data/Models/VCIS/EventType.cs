﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class EventType
    {
        public EventType()
        {
            Events = new HashSet<Event>();
        }

        public int Id { get; set; }
        public string EventTypeName { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Event> Events { get; set; }
    }
}
