﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Taxcode
    {
        public int Id { get; set; }
        public string TaxCode1 { get; set; }
    }
}
