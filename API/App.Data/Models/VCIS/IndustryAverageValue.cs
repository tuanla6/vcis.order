﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryAverageValue
    {
        public int Id { get; set; }
        public int BusinessLineId { get; set; }
        public decimal? AverageValue { get; set; }
        public bool Deleted { get; set; }
        public int? IndustryEverageYear { get; set; }
        public int? AverageIndustryId { get; set; }
        public short? IndustryAverageValueType { get; set; }

        public virtual AverageIndustry AverageIndustry { get; set; }
        public virtual BusinessLine BusinessLine { get; set; }
    }
}
