﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class JobPosition
    {
        public JobPosition()
        {
            PersonWorkingExperiences = new HashSet<PersonWorkingExperience>();
            staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public short? DisplayIndex { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<PersonWorkingExperience> PersonWorkingExperiences { get; set; }
        public virtual ICollection<Staff> staff { get; set; }
    }
}
