﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TempWriter
    {
        public string WriterName { get; set; }
        public string FullName { get; set; }
        public int? OfficeId { get; set; }
        public int? TypeId { get; set; }
    }
}
