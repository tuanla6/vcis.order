﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class AccessRight
    {
        public AccessRight()
        {
            RoleAccessRights = new HashSet<RoleAccessRight>();
        }

        public string AccessRightCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsPublic { get; set; }
        public bool? IsInternalDefault { get; set; }

        public virtual ICollection<RoleAccessRight> RoleAccessRights { get; set; }
    }
}
