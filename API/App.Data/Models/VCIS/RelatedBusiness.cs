﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class RelatedBusiness
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public int RelatedBusinessId { get; set; }
        public string Notes { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
        public virtual Business RelatedBusinessNavigation { get; set; }
    }
}
