﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Country
    {
        public Country()
        {
            BusinessAddresses = new HashSet<BusinessAddress>();
            PersonAddresses = new HashSet<PersonAddress>();
            Provinces = new HashSet<Province>();
        }

        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<BusinessAddress> BusinessAddresses { get; set; }
        public virtual ICollection<PersonAddress> PersonAddresses { get; set; }
        public virtual ICollection<Province> Provinces { get; set; }
    }
}
