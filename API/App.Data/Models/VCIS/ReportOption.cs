﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportOption
    {
        public ReportOption()
        {
            InverseParent = new HashSet<ReportOption>();
            ReportTypeOptions = new HashSet<ReportTypeOption>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int RecordOrder { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }
        public string Hierarchy { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public string Key { get; set; }

        public virtual ReportOption Parent { get; set; }
        public virtual ICollection<ReportOption> InverseParent { get; set; }
        public virtual ICollection<ReportTypeOption> ReportTypeOptions { get; set; }
    }
}
