﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BalanceSheetDefinition
    {
        public string AccountCode { get; set; }
        public string Name { get; set; }
        public byte? Level { get; set; }
        public string Standard { get; set; }
        public string Category { get; set; }
        public string Report { get; set; }
    }
}
