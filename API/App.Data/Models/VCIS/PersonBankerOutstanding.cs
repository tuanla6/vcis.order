﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonBankerOutstanding
    {
        public int Id { get; set; }
        public int PersonBankerId { get; set; }
        public decimal? OutstandingBalanceVnd { get; set; }
        public decimal? OutstandingBalanceUsd { get; set; }
        public short? OutstandingType { get; set; }
        public short? LoanClassification { get; set; }
        public DateTime? OutstandDate { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual PersonBanker PersonBanker { get; set; }
    }
}
