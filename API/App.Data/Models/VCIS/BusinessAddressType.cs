﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessAddressType
    {
        public BusinessAddressType()
        {
            BusinessAddresses = new HashSet<BusinessAddress>();
        }

        public int Id { get; set; }
        public string TypeName { get; set; }
        public short? DisplayIndex { get; set; }
        public bool Deleted { get; set; }
        public string VietnameseName { get; set; }
        public bool? AllowAccessOnline { get; set; }

        public virtual ICollection<BusinessAddress> BusinessAddresses { get; set; }
    }
}
