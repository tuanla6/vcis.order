﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Exchange
    {
        public int Id { get; set; }
        public string CurrencyCode { get; set; }
        public DateTime? Modified { get; set; }
        public decimal? Value { get; set; }
        public bool Deleted { get; set; }
    }
}
