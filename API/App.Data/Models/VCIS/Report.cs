﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Report
    {
        public Report()
        {
            ReportCreditScores = new HashSet<ReportCreditScore>();
        }

        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string RatingCode { get; set; }
        public string CreditRecommendation { get; set; }
        public string Interpretation { get; set; }
        public string Customer { get; set; }
        public string CustomerNotes { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public string Writer { get; set; }
        public bool? IsDefault { get; set; }
        public string AddressInterpretation { get; set; }
        public string ActivityInterpretation { get; set; }
        public string FinanceInterpretation { get; set; }
        public string FinanceConclustion { get; set; }
        public string NegativeNews { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public bool Deleted { get; set; }
        public int? ReportCreditRatingId { get; set; }
        public string Grade { get; set; }
        public int? VreportId { get; set; }
        public string GradeRatingCode { get; set; }

        public virtual Business Business { get; set; }
        public virtual ReportCreditRating ReportCreditRating { get; set; }
        public virtual ICollection<ReportCreditScore> ReportCreditScores { get; set; }
    }
}
