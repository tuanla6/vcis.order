﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class NeedIndex
    {
        public int Id { get; set; }
        public bool? NeedIndex1 { get; set; }
    }
}
