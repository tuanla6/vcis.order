﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Province
    {
        public Province()
        {
            BusinessAddresses = new HashSet<BusinessAddress>();
            Districts = new HashSet<District>();
            PersonAddresses = new HashSet<PersonAddress>();
        }

        public int Id { get; set; }
        public string ProvinceCode { get; set; }
        public string CountryCode { get; set; }
        public string ProvinceName { get; set; }
        public bool? IsCapital { get; set; }
        public string Category { get; set; }
        public string PhoneCode { get; set; }
        public int? CountryId { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Country Country { get; set; }
        public virtual ICollection<BusinessAddress> BusinessAddresses { get; set; }
        public virtual ICollection<District> Districts { get; set; }
        public virtual ICollection<PersonAddress> PersonAddresses { get; set; }
    }
}
