﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class UserUser
    {
        public int UserId { get; set; }
        public int UserId1 { get; set; }

        public virtual User User { get; set; }
        public virtual User UserId1Navigation { get; set; }
    }
}
