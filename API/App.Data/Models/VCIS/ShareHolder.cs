﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ShareHolder
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public int? ShareHolderBusinessId { get; set; }
        public int? ShareHolderPersonId { get; set; }
        public decimal? SharedCapital { get; set; }
        public double? SharedPercentage { get; set; }
        public long? SharedStocks { get; set; }
        public string ShareHolderOther { get; set; }
        public string Currency { get; set; }
        public string Notes { get; set; }
        public string BusinessRepresentative { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? RshareHolderId { get; set; }
        public DateTime? SharedPercentageDate { get; set; }
        public int? SubSharedUserId { get; set; }
        public double? SubPercentage { get; set; }
        public DateTime? SubPercentageDate { get; set; }

        public virtual Business Business { get; set; }
        public virtual Business ShareHolderBusiness { get; set; }
        public virtual Person ShareHolderPerson { get; set; }
    }
}
