﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportCreditCriterion
    {
        public ReportCreditCriterion()
        {
            ReportCreditScores = new HashSet<ReportCreditScore>();
        }

        public int Id { get; set; }
        public string Criterion { get; set; }
        public string Factor { get; set; }
        public short MaximumScore { get; set; }
        public short? DisplayOrder { get; set; }
        public string Type { get; set; }
        public bool Deleted { get; set; }
        public string VcriterionCode { get; set; }

        public virtual ICollection<ReportCreditScore> ReportCreditScores { get; set; }
    }
}
