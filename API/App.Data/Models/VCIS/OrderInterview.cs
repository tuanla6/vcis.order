﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class OrderInterview
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int BusinessId { get; set; }
        public string Code { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
