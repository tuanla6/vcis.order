﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessInterview
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string Interviewer { get; set; }
        public string Interviewees { get; set; }
        public DateTime? InterviewDate { get; set; }
        public string Phone { get; set; }
        public string Department { get; set; }
        public string CooperateAttitude { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public string Email { get; set; }
        public bool? Gender { get; set; }
        public bool? Result { get; set; }
        public string Notes { get; set; }

        public virtual Business Business { get; set; }
    }
}
