﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class EconomicIndicator
    {
        public int Id { get; set; }
        public int? Year { get; set; }
        public decimal? Gdp { get; set; }
        public decimal? GdpperCapital { get; set; }
        public decimal? Gdpgrowth { get; set; }
        public decimal? AgricultureForestryFishingGrowth { get; set; }
        public decimal? IndustryConstructionGrowth { get; set; }
        public decimal? TradeServicesGrowth { get; set; }
        public string Picture { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public short? DisplayIndex { get; set; }
        public bool Deleted { get; set; }
        public decimal? Population { get; set; }
    }
}
