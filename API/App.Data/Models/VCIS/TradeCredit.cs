﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TradeCredit
    {
        public TradeCredit()
        {
            AdditionalInformations = new HashSet<AdditionalInformation>();
            TradeCreditSecuredTransactionRecords = new HashSet<TradeCreditSecuredTransactionRecord>();
            TradeCreditTotaloutStandings = new HashSet<TradeCreditTotaloutStanding>();
        }

        public int Id { get; set; }
        public decimal? TradeCreditLimit { get; set; }
        public string Revision { get; set; }
        public string Notes { get; set; }
        public DateTime? TradeCreditDate { get; set; }
        public int? BusinessId { get; set; }
        public string Currency { get; set; }
        public decimal? CreditInquiry { get; set; }
        public string InquiryCurrency { get; set; }
        public string NegativePayment { get; set; }
        public string NegativeCreditRecords { get; set; }
        public string SpecialMentionAmounts { get; set; }
        public string AssetBasedLending { get; set; }
        public int? NumberOfCollateral { get; set; }
        public int? NumberOfCreditInstitutions { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? AdditionalId { get; set; }
        public string CreditEnquiryNotes { get; set; }
        public decimal? TradeCreditLimit2 { get; set; }
        public string Currency2 { get; set; }
        public string Notes2 { get; set; }
        public string CreditEnquiryTerm { get; set; }
        public string Term1 { get; set; }
        public string Term2 { get; set; }
        public DateTime? CreditLimitDate { get; set; }
        public bool? IsTradeCreditLimit1 { get; set; }
        public bool? IsTradeCreditLimit2 { get; set; }
        public short? Type { get; set; }
        public int? FinancialReportId { get; set; }
        public string CommonNotes { get; set; }
        public string GradeRatingCode { get; set; }
        public bool? IsNotRecommendation { get; set; }

        public virtual Business Business { get; set; }
        public virtual ICollection<AdditionalInformation> AdditionalInformations { get; set; }
        public virtual ICollection<TradeCreditSecuredTransactionRecord> TradeCreditSecuredTransactionRecords { get; set; }
        public virtual ICollection<TradeCreditTotaloutStanding> TradeCreditTotaloutStandings { get; set; }
    }
}
