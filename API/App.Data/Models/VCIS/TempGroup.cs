﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TempGroup
    {
        public string WriterName { get; set; }
        public int? GroupId { get; set; }
        public bool? IsLeader { get; set; }
    }
}
