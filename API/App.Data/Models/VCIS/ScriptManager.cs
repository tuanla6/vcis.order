﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ScriptManager
    {
        public int Id { get; set; }
        public string ScriptNumber { get; set; }
        public string ScriptName { get; set; }
    }
}
