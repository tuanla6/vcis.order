﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessAddress
    {
        public BusinessAddress()
        {
            BusinessBankers = new HashSet<BusinessBanker>();
            BusinessImages = new HashSet<BusinessImage>();
            PersonBankers = new HashSet<PersonBanker>();
        }

        public int Id { get; set; }
        public int BusinessId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string Notes { get; set; }
        public bool? IsDisabled { get; set; }
        public bool? IsDefault { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string OccupiedArea { get; set; }
        public string LandUseRight { get; set; }
        public int? PersonId { get; set; }
        public short? DisplayIndex { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? AddressTypeId { get; set; }
        public bool Deleted { get; set; }
        public int? VaddressId { get; set; }
        public int? RaddressId { get; set; }
        public string VnfullAddress { get; set; }
        public string PhoneByClient { get; set; }
        public string FaxByClient { get; set; }
        public string EmailByClient { get; set; }
        public string WebsiteByClient { get; set; }
        public string TaxCode { get; set; }

        public virtual BusinessAddressType AddressType { get; set; }
        public virtual Business Business { get; set; }
        public virtual Country Country { get; set; }
        public virtual District District { get; set; }
        public virtual Province Province { get; set; }
        public virtual ICollection<BusinessBanker> BusinessBankers { get; set; }
        public virtual ICollection<BusinessImage> BusinessImages { get; set; }
        public virtual ICollection<PersonBanker> PersonBankers { get; set; }
    }
}
