﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessesTest
    {
        public int Id { get; set; }
        public string Crid1 { get; set; }
        public string IndustryCode { get; set; }
        public int? BusinessTypeId { get; set; }
        public int? ParentBusinessId { get; set; }
        public string Name { get; set; }
        public string RegisteredName { get; set; }
        public string TradeName { get; set; }
        public string EnglishName { get; set; }
        public string TaxCode { get; set; }
        public string Website { get; set; }
        public string DetailedInfo { get; set; }
        public string Founder { get; set; }
        public DateTime? FoundDate { get; set; }
        public string EstablishmentDecisionNumber { get; set; }
        public DateTime? EstablishmentDate { get; set; }
        public string EstablishmentAuthority { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string RegistrationAuthority { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public decimal? RegisteredCapital { get; set; }
        public int? TotalEmployees { get; set; }
        public string Qualification { get; set; }
        public string Currency { get; set; }
        public string TradeMorality { get; set; }
        public string Liquidity { get; set; }
        public string Bankruptcy { get; set; }
        public string DevelopmentTrend { get; set; }
        public string PaymentStatus { get; set; }
        public string LitigationData { get; set; }
        public string FinancialSituation { get; set; }
        public string SaleMethod { get; set; }
        public string StatusBonds { get; set; }
        public string Stocksymbol { get; set; }
        public string StockMarket { get; set; }
        public int? OutstandingStock { get; set; }
        public decimal? PaidUpCapital { get; set; }
        public string BusinessLinecode { get; set; }
        public string BusinessSize { get; set; }
        public string Duration { get; set; }
        public string PublicOpinion { get; set; }
        public string InvestmentCertificateNo { get; set; }
        public decimal? RegisteredInvestmentAmount { get; set; }
        public string PaymentMethod { get; set; }
        public bool? IsSpecialDate { get; set; }
        public DateTime? InvestmentCertificateDate { get; set; }
        public string InvestmentCertificatePlace { get; set; }
        public bool IsRegisteredCapital { get; set; }
        public int? AdditionBusinessTypeId { get; set; }
        public bool? IsProjectCode { get; set; }
        public string BusinessNotes { get; set; }
        public string Status { get; set; }
        public bool? IsFdi { get; set; }
        public bool Deleted { get; set; }
        public int? IndustryId { get; set; }
        public int? Vid { get; set; }
        public int? Rid { get; set; }
        public bool? IsReport { get; set; }
        public string RegisteredInvestmentCurrency { get; set; }
        public string Crid { get; set; }
        public string Vnname { get; set; }
        public string VnregistrationAuthority { get; set; }
        public string Vndescription { get; set; }
        public bool IsPublished { get; set; }
        public bool IsTranslated { get; set; }
        public int? YearEmployees { get; set; }
        public bool? NeedIndex { get; set; }
        public int? SpecialBusinessType { get; set; }
        public bool? IsTop10Companies { get; set; }
        public bool? IsCompetitorAnalysis { get; set; }
        public string CurrencyPaidUpCapital { get; set; }
    }
}
