﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Banker
    {
        public Banker()
        {
            BusinessBankers = new HashSet<BusinessBanker>();
            PersonBankers = new HashSet<PersonBanker>();
        }

        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? UpdatedBy { get; set; }
        public int? VbankerId { get; set; }

        public virtual Business Business { get; set; }
        public virtual ICollection<BusinessBanker> BusinessBankers { get; set; }
        public virtual ICollection<PersonBanker> PersonBankers { get; set; }
    }
}
