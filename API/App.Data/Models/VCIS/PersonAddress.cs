﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonAddress
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int? DistrictId { get; set; }
        public int? ProvinceId { get; set; }
        public int? CountryId { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string Notes { get; set; }
        public bool? IsDisabled { get; set; }
        public bool? IsDefault { get; set; }
        public short? DisplayIndex { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? AddressTypeId { get; set; }
        public bool Deleted { get; set; }
        public int? VaddressId { get; set; }
        public int? RaddressId { get; set; }

        public virtual PersonAddressType AddressType { get; set; }
        public virtual Country Country { get; set; }
        public virtual District District { get; set; }
        public virtual Person Person { get; set; }
        public virtual Province Province { get; set; }
    }
}
