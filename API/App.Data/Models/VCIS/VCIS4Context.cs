﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class VCIS4Context : DbContext
    {
        public VCIS4Context() : base()
        {
        }

        public VCIS4Context(DbContextOptions<VCIS4Context> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessRight> AccessRights { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<AddIndustryInfoXoa> AddIndustryInfoXoas { get; set; }
        public virtual DbSet<AdditionalInformation> AdditionalInformations { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AverageIndustry> AverageIndustries { get; set; }
        public virtual DbSet<AverageIndustryComparison> AverageIndustryComparisons { get; set; }
        public virtual DbSet<BalanceSheetDefinition> BalanceSheetDefinitions { get; set; }
        public virtual DbSet<Banker> Bankers { get; set; }
        public virtual DbSet<Business> Businesses { get; set; }
        public virtual DbSet<BusinessAddress> BusinessAddresses { get; set; }
        public virtual DbSet<BusinessAddressType> BusinessAddressTypes { get; set; }
        public virtual DbSet<BusinessBanker> BusinessBankers { get; set; }
        public virtual DbSet<BusinessBankerOutstanding> BusinessBankerOutstandings { get; set; }
        public virtual DbSet<BusinessImage> BusinessImages { get; set; }
        public virtual DbSet<BusinessInterview> BusinessInterviews { get; set; }
        public virtual DbSet<BusinessLine> BusinessLines { get; set; }
        public virtual DbSet<BusinessLog> BusinessLogs { get; set; }
        public virtual DbSet<BusinessScale> BusinessScales { get; set; }
        public virtual DbSet<BusinessStatus> BusinessStatuses { get; set; }
        public virtual DbSet<BusinessType> BusinessTypes { get; set; }
        public virtual DbSet<BusinessesTest> BusinessesTests { get; set; }
        public virtual DbSet<CapNhatMaNganhChinh> CapNhatMaNganhChinhs { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Crawler> Crawlers { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<DocumentMedia> DocumentMedias { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<EconomicIndicator> EconomicIndicators { get; set; }
        public virtual DbSet<EmployeesDelete> EmployeesDeletes { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<EventType> EventTypes { get; set; }
        public virtual DbSet<Exchange> Exchanges { get; set; }
        public virtual DbSet<ExchangeRate> ExchangeRates { get; set; }
        public virtual DbSet<FamilyPosition> FamilyPositions { get; set; }
        public virtual DbSet<FinancialAccountDefinition> FinancialAccountDefinitions { get; set; }
        public virtual DbSet<FinancialAccountReportDefine> FinancialAccountReportDefines { get; set; }
        public virtual DbSet<FinancialReport> FinancialReports { get; set; }
        public virtual DbSet<FinancialReportData> FinancialReportDatas { get; set; }
        public virtual DbSet<IdentificationList> IdentificationLists { get; set; }
        public virtual DbSet<ImportExport> ImportExports { get; set; }
        public virtual DbSet<IndependentBranch> IndependentBranches { get; set; }
        public virtual DbSet<Industry> Industries { get; set; }
        public virtual DbSet<IndustryAverageReport> IndustryAverageReports { get; set; }
        public virtual DbSet<IndustryAverageValue> IndustryAverageValues { get; set; }
        public virtual DbSet<IndustryData> IndustryDatas { get; set; }
        public virtual DbSet<IndustryDataDefault> IndustryDataDefaults { get; set; }
        public virtual DbSet<IndustryDataDefinition> IndustryDataDefinitions { get; set; }
        public virtual DbSet<IndustryInformation> IndustryInformations { get; set; }
        public virtual DbSet<IndustryTitle> IndustryTitles { get; set; }
        public virtual DbSet<JobPosition> JobPositions { get; set; }
        public virtual DbSet<Keyword> Keywords { get; set; }
        public virtual DbSet<LaoDongTbn> LaoDongTbns { get; set; }
        public virtual DbSet<LitigationRecord> LitigationRecords { get; set; }
        public virtual DbSet<MajorClient> MajorClients { get; set; }
        public virtual DbSet<MajorSupplier> MajorSuppliers { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistories { get; set; }
        public virtual DbSet<NeedIndex> NeedIndices { get; set; }
        public virtual DbSet<NegativeInfo> NegativeInfos { get; set; }
        public virtual DbSet<NegativeNews> NegativeNews { get; set; }
        public virtual DbSet<NegativePayment> NegativePayments { get; set; }
        public virtual DbSet<NegativePaymentRecord> NegativePaymentRecords { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Office> Offices { get; set; }
        public virtual DbSet<OrderInterview> OrderInterviews { get; set; }
        public virtual DbSet<PaymentHistory> PaymentHistories { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PersonAddress> PersonAddresses { get; set; }
        public virtual DbSet<PersonAddressType> PersonAddressTypes { get; set; }
        public virtual DbSet<PersonBanker> PersonBankers { get; set; }
        public virtual DbSet<PersonBankerOutstanding> PersonBankerOutstandings { get; set; }
        public virtual DbSet<PersonCredit> PersonCredits { get; set; }
        public virtual DbSet<PersonDocument> PersonDocuments { get; set; }
        public virtual DbSet<PersonEducation> PersonEducations { get; set; }
        public virtual DbSet<PersonLitigationCheck> PersonLitigationChecks { get; set; }
        public virtual DbSet<PersonPressMediaResult> PersonPressMediaResults { get; set; }
        public virtual DbSet<PersonWorkingExperience> PersonWorkingExperiences { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<RelatedBusiness> RelatedBusinesses { get; set; }
        public virtual DbSet<RelatedPerson> RelatedPersons { get; set; }
        public virtual DbSet<Report> Reports { get; set; }
        public virtual DbSet<ReportCreditCriterion> ReportCreditCriterions { get; set; }
        public virtual DbSet<ReportCreditRating> ReportCreditRatings { get; set; }
        public virtual DbSet<ReportCreditScore> ReportCreditScores { get; set; }
        public virtual DbSet<ReportOption> ReportOptions { get; set; }
        public virtual DbSet<ReportType> ReportTypes { get; set; }
        public virtual DbSet<ReportTypeOption> ReportTypeOptions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleAccessRight> RoleAccessRights { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<RolesOld> RolesOlds { get; set; }
        public virtual DbSet<ScriptManager> ScriptManagers { get; set; }
        public virtual DbSet<SettingType> SettingTypes { get; set; }
        public virtual DbSet<ShareHolder> ShareHolders { get; set; }
        public virtual DbSet<SiteSetting> SiteSettings { get; set; }
        public virtual DbSet<SoLieuTbn> SoLieuTbns { get; set; }
        public virtual DbSet<Staff> Staffs { get; set; }
        public virtual DbSet<SyncVersionXoa> SyncVersionXoas { get; set; }
        public virtual DbSet<SysLogBusiness> SysLogBusinesses { get; set; }
        public virtual DbSet<Taxcode> Taxcodes { get; set; }
        public virtual DbSet<Tbn> Tbns { get; set; }
        public virtual DbSet<TempGroup> TempGroups { get; set; }
        public virtual DbSet<TempWriter> TempWriters { get; set; }
        public virtual DbSet<TradeCredit> TradeCredits { get; set; }
        public virtual DbSet<TradeCreditSecuredTransactionRecord> TradeCreditSecuredTransactionRecords { get; set; }
        public virtual DbSet<TradeCreditTotaloutStanding> TradeCreditTotaloutStandings { get; set; }
        public virtual DbSet<TradeReference> TradeReferences { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserInRole> UserInRoles { get; set; }
        public virtual DbSet<UserTypeInRolesXoa> UserTypeInRolesXoas { get; set; }
        public virtual DbSet<UserUser> UserUsers { get; set; }
        public virtual DbSet<UserUserGroup> UserUserGroups { get; set; }
        public virtual DbSet<YearEmployee> YearEmployees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=27.72.104.59,1434;Initial Catalog=VCIS4;Integrated Security=False;Persist Security Info=False;User ID=vnc_dev;Password=Dev@A123");
            }
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AccessRight>(entity =>
            {
                entity.HasKey(e => e.AccessRightCode)
                    .HasName("PK_SystemRight");

                entity.Property(e => e.AccessRightCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.IsInternalDefault).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsPublic).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<Activity>(entity =>
            {
                entity.HasIndex(e => new { e.IsMain, e.Deleted }, "Idx_activities_bussiness");

                entity.HasIndex(e => new { e.BusinessId, e.IsMain, e.Deleted }, "Idx_activities_bussiness_2");

                entity.HasIndex(e => new { e.BusinessLineId, e.IsMain, e.Deleted }, "Idx_activities_bussiness_3");

                entity.Property(e => e.ActivityDetails).HasMaxLength(4000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_LinesOfBusiness_Business");

                entity.HasOne(d => d.BusinessLine)
                    .WithMany(p => p.Activities)
                    .HasForeignKey(d => d.BusinessLineId)
                    .HasConstraintName("FK_Activities_BusinessLines");
            });

            modelBuilder.Entity<AddIndustryInfoXoa>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("AddIndustryInfo_Xoa");

                entity.Property(e => e.AverageAssets2019).HasColumnName("Average Assets 2019");

                entity.Property(e => e.AverageOwnerSEquity2019).HasColumnName("Average Owner's Equity 2019");

                entity.Property(e => e.AverageProfitAfterTax2019).HasColumnName("Average Profit after Tax 2019");

                entity.Property(e => e.AverageSales2019).HasColumnName("Average Sales 2019");

                entity.Property(e => e.Cagr).HasColumnName("CAGR");

                entity.Property(e => e.Cagr1).HasColumnName("CAGR1");

                entity.Property(e => e.Cagr2).HasColumnName("CAGR2");

                entity.Property(e => e.Cagr3).HasColumnName("CAGR3");

                entity.Property(e => e.Vsiccode)
                    .HasMaxLength(255)
                    .HasColumnName("VSICCode");
            });

            modelBuilder.Entity<AdditionalInformation>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.HasIndex(e => e.TradeCreditId, "IX_TradeCreditId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.AdditionalInformations)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.AdditionalInformations_dbo.Businesses_BusinessId");

                entity.HasOne(d => d.TradeCredit)
                    .WithMany(p => p.AdditionalInformations)
                    .HasForeignKey(d => d.TradeCreditId)
                    .HasConstraintName("FK_dbo.AdditionalInformations_dbo.TradeCredits_TradeCreditId");
            });

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.LandUseRight).HasMaxLength(50);

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.LastUpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.Number)
                    .HasMaxLength(200)
                    .HasComment("");

                entity.Property(e => e.OccupiedArea).HasMaxLength(100);

                entity.Property(e => e.PersonId).HasColumnName("PersonID");

                entity.Property(e => e.Phone).HasMaxLength(200);

                entity.Property(e => e.Postcode).HasMaxLength(50);

                entity.Property(e => e.ProvinceCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Street).HasMaxLength(200);

                entity.Property(e => e.Website).HasMaxLength(200);
            });

            modelBuilder.Entity<AverageIndustry>(entity =>
            {
                entity.Property(e => e.AverageIndustryCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AverageIndustryName)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.AverageUnit).HasMaxLength(20);
            });

            modelBuilder.Entity<AverageIndustryComparison>(entity =>
            {
                entity.Property(e => e.Oemax)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("OEMax");

                entity.Property(e => e.Oemin)
                    .HasColumnType("decimal(18, 0)")
                    .HasColumnName("OEMin");

                entity.Property(e => e.ProfitBeforeTaxMax).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ProfitBeforeTaxMin).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotalAssetsMax).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotalAssetsMin).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotalSalesMax).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TotalSalesMin).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.AverageIndustryComparisons)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("FK_AverageIndustryComparisons_IndustryAverageReports");
            });

            modelBuilder.Entity<BalanceSheetDefinition>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.AccountCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Report).HasMaxLength(50);

                entity.Property(e => e.Standard).HasMaxLength(50);
            });

            modelBuilder.Entity<Banker>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VbankerId).HasColumnName("VBankerId");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Bankers)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Banker_Business");
            });

            modelBuilder.Entity<Business>(entity =>
            {
                entity.HasIndex(e => new { e.TradeName, e.EnglishName }, "IX_EnglishName");

                entity.HasIndex(e => new { e.Name, e.RegisteredName }, "IX_Name_RegisteredName");

                entity.HasIndex(e => e.ParentBusinessId, "IX_ParentBusinessId");

                entity.HasIndex(e => e.TaxCode, "Ix_TaxCode");

                entity.Property(e => e.Bankruptcy).HasMaxLength(50);

                entity.Property(e => e.BusinessLinecode).HasMaxLength(50);

                entity.Property(e => e.BusinessNotes).HasMaxLength(800);

                entity.Property(e => e.BusinessSize)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Crid)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CRID");

                entity.Property(e => e.Crid1)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CRID1");

                entity.Property(e => e.Currency).HasMaxLength(50);

                entity.Property(e => e.CurrencyPaidUpCapital).HasMaxLength(5);

                entity.Property(e => e.DevelopmentTrend).HasMaxLength(50);

                entity.Property(e => e.Duration).HasMaxLength(200);

                entity.Property(e => e.EnglishName).HasMaxLength(200);

                entity.Property(e => e.EstablishmentAuthority).HasMaxLength(500);

                entity.Property(e => e.EstablishmentDate).HasColumnType("date");

                entity.Property(e => e.EstablishmentDecisionNumber)
                    .HasMaxLength(50)
                    .HasComment("Establishment Decision Number");

                entity.Property(e => e.FinancialSituation).HasMaxLength(100);

                entity.Property(e => e.FoundDate).HasColumnType("date");

                entity.Property(e => e.Founder).HasMaxLength(200);

                entity.Property(e => e.IndustryCode)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.InvestmentCertificateDate).HasColumnType("date");

                entity.Property(e => e.InvestmentCertificateNo).HasMaxLength(50);

                entity.Property(e => e.InvestmentCertificatePlace).HasMaxLength(500);

                entity.Property(e => e.IsPublished)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Liquidity).HasMaxLength(50);

                entity.Property(e => e.LitigationData).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.PaidUpCapital).HasColumnType("money");

                entity.Property(e => e.ParentBusinessId).HasColumnName("ParentBusinessID");

                entity.Property(e => e.PaymentMethod).HasMaxLength(100);

                entity.Property(e => e.PaymentStatus).HasMaxLength(100);

                entity.Property(e => e.PublicOpinion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Qualification).HasMaxLength(50);

                entity.Property(e => e.RegisteredCapital).HasColumnType("money");

                entity.Property(e => e.RegisteredInvestmentAmount).HasColumnType("money");

                entity.Property(e => e.RegisteredName).HasMaxLength(200);

                entity.Property(e => e.RegistrationAuthority).HasMaxLength(500);

                entity.Property(e => e.RegistrationDate).HasColumnType("date");

                entity.Property(e => e.RegistrationNumber)
                    .HasMaxLength(50)
                    .HasComment("Business Registration Number");

                entity.Property(e => e.Rid).HasColumnName("RID");

                entity.Property(e => e.SaleMethod).HasMaxLength(100);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.StatusBonds)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StockMarket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stocksymbol)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode).HasMaxLength(100);

                entity.Property(e => e.TradeMorality).HasMaxLength(50);

                entity.Property(e => e.TradeName).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Vid).HasColumnName("VID");

                entity.Property(e => e.Vndescription)
                    .HasMaxLength(1800)
                    .HasColumnName("VNDescription");

                entity.Property(e => e.Vnname)
                    .HasMaxLength(200)
                    .HasColumnName("VNName");

                entity.Property(e => e.VnregistrationAuthority)
                    .HasMaxLength(250)
                    .HasColumnName("VNRegistrationAuthority");

                entity.Property(e => e.Website).HasMaxLength(100);

                entity.HasOne(d => d.BusinessType)
                    .WithMany(p => p.Businesses)
                    .HasForeignKey(d => d.BusinessTypeId)
                    .HasConstraintName("FK_Businesses_BusinessTypes");
            });

            modelBuilder.Entity<BusinessAddress>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(250);

                entity.Property(e => e.EmailByClient).HasMaxLength(200);

                entity.Property(e => e.Fax).HasMaxLength(50);

                entity.Property(e => e.FaxByClient).HasMaxLength(200);

                entity.Property(e => e.LandUseRight).HasMaxLength(50);

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.Number).HasMaxLength(200);

                entity.Property(e => e.OccupiedArea).HasMaxLength(100);

                entity.Property(e => e.Phone).HasMaxLength(200);

                entity.Property(e => e.PhoneByClient).HasMaxLength(200);

                entity.Property(e => e.Postcode).HasMaxLength(50);

                entity.Property(e => e.RaddressId).HasColumnName("RAddressId");

                entity.Property(e => e.Street).HasMaxLength(200);

                entity.Property(e => e.TaxCode).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VaddressId).HasColumnName("VAddressId");

                entity.Property(e => e.VnfullAddress)
                    .HasMaxLength(250)
                    .HasColumnName("VNFullAddress");

                entity.Property(e => e.Website).HasMaxLength(200);

                entity.Property(e => e.WebsiteByClient).HasMaxLength(200);

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.BusinessAddresses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .HasConstraintName("FK_BusinessAddresses_BusinessAddressTypes");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.BusinessAddresses)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_BusinessAddress_Business");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.BusinessAddresses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_BusinessAddresses_Countries");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.BusinessAddresses)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_BusinessAddresses_Districts");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.BusinessAddresses)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_BusinessAddresses_Provinces");
            });

            modelBuilder.Entity<BusinessAddressType>(entity =>
            {
                entity.Property(e => e.TypeName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<BusinessBanker>(entity =>
            {
                entity.Property(e => e.AccountForeign).HasMaxLength(50);

                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditCardDate).HasColumnType("datetime");

                entity.Property(e => e.CreditCardName).HasMaxLength(255);

                entity.Property(e => e.CreditCardNoofDayOverDue).HasMaxLength(255);

                entity.Property(e => e.CreditCardOutstandingAmount).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.CreditCardOverdueAmountVnd)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("CreditCardOverdueAmountVND");

                entity.Property(e => e.OpeningDate).HasColumnType("datetime");

                entity.Property(e => e.OutstandingBalanceUsd)
                    .HasColumnType("decimal(23, 4)")
                    .HasColumnName("OutstandingBalanceUSD");

                entity.Property(e => e.OutstandingBalanceVnd)
                    .HasColumnType("decimal(23, 4)")
                    .HasColumnName("OutstandingBalanceVND");

                entity.Property(e => e.SwiftCode).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.BusinessBankers)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_BusinessBankers_BusinessAddresses");

                entity.HasOne(d => d.Banker)
                    .WithMany(p => p.BusinessBankers)
                    .HasForeignKey(d => d.BankerId)
                    .HasConstraintName("FK_BusinessBanker_Banker");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.BusinessBankers)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BusinessBankers_Businesses");
            });

            modelBuilder.Entity<BusinessBankerOutstanding>(entity =>
            {
                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OutstandDate).HasColumnType("datetime");

                entity.Property(e => e.OutstandingBalanceUsd)
                    .HasColumnType("decimal(23, 4)")
                    .HasColumnName("OutstandingBalanceUSD");

                entity.Property(e => e.OutstandingBalanceVnd)
                    .HasColumnType("decimal(23, 4)")
                    .HasColumnName("OutstandingBalanceVND");

                entity.Property(e => e.UpdatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.BusinessBanker)
                    .WithMany(p => p.BusinessBankerOutstandings)
                    .HasForeignKey(d => d.BusinessBankerId)
                    .HasConstraintName("FK_BusinessBankerOutstandings_BusinessBankers");
            });

            modelBuilder.Entity<BusinessImage>(entity =>
            {
                entity.HasIndex(e => e.BusinessAddressId, "IX_BusinessAddressId");

                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.ContentType).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FileName).HasMaxLength(200);

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Url).HasMaxLength(500);

                entity.HasOne(d => d.BusinessAddress)
                    .WithMany(p => p.BusinessImages)
                    .HasForeignKey(d => d.BusinessAddressId)
                    .HasConstraintName("FK_dbo.BusinessImages_dbo.BusinessAddresses_BusinessAddressId");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.BusinessImages)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.BusinessImages_dbo.Businesses_BusinessId");
            });

            modelBuilder.Entity<BusinessInterview>(entity =>
            {
                entity.Property(e => e.CooperateAttitude).HasMaxLength(1000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Department).HasMaxLength(400);

                entity.Property(e => e.Email).HasMaxLength(512);

                entity.Property(e => e.InterviewDate).HasColumnType("datetime");

                entity.Property(e => e.Interviewees).HasMaxLength(200);

                entity.Property(e => e.Interviewer).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.Phone).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.BusinessInterviews)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BusinessInterview_Business");
            });

            modelBuilder.Entity<BusinessLine>(entity =>
            {
                entity.HasIndex(e => new { e.Deleted, e.Vsic }, "Idx_BusinessLines_2");

                entity.Property(e => e.BusinessLineCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.BusinessLineFull).HasMaxLength(4000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.IndustryCode)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Nace)
                    .HasMaxLength(10)
                    .HasColumnName("NACE");

                entity.Property(e => e.Nacedescription)
                    .HasMaxLength(1000)
                    .HasColumnName("NACEDescription");

                entity.Property(e => e.Naics)
                    .HasMaxLength(10)
                    .HasColumnName("NAICS");

                entity.Property(e => e.Naicsdescription)
                    .HasMaxLength(1000)
                    .HasColumnName("NAICSDescription");

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.ParentId)
                    .HasMaxLength(50)
                    .HasColumnName("ParentID");

                entity.Property(e => e.RelatedNaceindustry)
                    .HasMaxLength(255)
                    .HasColumnName("RelatedNACEIndustry");

                entity.Property(e => e.RelatedNaicsindustry)
                    .HasMaxLength(255)
                    .HasColumnName("RelatedNAICSIndustry");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Vsic)
                    .HasMaxLength(10)
                    .HasColumnName("VSIC");

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.BusinessLines)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("FK_BusinessLines_Industries");
            });

            modelBuilder.Entity<BusinessLog>(entity =>
            {
                entity.Property(e => e.ColumnName).HasMaxLength(100);

                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.NewValue).HasMaxLength(1000);

                entity.Property(e => e.OldValue).HasMaxLength(1000);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.BusinessLogs)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_BusinessLog_Business");
            });

            modelBuilder.Entity<BusinessScale>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.MaxValue).HasColumnType("decimal(23, 4)");

                entity.Property(e => e.MinValue).HasColumnType("decimal(23, 4)");

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<BusinessStatus>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.BusinessStatus1)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("BusinessStatus")
                    .IsFixedLength(true);

                entity.Property(e => e.F4).HasMaxLength(255);

                entity.Property(e => e.Interview).HasMaxLength(255);

                entity.Property(e => e.Msdn)
                    .HasMaxLength(255)
                    .HasColumnName("MSDN");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .HasColumnName("STATUS");
            });

            modelBuilder.Entity<BusinessType>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.TypeName).HasMaxLength(100);
            });

            modelBuilder.Entity<BusinessesTest>(entity =>
            {
                entity.ToTable("Businesses_Test");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Bankruptcy).HasMaxLength(50);

                entity.Property(e => e.BusinessLinecode).HasMaxLength(50);

                entity.Property(e => e.BusinessNotes).HasMaxLength(800);

                entity.Property(e => e.BusinessSize)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Crid)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CRID");

                entity.Property(e => e.Crid1)
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .HasColumnName("CRID1");

                entity.Property(e => e.Currency).HasMaxLength(50);

                entity.Property(e => e.CurrencyPaidUpCapital).HasMaxLength(5);

                entity.Property(e => e.DevelopmentTrend).HasMaxLength(50);

                entity.Property(e => e.Duration).HasMaxLength(200);

                entity.Property(e => e.EnglishName).HasMaxLength(200);

                entity.Property(e => e.EstablishmentAuthority).HasMaxLength(500);

                entity.Property(e => e.EstablishmentDate).HasColumnType("date");

                entity.Property(e => e.EstablishmentDecisionNumber).HasMaxLength(50);

                entity.Property(e => e.FinancialSituation).HasMaxLength(100);

                entity.Property(e => e.FoundDate).HasColumnType("date");

                entity.Property(e => e.Founder).HasMaxLength(200);

                entity.Property(e => e.IndustryCode)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.InvestmentCertificateDate).HasColumnType("date");

                entity.Property(e => e.InvestmentCertificateNo).HasMaxLength(50);

                entity.Property(e => e.InvestmentCertificatePlace).HasMaxLength(500);

                entity.Property(e => e.Liquidity).HasMaxLength(50);

                entity.Property(e => e.LitigationData).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.PaidUpCapital).HasColumnType("money");

                entity.Property(e => e.ParentBusinessId).HasColumnName("ParentBusinessID");

                entity.Property(e => e.PaymentMethod).HasMaxLength(100);

                entity.Property(e => e.PaymentStatus).HasMaxLength(100);

                entity.Property(e => e.PublicOpinion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Qualification).HasMaxLength(50);

                entity.Property(e => e.RegisteredCapital).HasColumnType("money");

                entity.Property(e => e.RegisteredInvestmentAmount).HasColumnType("money");

                entity.Property(e => e.RegisteredName).HasMaxLength(200);

                entity.Property(e => e.RegistrationAuthority).HasMaxLength(500);

                entity.Property(e => e.RegistrationDate).HasColumnType("date");

                entity.Property(e => e.RegistrationNumber).HasMaxLength(50);

                entity.Property(e => e.Rid).HasColumnName("RID");

                entity.Property(e => e.SaleMethod).HasMaxLength(100);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.StatusBonds)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StockMarket)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stocksymbol)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaxCode).HasMaxLength(100);

                entity.Property(e => e.TradeMorality).HasMaxLength(50);

                entity.Property(e => e.TradeName).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Vid).HasColumnName("VID");

                entity.Property(e => e.Vndescription)
                    .HasMaxLength(1800)
                    .HasColumnName("VNDescription");

                entity.Property(e => e.Vnname)
                    .HasMaxLength(200)
                    .HasColumnName("VNName");

                entity.Property(e => e.VnregistrationAuthority)
                    .HasMaxLength(250)
                    .HasColumnName("VNRegistrationAuthority");

                entity.Property(e => e.Website).HasMaxLength(100);
            });

            modelBuilder.Entity<CapNhatMaNganhChinh>(entity =>
            {
                entity.ToTable("CapNhatMaNganhChinh");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.F3).HasMaxLength(255);

                entity.Property(e => e.F4).HasMaxLength(255);

                entity.Property(e => e.Mn4)
                    .HasMaxLength(255)
                    .HasColumnName("MN4");

                entity.Property(e => e.Mst)
                    .HasMaxLength(255)
                    .HasColumnName("MST");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountryName).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Crawler>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(2500);

                entity.Property(e => e.ObjectId).HasMaxLength(50);

                entity.Property(e => e.Sentiment).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.Source).HasMaxLength(1000);

                entity.Property(e => e.Title).HasMaxLength(500);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.CurrencyName).HasMaxLength(50);

                entity.Property(e => e.CurrencySymbol).HasMaxLength(10);

                entity.Property(e => e.Sector).HasMaxLength(1000);
            });

            modelBuilder.Entity<District>(entity =>
            {
                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName).HasMaxLength(100);

                entity.Property(e => e.ProvinceCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.Districts)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_Districts_Provinces");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerCode).HasMaxLength(200);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Name).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Documents)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Documents_Businesses");

                entity.HasOne(d => d.DocumentMedia)
                    .WithMany(p => p.Documents)
                    .HasForeignKey(d => d.DocumentMediaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Document_DocumentMedia");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.Documents)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Document_DocumentType");
            });

            modelBuilder.Entity<DocumentMedia>(entity =>
            {
                entity.Property(e => e.ContentType)
                    .IsRequired()
                    .HasMaxLength(120);

                entity.Property(e => e.IconFileName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<EconomicIndicator>(entity =>
            {
                entity.Property(e => e.AgricultureForestryFishingGrowth).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Gdp)
                    .HasColumnType("decimal(18, 3)")
                    .HasColumnName("GDP");

                entity.Property(e => e.Gdpgrowth)
                    .HasColumnType("decimal(18, 3)")
                    .HasColumnName("GDPGrowth");

                entity.Property(e => e.GdpperCapital)
                    .HasColumnType("decimal(18, 3)")
                    .HasColumnName("GDPPerCapital");

                entity.Property(e => e.IndustryConstructionGrowth).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.Picture)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Population).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.TradeServicesGrowth).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<EmployeesDelete>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Employees_Delete");

                entity.Property(e => e.Crid)
                    .HasMaxLength(50)
                    .HasColumnName("CRID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.MstVcis)
                    .HasMaxLength(255)
                    .HasColumnName("MST_VCIS");

                entity.Property(e => e.TotalEmp2019).HasColumnName("TotalEMP2019");
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity.HasIndex(e => e.PersonDocumentId, "IX_PersonDocument_Id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EventContent).HasColumnType("ntext");

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.EventName).HasMaxLength(100);

                entity.Property(e => e.PersonDocumentId).HasColumnName("PersonDocument_Id");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Event_Business");

                entity.HasOne(d => d.EventType)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.EventTypeId)
                    .HasConstraintName("FK_Event_EventType");

                entity.HasOne(d => d.PersonDocument)
                    .WithMany(p => p.Events)
                    .HasForeignKey(d => d.PersonDocumentId)
                    .HasConstraintName("FK_dbo.Events_dbo.PersonDocuments_PersonDocument_Id");
            });

            modelBuilder.Entity<EventType>(entity =>
            {
                entity.Property(e => e.EventTypeName).HasMaxLength(50);
            });

            modelBuilder.Entity<Exchange>(entity =>
            {
                entity.Property(e => e.CurrencyCode).HasMaxLength(10);

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<ExchangeRate>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Eur)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("EUR");

                entity.Property(e => e.Gbp)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("GBP");

                entity.Property(e => e.Jpy)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("JPY");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Usd)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("USD");
            });

            modelBuilder.Entity<FamilyPosition>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<FinancialAccountDefinition>(entity =>
            {
                entity.Property(e => e.AccountCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Formulate).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Parent)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Report).HasMaxLength(50);

                entity.Property(e => e.Standard).HasMaxLength(50);

                entity.Property(e => e.Type)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1 = Debit; 2 = Credit; 3 = Sum; 4 = Norm; 5 = Computed");
            });

            modelBuilder.Entity<FinancialAccountReportDefine>(entity =>
            {
                entity.Property(e => e.AccountCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Formulate).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.Parent)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Report).HasMaxLength(50);

                entity.Property(e => e.Standard).HasMaxLength(50);
            });

            modelBuilder.Entity<FinancialReport>(entity =>
            {
                entity.HasIndex(e => e.AuditorId, "IX_AuditorId");

                entity.HasIndex(e => new { e.FiscalYear, e.Deleted }, "Idx_Report_Businsess");

                entity.HasIndex(e => new { e.BusinessId, e.FiscalYear }, "NonClusteredIndex-20190106-112516");

                entity.Property(e => e.AuditStatus).HasMaxLength(50);

                entity.Property(e => e.CashFlowType).HasMaxLength(100);

                entity.Property(e => e.ChiefAccountantSignature)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ConsolidatedStatus).HasMaxLength(50);

                entity.Property(e => e.ConsolidationCode).HasMaxLength(1000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(50);

                entity.Property(e => e.DirectorSignedDate).HasColumnType("datetime");

                entity.Property(e => e.IsApi).HasColumnName("isAPI");

                entity.Property(e => e.ProfitAfterTax).HasColumnType("money");

                entity.Property(e => e.ReportTitle).HasMaxLength(100);

                entity.Property(e => e.ReportedDate).HasColumnType("datetime");

                entity.Property(e => e.Source).HasMaxLength(50);

                entity.Property(e => e.StandaloneCompany).HasMaxLength(50);

                entity.Property(e => e.TotalRevenue).HasColumnType("money");

                entity.Property(e => e.UnitCurrency).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VfinancialReportId).HasColumnName("VFinancialReportId");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.FinancialReports)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_FinancialRecord_Business");
            });

            modelBuilder.Entity<FinancialReportData>(entity =>
            {
                entity.HasIndex(e => e.FinancialReportId, "FinancialReportId_Index");

                entity.Property(e => e.AccountCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PostAmount).HasColumnType("decimal(23, 4)");

                entity.Property(e => e.PreAmount).HasColumnType("decimal(23, 4)");

                entity.HasOne(d => d.FinancialReport)
                    .WithMany(p => p.FinancialReportData)
                    .HasForeignKey(d => d.FinancialReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FinancialReportDatas_FinancialReports");
            });

            modelBuilder.Entity<IdentificationList>(entity =>
            {
                entity.ToTable("IdentificationList");

                entity.Property(e => e.ColumnName).HasMaxLength(100);
            });

            modelBuilder.Entity<ImportExport>(entity =>
            {
                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.CompanyAverage).HasMaxLength(250);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IndustryAverage).HasMaxLength(250);

                entity.Property(e => e.Mandate).HasMaxLength(250);

                entity.Property(e => e.Market).HasMaxLength(250);

                entity.Property(e => e.ModeOfPayment).HasMaxLength(100);

                entity.Property(e => e.Ratio).HasMaxLength(100);

                entity.Property(e => e.TypesOfProducts).HasMaxLength(250);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.ImportExports)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_ImportExport_Business");
            });

            modelBuilder.Entity<IndependentBranch>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.IndependentBranches)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.IndependentBranches_dbo.Businesses_BusinessId");
            });

            modelBuilder.Entity<Industry>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.IndustryCode)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<IndustryAverageReport>(entity =>
            {
                entity.Property(e => e.IndustryCode).HasMaxLength(5);

                entity.Property(e => e.IndustryName)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<IndustryAverageValue>(entity =>
            {
                entity.Property(e => e.AverageValue).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.AverageIndustry)
                    .WithMany(p => p.IndustryAverageValues)
                    .HasForeignKey(d => d.AverageIndustryId)
                    .HasConstraintName("FK_IndustryAverageValues_AverageIndustries");

                entity.HasOne(d => d.BusinessLine)
                    .WithMany(p => p.IndustryAverageValues)
                    .HasForeignKey(d => d.BusinessLineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IndustryAverageValues_IndustryAverageValues");
            });

            modelBuilder.Entity<IndustryData>(entity =>
            {
                entity.Property(e => e.BusinessLineCode)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IndustryDataDefaultValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IndustryDataPercent).HasColumnType("decimal(7, 4)");

                entity.Property(e => e.IndustryDataValue).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.IndustryDataDefinition)
                    .WithMany(p => p.IndustryData)
                    .HasForeignKey(d => d.IndustryDataDefinitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IndustryData_IndustryDataDefinition");
            });

            modelBuilder.Entity<IndustryDataDefault>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IndustryDataDefaultValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IndustryDataDefinitionId).HasColumnName("IndustryDataDefinitionID");

                entity.HasOne(d => d.IndustryDataDefinition)
                    .WithMany(p => p.IndustryDataDefaults)
                    .HasForeignKey(d => d.IndustryDataDefinitionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IndustryDataDefault_IndustryDataDefinition");
            });

            modelBuilder.Entity<IndustryDataDefinition>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IndustryDataDefinitionName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<IndustryInformation>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.OwnerEquity).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.OwnerEquityCagr).HasColumnName("OwnerEquityCAGR");

                entity.Property(e => e.ProfitAfterTax).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.ProfitAfterTaxCagr).HasColumnName("ProfitAfterTaxCAGR");

                entity.Property(e => e.Sales).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.SalesCagr).HasColumnName("SalesCAGR");

                entity.Property(e => e.TotalAssets).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.TotalAssetsCagr).HasColumnName("TotalAssetsCAGR");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.BusinessLine)
                    .WithMany(p => p.IndustryInformations)
                    .HasForeignKey(d => d.BusinessLineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IndustryInformations_BusinessLines");
            });

            modelBuilder.Entity<IndustryTitle>(entity =>
            {
                entity.Property(e => e.BusinessLineCode).HasMaxLength(50);

                entity.Property(e => e.EnglishIndustryTitle)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.IndustryTitles)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Vncref)
                    .HasMaxLength(10)
                    .HasColumnName("VNCRef");
            });

            modelBuilder.Entity<JobPosition>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(2000);

                entity.Property(e => e.Name).HasMaxLength(100);
            });

            modelBuilder.Entity<Keyword>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<LaoDongTbn>(entity =>
            {
                entity.ToTable("LaoDongTBN");

                entity.Property(e => e.MaNganh).HasMaxLength(255);
            });

            modelBuilder.Entity<LitigationRecord>(entity =>
            {
                entity.Property(e => e.AddressOfCourt).HasMaxLength(4000);

                entity.Property(e => e.CheckDate).HasColumnType("datetime");

                entity.Property(e => e.CourtName).HasMaxLength(200);

                entity.Property(e => e.CourtTaxCode).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(30);

                entity.Property(e => e.DateOfAcceptance).HasColumnType("datetime");

                entity.Property(e => e.DateOfJudgement).HasColumnType("datetime");

                entity.Property(e => e.NameOfCase).HasMaxLength(200);

                entity.Property(e => e.NameOfCourt).HasMaxLength(200);

                entity.Property(e => e.NameOfJudge).HasMaxLength(150);

                entity.Property(e => e.NumberOfCase)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ResultFile).HasMaxLength(250);

                entity.Property(e => e.ResultText).HasMaxLength(1000);

                entity.Property(e => e.TelOfCourt).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.ValueOfDispute).HasColumnType("decimal(23, 4)");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.LitigationRecordBusinesses)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_LitigationRecords_Business");

                entity.HasOne(d => d.PlaintiffBusiness)
                    .WithMany(p => p.LitigationRecordPlaintiffBusinesses)
                    .HasForeignKey(d => d.PlaintiffBusinessId)
                    .HasConstraintName("FK_dbo.LitigationRecords_dbo.Businesses_PlaintiffBusinessId");

                entity.HasOne(d => d.PlaintiffPerson)
                    .WithMany(p => p.LitigationRecords)
                    .HasForeignKey(d => d.PlaintiffPersonId)
                    .HasConstraintName("FK_LitigationRecords_Persons");

                entity.HasOne(d => d.RelatedPartiesBusiness)
                    .WithMany(p => p.LitigationRecordRelatedPartiesBusinesses)
                    .HasForeignKey(d => d.RelatedPartiesBusinessId)
                    .HasConstraintName("FK_dbo.LitigationRecords_dbo.Businesses_RelatedPartiesBusinessId");
            });

            modelBuilder.Entity<MajorClient>(entity =>
            {
                entity.Property(e => e.ContractDate).HasColumnType("datetime");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContractProduct).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherContract).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(23, 4)");
            });

            modelBuilder.Entity<MajorSupplier>(entity =>
            {
                entity.Property(e => e.ContractDate).HasColumnType("datetime");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContractProduct).HasMaxLength(500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherBusinessContract).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(23, 4)");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });

            modelBuilder.Entity<NeedIndex>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("NeedIndex");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.NeedIndex1).HasColumnName("NeedIndex");
            });

            modelBuilder.Entity<NegativeInfo>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Mst2)
                    .HasMaxLength(255)
                    .HasColumnName("MST2");

                entity.Property(e => e.T12312019).HasColumnName("T12_31_2019");

                entity.Property(e => e.T31062020).HasColumnName("T31_06_2020");

                entity.Property(e => e.T3312020).HasColumnName("T3_31_2020");

                entity.Property(e => e.T4302020).HasColumnName("T4_30_2020");

                entity.Property(e => e.T7302020).HasColumnName("T7_30_2020");
            });

            modelBuilder.Entity<NegativeNews>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfNews).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.NegativeNews)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.NegativeNews_dbo.Businesses_BusinessId");
            });

            modelBuilder.Entity<NegativePayment>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Adress).HasMaxLength(255);

                entity.Property(e => e.Debtor).HasMaxLength(255);

                entity.Property(e => e.NegativePaymentRecords).HasMaxLength(255);

                entity.Property(e => e.TaxId)
                    .HasMaxLength(255)
                    .HasColumnName("TaxID");
            });

            modelBuilder.Entity<NegativePaymentRecord>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.RecordDate).HasColumnType("datetime");

                entity.Property(e => e.RecordValue).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.NegativePaymentRecords)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.NegativePaymentRecords_dbo.Businesses_BusinessId");
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfNews).HasColumnType("datetime");

                entity.Property(e => e.DocumentId).HasColumnName("DocumentID");

                entity.Property(e => e.NewsContent).HasColumnType("ntext");

                entity.Property(e => e.Title).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.News)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_News_Business");
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.HasIndex(e => e.BusinessId, "IX_BusinessId");

                entity.Property(e => e.Content).HasMaxLength(1500);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Key).HasMaxLength(20);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_dbo.Notes_dbo.Businesses_BusinessId");
            });

            modelBuilder.Entity<Office>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<OrderInterview>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PaymentHistory>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditContractNo).HasMaxLength(100);

                entity.Property(e => e.Evaluation).HasMaxLength(100);

                entity.Property(e => e.LastModifiedBy).HasMaxLength(100);

                entity.Property(e => e.LoanTerm).HasMaxLength(200);

                entity.Property(e => e.LoansInterest).HasMaxLength(100);

                entity.Property(e => e.PaymentMethod).HasMaxLength(100);

                entity.Property(e => e.PeriodOfPayment).HasMaxLength(100);

                entity.Property(e => e.StatusOfPayment).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WithTheBank)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.PaymentHistories)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_PaymentHistory_Business");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasIndex(e => new { e.FirstName, e.LastName }, "IX_First_Last_Name");

                entity.HasIndex(e => new { e.FullName, e.VietnameseName }, "IX_FullName");

                entity.HasIndex(e => e.Idnumber, "IX_IDNumber");

                entity.HasIndex(e => e.TaxNumber, "NonClusteredIndex-20190106-140806");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Education).HasMaxLength(255);

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Experience).HasMaxLength(1000);

                entity.Property(e => e.FacebookLinkedIn).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(200);

                entity.Property(e => e.FormerIdnumber)
                    .HasMaxLength(200)
                    .HasColumnName("FormerIDNumber");

                entity.Property(e => e.FormerIdnumberIssueDate)
                    .HasColumnType("datetime")
                    .HasColumnName("FormerIDNumberIssueDate");

                entity.Property(e => e.FormerIdnumberIssuePlace)
                    .HasMaxLength(200)
                    .HasColumnName("FormerIDNumberIssuePlace");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.HomePhone).HasMaxLength(50);

                entity.Property(e => e.IdissueDate)
                    .HasColumnType("date")
                    .HasColumnName("IDIssueDate");

                entity.Property(e => e.IdissuePlace)
                    .HasMaxLength(200)
                    .HasColumnName("IDIssuePlace");

                entity.Property(e => e.Idnumber)
                    .HasMaxLength(50)
                    .HasColumnName("IDNumber");

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Major).HasMaxLength(255);

                entity.Property(e => e.MaritalStatus).HasMaxLength(50);

                entity.Property(e => e.MobilePhone).HasMaxLength(50);

                entity.Property(e => e.Nationality).HasMaxLength(100);

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.PassPort).HasMaxLength(50);

                entity.Property(e => e.PassPortIssueDate).HasColumnType("datetime");

                entity.Property(e => e.PassPortIssuePlace).HasMaxLength(200);

                entity.Property(e => e.PlaceOfBirth).HasMaxLength(1000);

                entity.Property(e => e.Rid).HasColumnName("RID");

                entity.Property(e => e.TaxDate).HasColumnType("datetime");

                entity.Property(e => e.TaxNumber).HasMaxLength(50);

                entity.Property(e => e.TaxStatus).HasMaxLength(1);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Vid).HasColumnName("VID");

                entity.Property(e => e.VietnameseName).HasMaxLength(200);

                entity.Property(e => e.Website).HasMaxLength(100);

                entity.Property(e => e.WorkPhone).HasMaxLength(50);
            });

            modelBuilder.Entity<PersonAddress>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.Number).HasMaxLength(200);

                entity.Property(e => e.Postcode).HasMaxLength(50);

                entity.Property(e => e.RaddressId).HasColumnName("RAddressId");

                entity.Property(e => e.Street).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VaddressId).HasColumnName("VAddressId");

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.PersonAddresses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .HasConstraintName("FK_PersonAddresses_PersonAddressTypes");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.PersonAddresses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_PersonAddresses_Countries");

                entity.HasOne(d => d.District)
                    .WithMany(p => p.PersonAddresses)
                    .HasForeignKey(d => d.DistrictId)
                    .HasConstraintName("FK_PersonAddresses_Districts");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonAddresses)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PersonAddresses_Persons");

                entity.HasOne(d => d.Province)
                    .WithMany(p => p.PersonAddresses)
                    .HasForeignKey(d => d.ProvinceId)
                    .HasConstraintName("FK_PersonAddresses_Provinces");
            });

            modelBuilder.Entity<PersonAddressType>(entity =>
            {
                entity.Property(e => e.AddressTypeName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PersonBanker>(entity =>
            {
                entity.HasIndex(e => e.AddressId, "IX_AddressId");

                entity.HasIndex(e => e.BankerId, "IX_BankerId");

                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditCardDate).HasColumnType("datetime");

                entity.Property(e => e.CreditCardName).HasMaxLength(255);

                entity.Property(e => e.CreditCardNoofDayOverDue).HasMaxLength(255);

                entity.Property(e => e.CreditCardOutstandingAmount).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.CreditCardOverdueAmountVnd)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("CreditCardOverdueAmountVND");

                entity.Property(e => e.OpeningDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.PersonBankers)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK_dbo.PersonBankers_dbo.BusinessAddresses_AddressId");

                entity.HasOne(d => d.Banker)
                    .WithMany(p => p.PersonBankers)
                    .HasForeignKey(d => d.BankerId)
                    .HasConstraintName("FK_dbo.PersonBankers_dbo.Bankers_BankerId");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonBankers)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonBankers_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonBankerOutstanding>(entity =>
            {
                entity.HasIndex(e => e.PersonBankerId, "IX_PersonBankerId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.OutstandDate).HasColumnType("datetime");

                entity.Property(e => e.OutstandingBalanceUsd)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("OutstandingBalanceUSD");

                entity.Property(e => e.OutstandingBalanceVnd)
                    .HasColumnType("decimal(23, 5)")
                    .HasColumnName("OutstandingBalanceVND");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.PersonBanker)
                    .WithMany(p => p.PersonBankerOutstandings)
                    .HasForeignKey(d => d.PersonBankerId)
                    .HasConstraintName("FK_dbo.PersonBankerOutstandings_dbo.PersonBankers_PersonBankerId");
            });

            modelBuilder.Entity<PersonCredit>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.AssetBasedContent).HasMaxLength(1000);

                entity.Property(e => e.AssetBasedDate).HasColumnType("datetime");

                entity.Property(e => e.AssetsDescription).HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(3);

                entity.Property(e => e.MortageDate).HasColumnType("datetime");

                entity.Property(e => e.OverdueCreditContent).HasMaxLength(1000);

                entity.Property(e => e.OverdueCreditDate).HasColumnType("datetime");

                entity.Property(e => e.OverdueLoanContent).HasMaxLength(1000);

                entity.Property(e => e.OverdueLoanDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(23, 5)");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonCredits)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonCredits_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonDocument>(entity =>
            {
                entity.HasIndex(e => e.DocumentMediaId, "IX_DocumentMediaId");

                entity.HasIndex(e => e.DocumentTypeId, "IX_DocumentTypeId");

                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.DocumentMedia)
                    .WithMany(p => p.PersonDocuments)
                    .HasForeignKey(d => d.DocumentMediaId)
                    .HasConstraintName("FK_dbo.PersonDocuments_dbo.DocumentMedias_DocumentMediaId");

                entity.HasOne(d => d.DocumentType)
                    .WithMany(p => p.PersonDocuments)
                    .HasForeignKey(d => d.DocumentTypeId)
                    .HasConstraintName("FK_dbo.PersonDocuments_dbo.DocumentTypes_DocumentTypeId");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonDocuments)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonDocuments_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonEducation>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");

                entity.Property(e => e.Degree).HasMaxLength(255);

                entity.Property(e => e.University).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonEducations)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonEducations_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonLitigationCheck>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.BankruptcyDate).HasColumnType("datetime");

                entity.Property(e => e.BankruptcySource).HasMaxLength(1000);

                entity.Property(e => e.BankruptcyStatus).HasMaxLength(255);

                entity.Property(e => e.CivilLitigationDate).HasColumnType("datetime");

                entity.Property(e => e.CivilLitigationSource).HasMaxLength(1000);

                entity.Property(e => e.CivilLitigationStatus).HasMaxLength(255);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CriminalLitigationDate).HasColumnType("datetime");

                entity.Property(e => e.CriminalLitigationSource).HasMaxLength(1000);

                entity.Property(e => e.CriminalLitigationStatus).HasMaxLength(255);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonLitigationChecks)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonLitigationChecks_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonPressMediaResult>(entity =>
            {
                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateOfPublication).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonPressMediaResults)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonPressMediaResults_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<PersonWorkingExperience>(entity =>
            {
                entity.HasIndex(e => e.JobPositionId, "IX_JobPositionId");

                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateFrom).HasColumnType("datetime");

                entity.Property(e => e.DateTo).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.JobPosition)
                    .WithMany(p => p.PersonWorkingExperiences)
                    .HasForeignKey(d => d.JobPositionId)
                    .HasConstraintName("FK_dbo.PersonWorkingExperiences_dbo.JobPositions_JobPositionId");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonWorkingExperiences)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("FK_dbo.PersonWorkingExperiences_dbo.Persons_PersonId");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsCapital).HasDefaultValueSql("((0))");

                entity.Property(e => e.PhoneCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceName).HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Provinces)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_Provinces_Countries");
            });

            modelBuilder.Entity<RelatedBusiness>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.RelatedBusinessBusinesses)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RelatedBusiness_Business");

                entity.HasOne(d => d.RelatedBusinessNavigation)
                    .WithMany(p => p.RelatedBusinessRelatedBusinessNavigations)
                    .HasForeignKey(d => d.RelatedBusinessId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RelatedBusiness_Business1");
            });

            modelBuilder.Entity<RelatedPerson>(entity =>
            {
                entity.HasIndex(e => e.FamilyPositionId, "IX_FamilyPositionId");

                entity.HasIndex(e => e.PersonId, "IX_PersonId");

                entity.HasIndex(e => e.RelatedPersonId, "IX_RelatedPersonId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.FamilyPosition)
                    .WithMany(p => p.RelatedPeople)
                    .HasForeignKey(d => d.FamilyPositionId)
                    .HasConstraintName("FK_dbo.RelatedPersons_dbo.FamilyPositions_FamilyPositionId");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.RelatedPersonPeople)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RelatedPersons_dbo.Persons_PersonId");

                entity.HasOne(d => d.RelatedPersonNavigation)
                    .WithMany(p => p.RelatedPersonRelatedPersonNavigations)
                    .HasForeignKey(d => d.RelatedPersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.RelatedPersons_dbo.Persons_RelatedPersonId");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.Property(e => e.ActivityInterpretation).HasColumnType("ntext");

                entity.Property(e => e.AddressInterpretation).HasColumnType("ntext");

                entity.Property(e => e.CompleteDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditRecommendation).HasMaxLength(4000);

                entity.Property(e => e.Customer).HasMaxLength(100);

                entity.Property(e => e.CustomerNotes).HasMaxLength(2000);

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.FinanceConclustion).HasColumnType("ntext");

                entity.Property(e => e.FinanceInterpretation).HasColumnType("ntext");

                entity.Property(e => e.Grade)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.GradeRatingCode).HasMaxLength(10);

                entity.Property(e => e.Interpretation).HasColumnType("ntext");

                entity.Property(e => e.NegativeNews).HasColumnType("ntext");

                entity.Property(e => e.OrderDate).HasColumnType("datetime");

                entity.Property(e => e.RatingCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VreportId).HasColumnName("VReportId");

                entity.Property(e => e.Writer).HasMaxLength(100);

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Reports)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_Report_Business");

                entity.HasOne(d => d.ReportCreditRating)
                    .WithMany(p => p.Reports)
                    .HasForeignKey(d => d.ReportCreditRatingId)
                    .HasConstraintName("FK_Reports_ReportCreditRatings");
            });

            modelBuilder.Entity<ReportCreditCriterion>(entity =>
            {
                entity.Property(e => e.Criterion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Factor)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.VcriterionCode)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("VCriterionCode");
            });

            modelBuilder.Entity<ReportCreditRating>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(1000);

                entity.Property(e => e.RatingCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Summary).HasMaxLength(50);
            });

            modelBuilder.Entity<ReportCreditScore>(entity =>
            {
                entity.HasOne(d => d.ReportCreditCriterion)
                    .WithMany(p => p.ReportCreditScores)
                    .HasForeignKey(d => d.ReportCreditCriterionId)
                    .HasConstraintName("FK_ReportCreditScores_ReportCreditCriterions");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.ReportCreditScores)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReportCreditScores_Reports");
            });

            modelBuilder.Entity<ReportOption>(entity =>
            {
                entity.HasIndex(e => e.ParentId, "IX_ParentId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Key).HasMaxLength(250);

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_dbo.ReportOptions_dbo.ReportOptions_ParentId");
            });

            modelBuilder.Entity<ReportType>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ReportTypeOption>(entity =>
            {
                entity.HasIndex(e => e.ReportOptionId, "IX_ReportOptionId");

                entity.HasIndex(e => e.ReportTypeId, "IX_ReportTypeId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.ReportOption)
                    .WithMany(p => p.ReportTypeOptions)
                    .HasForeignKey(d => d.ReportOptionId)
                    .HasConstraintName("FK_dbo.ReportTypeOptions_dbo.ReportOptions_ReportOptionId");

                entity.HasOne(d => d.ReportType)
                    .WithMany(p => p.ReportTypeOptions)
                    .HasForeignKey(d => d.ReportTypeId)
                    .HasConstraintName("FK_dbo.ReportTypeOptions_dbo.ReportTypes_ReportTypeId");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Key).IsRequired();

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<RoleAccessRight>(entity =>
            {
                entity.HasKey(e => new { e.AccessRightCode, e.RoleId })
                    .HasName("PK_RoleRight");

                entity.Property(e => e.AccessRightCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccessRightCodeNavigation)
                    .WithMany(p => p.RoleAccessRights)
                    .HasForeignKey(d => d.AccessRightCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleAccessRight_AccessRight");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleAccessRights)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleAccessRight_Role");
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.HasIndex(e => e.PermissionId, "IX_PermissionId");

                entity.HasIndex(e => e.RoleId, "IX_RoleId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .HasConstraintName("FK_dbo.RolePermissions_dbo.Permissions_PermissionId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.RolePermissions_dbo.Roles_RoleId");
            });

            modelBuilder.Entity<RolesOld>(entity =>
            {
                entity.ToTable("Roles_old");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ScriptManager>(entity =>
            {
                entity.ToTable("Script_Manager");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ScriptName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Script_Name");

                entity.Property(e => e.ScriptNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("Script_Number");
            });

            modelBuilder.Entity<SettingType>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ShareHolder>(entity =>
            {
                entity.Property(e => e.BusinessRepresentative).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(30);

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.RshareHolderId).HasColumnName("RShareHolderId");

                entity.Property(e => e.ShareHolderOther).HasMaxLength(200);

                entity.Property(e => e.SharedCapital).HasColumnType("money");

                entity.Property(e => e.SharedPercentageDate).HasColumnType("datetime");

                entity.Property(e => e.SubPercentageDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.ShareHolderBusinesses)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_ShareHolder_Business");

                entity.HasOne(d => d.ShareHolderBusiness)
                    .WithMany(p => p.ShareHolderShareHolderBusinesses)
                    .HasForeignKey(d => d.ShareHolderBusinessId)
                    .HasConstraintName("FK_dbo.ShareHolders_dbo.Businesses_ShareHolderBusinessId");

                entity.HasOne(d => d.ShareHolderPerson)
                    .WithMany(p => p.ShareHolders)
                    .HasForeignKey(d => d.ShareHolderPersonId)
                    .HasConstraintName("FK_ShareHolders_Persons");
            });

            modelBuilder.Entity<SiteSetting>(entity =>
            {
                entity.HasIndex(e => e.SettingTypeId, "IX_SettingTypeId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(512);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.SettingType)
                    .WithMany(p => p.SiteSettings)
                    .HasForeignKey(d => d.SettingTypeId)
                    .HasConstraintName("FK_dbo.SiteSettings_dbo.SettingTypes_SettingTypeId");
            });

            modelBuilder.Entity<SoLieuTbn>(entity =>
            {
                entity.ToTable("SoLieuTBN");

                entity.Property(e => e.CagrNetSales).HasColumnName("CAGR_NetSales");

                entity.Property(e => e.CagrOwner).HasColumnName("CAGR_Owner");

                entity.Property(e => e.CagrProfit).HasColumnName("CAGR_Profit");

                entity.Property(e => e.CagrTotalAssets).HasColumnName("CAGR_TotalAssets");

                entity.Property(e => e.MaNganh).HasMaxLength(255);
            });

            modelBuilder.Entity<Staff>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.staff)
                    .HasForeignKey(d => d.BusinessId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Staff_Business");

                entity.HasOne(d => d.JobPosition)
                    .WithMany(p => p.staff)
                    .HasForeignKey(d => d.JobPositionId)
                    .HasConstraintName("FK_Staffs_JobPositions");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.staff)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Staff_Person");
            });

            modelBuilder.Entity<SyncVersionXoa>(entity =>
            {
                entity.HasKey(e => e.SyncTable)
                    .HasName("PK_SyncVersion");

                entity.ToTable("SyncVersion_xoa");

                entity.Property(e => e.SyncTable)
                    .HasMaxLength(200)
                    .HasColumnName("Sync_Table");

                entity.Property(e => e.OldVersion).HasColumnName("Old_Version");

                entity.Property(e => e.SyncVersion).HasColumnName("Sync_Version");
            });

            modelBuilder.Entity<SysLogBusiness>(entity =>
            {
                entity.ToTable("SysLogBusiness");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FunctionName).HasMaxLength(250);

                entity.Property(e => e.LogType).HasMaxLength(50);

                entity.Property(e => e.SessionId).HasMaxLength(76);

                entity.Property(e => e.TableName).HasMaxLength(150);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserName).HasMaxLength(50);
            });

            modelBuilder.Entity<Taxcode>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.TaxCode1)
                    .HasMaxLength(100)
                    .HasColumnName("TaxCode");
            });

            modelBuilder.Entity<Tbn>(entity =>
            {
                entity.ToTable("TBN");

                entity.Property(e => e.Mn)
                    .HasMaxLength(255)
                    .HasColumnName("MN");

                entity.Property(e => e.Mn1)
                    .HasMaxLength(255)
                    .HasColumnName("MN1");

                entity.Property(e => e.Mn2)
                    .HasMaxLength(255)
                    .HasColumnName("MN2");

                entity.Property(e => e.Mn3)
                    .HasMaxLength(255)
                    .HasColumnName("MN3");

                entity.Property(e => e.Mn4)
                    .HasMaxLength(255)
                    .HasColumnName("MN4");

                entity.Property(e => e.Mn5)
                    .HasMaxLength(255)
                    .HasColumnName("MN5");

                entity.Property(e => e.Mn6)
                    .HasMaxLength(255)
                    .HasColumnName("MN6");

                entity.Property(e => e.Mn7)
                    .HasMaxLength(255)
                    .HasColumnName("MN7");

                entity.Property(e => e.Stt).HasColumnName("STT");
            });

            modelBuilder.Entity<TempGroup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TempGroup");

                entity.Property(e => e.WriterName).HasMaxLength(300);
            });

            modelBuilder.Entity<TempWriter>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TempWriter");

                entity.Property(e => e.FullName).HasMaxLength(300);

                entity.Property(e => e.WriterName).HasMaxLength(300);
            });

            modelBuilder.Entity<TradeCredit>(entity =>
            {
                entity.HasIndex(e => e.FinancialReportId, "IX_FinancialReportId");

                entity.Property(e => e.AssetBasedLending).HasMaxLength(50);

                entity.Property(e => e.CommonNotes).HasMaxLength(1000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreditEnquiryNotes).HasMaxLength(1000);

                entity.Property(e => e.CreditEnquiryTerm).HasMaxLength(1000);

                entity.Property(e => e.CreditInquiry).HasColumnType("money");

                entity.Property(e => e.CreditLimitDate).HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Currency2).HasMaxLength(3);

                entity.Property(e => e.GradeRatingCode).HasMaxLength(10);

                entity.Property(e => e.InquiryCurrency)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.NegativeCreditRecords).HasMaxLength(4000);

                entity.Property(e => e.Notes).HasMaxLength(2000);

                entity.Property(e => e.Notes2).HasMaxLength(1000);

                entity.Property(e => e.Revision).HasMaxLength(50);

                entity.Property(e => e.SpecialMentionAmounts).HasMaxLength(2000);

                entity.Property(e => e.Term1).HasMaxLength(1000);

                entity.Property(e => e.Term2).HasMaxLength(1000);

                entity.Property(e => e.TradeCreditDate).HasColumnType("datetime");

                entity.Property(e => e.TradeCreditLimit).HasColumnType("money");

                entity.Property(e => e.TradeCreditLimit2).HasColumnType("decimal(23, 5)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TradeCredits)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TradeCredits_Business");
            });

            modelBuilder.Entity<TradeCreditSecuredTransactionRecord>(entity =>
            {
                entity.HasIndex(e => e.PersonPartieSecuringId, "IX_PersonPartieSecuringId");

                entity.Property(e => e.Collatera).HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency).HasMaxLength(3);

                entity.Property(e => e.DateOfRecord).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnType("decimal(23, 5)");

                entity.HasOne(d => d.PersonPartieSecuring)
                    .WithMany(p => p.TradeCreditSecuredTransactionRecords)
                    .HasForeignKey(d => d.PersonPartieSecuringId)
                    .HasConstraintName("FK_dbo.TradeCreditSecuredTransactionRecords_dbo.Persons_PersonPartieSecuringId");

                entity.HasOne(d => d.TradeCredit)
                    .WithMany(p => p.TradeCreditSecuredTransactionRecords)
                    .HasForeignKey(d => d.TradeCreditId)
                    .HasConstraintName("FK_TradeCreditSecuredTransactionRecords_TradeCredits");
            });

            modelBuilder.Entity<TradeCreditTotaloutStanding>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.OutStandingBalance).HasColumnType("decimal(23, 4)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.TradeCredit)
                    .WithMany(p => p.TradeCreditTotaloutStandings)
                    .HasForeignKey(d => d.TradeCreditId)
                    .HasConstraintName("FK_TradeCreditTotaloutstandings_TradeCredits");
            });

            modelBuilder.Entity<TradeReference>(entity =>
            {
                entity.Property(e => e.Balance).HasColumnType("money");

                entity.Property(e => e.BalanceDate).HasColumnType("datetime");

                entity.Property(e => e.BusinessName).HasMaxLength(200);

                entity.Property(e => e.Category).HasMaxLength(10);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.TradeReferenceBusinesses)
                    .HasForeignKey(d => d.BusinessId)
                    .HasConstraintName("FK_TradeReferences_Business");

                entity.HasOne(d => d.ReferenceBusiness)
                    .WithMany(p => p.TradeReferenceReferenceBusinesses)
                    .HasForeignKey(d => d.ReferenceBusinessId)
                    .HasConstraintName("FK_TradeReferences_Business1");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.OfficeId, "IX_OfficeId");

                entity.Property(e => e.Avatar).HasMaxLength(512);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(200);

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.Property(e => e.SaltKey).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VerifyCode).HasMaxLength(10);

                entity.Property(e => e.VerifyTime).HasColumnType("datetime");

                entity.HasOne(d => d.Office)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.OfficeId)
                    .HasConstraintName("FK_dbo.Users_dbo.Offices_OfficeId");
            });

            modelBuilder.Entity<UserGroup>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<UserInRole>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserInRoles)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_dbo.UserInRoles_dbo.Users_UserId");
            });

            modelBuilder.Entity<UserTypeInRolesXoa>(entity =>
            {
                entity.ToTable("UserTypeInRoles_xoa");

                entity.Property(e => e.CreatedBy).HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserTypeInRolesXoas)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_dbo.UserTypeInRoles_dbo.Roles_RoleId");
            });

            modelBuilder.Entity<UserUser>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.UserId1 })
                    .HasName("PK_dbo.UserUsers");

                entity.Property(e => e.UserId).HasColumnName("User_Id");

                entity.Property(e => e.UserId1).HasColumnName("User_Id1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserUserUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.UserUsers_dbo.Users_User_Id");

                entity.HasOne(d => d.UserId1Navigation)
                    .WithMany(p => p.UserUserUserId1Navigations)
                    .HasForeignKey(d => d.UserId1)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.UserUsers_dbo.Users_User_Id1");
            });

            modelBuilder.Entity<UserUserGroup>(entity =>
            {
                entity.HasIndex(e => e.UserGroupId, "IX_UserGroupId");

                entity.HasIndex(e => e.UserId, "IX_UserId");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.UserGroup)
                    .WithMany(p => p.UserUserGroups)
                    .HasForeignKey(d => d.UserGroupId)
                    .HasConstraintName("FK_dbo.UserUserGroups_dbo.UserGroups_UserGroupId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserUserGroups)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_dbo.UserUserGroups_dbo.Users_UserId");
            });

            modelBuilder.Entity<YearEmployee>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<IndependentBranch>()
               .HasOne(e => e.Branch)
               .WithMany()
               .HasForeignKey(e => e.BranchId)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<MajorClient>()
               .HasOne(e => e.Business)
               .WithMany(p => p.majorClient)
               .HasForeignKey(e => e.BusinessId)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<MajorSupplier>()
              .HasOne(e => e.Business)
              .WithMany(p => p.majorSupplier)
              .HasForeignKey(e => e.BusinessId)
              .OnDelete(DeleteBehavior.SetNull);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
