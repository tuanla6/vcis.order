﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonBanker
    {
        public PersonBanker()
        {
            PersonBankerOutstandings = new HashSet<PersonBankerOutstanding>();
        }

        public int Id { get; set; }
        public int PersonId { get; set; }
        public int BankerId { get; set; }
        public int? PersonBankerPersonId { get; set; }
        public string AccountNumber { get; set; }
        public string SwiftCode { get; set; }
        public DateTime? OpeningDate { get; set; }
        public string AccountForeign { get; set; }
        public int? AddressId { get; set; }
        public string CreditCardName { get; set; }
        public decimal? CreditCardOutstandingAmount { get; set; }
        public decimal? CreditCardOverdueAmountVnd { get; set; }
        public DateTime? CreditCardDate { get; set; }
        public string CreditCardNoofDayOverDue { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual BusinessAddress Address { get; set; }
        public virtual Banker Banker { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<PersonBankerOutstanding> PersonBankerOutstandings { get; set; }
    }
}
