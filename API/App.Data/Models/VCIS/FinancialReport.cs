﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class FinancialReport
    {
        public FinancialReport()
        {
            FinancialReportData = new HashSet<FinancialReportData>();
        }

        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string ReportTitle { get; set; }
        public short? NumberOfWeeks { get; set; }
        public string ConsolidationCode { get; set; }
        public short FiscalYear { get; set; }
        public string Source { get; set; }
        public DateTime? ReportedDate { get; set; }
        public string ChiefAccountantSignature { get; set; }
        public string ConsolidatedStatus { get; set; }
        public DateTime? DirectorSignedDate { get; set; }
        public string Currency { get; set; }
        public decimal? ProfitAfterTax { get; set; }
        public decimal? TotalRevenue { get; set; }
        public decimal? UnitCurrency { get; set; }
        public string CashFlowType { get; set; }
        public bool? IsQuarter { get; set; }
        public int? QuarterNumber { get; set; }
        public string AuditStatus { get; set; }
        public int? ReportForm { get; set; }
        public string StandaloneCompany { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? VfinancialReportId { get; set; }
        public bool? OneYear { get; set; }
        public bool? OverOneYear { get; set; }
        public bool? IsApi { get; set; }
        public bool? IsPrioritize { get; set; }
        public string OrderStatus { get; set; }
        public bool? IsReview { get; set; }
        public int? AuditorId { get; set; }
        public bool? IsProfit { get; set; }
        public bool? IsCashFlow { get; set; }

        public virtual Business Business { get; set; }
        public virtual ICollection<FinancialReportData> FinancialReportData { get; set; }
    }
}
