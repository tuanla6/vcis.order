﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class NegativeInfo
    {
        public string Mst2 { get; set; }
        public double? T12312019 { get; set; }
        public double? T3312020 { get; set; }
        public double? T4302020 { get; set; }
        public double? T31062020 { get; set; }
        public double? T7302020 { get; set; }
        public int Id { get; set; }
    }
}
