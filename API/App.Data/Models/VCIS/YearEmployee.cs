﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class YearEmployee
    {
        public int Id { get; set; }
        public int? YearEmployees { get; set; }
    }
}
