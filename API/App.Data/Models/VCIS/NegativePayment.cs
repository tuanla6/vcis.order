﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class NegativePayment
    {
        public string TaxId { get; set; }
        public string Debtor { get; set; }
        public string NegativePaymentRecords { get; set; }
        public string Adress { get; set; }
    }
}
