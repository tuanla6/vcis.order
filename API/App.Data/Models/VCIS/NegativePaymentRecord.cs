﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class NegativePaymentRecord
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public DateTime? RecordDate { get; set; }
        public decimal? RecordValue { get; set; }
        public int? RecordYear { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public string Note { get; set; }
        public short? Type { get; set; }

        public virtual Business Business { get; set; }
    }
}
