﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryDataDefinition
    {
        public IndustryDataDefinition()
        {
            IndustryData = new HashSet<IndustryData>();
            IndustryDataDefaults = new HashSet<IndustryDataDefault>();
        }

        public int Id { get; set; }
        public string IndustryDataDefinitionName { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<IndustryData> IndustryData { get; set; }
        public virtual ICollection<IndustryDataDefault> IndustryDataDefaults { get; set; }
    }
}
