﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonDocument
    {
        public PersonDocument()
        {
            Events = new HashSet<Event>();
        }

        public int Id { get; set; }
        public int PersonId { get; set; }
        public int DocumentTypeId { get; set; }
        public int DocumentMediaId { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public bool IsDefault { get; set; }
        public bool? IsWordReport { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual DocumentMedia DocumentMedia { get; set; }
        public virtual DocumentType DocumentType { get; set; }
        public virtual Person Person { get; set; }
        public virtual ICollection<Event> Events { get; set; }
    }
}
