﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class FinancialReportData
    {
        public int Id { get; set; }
        public int FinancialReportId { get; set; }
        public decimal? PreAmount { get; set; }
        public decimal? PostAmount { get; set; }
        public string AccountCode { get; set; }
        public bool Deleted { get; set; }

        public virtual FinancialReport FinancialReport { get; set; }
    }
}
