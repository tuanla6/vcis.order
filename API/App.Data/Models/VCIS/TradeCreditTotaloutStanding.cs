﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TradeCreditTotaloutStanding
    {
        public int Id { get; set; }
        public int? TradeCreditId { get; set; }
        public int? Month { get; set; }
        public decimal? OutStandingBalance { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? Year { get; set; }

        public virtual TradeCredit TradeCredit { get; set; }
    }
}
