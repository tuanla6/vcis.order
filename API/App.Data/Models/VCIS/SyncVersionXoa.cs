﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class SyncVersionXoa
    {
        public string SyncTable { get; set; }
        public int? SyncVersion { get; set; }
        public int? OldVersion { get; set; }
    }
}
