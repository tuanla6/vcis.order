﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class UserTypeInRolesXoa
    {
        public int Id { get; set; }
        public int UserTypeId { get; set; }
        public int RoleId { get; set; }
        public string Description { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public virtual RolesOld Role { get; set; }
    }
}
