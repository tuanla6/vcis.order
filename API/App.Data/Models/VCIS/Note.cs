﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Note
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string Key { get; set; }
        public string Content { get; set; }
        public bool IsDefault { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Business Business { get; set; }
    }
}
