﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class FinancialAccountReportDefine
    {
        public int Id { get; set; }
        public string AccountCode { get; set; }
        public string Parent { get; set; }
        public byte? Level { get; set; }
        public string Name { get; set; }
        public byte? Type { get; set; }
        public short? DisplayOrder { get; set; }
        public string Category { get; set; }
        public string Report { get; set; }
        public string Standard { get; set; }
        public string Formulate { get; set; }
        public bool Deleted { get; set; }
    }
}
