﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryDataDefault
    {
        public int Id { get; set; }
        public int YearNumber { get; set; }
        public decimal IndustryDataDefaultValue { get; set; }
        public int IndustryDataDefinitionId { get; set; }
        public bool Deleted { get; set; }

        public virtual IndustryDataDefinition IndustryDataDefinition { get; set; }
    }
}
