﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryInformation
    {
        public int Id { get; set; }
        public int BusinessLineId { get; set; }
        public int? NumberCompany { get; set; }
        public int? NumberEmployee { get; set; }
        public int Year { get; set; }
        public decimal? TotalAssets { get; set; }
        public decimal? OwnerEquity { get; set; }
        public decimal? Sales { get; set; }
        public decimal? ProfitAfterTax { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public float? TotalAssetsCagr { get; set; }
        public float? OwnerEquityCagr { get; set; }
        public float? SalesCagr { get; set; }
        public float? ProfitAfterTaxCagr { get; set; }
        public short? IndustryInformationType { get; set; }

        public virtual BusinessLine BusinessLine { get; set; }
    }
}
