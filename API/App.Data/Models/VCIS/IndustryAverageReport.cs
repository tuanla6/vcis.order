﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryAverageReport
    {
        public IndustryAverageReport()
        {
            AverageIndustryComparisons = new HashSet<AverageIndustryComparison>();
        }

        public int Id { get; set; }
        public string IndustryName { get; set; }
        public int? Year { get; set; }
        public string IndustryCode { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<AverageIndustryComparison> AverageIndustryComparisons { get; set; }
    }
}
