﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class User
    {
        public User()
        {
            UserInRoles = new HashSet<UserInRole>();
            UserUserGroups = new HashSet<UserUserGroup>();
            UserUserUserId1Navigations = new HashSet<UserUser>();
            UserUserUsers = new HashSet<UserUser>();
        }

        public int Id { get; set; }
        public int? RoleId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsBuiltin { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string SaltKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? UserTypeId { get; set; }
        public bool Deleted { get; set; }
        public bool DocumentDeleteable { get; set; }
        public bool AppLogin { get; set; }
        public int? OfficeId { get; set; }
        public string Avatar { get; set; }
        public string PhoneNumber { get; set; }
        public string VerifyCode { get; set; }
        public DateTime? VerifyTime { get; set; }
        public bool? IsActive { get; set; }

        public virtual Office Office { get; set; }
        public virtual ICollection<UserInRole> UserInRoles { get; set; }
        public virtual ICollection<UserUserGroup> UserUserGroups { get; set; }
        public virtual ICollection<UserUser> UserUserUserId1Navigations { get; set; }
        public virtual ICollection<UserUser> UserUserUsers { get; set; }
    }
}
