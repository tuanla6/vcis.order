﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonLitigationCheck
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public DateTime? CriminalLitigationDate { get; set; }
        public string CriminalLitigationStatus { get; set; }
        public string CriminalLitigationSource { get; set; }
        public DateTime? CivilLitigationDate { get; set; }
        public string CivilLitigationStatus { get; set; }
        public string CivilLitigationSource { get; set; }
        public DateTime? BankruptcyDate { get; set; }
        public string BankruptcyStatus { get; set; }
        public string BankruptcySource { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Person Person { get; set; }
    }
}
