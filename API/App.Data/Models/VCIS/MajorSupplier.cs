﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class MajorSupplier
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public int? BusinessContractId { get; set; }
        public string ContractNo { get; set; }
        public string ContractProduct { get; set; }
        public DateTime? ContractDate { get; set; }
        public string ContractNote { get; set; }
        public decimal? Value { get; set; }
        public string Currency { get; set; }
        public int? UpdatedBy { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public string OtherBusinessContract { get; set; }
        public virtual Business Business { get; set; }
    }
}
