﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class CapNhatMaNganhChinh
    {
        public string Mst { get; set; }
        public string Mn4 { get; set; }
        public string F3 { get; set; }
        public string F4 { get; set; }
        public int Id { get; set; }
        public bool? IsNew { get; set; }
        public int? BusinessId { get; set; }
        public int? BusinessLineId { get; set; }
        public bool? IsInput { get; set; }
        public bool? Processed { get; set; }
        public bool? IsUpdate { get; set; }
    }
}
