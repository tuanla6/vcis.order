﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessStatus
    {
        public string Msdn { get; set; }
        public string Status { get; set; }
        public string Interview { get; set; }
        public string F4 { get; set; }
        public int Id { get; set; }
        public string BusinessStatus1 { get; set; }
    }
}
