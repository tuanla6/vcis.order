﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportType
    {
        public ReportType()
        {
            ReportTypeOptions = new HashSet<ReportTypeOption>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool AvailableOtherSite { get; set; }
        public int RecordOrder { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int ReportTemplate { get; set; }
        public string SampleReportFile { get; set; }

        public virtual ICollection<ReportTypeOption> ReportTypeOptions { get; set; }
    }
}
