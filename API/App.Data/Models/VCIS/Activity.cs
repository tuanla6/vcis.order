﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Activity
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public int? BusinessLineId { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsMain { get; set; }
        public string ActivityDetails { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
        public virtual BusinessLine BusinessLine { get; set; }
    }
}
