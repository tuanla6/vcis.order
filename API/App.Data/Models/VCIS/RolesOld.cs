﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class RolesOld
    {
        public RolesOld()
        {
            RoleAccessRights = new HashSet<RoleAccessRight>();
            UserTypeInRolesXoas = new HashSet<UserTypeInRolesXoa>();
        }

        public int Id { get; set; }
        public string RoleName { get; set; }
        public bool IsDefault { get; set; }
        public bool? IsAdvancedSearch { get; set; }
        public bool? IsActivitySearch { get; set; }
        public bool? IsExportExcel { get; set; }
        public bool? IsLogReport { get; set; }
        public bool? IsLogBusiness { get; set; }
        public bool? IsUserAccount { get; set; }
        public bool? IsLocalities { get; set; }
        public bool? IsUserRoles { get; set; }
        public bool? IsBusinessLines { get; set; }
        public bool? IsBusinessTypes { get; set; }
        public bool? IsJobPositions { get; set; }
        public bool? IsBankers { get; set; }
        public bool? IsAverageIndustry { get; set; }
        public bool? IsForeignExchange { get; set; }
        public bool? IsForumManagement { get; set; }
        public bool? IsCurrency { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<RoleAccessRight> RoleAccessRights { get; set; }
        public virtual ICollection<UserTypeInRolesXoa> UserTypeInRolesXoas { get; set; }
    }
}
