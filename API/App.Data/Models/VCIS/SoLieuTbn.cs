﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class SoLieuTbn
    {
        public string MaNganh { get; set; }
        public double? CagrTotalAssets { get; set; }
        public double? TotalAssets2019 { get; set; }
        public double? TotalAssets2018 { get; set; }
        public double? TotalAssets2017 { get; set; }
        public double? TotalAssets2016 { get; set; }
        public double? TotalAssets2015 { get; set; }
        public double? CagrOwner { get; set; }
        public double? OwnersEquity2019 { get; set; }
        public double? OwnersEquity2018 { get; set; }
        public double? OwnersEquity2017 { get; set; }
        public double? OwnersEquity2016 { get; set; }
        public double? OwnersEquity2015 { get; set; }
        public double? CagrNetSales { get; set; }
        public double? NetSales2019 { get; set; }
        public double? NetSales2018 { get; set; }
        public double? NetSales2017 { get; set; }
        public double? NetSales2016 { get; set; }
        public double? NetSales2015 { get; set; }
        public double? CagrProfit { get; set; }
        public double? Profit2019 { get; set; }
        public double? Profit2018 { get; set; }
        public double? Profit2017 { get; set; }
        public double? Profit2016 { get; set; }
        public double? Profit2015 { get; set; }
        public int Id { get; set; }
        public int? SoLaoDong { get; set; }
        public int? SoCongTy { get; set; }
    }
}
