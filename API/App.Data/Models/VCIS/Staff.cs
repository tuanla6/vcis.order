﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Staff
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public int? PersonId { get; set; }
        public int? JobPositionId { get; set; }
        public bool? Representative { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
        public virtual JobPosition JobPosition { get; set; }
        public virtual Person Person { get; set; }
    }
}
