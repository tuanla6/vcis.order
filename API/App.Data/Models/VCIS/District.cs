﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class District
    {
        public District()
        {
            BusinessAddresses = new HashSet<BusinessAddress>();
            PersonAddresses = new HashSet<PersonAddress>();
        }

        public int Id { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceCode { get; set; }
        public string Category { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? ProvinceId { get; set; }
        public bool Deleted { get; set; }

        public virtual Province Province { get; set; }
        public virtual ICollection<BusinessAddress> BusinessAddresses { get; set; }
        public virtual ICollection<PersonAddress> PersonAddresses { get; set; }
    }
}
