﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class AddIndustryInfoXoa
    {
        public string Vsiccode { get; set; }
        public double? Cagr { get; set; }
        public double? AverageAssets2019 { get; set; }
        public double? Cagr1 { get; set; }
        public double? AverageOwnerSEquity2019 { get; set; }
        public double? Cagr2 { get; set; }
        public double? AverageSales2019 { get; set; }
        public double? Cagr3 { get; set; }
        public double? AverageProfitAfterTax2019 { get; set; }
        public int? BusinessLineId { get; set; }
    }
}
