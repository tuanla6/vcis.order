﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonEducation
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public DateTime? DateFrom { get; set; }
        public bool? IsSpecialDateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public bool? IsSpecialDateTo { get; set; }
        public string University { get; set; }
        public string Degree { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Person Person { get; set; }
    }
}
