﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Person
    {
        public Person()
        {
            LitigationRecords = new HashSet<LitigationRecord>();
            PersonAddresses = new HashSet<PersonAddress>();
            PersonBankers = new HashSet<PersonBanker>();
            PersonCredits = new HashSet<PersonCredit>();
            PersonDocuments = new HashSet<PersonDocument>();
            PersonEducations = new HashSet<PersonEducation>();
            PersonLitigationChecks = new HashSet<PersonLitigationCheck>();
            PersonPressMediaResults = new HashSet<PersonPressMediaResult>();
            PersonWorkingExperiences = new HashSet<PersonWorkingExperience>();
            RelatedPersonPeople = new HashSet<RelatedPerson>();
            RelatedPersonRelatedPersonNavigations = new HashSet<RelatedPerson>();
            ShareHolders = new HashSet<ShareHolder>();
            TradeCreditSecuredTransactionRecords = new HashSet<TradeCreditSecuredTransactionRecord>();
            staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? Gender { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? Birthday { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Notes { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Idnumber { get; set; }
        public DateTime? IdissueDate { get; set; }
        public string IdissuePlace { get; set; }
        public string TaxNumber { get; set; }
        public string Education { get; set; }
        public string Major { get; set; }
        public string Experience { get; set; }
        public string Nationality { get; set; }
        public string FullName { get; set; }
        public bool? IsSpecialDate { get; set; }
        public string VietnameseName { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? Rid { get; set; }
        public int? Vid { get; set; }
        public string FormerIdnumber { get; set; }
        public DateTime? FormerIdnumberIssueDate { get; set; }
        public string FormerIdnumberIssuePlace { get; set; }
        public string PassPort { get; set; }
        public DateTime? PassPortIssueDate { get; set; }
        public string PassPortIssuePlace { get; set; }
        public string TaxStatus { get; set; }
        public string PlaceOfBirth { get; set; }
        public string FacebookLinkedIn { get; set; }
        public string Summary { get; set; }
        public string SupplimentalInformation { get; set; }
        public string ImmigrationAndCriminal { get; set; }
        public string Avatar { get; set; }
        public DateTime? TaxDate { get; set; }

        public virtual ICollection<LitigationRecord> LitigationRecords { get; set; }
        public virtual ICollection<PersonAddress> PersonAddresses { get; set; }
        public virtual ICollection<PersonBanker> PersonBankers { get; set; }
        public virtual ICollection<PersonCredit> PersonCredits { get; set; }
        public virtual ICollection<PersonDocument> PersonDocuments { get; set; }
        public virtual ICollection<PersonEducation> PersonEducations { get; set; }
        public virtual ICollection<PersonLitigationCheck> PersonLitigationChecks { get; set; }
        public virtual ICollection<PersonPressMediaResult> PersonPressMediaResults { get; set; }
        public virtual ICollection<PersonWorkingExperience> PersonWorkingExperiences { get; set; }
        public virtual ICollection<RelatedPerson> RelatedPersonPeople { get; set; }
        public virtual ICollection<RelatedPerson> RelatedPersonRelatedPersonNavigations { get; set; }
        public virtual ICollection<ShareHolder> ShareHolders { get; set; }
        public virtual ICollection<TradeCreditSecuredTransactionRecord> TradeCreditSecuredTransactionRecords { get; set; }
        public virtual ICollection<Staff> staff { get; set; }
    }
}
