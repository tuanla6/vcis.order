﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Event
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public int? EventTypeId { get; set; }
        public string EventName { get; set; }
        public string EventContent { get; set; }
        public DateTime? EventDate { get; set; }
        public int? DocumentId { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? PersonDocumentId { get; set; }

        public virtual Business Business { get; set; }
        public virtual EventType EventType { get; set; }
        public virtual PersonDocument PersonDocument { get; set; }
    }
}
