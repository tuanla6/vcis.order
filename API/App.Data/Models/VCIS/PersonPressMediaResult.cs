﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonPressMediaResult
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string Source { get; set; }
        public DateTime? DateOfPublication { get; set; }
        public string Briefing { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Person Person { get; set; }
    }
}
