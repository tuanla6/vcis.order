﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class LitigationRecord
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string NumberOfCase { get; set; }
        public string NameOfCase { get; set; }
        public DateTime? DateOfAcceptance { get; set; }
        public string NameOfCourt { get; set; }
        public string CourtName { get; set; }
        public string AddressOfCourt { get; set; }
        public string TelOfCourt { get; set; }
        public string CourtTaxCode { get; set; }
        public short? TypeOfcase { get; set; }
        public decimal? ValueOfDispute { get; set; }
        public string Currency { get; set; }
        public short? LevelOfCourt { get; set; }
        public string ResultText { get; set; }
        public string ResultFile { get; set; }
        public string NameOfJudge { get; set; }
        public DateTime? DateOfJudgement { get; set; }
        public int? PlaintiffBusinessId { get; set; }
        public int? PlaintiffPersonId { get; set; }
        public int? RelatedPartiesBusinessId { get; set; }
        public int? RelatedPartiesPersonId { get; set; }
        public DateTime? CheckDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
        public virtual Business PlaintiffBusiness { get; set; }
        public virtual Person PlaintiffPerson { get; set; }
        public virtual Business RelatedPartiesBusiness { get; set; }
    }
}
