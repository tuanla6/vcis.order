﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportTypeOption
    {
        public int Id { get; set; }
        public int ReportTypeId { get; set; }
        public int ReportOptionId { get; set; }
        public string Title { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual ReportOption ReportOption { get; set; }
        public virtual ReportType ReportType { get; set; }
    }
}
