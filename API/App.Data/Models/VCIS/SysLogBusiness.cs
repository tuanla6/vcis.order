﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class SysLogBusiness
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string LogType { get; set; }
        public string SessionId { get; set; }
        public string UserName { get; set; }
        public string DataOld { get; set; }
        public string DataNew { get; set; }
        public string Comment { get; set; }
        public string TableName { get; set; }
        public string FunctionName { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
