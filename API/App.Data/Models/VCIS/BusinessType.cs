﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessType
    {
        public BusinessType()
        {
            Businesses = new HashSet<Business>();
        }

        public int Id { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public int? Position { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Business> Businesses { get; set; }
    }
}
