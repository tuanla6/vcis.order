﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonCredit
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public string OverdueLoanContent { get; set; }
        public DateTime? OverdueLoanDate { get; set; }
        public string OverdueCreditContent { get; set; }
        public DateTime? OverdueCreditDate { get; set; }
        public string AssetBasedContent { get; set; }
        public DateTime? AssetBasedDate { get; set; }
        public int? NumberOfCollateral { get; set; }
        public int? NoBankersHeld { get; set; }
        public int? BusinessPartieSecuringId { get; set; }
        public int? PersonPartieSecuringId { get; set; }
        public int? BusinessPartieSecuredId { get; set; }
        public short? MortageType { get; set; }
        public DateTime? MortageDate { get; set; }
        public decimal? Value { get; set; }
        public string Currency { get; set; }
        public string AssetsDescription { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Person Person { get; set; }
    }
}
