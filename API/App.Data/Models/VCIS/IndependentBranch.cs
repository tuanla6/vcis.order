﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndependentBranch
    {
        public int Id { get; set; }
        public string Notes { get; set; }
        public int BusinessId { get; set; }
        public int BranchId { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual Business Business { get; set; }
        public virtual Business Branch { get; set; }
    }
}
