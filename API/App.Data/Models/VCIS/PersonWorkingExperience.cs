﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonWorkingExperience
    {
        public int Id { get; set; }
        public int PersonId { get; set; }
        public int? JobPositionId { get; set; }
        public DateTime? DateFrom { get; set; }
        public bool? IsSpecialDateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public bool? IsSpecialDateTo { get; set; }
        public int? Employer { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }

        public virtual JobPosition JobPosition { get; set; }
        public virtual Person Person { get; set; }
    }
}
