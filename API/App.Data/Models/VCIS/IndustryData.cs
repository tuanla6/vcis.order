﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryData
    {
        public int Id { get; set; }
        public int IndustryDataYear { get; set; }
        public string BusinessLineCode { get; set; }
        public int IndustryDataDefinitionId { get; set; }
        public decimal IndustryDataValue { get; set; }
        public decimal IndustryDataPercent { get; set; }
        public decimal IndustryDataDefaultValue { get; set; }
        public bool Deleted { get; set; }

        public virtual IndustryDataDefinition IndustryDataDefinition { get; set; }
    }
}
