﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TradeCreditSecuredTransactionRecord
    {
        public int Id { get; set; }
        public int? TradeCreditId { get; set; }
        public int? BusinessPartieSecuringId { get; set; }
        public int? BusinessPartieSecuredId { get; set; }
        public string Collatera { get; set; }
        public DateTime? DateOfRecord { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? PersonPartieSecuringId { get; set; }
        public short? MortageType { get; set; }
        public decimal? Value { get; set; }
        public string Currency { get; set; }

        public virtual Person PersonPartieSecuring { get; set; }
        public virtual TradeCredit TradeCredit { get; set; }
    }
}
