﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class RoleAccessRight
    {
        public string AccessRightCode { get; set; }
        public int RoleId { get; set; }

        public virtual AccessRight AccessRightCodeNavigation { get; set; }
        public virtual RolesOld Role { get; set; }
    }
}
