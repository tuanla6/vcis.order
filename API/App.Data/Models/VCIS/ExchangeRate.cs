﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ExchangeRate
    {
        public int Id { get; set; }
        public int YearExchangeRate { get; set; }
        public decimal? Usd { get; set; }
        public decimal? Eur { get; set; }
        public decimal? Gbp { get; set; }
        public decimal? Jpy { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
    }
}
