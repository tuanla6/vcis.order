﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ImportExport
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string Category { get; set; }
        public string TypesOfProducts { get; set; }
        public string Market { get; set; }
        public string Ratio { get; set; }
        public string ModeOfPayment { get; set; }
        public string Mandate { get; set; }
        public string CompanyAverage { get; set; }
        public string IndustryAverage { get; set; }
        public string Detail { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
    }
}
