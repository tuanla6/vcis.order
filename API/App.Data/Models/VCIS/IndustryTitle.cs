﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class IndustryTitle
    {
        public int Id { get; set; }
        public string BusinessLineCode { get; set; }
        public string Vncref { get; set; }
        public string IndustryTitles { get; set; }
        public string EnglishIndustryTitle { get; set; }
        public bool Deleted { get; set; }
    }
}
