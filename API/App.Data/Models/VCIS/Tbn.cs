﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Tbn
    {
        public int Id { get; set; }
        public double? Stt { get; set; }
        public string Mn { get; set; }
        public string Mn1 { get; set; }
        public string Mn2 { get; set; }
        public string Mn3 { get; set; }
        public string Mn4 { get; set; }
        public string Mn5 { get; set; }
        public string Mn6 { get; set; }
        public string Mn7 { get; set; }
        public double? D1 { get; set; }
        public double? D2 { get; set; }
        public double? D3 { get; set; }
        public double? D4 { get; set; }
        public double? D5 { get; set; }
        public double? D6 { get; set; }
        public double? D7 { get; set; }
        public double? D8 { get; set; }
        public double? D9 { get; set; }
        public double? D10 { get; set; }
        public double? D11 { get; set; }
        public double? D12 { get; set; }
        public double? D13 { get; set; }
        public double? D14 { get; set; }
        public double? D15 { get; set; }
        public double? D16 { get; set; }
        public double? D17 { get; set; }
        public double? D18 { get; set; }
        public double? D19 { get; set; }
        public double? D20 { get; set; }
        public double? D21 { get; set; }
        public double? D22 { get; set; }
        public double? D23 { get; set; }
        public double? D24 { get; set; }
        public double? D25 { get; set; }
        public double? D26 { get; set; }
        public double? D27 { get; set; }
        public double? D28 { get; set; }
        public double? D29 { get; set; }
        public double? D30 { get; set; }
        public double? D31 { get; set; }
        public double? D32 { get; set; }
        public double? D33 { get; set; }
        public double? D34 { get; set; }
        public double? D35 { get; set; }
        public double? D36 { get; set; }
        public double? D37 { get; set; }
        public double? D38 { get; set; }
        public double? D39 { get; set; }
        public double? D40 { get; set; }
        public double? D41 { get; set; }
        public double? D42 { get; set; }
        public double? D43 { get; set; }
        public double? D44 { get; set; }
        public double? D45 { get; set; }
        public double? D46 { get; set; }
    }
}
