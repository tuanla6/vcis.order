﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Address
    {
        public int AddressId { get; set; }
        public int? DistrictId { get; set; }
        public string ProvinceCode { get; set; }
        public string CountryCode { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string Notes { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsDefault { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string OccupiedArea { get; set; }
        public string LandUseRight { get; set; }
        public int? PersonId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public int? DisplayIndex { get; set; }
    }
}
