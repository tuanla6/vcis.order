﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class AverageIndustryComparison
    {
        public int Id { get; set; }
        public decimal? TotalAssetsMax { get; set; }
        public decimal? TotalSalesMax { get; set; }
        public decimal? Oemax { get; set; }
        public decimal? ProfitBeforeTaxMax { get; set; }
        public int? EmployeeMax { get; set; }
        public int? IndustryId { get; set; }
        public decimal? TotalAssetsMin { get; set; }
        public decimal? TotalSalesMin { get; set; }
        public decimal? Oemin { get; set; }
        public decimal? ProfitBeforeTaxMin { get; set; }
        public int? EmployeeMin { get; set; }
        public bool Deleted { get; set; }

        public virtual IndustryAverageReport Industry { get; set; }
    }
}
