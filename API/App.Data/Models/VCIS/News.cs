﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class News
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public string Title { get; set; }
        public string NewsContent { get; set; }
        public int? DocumentId { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public DateTime? DateOfNews { get; set; }

        public virtual Business Business { get; set; }
    }
}
