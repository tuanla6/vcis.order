﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportCreditRating
    {
        public ReportCreditRating()
        {
            Reports = new HashSet<Report>();
        }

        public int Id { get; set; }
        public string RatingCode { get; set; }
        public short MinScore { get; set; }
        public short MaxScore { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Report> Reports { get; set; }
    }
}
