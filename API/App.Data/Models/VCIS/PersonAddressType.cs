﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PersonAddressType
    {
        public PersonAddressType()
        {
            PersonAddresses = new HashSet<PersonAddress>();
        }

        public int Id { get; set; }
        public string AddressTypeName { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<PersonAddress> PersonAddresses { get; set; }
    }
}
