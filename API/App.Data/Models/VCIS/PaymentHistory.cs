﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class PaymentHistory
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string CreditContractNo { get; set; }
        public string WithTheBank { get; set; }
        public decimal Amount { get; set; }
        public string LoanTerm { get; set; }
        public string LoansInterest { get; set; }
        public string PeriodOfPayment { get; set; }
        public string StatusOfPayment { get; set; }
        public string Evaluation { get; set; }
        public string PaymentMethod { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
    }
}
