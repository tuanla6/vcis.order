﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class ReportCreditScore
    {
        public int Id { get; set; }
        public int ReportId { get; set; }
        public short? Score { get; set; }
        public short? DisplayOrder { get; set; }
        public int? ReportCreditCriterionId { get; set; }
        public bool Deleted { get; set; }

        public virtual Report Report { get; set; }
        public virtual ReportCreditCriterion ReportCreditCriterion { get; set; }
    }
}
