﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class Industry
    {
        public Industry()
        {
            BusinessLines = new HashSet<BusinessLine>();
        }

        public int Id { get; set; }
        public string IndustryCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<BusinessLine> BusinessLines { get; set; }
    }
}
