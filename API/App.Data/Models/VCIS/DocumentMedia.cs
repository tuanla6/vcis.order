﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class DocumentMedia
    {
        public DocumentMedia()
        {
            Documents = new HashSet<Document>();
            PersonDocuments = new HashSet<PersonDocument>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string IconFileName { get; set; }
        public bool Deleted { get; set; }

        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<PersonDocument> PersonDocuments { get; set; }
    }
}
