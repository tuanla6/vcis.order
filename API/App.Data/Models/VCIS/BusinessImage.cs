﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessImage
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Url { get; set; }
        public bool IsActive { get; set; }
        public bool? IsFromApp { get; set; }
        public string Notes { get; set; }
        public int? BusinessAddressId { get; set; }
        public int BusinessId { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int ContentLength { get; set; }

        public virtual Business Business { get; set; }
        public virtual BusinessAddress BusinessAddress { get; set; }
    }
}
