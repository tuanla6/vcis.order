﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class LaoDongTbn
    {
        public string MaNganh { get; set; }
        public int? SoLaoDong { get; set; }
        public int? SoLuongDoanhNghiep { get; set; }
        public int Id { get; set; }
    }
}
