﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class TradeReference
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public decimal? Balance { get; set; }
        public string Currency { get; set; }
        public DateTime? BalanceDate { get; set; }
        public string Notes { get; set; }
        public string Category { get; set; }
        public int? ReferenceBusinessId { get; set; }
        public string BusinessName { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }

        public virtual Business Business { get; set; }
        public virtual Business ReferenceBusiness { get; set; }
    }
}
