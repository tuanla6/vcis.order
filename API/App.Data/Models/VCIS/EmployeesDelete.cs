﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class EmployeesDelete
    {
        public string MstVcis { get; set; }
        public double? TotalEmp2019 { get; set; }
        public int Id { get; set; }
        public string Crid { get; set; }
        public bool? IsNew { get; set; }
        public int? Years { get; set; }
    }
}
