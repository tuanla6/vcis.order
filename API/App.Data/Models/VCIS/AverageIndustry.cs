﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class AverageIndustry
    {
        public AverageIndustry()
        {
            IndustryAverageValues = new HashSet<IndustryAverageValue>();
        }

        public int Id { get; set; }
        public string AverageIndustryName { get; set; }
        public string AverageUnit { get; set; }
        public string AverageIndustryCode { get; set; }
        public bool Deleted { get; set; }
        public int? IndustryAverageGroupId { get; set; }
        public short? DisplayIndex { get; set; }
        public short? AverageIndustryType { get; set; }

        public virtual ICollection<IndustryAverageValue> IndustryAverageValues { get; set; }
    }
}
