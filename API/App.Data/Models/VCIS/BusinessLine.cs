﻿using System;
using System.Collections.Generic;

#nullable disable

namespace App.Data.Models.VCIS
{
    public partial class BusinessLine
    {
        public BusinessLine()
        {
            Activities = new HashSet<Activity>();
            IndustryAverageValues = new HashSet<IndustryAverageValue>();
            IndustryInformations = new HashSet<IndustryInformation>();
        }

        public int Id { get; set; }
        public string BusinessLineCode { get; set; }
        public string IndustryCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? BusinessLevel { get; set; }
        public string ParentId { get; set; }
        public string BusinessLineFull { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? IndustryId { get; set; }
        public int? ParentBusinessLineId { get; set; }
        public string Vsic { get; set; }
        public string Nace { get; set; }
        public string Naics { get; set; }
        public string Nacedescription { get; set; }
        public string Naicsdescription { get; set; }
        public string RelatedNaceindustry { get; set; }
        public string RelatedNaicsindustry { get; set; }

        public virtual Industry Industry { get; set; }
        public virtual ICollection<Activity> Activities { get; set; }
        public virtual ICollection<IndustryAverageValue> IndustryAverageValues { get; set; }
        public virtual ICollection<IndustryInformation> IndustryInformations { get; set; }
    }
}
