using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Data.Models
{
    [Table("User")]
    public class User : BaseModel
    {
        public User()
        : base()
        {
            
        }
        [Required]
        [StringLength(255)]
        public string tai_khoan { get; set; }

        [Required]
        [StringLength(255)]
        public string mat_khau { get; set; }

        [Required]
        [StringLength(32)]
        public string salt_code { get; set; }

        [Required]
        [StringLength(255)]
        public string ten { get; set; }

        [Required]
        public int trang_thai { get; set; }

        public bool super_admin { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(32)]
        public string so_dien_thoai { get; set; }

        [StringLength(1024)]
        public string anh_dai_dien_url { get; set; }
        
    }
}
