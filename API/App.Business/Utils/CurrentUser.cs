﻿using App.Business.Services;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;

namespace App.Business.Utils
{
    public static class AppHelpers
    {
        private static IHttpContextAccessor httpContextAccessor;
        public static void SetHttpContextAccessor(IHttpContextAccessor accessor)
        {
            httpContextAccessor = accessor;
        }
        private static string getClaim(this ClaimsPrincipal user, string claimType)
        {
            var temp = user.FindFirst(claimType);
            return temp != null ? temp.Value : null;
        }
        public static NguoiDungDTO GetCurrentUser()
        {
            var curUser = httpContextAccessor.HttpContext.User;
            var currentUser = new NguoiDungDTO();
            try
            {
                if (curUser != null)
                {
                    currentUser.Id = Int16.Parse(curUser.getClaim("id"));
                    currentUser.tai_khoan = curUser.getClaim("tai_khoan");
                    currentUser.ten = curUser.getClaim("ten");
                    currentUser.super_admin = Convert.ToBoolean(curUser.getClaim("super_admin"));
                }
            }
            catch (Exception)
            {
            }
            return currentUser;
        }
    }
}
