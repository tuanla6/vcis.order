﻿using App.Data;
using App.Data.Models;
using AutoMapper;
using System;

namespace App.Business.Base
{
    public class BaseDTO
    {
        public int Id { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool? Deleted { get; set; }
        public NguoiTaoDTO CreatedBy { get; set; }
        public NguoiChinhSuaDTO UpdatedBy { get; set; }
    }
    public class NguoiTaoDTO
    {
        public int Id { get; set; }
        public string tai_khoan { get; set; }
        public string ten { get; set; }
    }
    public class NguoiChinhSuaDTO
    {
        public int Id { get; set; }
        public string tai_khoan { get; set; }
        public string ten { get; set; }
    }
    public class SelectDTO
    {
        public int Id { get; set; }
        public string ten { get; set; }
    }
    public class BaseProfile : Profile
    {
        public BaseProfile()
        {
            CreateMap<User, NguoiChinhSuaDTO>();
            CreateMap<User, NguoiTaoDTO>();
            CreateMap<BaseDTO, BaseModel>()
            .ForMember(d => d.CreatedBy, opt => opt.Ignore())
            .ForMember(d => d.UpdatedBy, opt => opt.Ignore());
        }
    }
}
