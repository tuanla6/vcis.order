using App.Business.Services;
using App.Data;
using Microsoft.Extensions.DependencyInjection;

namespace App.Business
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddAppBusinessLibrary(this IServiceCollection services)
        {

            services.AddAppDataLibrary();
            services.AddScoped<INguoiDungService, NguoiDungService>();
            services.AddScoped<IBusinessesService, BusinessesService>();

            services.AddScoped<IBusinessService, BusinessService>();
            services.AddScoped<IBoardOfDirectorService, BoardOfDirectorService>();
            services.AddScoped<IShareholderService, ShareholderService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<INegativePaymentService, NegativePaymentService>();
            services.AddScoped<ILitigationService, LitigationService>();
            services.AddScoped<INegativeNewsesService, NegativeNewsesService>();
            services.AddScoped<IImportExportsService, ImportExportsService>();
            services.AddScoped<ILineofBusinessService, LineofBusinessService>();
            services.AddScoped<IBusinessBankersService, BusinessBankersService>();
            services.AddScoped<IReportsService, ReportsService>();

            return services;
        }
    }
}
