﻿using App.Business.Base;
using App.Data.Models;
using System;
using System.Collections.Generic;
using static App.Business.Services.BusinessesDTO;

namespace App.Business.Services
{
    public interface IBusinessesService
    {
        SearchResult SearchBusiness(SearchConditionsViewModel filter, int pageNo, int pageSize, string sortColumn = null, bool isAscSort = true);
    }
}
