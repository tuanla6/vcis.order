﻿using App.Business.Base;
using App.Data;
using App.Data.Models;
using AutoMapper;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Business.Services
{
    public class BusinessesDTO : BaseDTO
    {
        public class SearchResult
        {
            public int Total { get; set; }
            public int Page { get; set; }
            public IEnumerable<SearchViewModel> Results { get; set; }
            public long ElapsedMilliseconds { get; set; }
        }
        public class SearchConditionsViewModel
        {
            public string Keyword { get; set; }

            public string BusinessName { get; set; }

            public string TaxCodeOrRegistrationNumber { get; set; }

            public int? IndustryId { get; set; }

            //address
            public int? CountryId { get; set; }

            public int? ProvinceId { get; set; }

            public int? DistrictId { get; set; }

            public string Address { get; set; }

            public string FaxOrPhone { get; set; }
            public string CRID { get; set; }
        }
        public class SearchViewModel
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string CRID { get; set; }

            public string EnglishName { get; set; }

            public string RegisteredName { get; set; }

            public string TradeName { get; set; }

            public string TaxCode { get; set; }

            public string RegistrationNumber { get; set; }

            public DateTime? RegistrationDate { get; set; }

            public decimal? RegisteredCapital { get; set; }

            public int? TotalEmployees { get; set; }

            //public IEnumerable<TurnOverViewModel> TurnOver { get; set; }

            //public IEnumerable<TurnOverViewModel> Profit { get; set; }

            public string Stocksymbol { get; set; }

            public string StockMarket { get; set; }

            public string BusinessLineCode { get; set; }

            public string InvestmentCertificationNo { get; set; }

            public decimal? RegisteredInvestmentAmount { get; set; }

            public DateTime? InvestmentCertificationDate { get; set; }

            public string InvestmentCertificationPlace { get; set; }

            public bool? IsFdi { get; set; }

            public List<short> FiscalYears { get; set; }

            public int? IndustryId { get; set; }

            public List<AddressViewModel> Addresses { get; set; }

            //public List<StaffViewModel> Staffs { get; set; }

            public string VnName { get; set; }

            public bool IsPublished { get; set; }

            public double? Score { get; set; }

            public DateTime? UpdatedDate { get; set; }
            public string DefaultAddress { get; set; }

        }
        public class ElasticSearchSetting
        {
            //public override ComplexSettingSetup GetSetup()
            //{
            //    return new ComplexSettingSetup
            //    {
            //        Name = "Elastic Search Setting",
            //        Type = SettingEnums.SettingType.BackEnd,
            //        DefaultValue = new ElasticSearchSetting
            //        {
            //            BusinessIndexName = "businesses",
            //            PersonIndexName = "persons",
            //            ItemsPerPage = 500,
            //            ClientUrl = "http://localhost:9200"
            //        }
            //    };
            //}

            #region Public Properties

            [DisplayName(@"Business Index Name")]
            public string BusinessIndexName { get; set; }

            [DisplayName(@"Person Index Name")]
            public string PersonIndexName { get; set; }

            [DisplayName(@"Item Per Page")]
            public int ItemsPerPage { get; set; }

            [DisplayName(@"Client URL")]
            public string ClientUrl { get; set; }

            #endregion
        }
        [ElasticsearchType]
        public class SearchESModel
        {
            public int Id { get; set; }

            public string Crid { get; set; }

            public string Name { get; set; }

            public string EnglishName { get; set; }

            public string RegisteredName { get; set; }

            public string TradeName { get; set; }

            public string TaxCode { get; set; }
            public string Status { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string RegistrationNumber { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public DateTime? RegistrationDate { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? RegisteredCapital { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? TotalEmployees { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? TotalTurnOvers { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? TotalProfits { get; set; }

            public string Stocksymbol { get; set; }

            public string StockMarket { get; set; }

            public string BusinessLineCode { get; set; }

            public string InvestmentCertificationNo { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? RegisteredInvestmentAmount { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public DateTime? InvestmentCertificationDate { get; set; }

            public string InvestmentCertificationPlace { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public bool? IsFdi { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? IndustryId { get; set; }

            public List<short> FiscalYears { get; set; }

            public DateTime? UpdatedDate { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string VietnamName { get; set; }

            public string VNRegistrationAuthority { get; set; }

            public string VNDescription { get; set; }

            public bool IsPublished { get; set; }

            [Nested]
            public List<AddressESModel> Addresses { get; set; }

            //[Nested]
            //public List<StaffEsModel> Staffs { get; set; }

            //[Nested]
            //public List<SearchEsDecimal> FinancialReportDataTurnOvers { get; set; }

            //[Nested]
            //public List<SearchEsDecimal> FinancialReportDataProfits { get; set; }
        }
        public class AddressESModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? CountryId { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string CountryName { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? ProvinceId { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string ProvinceName { get; set; }
            public string ProvinceCategory { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? DistrictId { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string DistrictName { get; set; }

            public string DistrictCategory { get; set; }

            public string Address { get; set; }

            public string Postcode { get; set; }

            [Keyword]
            public string RawFax { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string Fax { get; set; }

            [Keyword]
            public string RawPhone { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string Phone { get; set; }

            public bool? IsHeadOffice { get; set; }
            public DateTime? LastUpdatedDate { get; set; }

            public string VNFullAddress { get; set; }
        }
        public class AddressViewModel
        {
            public int? DistrictId { get; set; }

            public string DistrictName { get; set; }

            public int? CountryId { get; set; }

            public string CountryName { get; set; }

            public int? ProvinceId { get; set; }

            public string ProvinceName { get; set; }

            public string Address { get; set; }

            public string Postcode { get; set; }

            public string Phone { get; set; }

            public string Fax { get; set; }

            public string Email { get; set; }

            public string ProvinceCategory { get; set; }

            public string DistrictCategory { get; set; }

            public bool? IsHeadOffice { get; set; }

            public DateTime? LastUpdatedDate { get; set; }
        }
    }
}
