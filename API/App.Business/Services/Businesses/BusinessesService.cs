﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Common.Library;
using App.Data;
using App.Data.Models;
using App.Data.Models.VCIS;
using AutoMapper;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static App.Business.Services.BusinessesDTO;

namespace App.Business.Services
{
    public class BusinessesService : IBusinessesService
    {
        private VCIS4Context _vcisContext;
        public BusinessesService(VCIS4Context vcisContext)
        {
            _vcisContext = vcisContext;
        }

        public SearchResult SearchBusiness(SearchConditionsViewModel filter, int pageNo, int pageSize, string sortColumn = null, bool isAscSort = true)
        {
            
            if (filter.Keyword == null) filter.Keyword = string.Empty;
            filter.Keyword = filter.Keyword.RemoveVietnameseTone();

            // Case keyword is tax code of child company (example: 1234567890-123)
            var regex = new Regex("^([0-9]){11}-[0-9]{3}$");

            var isTaxCode = regex.IsMatch(filter.Keyword);
            if (isTaxCode)
            {
                filter.Keyword = filter.Keyword.Replace("-", "");
            }

            // Remove common words
            regex = new Regex("([., ]+jsc$|[ ]+jsc[., ]+|(([ ]+co)*[., ]+ltd)$|(([ ]+co)*[., ]+ltd)[ ]+)");
            filter.Keyword = regex.Replace(filter.Keyword, " ");

            SearchResult result;
            // 1.Search by stock symbol
            //if (filter.Keyword.Count() == 3)
            //{
            //    result = SearchStockCompanies(filter, pageNo, pageSize, sortColumn, isAscSort);
            //    if (result.Results.Count() > 0) return result;
            //}

            // 2.Search exactly 
            result = GetEsData(filter, pageNo, pageSize, sortColumn, isAscSort);
            if (result.Total > 0) return result;

            // 3.Search fuzzy
            result = SearchFuzzyCompanies(filter, pageNo, pageSize, sortColumn, isAscSort);
            return result;
        }
        public SearchResult GetEsData(SearchConditionsViewModel filter, int pageNo, int pageSize, string sortColumn = null, bool isAscSort = true)
        {
            string ESSuffix = "keyword";

            var elasticSearchSetting = GetElasticSetting();
            var esIndex = elasticSearchSetting.BusinessIndexName;
            var client = GetElasticClient(elasticSearchSetting);

            var filterQuery = new List<Func<QueryContainerDescriptor<SearchESModel>, QueryContainer>>();

            #region Apply Filter


            #region Keyword

            if (!string.IsNullOrEmpty(filter.Keyword))
            {
                filterQuery.Add(s => s
                    .Bool(b2 => b2
                        .Should(
                            sh1 => sh1
                                .MultiMatch(mp => mp
                                    .Query(filter.Keyword)
                                    .Fields(fs => fs
                                        .Fields(f1 => f1.Name, f2 => f2.TradeName, f3 => f3.RegisteredName, f4 => f4.EnglishName, f5 => f5.TaxCode, f6 => f6.RegistrationNumber)
                                    )
                                    .Type(TextQueryType.PhrasePrefix)
                                )
                            , sh2 => sh2
                                .Nested(n1 => n1
                                    .Path(p => p.Addresses)
                                    .Query(q1 => q1
                                        .Bool(b => b
                                            .Filter(
                                                m => m
                                                    .MatchPhrasePrefix(mt => mt
                                                        .Field(f => f.Addresses.First().Address)
                                                        .Query(filter.Keyword))
                                                , m => m
                                                    .MultiMatch(mts => mts
                                                        .Query(filter.Keyword)
                                                        .Fields(fs => fs
                                                            .Fields(f => f.Addresses.First().Fax,
                                                                f => f.Addresses.First().Phone)))
                                            )
                                        )
                                    )
                                )
                        )
                    )
                );
            }
            #endregion

            // Business Name 
            if (!string.IsNullOrEmpty(filter.BusinessName))
            {
                filterQuery.Add(s => s
                    .Bool(b2 => b2
                        .Filter(m1 => m1
                            .MultiMatch(mp => mp
                                .Query(filter.BusinessName)
                                .Fields(fs => fs
                                    .Fields(f2 => f2.Name, f3 => f3.TradeName, f4 => f4.RegisteredName, f5 => f5.EnglishName, f6 => f6.VietnamName)
                                )
                                .Type(TextQueryType.PhrasePrefix)
                            )
                        )
                    ));
            }

            // Business CRID 
            if (!string.IsNullOrEmpty(filter.CRID))
            {
                filterQuery.Add(s => s
                    .Bool(b2 => b2
                        .Filter(m1 => m1
                            .MultiMatch(mp => mp
                                .Query(filter.CRID)
                                .Fields(fs => fs
                                    .Fields(f2 => f2.Crid)
                                )
                                .Type(TextQueryType.PhrasePrefix)
                            )
                        )
                    ));
            }

            // TaxCode: Tax code and Registration Number

            if (!string.IsNullOrEmpty(filter.TaxCodeOrRegistrationNumber))
            {
                filterQuery.Add(s => s
                    .Bool(b2 => b2
                        .Filter(m1 => m1
                            .MultiMatch(mp => mp
                                .Query(filter.TaxCodeOrRegistrationNumber)
                                .Fields(fs => fs
                                    .Fields(f1 => f1.TaxCode, f2 => f2.RegistrationNumber)
                                )
                                .Type(TextQueryType.PhrasePrefix)
                            )
                        )
                    ));
            }


            #endregion

            #region Apply Sorting

            Func<SortDescriptor<SearchESModel>, IPromise<IList<ISort>>> sortDescriptor = st => st.Field(f => f.Field("_score").Descending());

            sortDescriptor = descriptor => descriptor.Descending(x => x.UpdatedDate).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                switch (sortColumn)
                {
                    case nameof(SearchViewModel.Id):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.Id);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.Id);
                        break;
                    //Name = business.RegisteredName
                    case nameof(SearchViewModel.Name):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegisteredName.Suffix(ESSuffix)).Descending(SortSpecialField.Score);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegisteredName.Suffix(ESSuffix)).Descending(SortSpecialField.Score);
                        break;
                    case nameof(SearchViewModel.CRID):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.Crid.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.Crid.Suffix(ESSuffix));
                        break;
                    //English Name = business.Name
                    case nameof(SearchViewModel.EnglishName):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.Name.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.Name.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder); ;
                        break;
                    case nameof(SearchViewModel.RegisteredName):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegisteredName.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegisteredName.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        break;
                    case nameof(SearchViewModel.TradeName):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.TradeName.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.TradeName.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        break;
                    case nameof(SearchViewModel.TaxCode):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.TaxCode.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.TaxCode.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.RegistrationNumber):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegistrationNumber.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegistrationNumber.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.RegistrationDate):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegistrationDate);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegistrationDate);
                        break;
                    case nameof(SearchViewModel.RegisteredCapital):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegisteredCapital);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegisteredCapital);
                        break;
                    case nameof(SearchViewModel.TotalEmployees):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.TotalEmployees);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.TotalEmployees);
                        break;
                    case nameof(SearchViewModel.Stocksymbol):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.Stocksymbol.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.Stocksymbol.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.StockMarket):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.StockMarket.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.StockMarket.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.BusinessLineCode):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.BusinessLineCode.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.BusinessLineCode.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.InvestmentCertificationNo):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.InvestmentCertificationNo.Suffix(ESSuffix));
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.InvestmentCertificationNo.Suffix(ESSuffix));
                        break;
                    case nameof(SearchViewModel.InvestmentCertificationDate):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.InvestmentCertificationDate);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.InvestmentCertificationDate);
                        break;
                    case nameof(SearchViewModel.RegisteredInvestmentAmount):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.RegisteredInvestmentAmount);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.RegisteredInvestmentAmount);
                        break;
                    case nameof(SearchViewModel.InvestmentCertificationPlace):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.InvestmentCertificationPlace.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.InvestmentCertificationPlace.Suffix(ESSuffix)).Descending(SortSpecialField.Score).Ascending(SortSpecialField.DocumentIndexOrder);
                        break;
                    case nameof(SearchViewModel.IsFdi):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.IsFdi);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.IsFdi);
                        break;
                    case nameof(SearchViewModel.IndustryId):
                        if (isAscSort)
                            sortDescriptor = descriptor => descriptor.Ascending(a => a.IndustryId);
                        else
                            sortDescriptor = descriptor => descriptor.Descending(a => a.IndustryId);
                        break;
                }
            }

            ISearchResponse<SearchESModel> response = client.Search<SearchESModel>(es => es
                .Index(esIndex)
                .From(pageNo * pageSize).Size(pageSize)
                .Query(q => q
                    .Bool(b => b
                        .Filter(filterQuery)
                    )
                )
                .TrackScores()
                .Sort(sortDescriptor));

            #endregion

            var result = new List<SearchViewModel>();
            foreach (var item in response.Documents.Where(s => s.IsPublished))
            {
                var defaultAddress = string.Empty;
                var defaultOffice = item.Addresses.FirstOrDefault(b => b.IsHeadOffice.HasValue && b.IsHeadOffice.Value);
                if (defaultOffice != null)
                {
                    defaultAddress = StringUtilities.GenerateAddress(defaultOffice.Address, defaultOffice.DistrictName, defaultOffice.ProvinceName, defaultOffice.CountryName, defaultOffice.ProvinceCategory, defaultOffice.DistrictCategory);
                }
                result.Add(new SearchViewModel
                {
                    Id = item.Id,
                    CRID = item.Crid,
                    Name = item.Name,
                    EnglishName = item.Name,
                    RegisteredName = item.RegisteredName,
                    TradeName = item.Name,
                    TaxCode = item.TaxCode,
                    DefaultAddress = defaultAddress
                });
            }

            var resultData = GetHitsSearchResult(response);
            resultData.Page = pageNo;
            return resultData;
        }
        private SearchResult GetHitsSearchResult(ISearchResponse<SearchESModel> response)
        {
            var result = new List<SearchViewModel>();
            foreach (var itemHits in response.Hits)
            {
                var item = itemHits.Source;
                if (!item.IsPublished) continue;

                var defaultAddress = string.Empty;
                var defaultOffice = item.Addresses.FirstOrDefault(b => b.IsHeadOffice.HasValue && b.IsHeadOffice.Value);
                if (defaultOffice != null)
                {
                    defaultAddress = StringUtilities.GenerateAddress(defaultOffice.Address, defaultOffice.DistrictName, defaultOffice.ProvinceName, defaultOffice.CountryName, defaultOffice.ProvinceCategory, defaultOffice.DistrictCategory);
                }

                result.Add(new SearchViewModel
                {
                    Score = itemHits.Score,
                    Id = item.Id,
                    CRID = item.Crid,
                    Name = item.Name,
                    EnglishName = item.Name,
                    RegisteredName = item.RegisteredName,
                    TradeName = item.Name,
                    TaxCode = item.TaxCode,
                    DefaultAddress = defaultAddress
                });
            }

            return new SearchResult
            {
                Total = (int)response.Total,
                //Page = pageNo,
                Results = result,
                ElapsedMilliseconds = response.Took
            };
        }

        private SearchResult SearchFuzzyCompanies(SearchConditionsViewModel filter, int pageNo, int pageSize, string sortColumn = null, bool isAscSort = true)
        {
            var elasticSearchSetting = GetElasticSetting();
            var esIndex = elasticSearchSetting.BusinessIndexName;
            var client = GetElasticClient(elasticSearchSetting);


            var searchResponse = client.Search<SearchESModel>(s => s
                .MinScore(12)
                .Query(q => q
                    .SimpleQueryString(ss => ss
                        .Fields(f => f
                            .Fields(ff => ff.Name, f2 => f2.RegisteredName)
                        )
                        .Query(filter.Keyword)
                        .Analyzer("standard")
                        .DefaultOperator(Operator.Or)
                        .Flags(SimpleQueryStringFlags.And)
                        .Lenient()
                        .AnalyzeWildcard()
                        .MinimumShouldMatch("75%")
                        .FuzzyPrefixLength(0)
                        .FuzzyMaxExpansions(50)
                        .FuzzyTranspositions()
                        .AutoGenerateSynonymsPhraseQuery(true)
                    )
                )
                .Index(esIndex)
                .From(pageNo * pageSize)
                .Size(pageSize)
            );

            var searchData = GetHitsSearchResult(searchResponse);
            searchData.Page = pageNo;
            return searchData;
        }
        public ElasticClient GetElasticClient(ElasticSearchSetting setting)
        {
            var indexUrl = setting.ClientUrl;
            indexUrl = string.IsNullOrEmpty(indexUrl) ? "http://27.72.104.59:9200/" : indexUrl;
            var node = new Uri(indexUrl);
            var settings = new ConnectionSettings(node).DisableDirectStreaming();
            return new ElasticClient(settings);
        }
        private ElasticSearchSetting GetElasticSetting()
        {
            //var elasticSearchSettings = AppCaches.ListSettings.FirstOrDefault(x => x.Key.Equals(Constants.ElasticSearch));
            //if (elasticSearchSettings == null)
            //{
            //    throw new ArgumentNullException("filter", @"Elastic Search configuration is not found.");
            //}

            var result = new ElasticSearchSetting
            {
                BusinessIndexName = "businesses",
                PersonIndexName = "persons",
                ItemsPerPage = 500,
                //  ClientUrl = elasticSearchSettings.Value
                ClientUrl = "http://27.72.104.59:9200/"
            };
            return result;
        }
    }
}
