﻿using App.Business.Base;
using App.Data;
using App.Data.Models;
using AutoMapper;

namespace App.Business.Services
{
    public class NguoiDungDTO : BaseDTO
    {
        public string tai_khoan { get; set; }
        public string mat_khau { get; set; }
        public string salt_code { get; set; }
        public string ten { get; set; }
        public int trang_thai { get; set; }
        public bool super_admin { get; set; }
        public string email { get; set; }
        public string so_dien_thoai { get; set; }
        public string anh_dai_dien_url { get; set; }
    }
    public class NguoiDungProfile : Profile
    {
        public NguoiDungProfile()
        {
            CreateMap<User, NguoiDungDTO>()
                .ForMember(x => x.mat_khau, otp => otp.Ignore())
                .ForMember(x => x.salt_code, otp => otp.Ignore());
            CreateMap<NguoiDungDTO, User>()
                .IncludeBase<BaseDTO, BaseModel>()
                .ForMember(x => x.mat_khau, otp => otp.Ignore())
                .ForMember(x => x.salt_code, otp => otp.Ignore());
        }
    }
}
