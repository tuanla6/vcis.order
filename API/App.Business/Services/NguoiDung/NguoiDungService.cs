﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Business.Services
{
    public class NguoiDungService : GenericService<User>, INguoiDungService
    {
        public NguoiDungService(APPContext dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<NguoiDungProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<User> QueryBuilder(IQueryable<User> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                String tai_khoan = filter.tai_khoan;
                if (!string.IsNullOrEmpty(tai_khoan))
                {
                    tai_khoan = tai_khoan.ToLower().Trim();
                    query = query.Where(x => x.tai_khoan.ToLower().Contains(tai_khoan));
                }
                String ten = filter.ten;
                if (!string.IsNullOrEmpty(ten))
                {
                    ten = ten.ToLower().Trim();
                    query = query.Where(x => x.ten.ToLower().Contains(ten));
                }
            }
            if (search != null && search != "")
            {
                search = search.ToLower().Trim();
                query = query.Where(x => x.tai_khoan.ToLower().Contains(search) ||
                                         x.ten.ToLower().Contains(search) ||
                                         x.so_dien_thoai == search ||  
                                         x.email == search);
            }
            return query;
        }
        protected override void BeforeMapper<TDto>(bool isNew, ref TDto dto)
        {
            //check trùng dữ liệu
            if (typeof(TDto) == typeof(NguoiDungDTO))
            {
                NguoiDungDTO nguoidungDTO = (NguoiDungDTO)(object)dto;
                var isExits = _repo.Where(x => x.tai_khoan.ToLower() == nguoidungDTO.tai_khoan.ToLower() && x.Id != nguoidungDTO.Id).Count() > 0;
                if (isExits)
                {
                    ErrorCtr.DataExits("Tài khoản đã tồn tại, vui lòng kiểm tra lại thông tin");
                }
            }
        }
        protected override void BeforeAddOrUpdate<TDto>(bool isNew, ref TDto dto, ref User entity)
        {
            if (typeof(TDto) != typeof(NguoiDungDTO))
            {
                return;
            }
            NguoiDungDTO nguoidungDTO = (NguoiDungDTO)(object)dto;
        }

        protected override void AfferAddOrUpdate<TDto>(bool isNew, ref TDto dto, ref User entity)
        {
            if (typeof(TDto) != typeof(NguoiDungDTO))
            {
                return;
            }
            NguoiDungDTO nguoidungDTO = (NguoiDungDTO)(object)dto;
            
        }

        public NguoiDungDTO XacThucDangNhap(string tai_khoan, string mat_khau)
        {
            try
            {
                NguoiDungDTO thongTinNguoiDung = null;
                var nguoiDung = _repo.Where(x => x.tai_khoan == tai_khoan).FirstOrDefault();
                if (nguoiDung != null)
                {
                    if (MD5algorithm.VerifyMd5Hash(mat_khau, nguoiDung.mat_khau, nguoiDung.salt_code))
                    {
                        thongTinNguoiDung = _mapper.Map<User, NguoiDungDTO>(nguoiDung);
                        return thongTinNguoiDung;
                    }

                }
                ErrorCtr.Reject(System.Net.HttpStatusCode.BadRequest, "invalid_password", "Mật khẩu không chính xác. Vui lòng kiểm tra lại!");
                return thongTinNguoiDung;
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
