﻿using App.Business.Base;
using App.Data.Models;
using System;
using System.Collections.Generic;

namespace App.Business.Services
{
    public interface INguoiDungService : IGenericService<User>
    {
        NguoiDungDTO XacThucDangNhap(string tai_khoan, string mat_khau);
    }
}
