﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class BusinessBankersService : GenericService<BusinessBanker>, IBusinessBankersService
    {
        public BusinessBankersService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<BusinessBankersProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<BusinessBanker> QueryBuilder(IQueryable<BusinessBanker> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

        public List<BusinessBankersDTO> GetBusinessBanker(int bid)
        {
            try
            {
                var res = new List<BusinessBankersDTO>();

                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var bankers = business.BusinessBankers.ToList();
                    if (bankers.Count > 0)
                    {
                        res = _mapper.Map<List<BusinessBanker>, List<BusinessBankersDTO>>(bankers);
                    }
                }

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
