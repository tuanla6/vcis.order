﻿using AutoMapper;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Common.Library;

namespace App.Business.Services
{
    public class BusinessBankersDTO
    {
        public int Id { get; set; }
        public string BankerName { get; set; }
        public int? AddressId { get; set; }
        public AddressDTO AddressObj { get; set; }
        public string Address
        {
            get
            {
                return StringUtilities.GenerateAddress(AddressObj);
            }
        }
    }
    public class BusinessBankersProfile : Profile
    {
        public BusinessBankersProfile()
        {
            CreateMap<BusinessBanker, BusinessBankersDTO>(MemberList.None)
                .ForMember(x => x.BankerName, opt => opt.MapFrom(x => x.Banker.Business.Name))
                .ForMember(x => x.AddressObj, opt => opt.MapFrom(x =>
                    new AddressDTO
                    {
                        Street = x.Address.Street,
                        District = x.Address.District != null ? x.Address.District.DistrictName : string.Empty,
                        Province = x.Address.Province != null ? x.Address.Province.ProvinceName : string.Empty,
                        Country = x.Address.Country != null ? x.Address.Country.CountryName : string.Empty,
                        ProvinceCategory = x.Address.Province != null ? x.Address.Province.Category : string.Empty,
                        DistrictCategory = x.Address.District != null ? x.Address.District.Category : string.Empty,
                        Phone = x.Address.Phone,
                        Fax = x.Address.Fax
                    }))
                ;
        }
    }
}
