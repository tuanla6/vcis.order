﻿using App.Business.Base;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Business.Services
{
    public class ImportExportDTO
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string Category { get; set; }
        public string TypesOfProducts { get; set; }
        public string Market { get; set; }
        public string Ratio { get; set; }
        public string ModeOfPayment { get; set; }
    }

    public class MajorDTO
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string Country { get; set; }
    }

    public class ResualtIE
    {
        public List<ImportExportDTO> ImportExports { get; set; }
        public List<MajorDTO> MajorClients { get; set; }
        public List<MajorDTO> MajorSuppliers { get; set; }
    }

    public class ImportExportsProfile : Profile
    {
        public ImportExportsProfile()
        {
            CreateMap<ImportExport, ImportExportDTO>();
            CreateMap<MajorClient, MajorDTO>()
                .ForMember(x => x.BusinessName, opt => opt.MapFrom(x => x.Business.Name))
                .ForMember(x => x.Country, opt => opt.MapFrom(x => 
                    x.Business.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => z.Country != null ? z.Country.CountryName : string.Empty).FirstOrDefault()));
            CreateMap<MajorSupplier, MajorDTO>()
                .ForMember(x => x.BusinessName, opt => opt.MapFrom(x => x.Business.Name))
                .ForMember(x => x.Country, opt => opt.MapFrom(x =>
                    x.Business.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => z.Country != null ? z.Country.CountryName : string.Empty).FirstOrDefault()));
        }
    }
}



