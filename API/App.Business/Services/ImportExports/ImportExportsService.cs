﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class ImportExportsService : GenericService<ImportExport>, IImportExportsService
    {
        public ImportExportsService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<ImportExportsProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<ImportExport> QueryBuilder(IQueryable<ImportExport> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

        public ResualtIE GetImportExports(int bid)
        {
            try
            {
                var res = new ResualtIE();

                var importexports = new List<ImportExportDTO>();
                var majorClients = new List<MajorDTO>();
                var majorSuppliers = new List<MajorDTO>();

                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var ImportExports = business.ImportExports.ToList();
                    if (ImportExports.Count > 0)
                    {
                        importexports = _mapper.Map<List<ImportExport>, List<ImportExportDTO>>(ImportExports);
                    }

                    var clients = business.majorClient.ToList();
                    if (clients.Count > 0)
                    {
                        majorClients = _mapper.Map<List<MajorClient>, List<MajorDTO>>(clients);
                    }

                    var supplier = business.majorSupplier.ToList();
                    if (supplier.Count > 0)
                    {
                        majorSuppliers = _mapper.Map<List<MajorSupplier>, List<MajorDTO>>(supplier);
                    }
                }

                res.ImportExports = importexports;
                res.MajorClients = majorClients;
                res.MajorSuppliers = majorSuppliers;

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
