﻿using App.Business.Base;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;

namespace App.Business.Services
{
    public interface IImportExportsService : IGenericService<ImportExport>
    {
        ResualtIE GetImportExports(int bid);
    }
}
