﻿using App.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public interface IReportsService
    {
        List<ReportSummaryDTO> GetReportSummary(int bid, int FSYear, int? Quarter, string Type = "1");

        ResualReportDT0 GetSummaryFSOverView(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetBalanceSheet(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetIncomeStatement(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetCashflowDirect(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetCashflowInDirect(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetGrowthRateRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetLiquidityRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetPerformance(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetReturnOnInvestment(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetReturnOnNetSales(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetSolvencyRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetActivityRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetCreditRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetCoverageRatios(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetFinancialAnalysis(int bid, int FSYear, int? Quarter, string Type = "1");
        ResualReportDT0 GetCashConversionCycle(int bid, int FSYear, int? Quarter, string Type = "1");
        List<ResualDADTO> GetDupontAnalysis(DupontAnalysisDTO dto);
        ResualReportDT0 GetDataMargin(int bid, int FSYear, int? Quarter, string Type = "1");
        List<ResualDADTO> GetTop10Companies(DupontAnalysisDTO dto);
    }
}
