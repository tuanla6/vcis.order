﻿using App.Common.Base;
using App.Data.Models.VCIS;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{

    public static class BusinessReportsService
    {
        public static ReportSummaryDTO GetTotalAssets(List<ReportsDTO> datas)
        {
            try
            {
                #region Total Assets = 270
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var TotalAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Total Assets",
                    Year1 = _270.Year1,
                    Year2 = _270.Year2,
                    Year3 = _270.Year3,
                    Year4 = _270.Year4,
                    Year5 = _270.Year5,
                    Year6 = _270.Year6,
                };

                #endregion

                return TotalAssets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetOperatingAssets(List<ReportsDTO> datas)
        {
            try
            {
                #region Operating Assets = 270-(110+120+426+135+428+159+158+444+218+230+240+245+250+268+448+269)
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var subOA = ReportBase.GetCodeValue(datas, "110+120+426+135+428+159+158+444+218+230+240+245+250+268+448+269");
                var OperatingAssets = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Operating Assets",
                    Year1 = _270.Year1 - subOA.Year1,
                    Year2 = _270.Year2 - subOA.Year2,
                    Year3 = _270.Year3 - subOA.Year3,
                    Year4 = _270.Year4 - subOA.Year4,
                    Year5 = _270.Year5 - subOA.Year5,
                    Year6 = _270.Year6 - subOA.Year6
                };

                #endregion

                return OperatingAssets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetOwnerEquity(List<ReportsDTO> datas)
        {
            try
            {
                #region Owner's Equity = 400
                var _400 = ReportBase.GetCodeValue(datas, "400");
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var OwnerEquity = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Owner's Equity",
                    Year1 = _400.Year1,
                    Year2 = _400.Year2,
                    Year3 = _400.Year3,
                    Year4 = _400.Year4,
                    Year5 = _400.Year5,
                    Year6 = _400.Year6,
                    IsBold = true
                };
                OwnerEquity.AddPercent1 = ReportBase.GetDiv(OwnerEquity.Year1, _270.Year1);
                OwnerEquity.AddPercent2 = ReportBase.GetDiv(OwnerEquity.Year2, _270.Year2);
                OwnerEquity.AddPercent3 = ReportBase.GetDiv(OwnerEquity.Year3, _270.Year3);
                #endregion

                return OwnerEquity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetNetSales(List<ReportsDTO> datas)
        {
            try
            {
                #region Net Sales=P10
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var NetSales = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Net Sales",
                    Year1 = P10.Year1,
                    Year2 = P10.Year2,
                    Year3 = P10.Year3,
                    Year4 = P10.Year4,
                    Year5 = P10.Year5,
                    Year6 = P10.Year6,
                    InitIndex = 1
                };
                NetSales.AddPercent1 = ReportBase.GetDiv(NetSales.Year1, NetSales.Year1);
                NetSales.AddPercent2 = ReportBase.GetDiv(NetSales.Year2, NetSales.Year2);
                NetSales.AddPercent3 = ReportBase.GetDiv(NetSales.Year3, NetSales.Year3);
                #endregion

                return NetSales;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetNetIncome(List<ReportsDTO> datas)
        {
            try
            {
                #region Net Income = EBT - (P51+P52) = P20 - (P24+P25) + (P21+P53) - P22 + P40 - (P51+P52) = (P20+P21+P53+P40)-(P24+P25+P22+P51+P52);
                var subNI1 = ReportBase.GetCodeValue(datas, "P20+P21+P53+P40");
                var subNI2 = ReportBase.GetCodeValue(datas, "P24+P25+P22+P51+P52");

                var NetIncome = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Net Income",
                    Year1 = subNI1.Year1 - subNI2.Year1,
                    Year2 = subNI1.Year2 - subNI2.Year2,
                    Year3 = subNI1.Year3 - subNI2.Year3,
                    Year4 = subNI1.Year4 - subNI2.Year4,
                    Year5 = subNI1.Year5 - subNI2.Year5,
                    Year6 = subNI1.Year6 - subNI2.Year6,
                    InitIndex = 2
                };

                #endregion

                return NetIncome;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetOperatingLiabilities(List<ReportsDTO> datas)
        {
            try
            {
                #region Operating Liabilities = 300-(319+311+324+333+334+342+344+321)
                var _300 = ReportBase.GetCodeValue(datas, "300");
                var subOL = ReportBase.GetCodeValue(datas, "319+311+324+333+334+342+344+321");
                var OperatingLiabilities = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Operating Liabilities",
                    Year1 = _300.Year1 - subOL.Year1,
                    Year2 = _300.Year2 - subOL.Year2,
                    Year3 = _300.Year3 - subOL.Year3,
                    Year4 = _300.Year4 - subOL.Year4,
                    Year5 = _300.Year5 - subOL.Year5,
                    Year6 = _300.Year6 - subOL.Year6
                };
                #endregion

                return OperatingLiabilities;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetInterestBearingDebts(List<ReportsDTO> datas)
        {
            try
            {
                #region Interest Bearing Debts=311+334+342+344
                var IBDs = ReportBase.GetCodeValue(datas, "311+334+342+344");
                var InterestBearingDebts = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Interest Bearing Debts",
                    Year1 = IBDs.Year1,
                    Year2 = IBDs.Year2,
                    Year3 = IBDs.Year3,
                    Year4 = IBDs.Year4,
                    Year5 = IBDs.Year5,
                    Year6 = IBDs.Year6
                };
                #endregion

                return InterestBearingDebts;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetTangibleNetWorth(List<ReportsDTO> datas)
        {
            try
            {
                #region Tangible Net Worth = 270-300-448-269-227
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var subTNW = ReportBase.GetCodeValue(datas, "300+448+269+227");
                var TangibleNetWorth = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Tangible Net Worth",
                    Year1 = _270.Year1 - subTNW.Year1,
                    Year2 = _270.Year2 - subTNW.Year2,
                    Year3 = _270.Year3 - subTNW.Year3,
                    Year4 = _270.Year4 - subTNW.Year4,
                    Year5 = _270.Year5 - subTNW.Year5,
                    Year6 = _270.Year6 - subTNW.Year6
                };

                #endregion

                return TangibleNetWorth;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetWorkingCapital(List<ReportsDTO> datas)
        {
            try
            {
                #region Working Capital = 100-310
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var _310 = ReportBase.GetCodeValue(datas, "310");
                var WorkingCapital = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Working Capital",
                    Year1 = _100.Year1 - _310.Year1,
                    Year2 = _100.Year2 - _310.Year2,
                    Year3 = _100.Year3 - _310.Year3,
                    Year4 = _100.Year4 - _310.Year4,
                    Year5 = _100.Year5 - _310.Year5,
                    Year6 = _100.Year6 - _310.Year6
                };

                #endregion

                return WorkingCapital;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetGrossWorkingCapital(List<ReportsDTO> datas)
        {
            try
            {
                #region Gross Working Capital = 100
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var GrossWorkingCapital = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Gross Working Capital",
                    Year1 = _100.Year1,
                    Year2 = _100.Year2,
                    Year3 = _100.Year3,
                    Year4 = _100.Year4,
                    Year5 = _100.Year5,
                    Year6 = _100.Year6
                };

                #endregion

                return GrossWorkingCapital;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetEBITDA(List<ReportsDTO> datas)
        {
            try
            {
                #region EBITDA = P50+P23+ IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0))
                // DK: nam trc(Year2), CK: nam hien tai(Year1)
                var subEBITDA1 = ReportBase.GetCodeValue(datas, "P50+P23");
                var _2 = ReportBase.GetCodeValue(datas, "2");
                var subEBITDA2 = ReportBase.GetCodeValue(datas, "223+226+229+242");

                decimal? subEBITDA_Year1 = _2.Year1 ?? 0;
                decimal? subEBITDA_Year2 = _2.Year2 ?? 0;
                decimal? subEBITDA_Year3 = _2.Year3 ?? 0;
                decimal? subEBITDA_Year4 = _2.Year4 ?? 0;
                decimal? subEBITDA_Year5 = _2.Year5 ?? 0;
                decimal? subEBITDA_Year6 = _2.Year6 ?? 0;

                //IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0))

                subEBITDA_Year1 = subEBITDA_Year1 > 0 ? subEBITDA_Year1 : (subEBITDA2.Year2 - subEBITDA2.Year1) > 0 ? (subEBITDA2.Year2 - subEBITDA2.Year1) : 0;
                subEBITDA_Year2 = subEBITDA_Year2 > 0 ? subEBITDA_Year2 : (subEBITDA2.Year3 - subEBITDA2.Year2) > 0 ? (subEBITDA2.Year3 - subEBITDA2.Year2) : 0;
                subEBITDA_Year3 = subEBITDA_Year3 > 0 ? subEBITDA_Year3 : (subEBITDA2.Year4 - subEBITDA2.Year3) > 0 ? (subEBITDA2.Year4 - subEBITDA2.Year3) : 0;
                subEBITDA_Year4 = subEBITDA_Year4 > 0 ? subEBITDA_Year4 : (subEBITDA2.Year5 - subEBITDA2.Year4) > 0 ? (subEBITDA2.Year5 - subEBITDA2.Year4) : 0;
                subEBITDA_Year5 = subEBITDA_Year5 > 0 ? subEBITDA_Year5 : (subEBITDA2.Year6 - subEBITDA2.Year5) > 0 ? (subEBITDA2.Year6 - subEBITDA2.Year5) : 0;
                subEBITDA_Year6 = subEBITDA_Year6 > 0 ? subEBITDA_Year6 : (0 - subEBITDA2.Year6) > 0 ? (0 - subEBITDA2.Year6) : 0;

                var EBITDA = new ReportSummaryDTO
                {
                    Index = 10,
                    Name = "EBITDA",
                    Year1 = subEBITDA1.Year1 + subEBITDA_Year1,
                    Year2 = subEBITDA1.Year2 + subEBITDA_Year2,
                    Year3 = subEBITDA1.Year3 + subEBITDA_Year3,
                    Year4 = subEBITDA1.Year4 + subEBITDA_Year4,
                    Year5 = subEBITDA1.Year5 + subEBITDA_Year5,
                    Year6 = subEBITDA1.Year6 + subEBITDA_Year6
                };

                #endregion

                return EBITDA;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDebtToEquity(List<ReportsDTO> datas)
        {
            try
            {
                #region Debt to Equity Ratio = ((311+334+342+344)/400)*100
                var sub = ReportBase.GetCodeValue(datas, "311+334+342+344");
                var _400 = ReportBase.GetCodeValue(datas, "400");
                var DebtToEquity = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Debt to Equity Ratio",
                    Year1 = _400.Year1 != 0 ? sub.Year1 / _400.Year1 * 100 : null,
                    Year2 = _400.Year2 != 0 ? sub.Year2 / _400.Year2 * 100 : null,
                    Year3 = _400.Year3 != 0 ? sub.Year3 / _400.Year3 * 100 : null,
                    Year4 = _400.Year4 != 0 ? sub.Year4 / _400.Year4 * 100 : null,
                    Year5 = _400.Year5 != 0 ? sub.Year5 / _400.Year5 * 100 : null,
                    Year6 = _400.Year6 != 0 ? sub.Year6 / _400.Year6 * 100 : null
                };

                #endregion

                return DebtToEquity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDebtToAssets(List<ReportsDTO> datas)
        {
            try
            {
                #region Debt to Assets Ratio = ((311+334+342+344)/270)*100
                var sub = ReportBase.GetCodeValue(datas, "311+334+342+344");
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var DebtToAssets = new ReportSummaryDTO
                {
                    Index = 12,
                    Name = "Debt to Assets Ratio",
                    Year1 = _270.Year1 != 0 ? sub.Year1 / _270.Year1 * 100 : null,
                    Year2 = _270.Year2 != 0 ? sub.Year2 / _270.Year2 * 100 : null,
                    Year3 = _270.Year3 != 0 ? sub.Year3 / _270.Year3 * 100 : null,
                    Year4 = _270.Year4 != 0 ? sub.Year4 / _270.Year4 * 100 : null,
                    Year5 = _270.Year5 != 0 ? sub.Year5 / _270.Year5 * 100 : null,
                    Year6 = _270.Year6 != 0 ? sub.Year6 / _270.Year6 * 100 : null
                };

                #endregion

                return DebtToAssets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDebtCoverage(List<ReportsDTO> datas)
        {
            try
            {
                #region Debt Coverage = (20+C8)/(IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344))
                var sub1 = ReportBase.GetCodeValue(datas, "20+C8");
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var sub2 = ReportBase.GetCodeValue(datas, "311+334+342+344");

                decimal? Year1 = sub2.Year1 ?? 0;
                decimal? Year2 = sub2.Year2 ?? 0;
                decimal? Year3 = sub2.Year3 ?? 0;
                decimal? Year4 = sub2.Year4 ?? 0;
                decimal? Year5 = sub2.Year5 ?? 0;
                decimal? Year6 = sub2.Year6 ?? 0;

                //IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344)
                Year1 = _270.Year2 > 0 ? (Year1 + Year2) / 2 : Year1;
                Year2 = _270.Year3 > 0 ? (Year2 + Year3) / 2 : Year2;
                Year3 = _270.Year4 > 0 ? (Year3 + Year4) / 2 : Year3;
                Year4 = _270.Year5 > 0 ? (Year4 + Year5) / 2 : Year4;
                Year5 = _270.Year6 > 0 ? (Year5 + Year6) / 2 : Year5;


                var DebtCoverage = new ReportSummaryDTO
                {
                    Index = 13,
                    Name = "Debt Coverage",
                    Year1 = Year1 != 0 ? sub1.Year1 / Year1 : null,
                    Year2 = Year2 != 0 ? sub1.Year2 / Year2 : null,
                    Year3 = Year3 != 0 ? sub1.Year3 / Year3 : null,
                    Year4 = Year4 != 0 ? sub1.Year4 / Year4 : null,
                    Year5 = Year5 != 0 ? sub1.Year5 / Year5 : null,
                    Year6 = Year6 != 0 ? sub1.Year6 / Year6 : null
                };

                #endregion

                return DebtCoverage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetCurrentRatio(List<ReportsDTO> datas)
        {
            try
            {
                #region Current Ratio = 100/310
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var _310 = ReportBase.GetCodeValue(datas, "310");

                var CurrentRatio = new ReportSummaryDTO
                {
                    Index = 14,
                    Name = "Current Ratio",
                    Year1 = _310.Year1 != 0 ? _100.Year1 / _310.Year1 : null,
                    Year2 = _310.Year2 != 0 ? _100.Year2 / _310.Year2 : null,
                    Year3 = _310.Year3 != 0 ? _100.Year3 / _310.Year3 : null,
                    Year4 = _310.Year4 != 0 ? _100.Year4 / _310.Year4 : null,
                    Year5 = _310.Year5 != 0 ? _100.Year5 / _310.Year5 : null,
                    Year6 = _310.Year6 != 0 ? _100.Year6 / _310.Year6 : null
                };

                #endregion

                return CurrentRatio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetROA(List<ReportsDTO> datas)
        {
            try
            {
                #region ROA = IF(DK270>0,P60/((CK270+DK270)/2)*100,(P60)/CK270)*100
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var P60 = ReportBase.GetCodeValue(datas, "P60");

                var Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(P60.Year1, (_270.Year1 + _270.Year2) / 2) * 100 : ReportBase.GetDiv(P60.Year1, _270.Year1) * 100;
                var Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(P60.Year2, (_270.Year2 + _270.Year3) / 2) * 100 : ReportBase.GetDiv(P60.Year2, _270.Year2) * 100;
                var Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(P60.Year3, (_270.Year3 + _270.Year4) / 2) * 100 : ReportBase.GetDiv(P60.Year3, _270.Year3) * 100;
                var Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(P60.Year4, (_270.Year4 + _270.Year5) / 2) * 100 : ReportBase.GetDiv(P60.Year4, _270.Year4) * 100;
                var Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(P60.Year5, (_270.Year5 + _270.Year6) / 2) * 100 : ReportBase.GetDiv(P60.Year5, _270.Year5) * 100;
                var Year6 = ReportBase.GetDiv(P60.Year6, _270.Year6) * 100;

                var ROA = new ReportSummaryDTO
                {
                    Index = 15,
                    Name = "ROA",
                    Year1 = Year1,
                    Year2 = Year2,
                    Year3 = Year3,
                    Year4 = Year4,
                    Year5 = Year5,
                    Year6 = Year6,
                    InitIndex = 3
                };

                #endregion

                return ROA;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetROE(List<ReportsDTO> datas)
        {
            try
            {
                #region ROE =IF((CK400+DK400)>0,(P60/IF(DK400'=0,CK400,(CK400+DK400)/2))*100,“-”)
                var _400 = ReportBase.GetCodeValue(datas, "_400");
                var P60 = ReportBase.GetCodeValue(datas, "P60");

                var Year1 = (_400.Year1 + _400.Year2) > 0 ? (_400.Year2 == 0 ? ReportBase.GetDiv(P60.Year1, _400.Year1) * 100 : ReportBase.GetDiv(P60.Year1, (_400.Year1 + _400.Year2) / 2) * 100) : null;
                var Year2 = (_400.Year2 + _400.Year3) > 0 ? (_400.Year3 == 0 ? ReportBase.GetDiv(P60.Year2, _400.Year2) * 100 : ReportBase.GetDiv(P60.Year2, (_400.Year2 + _400.Year3) / 2) * 100) : null;
                var Year3 = (_400.Year3 + _400.Year4) > 0 ? (_400.Year4 == 0 ? ReportBase.GetDiv(P60.Year3, _400.Year3) * 100 : ReportBase.GetDiv(P60.Year3, (_400.Year3 + _400.Year4) / 2) * 100) : null;
                var Year4 = (_400.Year4 + _400.Year5) > 0 ? (_400.Year5 == 0 ? ReportBase.GetDiv(P60.Year4, _400.Year4) * 100 : ReportBase.GetDiv(P60.Year4, (_400.Year4 + _400.Year5) / 2) * 100) : null;
                var Year5 = (_400.Year5 + _400.Year6) > 0 ? (_400.Year6 == 0 ? ReportBase.GetDiv(P60.Year5, _400.Year5) * 100 : ReportBase.GetDiv(P60.Year5, (_400.Year5 + _400.Year6) / 2) * 100) : null;
                var Year6 = (_400.Year6 + _400.Year7) > 0 ? (_400.Year7 == 0 ? ReportBase.GetDiv(P60.Year6, _400.Year6) * 100 : ReportBase.GetDiv(P60.Year6, (_400.Year6 + _400.Year7) / 2) * 100) : null;

                var ROE = new ReportSummaryDTO
                {
                    Index = 16,
                    Name = "ROE",
                    Year1 = Year1,
                    Year2 = Year2,
                    Year3 = Year3,
                    Year4 = Year4,
                    Year5 = Year5,
                    Year6 = Year6,
                    InitIndex = 4
                };

                #endregion

                return ROE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetNetIncomeMargin(List<ReportsDTO> datas)
        {
            try
            {
                #region Net Income Margin (%) = P60/P10*100
                var P60 = ReportBase.GetCodeValue(datas, "P60");
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var NetIncomeMargin = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Net Income Margin (%)",
                    Year1 = P10.Year1 != 0 ? P60.Year1 / P10.Year1 * 100 : null,
                    Year2 = P10.Year2 != 0 ? P60.Year2 / P10.Year2 * 100 : null,
                    Year3 = P10.Year3 != 0 ? P60.Year3 / P10.Year3 * 100 : null,
                    Year4 = P10.Year4 != 0 ? P60.Year4 / P10.Year4 * 100 : null,
                    Year5 = P10.Year5 != 0 ? P60.Year5 / P10.Year5 * 100 : null,
                    Year6 = P10.Year6 != 0 ? P60.Year6 / P10.Year6 * 100 : null,
                    AverageIndustryCode = "D26"
                };

                #endregion

                return NetIncomeMargin;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetEBTMargin(List<ReportsDTO> datas)
        {
            try
            {
                #region EBTMargin (%) = P50/P10*100
                var P50 = ReportBase.GetCodeValue(datas, "P50");
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var EBTMargin = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "EBT Margin (%)",
                    Year1 = P10.Year1 != 0 ? P50.Year1 / P10.Year1 * 100 : null,
                    Year2 = P10.Year2 != 0 ? P50.Year2 / P10.Year2 * 100 : null,
                    Year3 = P10.Year3 != 0 ? P50.Year3 / P10.Year3 * 100 : null,
                    Year4 = P10.Year4 != 0 ? P50.Year4 / P10.Year4 * 100 : null,
                    Year5 = P10.Year5 != 0 ? P50.Year5 / P10.Year5 * 100 : null,
                    Year6 = P10.Year6 != 0 ? P50.Year6 / P10.Year6 * 100 : null,
                    AverageIndustryCode = "D25"
                };

                #endregion

                return EBTMargin;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetEBITMargin(List<ReportsDTO> datas)
        {
            try
            {
                #region EBITMargin (%) = (P50+P23)/P10*100
                var sub = ReportBase.GetCodeValue(datas, "P50+P23");
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var EBITMargin = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "EBIT Margin (%)",
                    Year1 = P10.Year1 != 0 ? sub.Year1 / P10.Year1 * 100 : null,
                    Year2 = P10.Year2 != 0 ? sub.Year2 / P10.Year2 * 100 : null,
                    Year3 = P10.Year3 != 0 ? sub.Year3 / P10.Year3 * 100 : null,
                    Year4 = P10.Year4 != 0 ? sub.Year4 / P10.Year4 * 100 : null,
                    Year5 = P10.Year5 != 0 ? sub.Year5 / P10.Year5 * 100 : null,
                    Year6 = P10.Year6 != 0 ? sub.Year6 / P10.Year6 * 100 : null,
                    AverageIndustryCode = "D24"
                };

                #endregion

                return EBITMargin;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetGrossProfitMargin(List<ReportsDTO> datas)
        {
            try
            {
                #region Gross Profit Margin (%) =  P20/P10*100
                var P20 = ReportBase.GetCodeValue(datas, "P20");
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var GrossProfitMargin = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Gross Profit Margin (%)",
                    Year1 = P10.Year1 != 0 ? (P20.Year1 / P10.Year1) * 100 : null,
                    Year2 = P10.Year2 != 0 ? (P20.Year2 / P10.Year2) * 100 : null,
                    Year3 = P10.Year3 != 0 ? (P20.Year3 / P10.Year3) * 100 : null,
                    Year4 = P10.Year4 != 0 ? (P20.Year4 / P10.Year4) * 100 : null,
                    Year5 = P10.Year5 != 0 ? (P20.Year5 / P10.Year5) * 100 : null,
                    Year6 = P10.Year6 != 0 ? (P20.Year6 / P10.Year6) * 100 : null,
                    AverageIndustryCode = "D21"
                };

                #endregion

                return GrossProfitMargin;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetReceivableAccount(List<ReportsDTO> datas)
        {
            try
            {
                #region ReceivableAccount =  131
                var _131 = ReportBase.GetCodeValue(datas, "131");
                var ReceivableAccount = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Receivable Account",
                    Year1 = _131.Year1,
                    Year2 = _131.Year2,
                    Year3 = _131.Year3,
                    Year4 = _131.Year4,
                    Year5 = _131.Year5,
                    Year6 = _131.Year6
                };

                #endregion

                return ReceivableAccount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetPayableAccount(List<ReportsDTO> datas)
        {
            try
            {
                #region PayableAccount = 312
                var _312 = ReportBase.GetCodeValue(datas, "312");
                var PayableAccount = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Payable Account",
                    Year1 = _312.Year1,
                    Year2 = _312.Year2,
                    Year3 = _312.Year3,
                    Year4 = _312.Year4,
                    Year5 = _312.Year5,
                    Year6 = _312.Year6
                };
                #endregion

                return PayableAccount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDSO(List<ReportsDTO> datas)
        {
            try
            {
                #region DSO =IF(P10>0,365/(P10/IF(AND(CK131>0,DK131>0),(CK131+DK131)/2,CK131)),“-”)
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var _131 = ReportBase.GetCodeValue(datas, "131");
                var DSO = new ReportSummaryDTO
                {
                    Index = 12,
                    Name = "Number of days of payables (DPO)",
                    Year1 = P10.Year1 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year1, (_131.Year1 > 0 && _131.Year2 > 0) ? (_131.Year1 + _131.Year2) / 2 : _131.Year1)) : null,
                    Year2 = P10.Year2 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year2, (_131.Year2 > 0 && _131.Year3 > 0) ? (_131.Year2 + _131.Year3) / 2 : _131.Year2)) : null,
                    Year3 = P10.Year3 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year3, (_131.Year3 > 0 && _131.Year4 > 0) ? (_131.Year3 + _131.Year4) / 2 : _131.Year3)) : null,
                    Year4 = P10.Year4 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year4, (_131.Year4 > 0 && _131.Year5 > 0) ? (_131.Year4 + _131.Year5) / 2 : _131.Year4)) : null,
                    Year5 = P10.Year5 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year5, (_131.Year5 > 0 && _131.Year6 > 0) ? (_131.Year5 + _131.Year6) / 2 : _131.Year5)) : null,
                    Year6 = P10.Year6 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P10.Year6, (_131.Year6 > 0 && _131.Year7 > 0) ? (_131.Year6 + _131.Year7) / 2 : _131.Year6)) : null,
                };
                #endregion

                return DSO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDOH(List<ReportsDTO> datas)
        {
            try
            {
                #region DOH =IF(P11>0,365/(P11/IF(AND(CK140>0,DK140>0),(CK140+DK140)/2,CK140)),“-”)
                var P11 = ReportBase.GetCodeValue(datas, "P11");
                var _140 = ReportBase.GetCodeValue(datas, "140");
                var DOH = new ReportSummaryDTO
                {
                    Index = 14,
                    Name = "Days of inventory on hand (DOH)",
                    Year1 = P11.Year1 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year1, (_140.Year1 > 0 && _140.Year2 > 0) ? (_140.Year1 + _140.Year2) / 2 : _140.Year1)) : null,
                    Year2 = P11.Year2 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year2, (_140.Year2 > 0 && _140.Year3 > 0) ? (_140.Year2 + _140.Year3) / 2 : _140.Year2)) : null,
                    Year3 = P11.Year3 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year3, (_140.Year3 > 0 && _140.Year4 > 0) ? (_140.Year3 + _140.Year4) / 2 : _140.Year3)) : null,
                    Year4 = P11.Year4 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year4, (_140.Year4 > 0 && _140.Year5 > 0) ? (_140.Year4 + _140.Year5) / 2 : _140.Year4)) : null,
                    Year5 = P11.Year5 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year5, (_140.Year5 > 0 && _140.Year6 > 0) ? (_140.Year5 + _140.Year6) / 2 : _140.Year5)) : null,
                    Year6 = P11.Year6 > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year6, (_140.Year6 > 0 && _140.Year7 > 0) ? (_140.Year6 + _140.Year7) / 2 : _140.Year6)) : null,
                };
                #endregion

                return DOH;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDPO(List<ReportsDTO> datas)
        {
            try
            {
                #region DPO =IF(P11+CK140-DK140>0,365/((P11+CK140-DK140)/IF(AND(CK312>0,DK312>0),(CK312+DK312)/2,CK312)),“-”)
                var P11 = ReportBase.GetCodeValue(datas, "P11");
                var _140 = ReportBase.GetCodeValue(datas, "140");
                var _312 = ReportBase.GetCodeValue(datas, "312");
                var DPO = new ReportSummaryDTO
                {
                    Index = 13,
                    Name = "Number of days of payables (DPO)",
                    Year1 = (P11.Year1 + _140.Year1 - _140.Year2) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year1 + _140.Year1 - _140.Year2, (_312.Year1 > 0 && _312.Year2 > 0) ? (_312.Year1 + _312.Year2) / 2 : _312.Year1)) : null,
                    Year2 = (P11.Year2 + _140.Year2 - _140.Year3) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year2 + _140.Year2 - _140.Year3, (_312.Year2 > 0 && _312.Year3 > 0) ? (_312.Year2 + _312.Year3) / 2 : _312.Year2)) : null,
                    Year3 = (P11.Year3 + _140.Year3 - _140.Year4) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year3 + _140.Year3 - _140.Year4, (_312.Year3 > 0 && _312.Year4 > 0) ? (_312.Year3 + _312.Year4) / 2 : _312.Year3)) : null,
                    Year4 = (P11.Year4 + _140.Year4 - _140.Year5) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year4 + _140.Year4 - _140.Year5, (_312.Year4 > 0 && _312.Year5 > 0) ? (_312.Year4 + _312.Year5) / 2 : _312.Year4)) : null,
                    Year5 = (P11.Year5 + _140.Year5 - _140.Year6) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year5 + _140.Year5 - _140.Year6, (_312.Year5 > 0 && _312.Year6 > 0) ? (_312.Year5 + _312.Year6) / 2 : _312.Year5)) : null,
                    Year6 = (P11.Year6 + _140.Year6 - _140.Year7) > 0 ? ReportBase.GetDiv(365, ReportBase.GetDiv(P11.Year6 + _140.Year6 - _140.Year7, (_312.Year6 > 0 && _312.Year7 > 0) ? (_312.Year6 + _312.Year7) / 2 : _312.Year6)) : null,
                };
                #endregion

                return DPO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetCCC(List<ReportsDTO> datas)
        {
            try
            {
                #region CCC =(DOH + DSO – DPO)

                var DOH = GetDOH(datas);
                var DSO = GetDSO(datas);
                var DPO = GetDPO(datas);
                var CCC = new ReportSummaryDTO
                {
                    Index = 14,
                    Name = "Cash Conversion Cycle",
                    Year1 = (DOH.Year1 ?? 0) + (DSO.Year1 ?? 0) - (DPO.Year1 ?? 0),
                    Year2 = (DOH.Year2 ?? 0) + (DSO.Year2 ?? 0) - (DPO.Year2 ?? 0),
                    Year3 = (DOH.Year3 ?? 0) + (DSO.Year3 ?? 0) - (DPO.Year3 ?? 0),
                    Year4 = (DOH.Year4 ?? 0) + (DSO.Year4 ?? 0) - (DPO.Year4 ?? 0),
                    Year5 = (DOH.Year5 ?? 0) + (DSO.Year5 ?? 0) - (DPO.Year5 ?? 0),
                    Year6 = (DOH.Year6 ?? 0) + (DSO.Year6 ?? 0) - (DPO.Year6 ?? 0),
                };
                #endregion
                return CCC;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetCAPEX(List<ReportsDTO> datas)
        {
            try
            {
                #region CAPEX=if(DK222>0,IF((CK222+CK225+CK241-DK222-DK225-DK241)>(ABS(21)+ABS(C9)),(CK222+CK225+CK241-DK222-DK225-DK241),ABS(21)+ABS(C9))),"-")
                var _222 = ReportBase.GetCodeValue(datas, "222");
                var sub = ReportBase.GetCodeValue(datas, "222+225+241");
                var _21 = ReportBase.GetCodeValue(datas, "21");
                var C9 = ReportBase.GetCodeValue(datas, "C9");

                var CAPEX = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "CAPEX (Capital expenditures)",
                    Year1 = _222.Year2 > 0 ? (sub.Year1 - sub.Year2) > Math.Abs(_21.Year1 ?? 0) + Math.Abs(C9.Year1 ?? 0) ? sub.Year1 - sub.Year2 : Math.Abs(_21.Year1 ?? 0) + Math.Abs(C9.Year1 ?? 0) : null,
                    Year2 = _222.Year3 > 0 ? (sub.Year2 - sub.Year3) > Math.Abs(_21.Year2 ?? 0) + Math.Abs(C9.Year2 ?? 0) ? sub.Year2 - sub.Year3 : Math.Abs(_21.Year2 ?? 0) + Math.Abs(C9.Year2 ?? 0) : null,
                    Year3 = _222.Year4 > 0 ? (sub.Year3 - sub.Year4) > Math.Abs(_21.Year3 ?? 0) + Math.Abs(C9.Year3 ?? 0) ? sub.Year3 - sub.Year4 : Math.Abs(_21.Year3 ?? 0) + Math.Abs(C9.Year3 ?? 0) : null,
                    Year4 = _222.Year5 > 0 ? (sub.Year4 - sub.Year5) > Math.Abs(_21.Year4 ?? 0) + Math.Abs(C9.Year4 ?? 0) ? sub.Year4 - sub.Year5 : Math.Abs(_21.Year4 ?? 0) + Math.Abs(C9.Year4 ?? 0) : null,
                    Year5 = _222.Year6 > 0 ? (sub.Year5 - sub.Year6) > Math.Abs(_21.Year5 ?? 0) + Math.Abs(C9.Year5 ?? 0) ? sub.Year5 - sub.Year6 : Math.Abs(_21.Year5 ?? 0) + Math.Abs(C9.Year5 ?? 0) : null,
                    Year6 = _222.Year7 > 0 ? (sub.Year6 - sub.Year7) > Math.Abs(_21.Year6 ?? 0) + Math.Abs(C9.Year6 ?? 0) ? sub.Year6 - sub.Year7 : Math.Abs(_21.Year6 ?? 0) + Math.Abs(C9.Year6 ?? 0) : null
                };
                #endregion

                return CAPEX;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportSummaryDTO GetDepreciationAndAmortization(List<ReportsDTO> datas)
        {
            try
            {
                #region Depreciation and Amortization= IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0))
                var _2 = ReportBase.GetCodeValue(datas, "2");
                var sub = ReportBase.GetCodeValue(datas, "223+226+229+242");

                var DaA = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Depreciation and Amortization",
                    Year1 = _2.Year1 > 0 ? _2.Year1 : (sub.Year2 - sub.Year1) > 0 ? sub.Year2 - sub.Year1 : 0,
                    Year2 = _2.Year2 > 0 ? _2.Year2 : (sub.Year3 - sub.Year2) > 0 ? sub.Year3 - sub.Year2 : 0,
                    Year3 = _2.Year3 > 0 ? _2.Year3 : (sub.Year4 - sub.Year3) > 0 ? sub.Year4 - sub.Year3 : 0,
                    Year4 = _2.Year4 > 0 ? _2.Year4 : (sub.Year5 - sub.Year4) > 0 ? sub.Year5 - sub.Year4 : 0,
                    Year5 = _2.Year5 > 0 ? _2.Year5 : (sub.Year6 - sub.Year5) > 0 ? sub.Year6 - sub.Year5 : 0,
                    Year6 = _2.Year6 > 0 ? _2.Year6 : (sub.Year7 - sub.Year6) > 0 ? sub.Year7 - sub.Year6 : 0
                };
                #endregion

                return DaA;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //////////////////////////////////////////////////////////////////////

        public static List<ReportSummaryDTO> GetBalanceSheet(List<ReportsDTO> datas)
        {
            try
            {
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var res = new List<ReportSummaryDTO>();

                #region CurrentAssets = 100
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Current Assets",
                    Year1 = _100.Year1,
                    Year2 = _100.Year2,
                    Year3 = _100.Year3,
                    Year4 = _100.Year4,
                    Year5 = _100.Year5,
                    Year6 = _100.Year6,
                    IsBold = true
                };
                CurrentAssets.AddPercent1 = ReportBase.GetDiv(CurrentAssets.Year1, _270.Year1);
                CurrentAssets.AddPercent2 = ReportBase.GetDiv(CurrentAssets.Year2, _270.Year2);
                CurrentAssets.AddPercent3 = ReportBase.GetDiv(CurrentAssets.Year3, _270.Year3);
                res.Add(CurrentAssets);
                #endregion

                #region Cash and cash equivalents = 110
                var _110 = ReportBase.GetCodeValue(datas, "110");
                var CAE = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Cash and cash equivalents",
                    Year1 = _110.Year1,
                    Year2 = _110.Year2,
                    Year3 = _110.Year3,
                    Year4 = _110.Year4,
                    Year5 = _110.Year5,
                    Year6 = _110.Year6,
                    IsBold = false
                };
                CAE.AddPercent1 = ReportBase.GetDiv(CAE.Year1, _270.Year1);
                CAE.AddPercent2 = ReportBase.GetDiv(CAE.Year2, _270.Year2);
                CAE.AddPercent3 = ReportBase.GetDiv(CAE.Year3, _270.Year3);
                res.Add(CAE);
                #endregion

                #region Short-term marketable investments = 120
                var _120 = ReportBase.GetCodeValue(datas, "120");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Short-term marketable investments",
                    Year1 = _120.Year1,
                    Year2 = _120.Year2,
                    Year3 = _120.Year3,
                    Year4 = _120.Year4,
                    Year5 = _120.Year5,
                    Year6 = _120.Year6,
                    IsBold = false
                };
                marketableInvestments.AddPercent1 = ReportBase.GetDiv(marketableInvestments.Year1, _270.Year1);
                marketableInvestments.AddPercent2 = ReportBase.GetDiv(marketableInvestments.Year2, _270.Year2);
                marketableInvestments.AddPercent3 = ReportBase.GetDiv(marketableInvestments.Year3, _270.Year3);
                res.Add(marketableInvestments);

                #endregion

                #region Trade debts = 131
                var _131 = ReportBase.GetCodeValue(datas, "131");
                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Trade debts",
                    Year1 = _131.Year1,
                    Year2 = _131.Year2,
                    Year3 = _131.Year3,
                    Year4 = _131.Year4,
                    Year5 = _131.Year5,
                    Year6 = _131.Year6,
                    IsBold = false
                };
                tradeDebtor.AddPercent1 = ReportBase.GetDiv(tradeDebtor.Year1, _270.Year1);
                tradeDebtor.AddPercent2 = ReportBase.GetDiv(tradeDebtor.Year2, _270.Year2);
                tradeDebtor.AddPercent3 = ReportBase.GetDiv(tradeDebtor.Year3, _270.Year3);
                res.Add(tradeDebtor);

                #endregion

                #region Inventories = 140
                var _140 = ReportBase.GetCodeValue(datas, "140");
                var inventories = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Inventories",
                    Year1 = _140.Year1,
                    Year2 = _140.Year2,
                    Year3 = _140.Year3,
                    Year4 = _140.Year4,
                    Year5 = _140.Year5,
                    Year6 = _140.Year6,
                    IsBold = false
                };
                inventories.AddPercent1 = ReportBase.GetDiv(inventories.Year1, _270.Year1);
                inventories.AddPercent2 = ReportBase.GetDiv(inventories.Year2, _270.Year2);
                inventories.AddPercent3 = ReportBase.GetDiv(inventories.Year3, _270.Year3);
                res.Add(inventories);

                #endregion

                #region Other Current Assets =100-110-120-131-140
                var subOCA = ReportBase.GetCodeValue(datas, "110+120+131+140");
                var OCA = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Other Current Assets",
                    Year1 = _100.Year1 - subOCA.Year1,
                    Year2 = _100.Year2 - subOCA.Year2,
                    Year3 = _100.Year3 - subOCA.Year3,
                    Year4 = _100.Year4 - subOCA.Year4,
                    Year5 = _100.Year5 - subOCA.Year5,
                    Year6 = _100.Year6 - subOCA.Year6,
                    IsBold = false
                };
                OCA.AddPercent1 = ReportBase.GetDiv(OCA.Year1, _270.Year1);
                OCA.AddPercent2 = ReportBase.GetDiv(OCA.Year2, _270.Year2);
                OCA.AddPercent3 = ReportBase.GetDiv(OCA.Year3, _270.Year3);
                res.Add(OCA);
                #endregion

                #region Non- Current Assets = 200
                var _200 = ReportBase.GetCodeValue(datas, "200");
                var NCA = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Non- Current Assets",
                    Year1 = _200.Year1,
                    Year2 = _200.Year2,
                    Year3 = _200.Year3,
                    Year4 = _200.Year4,
                    Year5 = _200.Year5,
                    Year6 = _200.Year6,
                    IsBold = true
                };
                NCA.AddPercent1 = ReportBase.GetDiv(NCA.Year1, _270.Year1);
                NCA.AddPercent2 = ReportBase.GetDiv(NCA.Year2, _270.Year2);
                NCA.AddPercent3 = ReportBase.GetDiv(NCA.Year3, _270.Year3);
                res.Add(NCA);
                #endregion

                #region Property, plant, and equipment = 221+224
                var subPPAE = ReportBase.GetCodeValue(datas, "221+224");

                var PPAE = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Property, plant, and equipment",
                    Year1 = subPPAE.Year1,
                    Year2 = subPPAE.Year2,
                    Year3 = subPPAE.Year3,
                    Year4 = subPPAE.Year4,
                    Year5 = subPPAE.Year5,
                    Year6 = subPPAE.Year6,
                    IsBold = false
                };
                PPAE.AddPercent1 = ReportBase.GetDiv(PPAE.Year1, _270.Year1);
                PPAE.AddPercent2 = ReportBase.GetDiv(PPAE.Year2, _270.Year2);
                PPAE.AddPercent3 = ReportBase.GetDiv(PPAE.Year3, _270.Year3);
                res.Add(PPAE);
                #endregion

                #region Investment property = 240
                var _240 = ReportBase.GetCodeValue(datas, "240");
                var IP = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Investment property",
                    Year1 = _240.Year1,
                    Year2 = _240.Year2,
                    Year3 = _240.Year3,
                    Year4 = _240.Year4,
                    Year5 = _240.Year5,
                    Year6 = _240.Year6,
                    IsBold = false
                };
                IP.AddPercent1 = ReportBase.GetDiv(IP.Year1, _270.Year1);
                IP.AddPercent2 = ReportBase.GetDiv(IP.Year2, _270.Year2);
                IP.AddPercent3 = ReportBase.GetDiv(IP.Year3, _270.Year3);
                res.Add(IP);

                #endregion

                #region Long-term investments = 250
                var _250 = ReportBase.GetCodeValue(datas, "250");
                var LTI = new ReportSummaryDTO
                {
                    Index = 10,
                    Name = "Long-term investments",
                    Year1 = _250.Year1,
                    Year2 = _250.Year2,
                    Year3 = _250.Year3,
                    Year4 = _250.Year4,
                    Year5 = _250.Year5,
                    Year6 = _250.Year6,
                    IsBold = false
                };
                LTI.AddPercent1 = ReportBase.GetDiv(LTI.Year1, _270.Year1);
                LTI.AddPercent2 = ReportBase.GetDiv(LTI.Year2, _270.Year2);
                LTI.AddPercent3 = ReportBase.GetDiv(LTI.Year3, _270.Year3);
                res.Add(LTI);

                #endregion

                #region Other Non- Current Assets  = 200-221-224-240-250
                var subONCA = ReportBase.GetCodeValue(datas, "221+224+240+250");

                var ONCA = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Other Non- Current Assets ",
                    Year1 = _200.Year1 - subONCA.Year1,
                    Year2 = _200.Year2 - subONCA.Year2,
                    Year3 = _200.Year3 - subONCA.Year3,
                    Year4 = _200.Year4 - subONCA.Year4,
                    Year5 = _200.Year5 - subONCA.Year5,
                    Year6 = _200.Year6 - subONCA.Year6,
                    IsBold = false
                };
                ONCA.AddPercent1 = ReportBase.GetDiv(ONCA.Year1, _270.Year1);
                ONCA.AddPercent2 = ReportBase.GetDiv(ONCA.Year2, _270.Year2);
                ONCA.AddPercent3 = ReportBase.GetDiv(ONCA.Year3, _270.Year3);
                res.Add(ONCA);

                #endregion

                #region Total Assets  = 270

                var TA = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Total Assets",
                    Year1 = _270.Year1,
                    Year2 = _270.Year2,
                    Year3 = _270.Year3,
                    Year4 = _270.Year4,
                    Year5 = _270.Year5,
                    Year6 = _270.Year6,
                    IsBold = true
                };
                TA.AddPercent1 = 1;
                TA.AddPercent2 = 1;
                TA.AddPercent3 = 1;
                res.Add(TA);

                #endregion

                #region Current liabilities  = 310
                var _310 = ReportBase.GetCodeValue(datas, "310");
                var CL = new ReportSummaryDTO
                {
                    Index = 12,
                    Name = "Current liabilities",
                    Year1 = _310.Year1,
                    Year2 = _310.Year2,
                    Year3 = _310.Year3,
                    Year4 = _310.Year4,
                    Year5 = _310.Year5,
                    Year6 = _310.Year6,
                    IsBold = true
                };
                CL.AddPercent1 = ReportBase.GetDiv(CL.Year1, _270.Year1);
                CL.AddPercent2 = ReportBase.GetDiv(CL.Year2, _270.Year2);
                CL.AddPercent3 = ReportBase.GetDiv(CL.Year3, _270.Year3);
                res.Add(CL);

                #endregion

                #region Trade creditor = 312
                var _312 = ReportBase.GetCodeValue(datas, "312");
                var TC = new ReportSummaryDTO
                {
                    Index = 13,
                    Name = "Trade creditor",
                    Year1 = _312.Year1,
                    Year2 = _312.Year2,
                    Year3 = _312.Year3,
                    Year4 = _312.Year4,
                    Year5 = _312.Year5,
                    Year6 = _312.Year6,
                    IsBold = false
                };
                TC.AddPercent1 = ReportBase.GetDiv(TC.Year1, _270.Year1);
                TC.AddPercent2 = ReportBase.GetDiv(TC.Year2, _270.Year2);
                TC.AddPercent3 = ReportBase.GetDiv(TC.Year3, _270.Year3);
                res.Add(TC);
                #endregion

                #region Short-term loans = 311
                var _311 = ReportBase.GetCodeValue(datas, "311");
                var STL = new ReportSummaryDTO
                {
                    Index = 14,
                    Name = "Short-term loans",
                    Year1 = _311.Year1,
                    Year2 = _311.Year2,
                    Year3 = _311.Year3,
                    Year4 = _311.Year4,
                    Year5 = _311.Year5,
                    Year6 = _311.Year6,
                    IsBold = false
                };
                STL.AddPercent1 = ReportBase.GetDiv(STL.Year1, _270.Year1);
                STL.AddPercent2 = ReportBase.GetDiv(STL.Year2, _270.Year2);
                STL.AddPercent3 = ReportBase.GetDiv(STL.Year3, _270.Year3);
                res.Add(STL);
                #endregion

                #region Other current liabilities = 310-312-311
                var subOCL = ReportBase.GetCodeValue(datas, "312+311");

                var OCL = new ReportSummaryDTO
                {
                    Index = 15,
                    Name = "Other current liabilities",
                    Year1 = _310.Year1 - subOCL.Year1,
                    Year2 = _310.Year2 - subOCL.Year2,
                    Year3 = _310.Year3 - subOCL.Year3,
                    Year4 = _310.Year4 - subOCL.Year4,
                    Year5 = _310.Year5 - subOCL.Year5,
                    Year6 = _310.Year6 - subOCL.Year6,
                    IsBold = false
                };
                OCL.AddPercent1 = ReportBase.GetDiv(OCL.Year1, _270.Year1);
                OCL.AddPercent2 = ReportBase.GetDiv(OCL.Year2, _270.Year2);
                OCL.AddPercent3 = ReportBase.GetDiv(OCL.Year3, _270.Year3);
                res.Add(OCL);
                #endregion

                #region Non-Current liabilities = 330
                var _330 = ReportBase.GetCodeValue(datas, "330");

                var NCL = new ReportSummaryDTO
                {
                    Index = 16,
                    Name = "Non-Current liabilities",
                    Year1 = _330.Year1,
                    Year2 = _330.Year2,
                    Year3 = _330.Year3,
                    Year4 = _330.Year4,
                    Year5 = _330.Year5,
                    Year6 = _330.Year6,
                    IsBold = true
                };
                NCL.AddPercent1 = ReportBase.GetDiv(NCL.Year1, _270.Year1);
                NCL.AddPercent2 = ReportBase.GetDiv(NCL.Year2, _270.Year2);
                NCL.AddPercent3 = ReportBase.GetDiv(NCL.Year3, _270.Year3);
                res.Add(NCL);
                #endregion

                #region Long-term loans  = 334
                var _334 = ReportBase.GetCodeValue(datas, "334");

                var LTL = new ReportSummaryDTO
                {
                    Index = 17,
                    Name = "Long-term loans ",
                    Year1 = _334.Year1,
                    Year2 = _334.Year2,
                    Year3 = _334.Year3,
                    Year4 = _334.Year4,
                    Year5 = _334.Year5,
                    Year6 = _334.Year6,
                    IsBold = false
                };
                LTL.AddPercent1 = ReportBase.GetDiv(LTL.Year1, _270.Year1);
                LTL.AddPercent2 = ReportBase.GetDiv(LTL.Year2, _270.Year2);
                LTL.AddPercent3 = ReportBase.GetDiv(LTL.Year3, _270.Year3);
                res.Add(LTL);
                #endregion

                #region Other non-current liabilities  = 330-334
                var ONCL = new ReportSummaryDTO
                {
                    Index = 18,
                    Name = "Other non-current liabilities",
                    Year1 = _330.Year1 - _334.Year1,
                    Year2 = _330.Year2 - _334.Year2,
                    Year3 = _330.Year3 - _334.Year3,
                    Year4 = _330.Year4 - _334.Year4,
                    Year5 = _330.Year5 - _334.Year5,
                    Year6 = _330.Year6 - _334.Year6,
                    IsBold = false
                };
                ONCL.AddPercent1 = ReportBase.GetDiv(ONCL.Year1, _270.Year1);
                ONCL.AddPercent2 = ReportBase.GetDiv(ONCL.Year2, _270.Year2);
                ONCL.AddPercent3 = ReportBase.GetDiv(ONCL.Year3, _270.Year3);
                res.Add(ONCL);
                #endregion

                #region Total Liabilities  = 300
                var _300 = ReportBase.GetCodeValue(datas, "300");
                var TL = new ReportSummaryDTO
                {
                    Index = 19,
                    Name = "Total Liabilities",
                    Year1 = _300.Year1,
                    Year2 = _300.Year2,
                    Year3 = _300.Year3,
                    Year4 = _300.Year4,
                    Year5 = _300.Year5,
                    Year6 = _300.Year6,
                    IsBold = true
                };
                TL.AddPercent1 = ReportBase.GetDiv(TL.Year1, _270.Year1);
                TL.AddPercent2 = ReportBase.GetDiv(TL.Year2, _270.Year2);
                TL.AddPercent3 = ReportBase.GetDiv(TL.Year3, _270.Year3);
                res.Add(TL);
                #endregion

                #region  Owner's investment capital  = 411
                var _411 = ReportBase.GetCodeValue(datas, "411");
                var OIC = new ReportSummaryDTO
                {
                    Index = 20,
                    Name = "Owner's investment capital",
                    Year1 = _411.Year1,
                    Year2 = _411.Year2,
                    Year3 = _411.Year3,
                    Year4 = _411.Year4,
                    Year5 = _411.Year5,
                    Year6 = _411.Year6,
                    IsBold = false
                };
                OIC.AddPercent1 = ReportBase.GetDiv(OIC.Year1, _270.Year1);
                OIC.AddPercent2 = ReportBase.GetDiv(OIC.Year2, _270.Year2);
                OIC.AddPercent3 = ReportBase.GetDiv(OIC.Year3, _270.Year3);
                res.Add(OIC);
                #endregion

                #region  Retained earnings  = 420
                var _420 = ReportBase.GetCodeValue(datas, "420");
                var RE = new ReportSummaryDTO
                {
                    Index = 21,
                    Name = "Retained earnings",
                    Year1 = _420.Year1,
                    Year2 = _420.Year2,
                    Year3 = _420.Year3,
                    Year4 = _420.Year4,
                    Year5 = _420.Year5,
                    Year6 = _420.Year6,
                    IsBold = false
                };
                RE.AddPercent1 = ReportBase.GetDiv(RE.Year1, _270.Year1);
                RE.AddPercent2 = ReportBase.GetDiv(RE.Year2, _270.Year2);
                RE.AddPercent3 = ReportBase.GetDiv(RE.Year3, _270.Year3);
                res.Add(RE);
                #endregion

                #region  Other owner’s equity  = 410-411-420+430+434
                var subOOE1 = ReportBase.GetCodeValue(datas, "410+430+434");
                var subOOE2 = ReportBase.GetCodeValue(datas, "411+420");
                var OOE = new ReportSummaryDTO
                {
                    Index = 22,
                    Name = "Other owner’s equity",
                    Year1 = subOOE1.Year1 - subOOE2.Year1,
                    Year2 = subOOE1.Year2 - subOOE2.Year2,
                    Year3 = subOOE1.Year3 - subOOE2.Year3,
                    Year4 = subOOE1.Year4 - subOOE2.Year4,
                    Year5 = subOOE1.Year5 - subOOE2.Year5,
                    Year6 = subOOE1.Year6 - subOOE2.Year6,
                    IsBold = false
                };
                OOE.AddPercent1 = ReportBase.GetDiv(OOE.Year1, _270.Year1);
                OOE.AddPercent2 = ReportBase.GetDiv(OOE.Year2, _270.Year2);
                OOE.AddPercent3 = ReportBase.GetDiv(OOE.Year3, _270.Year3);
                res.Add(OOE);
                #endregion

                #region  Owner’s Equity  = 400
                var OE = GetOwnerEquity(datas);
                OE.Index = 23;
                res.Add(OE);
                #endregion

                #region  Total Liabilities & Owner’s Equity  = 440
                var _440 = ReportBase.GetCodeValue(datas, "440");

                var TLOE = new ReportSummaryDTO
                {
                    Index = 24,
                    Name = "Total Liabilities & Owner’s Equity",
                    Year1 = _440.Year1,
                    Year2 = _440.Year2,
                    Year3 = _440.Year3,
                    Year4 = _440.Year4,
                    Year5 = _440.Year5,
                    Year6 = _440.Year6,
                    IsBold = true
                };
                TLOE.AddPercent1 = ReportBase.GetDiv(TLOE.Year1, _270.Year1);
                TLOE.AddPercent2 = ReportBase.GetDiv(TLOE.Year2, _270.Year2);
                TLOE.AddPercent3 = ReportBase.GetDiv(TLOE.Year3, _270.Year3);
                res.Add(TLOE);
                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<ReportSummaryDTO> GetIncomeStatement(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Net Sales
                var netSales = BusinessReportsService.GetNetSales(datas);
                netSales.Index = 1;
                netSales.IsBold = true;
                res.Add(netSales);
                #endregion

                #region Costs of goods sold = p11
                var p10 = ReportBase.GetCodeValue(datas, "P10");
                var p11 = ReportBase.GetCodeValue(datas, "P11");
                var costsofGoodsSold = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Costs of goods sold",
                    Year1 = p11.Year1,
                    Year2 = p11.Year2,
                    Year3 = p11.Year3,
                    Year4 = p11.Year4,
                    Year5 = p11.Year5,
                    Year6 = p11.Year6
                };
                costsofGoodsSold.AddPercent1 = ReportBase.GetDiv(costsofGoodsSold.Year1, p10.Year1);
                costsofGoodsSold.AddPercent2 = ReportBase.GetDiv(costsofGoodsSold.Year2, p10.Year2);
                costsofGoodsSold.AddPercent3 = ReportBase.GetDiv(costsofGoodsSold.Year3, p10.Year3);
                res.Add(costsofGoodsSold);
                #endregion

                #region Gross profit
                var p20 = ReportBase.GetCodeValue(datas, "P20");
                var grossProfit = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Gross profit",
                    Year1 = p20.Year1,
                    Year2 = p20.Year2,
                    Year3 = p20.Year3,
                    Year4 = p20.Year4,
                    Year5 = p20.Year5,
                    Year6 = p20.Year6,
                    IsBold = true
                };
                grossProfit.AddPercent1 = ReportBase.GetDiv(grossProfit.Year1, p10.Year1);
                grossProfit.AddPercent2 = ReportBase.GetDiv(grossProfit.Year2, p10.Year2);
                grossProfit.AddPercent3 = ReportBase.GetDiv(grossProfit.Year3, p10.Year3);
                res.Add(grossProfit);
                #endregion

                #region Sales and G&A Expenses
                var gaExpenses = ReportBase.GetCodeValue(datas, "P24+P25");
                var salesandExpenses = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Sales and G&A Expenses",
                    Year1 = gaExpenses.Year1,
                    Year2 = gaExpenses.Year2,
                    Year3 = gaExpenses.Year3,
                    Year4 = gaExpenses.Year4,
                    Year5 = gaExpenses.Year5,
                    Year6 = gaExpenses.Year6
                };
                salesandExpenses.AddPercent1 = ReportBase.GetDiv(salesandExpenses.Year1, p10.Year1);
                salesandExpenses.AddPercent2 = ReportBase.GetDiv(salesandExpenses.Year2, p10.Year2);
                salesandExpenses.AddPercent3 = ReportBase.GetDiv(salesandExpenses.Year3, p10.Year3);
                res.Add(salesandExpenses);
                #endregion

                #region Operating expenses
                // var gaExpenses = ReportBase.GetCodeValue(datas, "P24+P25");
                var operatingExpenses = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Operating expenses",
                    Year1 = gaExpenses.Year1,
                    Year2 = gaExpenses.Year2,
                    Year3 = gaExpenses.Year3,
                    Year4 = gaExpenses.Year4,
                    Year5 = gaExpenses.Year5,
                    Year6 = gaExpenses.Year6,
                    IsBold = true
                };
                operatingExpenses.AddPercent1 = ReportBase.GetDiv(operatingExpenses.Year1, p10.Year1);
                operatingExpenses.AddPercent2 = ReportBase.GetDiv(operatingExpenses.Year2, p10.Year2);
                operatingExpenses.AddPercent3 = ReportBase.GetDiv(operatingExpenses.Year3, p10.Year3);
                res.Add(operatingExpenses);
                #endregion

                #region Operating Income
                var operatingIncome = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Operating Income",
                    Year1 = p20.Year1 - gaExpenses.Year1,
                    Year2 = p20.Year2 - gaExpenses.Year2,
                    Year3 = p20.Year3 - gaExpenses.Year3,
                    Year4 = p20.Year4 - gaExpenses.Year4,
                    Year5 = p20.Year5 - gaExpenses.Year5,
                    Year6 = p20.Year6 - gaExpenses.Year6,
                    IsBold = true
                };
                operatingIncome.AddPercent1 = ReportBase.GetDiv(operatingIncome.Year1, p10.Year1);
                operatingIncome.AddPercent2 = ReportBase.GetDiv(operatingIncome.Year2, p10.Year2);
                operatingIncome.AddPercent3 = ReportBase.GetDiv(operatingIncome.Year3, p10.Year3);
                res.Add(operatingIncome);
                #endregion

                #region Financial income (expense)
                var financialIncomeData = ReportBase.GetCodeValue(datas, "P21+P53");
                var financialIncome = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Financial income (expense)",
                    Year1 = financialIncomeData.Year1,
                    Year2 = financialIncomeData.Year2,
                    Year3 = financialIncomeData.Year3,
                    Year4 = financialIncomeData.Year4,
                    Year5 = financialIncomeData.Year5,
                    Year6 = financialIncomeData.Year6
                };
                financialIncome.AddPercent1 = ReportBase.GetDiv(financialIncome.Year1, p10.Year1);
                financialIncome.AddPercent2 = ReportBase.GetDiv(financialIncome.Year2, p10.Year2);
                financialIncome.AddPercent3 = ReportBase.GetDiv(financialIncome.Year3, p10.Year3);
                res.Add(financialIncome);
                #endregion

                #region Financial expense   
                var p22 = ReportBase.GetCodeValue(datas, "P22");
                var financialExpense = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Financial expense",
                    Year1 = p22.Year1,
                    Year2 = p22.Year2,
                    Year3 = p22.Year3,
                    Year4 = p22.Year4,
                    Year5 = p22.Year5,
                    Year6 = p22.Year6
                };
                financialExpense.AddPercent1 = ReportBase.GetDiv(financialExpense.Year1, p10.Year1);
                financialExpense.AddPercent2 = ReportBase.GetDiv(financialExpense.Year2, p10.Year2);
                financialExpense.AddPercent3 = ReportBase.GetDiv(financialExpense.Year3, p10.Year3);
                res.Add(financialExpense);
                #endregion

                #region Other income (expense)
                var p40 = ReportBase.GetCodeValue(datas, "P40");
                var otherIncome = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Other income (expense)",
                    Year1 = p40.Year1,
                    Year2 = p40.Year2,
                    Year3 = p40.Year3,
                    Year4 = p40.Year4,
                    Year5 = p40.Year5,
                    Year6 = p40.Year6
                };
                otherIncome.AddPercent1 = ReportBase.GetDiv(otherIncome.Year1, p10.Year1);
                otherIncome.AddPercent2 = ReportBase.GetDiv(otherIncome.Year2, p10.Year2);
                otherIncome.AddPercent3 = ReportBase.GetDiv(otherIncome.Year3, p10.Year3);
                res.Add(otherIncome);
                #endregion

                #region EBT
                var ebt = new ReportSummaryDTO
                {
                    Index = 10,
                    Name = "EBT",
                    Year1 = p20.Year1 - gaExpenses.Year1 + financialIncome.Year1 - p22.Year1 + p40.Year1,
                    Year2 = p20.Year2 - gaExpenses.Year2 + financialIncome.Year2 - p22.Year2 + p40.Year2,
                    Year3 = p20.Year3 - gaExpenses.Year3 + financialIncome.Year3 - p22.Year3 + p40.Year3,
                    Year4 = p20.Year4 - gaExpenses.Year4 + financialIncome.Year4 - p22.Year4 + p40.Year4,
                    Year5 = p20.Year5 - gaExpenses.Year5 + financialIncome.Year5 - p22.Year5 + p40.Year5,
                    Year6 = p20.Year6 - gaExpenses.Year6 + financialIncome.Year6 - p22.Year6 + p40.Year6,
                    IsBold = true
                };
                ebt.AddPercent1 = ReportBase.GetDiv(ebt.Year1, p10.Year1);
                ebt.AddPercent2 = ReportBase.GetDiv(ebt.Year2, p10.Year2);
                ebt.AddPercent3 = ReportBase.GetDiv(ebt.Year3, p10.Year3);
                res.Add(ebt);
                #endregion

                #region Income tax
                var p51 = ReportBase.GetCodeValue(datas, "P51");
                var incomeTax = new ReportSummaryDTO
                {
                    Index = 11,
                    Name = "Income tax",
                    Year1 = p51.Year1,
                    Year2 = p51.Year2,
                    Year3 = p51.Year3,
                    Year4 = p51.Year4,
                    Year5 = p51.Year5,
                    Year6 = p51.Year6
                };
                incomeTax.AddPercent1 = ReportBase.GetDiv(incomeTax.Year1, p10.Year1);
                incomeTax.AddPercent2 = ReportBase.GetDiv(incomeTax.Year2, p10.Year2);
                incomeTax.AddPercent3 = ReportBase.GetDiv(incomeTax.Year3, p10.Year3);
                res.Add(incomeTax);
                #endregion

                #region Deferred income tax
                var p52 = ReportBase.GetCodeValue(datas, "P52");
                var deferredIncomeTax = new ReportSummaryDTO
                {
                    Index = 12,
                    Name = "Deferred income tax",
                    Year1 = p52.Year1,
                    Year2 = p52.Year2,
                    Year3 = p52.Year3,
                    Year4 = p52.Year4,
                    Year5 = p52.Year5,
                    Year6 = p52.Year6
                };
                deferredIncomeTax.AddPercent1 = ReportBase.GetDiv(deferredIncomeTax.Year1, p10.Year1);
                deferredIncomeTax.AddPercent2 = ReportBase.GetDiv(deferredIncomeTax.Year2, p10.Year2);
                deferredIncomeTax.AddPercent3 = ReportBase.GetDiv(deferredIncomeTax.Year3, p10.Year3);
                res.Add(deferredIncomeTax);
                #endregion

                #region Net Income
                var netIncome = GetNetIncome(datas);
                netIncome.Index = 13;
                netIncome.IsBold = true;
                netIncome.AddPercent1 = ReportBase.GetDiv(netIncome.Year1, p10.Year1);
                netIncome.AddPercent2 = ReportBase.GetDiv(netIncome.Year2, p10.Year2);
                netIncome.AddPercent3 = ReportBase.GetDiv(netIncome.Year3, p10.Year3);
                res.Add(netIncome);
                #endregion

                #region Loan interest expenses
                var p23 = ReportBase.GetCodeValue(datas, "P23");
                var loanInteresExpenses = new ReportSummaryDTO
                {
                    Index = 14,
                    Name = "Loan interest expenses",
                    Year1 = p23.Year1,
                    Year2 = p23.Year2,
                    Year3 = p23.Year3,
                    Year4 = p23.Year4,
                    Year5 = p23.Year5,
                    Year6 = p23.Year6
                };
                loanInteresExpenses.AddPercent1 = ReportBase.GetDiv(loanInteresExpenses.Year1, p10.Year1);
                loanInteresExpenses.AddPercent2 = ReportBase.GetDiv(loanInteresExpenses.Year2, p10.Year2);
                loanInteresExpenses.AddPercent3 = ReportBase.GetDiv(loanInteresExpenses.Year3, p10.Year3);
                res.Add(loanInteresExpenses);
                #endregion

                #region EBIT
                var editData = ReportBase.GetCodeValue(datas, "P50+P23");
                var edit = new ReportSummaryDTO
                {
                    Index = 15,
                    Name = "EBIT",
                    Year1 = editData.Year1,
                    Year2 = editData.Year2,
                    Year3 = editData.Year3,
                    Year4 = editData.Year4,
                    Year5 = editData.Year5,
                    Year6 = editData.Year6,
                    IsBold = true
                };
                edit.AddPercent1 = ReportBase.GetDiv(edit.Year1, p10.Year1);
                edit.AddPercent2 = ReportBase.GetDiv(edit.Year2, p10.Year2);
                edit.AddPercent3 = ReportBase.GetDiv(edit.Year3, p10.Year3);
                res.Add(edit);
                #endregion

                #region Depreciation and Amortization

                var depreciationAndAmortization = GetDepreciationAndAmortization(datas);
                depreciationAndAmortization.Index = 16;

                depreciationAndAmortization.AddPercent1 = ReportBase.GetDiv(depreciationAndAmortization.Year1, p10.Year1);
                depreciationAndAmortization.AddPercent2 = ReportBase.GetDiv(depreciationAndAmortization.Year2, p10.Year2);
                depreciationAndAmortization.AddPercent3 = ReportBase.GetDiv(depreciationAndAmortization.Year3, p10.Year3);
                res.Add(depreciationAndAmortization);
                #endregion

                #region EBITDA  = P50+P23 + Depreciation and Amortization
                var ebitDa1 = ReportBase.GetCodeValue(datas, "P50+P23").Year1 + (depreciationAndAmortization.Year1 ?? 0);
                var ebitDa2 = ReportBase.GetCodeValue(datas, "P50+P23").Year2 + (depreciationAndAmortization.Year2 ?? 0);
                var ebitDa3 = ReportBase.GetCodeValue(datas, "P50+P23").Year3 + (depreciationAndAmortization.Year3 ?? 0);
                var ebitDa4 = ReportBase.GetCodeValue(datas, "P50+P23").Year4 + (depreciationAndAmortization.Year4 ?? 0);
                var ebitDa5 = ReportBase.GetCodeValue(datas, "P50+P23").Year5 + (depreciationAndAmortization.Year5 ?? 0);
                var ebitda = new ReportSummaryDTO
                {
                    Index = 15,
                    Name = "EBITDA",
                    Year1 = ebitDa1,
                    Year2 = ebitDa2,
                    Year3 = ebitDa3,
                    Year4 = ebitDa4,
                    Year5 = ebitDa5,
                    IsBold = true
                };
                ebitda.AddPercent1 = ReportBase.GetDiv(ebitda.Year1, p10.Year1);
                ebitda.AddPercent2 = ReportBase.GetDiv(ebitda.Year2, p10.Year2);
                ebitda.AddPercent3 = ReportBase.GetDiv(ebitda.Year3, p10.Year3);
                res.Add(ebitda);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ReportSummaryDTO> GetGrowthRateRatios(List<ReportsDTO> datas)
        {
            try
            {
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var res = new List<ReportSummaryDTO>();

                #region Total Assets Growth Rate(%) = =IF(DK270>0,((CK270-DK270)/DK270))*100,"-")
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Total Assets Growth Rate(%)",
                    Year1 = _270.Year2 > 0 ? (_270.Year1 - _270.Year2) / _270.Year2 * 100 : null,
                    Year2 = _270.Year3 > 0 ? (_270.Year2 - _270.Year3) / _270.Year3 * 100 : null,
                    Year3 = _270.Year4 > 0 ? (_270.Year3 - _270.Year4) / _270.Year4 * 100 : null,
                    Year4 = _270.Year5 > 0 ? (_270.Year4 - _270.Year5) / _270.Year5 * 100 : null,
                    Year5 = _270.Year6 > 0 ? (_270.Year5 - _270.Year6) / _270.Year6 * 100 : null,
                    Year6 = _270.Year7 > 0 ? (_270.Year6 - _270.Year7) / _270.Year7 * 100 : null,
                };
                res.Add(CurrentAssets);
                #endregion

                #region Owners Equity Growth Rate (%) = 'IF(DK270<>0,((CK400-DK400)/(ABS(DK400))*100,"-")
                var _400 = ReportBase.GetCodeValue(datas, "400");
                var CAE = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Owners Equity Growth Rate (%)",
                    Year1 = (_270.Year2 != 0 && _400.Year2 != 0) ? (_400.Year1 - _400.Year2) / Math.Abs(_400.Year2.Value) * 100 : null,
                    Year2 = (_270.Year3 != 0 && _400.Year3 != 0) ? (_400.Year2 - _400.Year3) / Math.Abs(_400.Year3.Value) * 100 : null,
                    Year3 = (_270.Year4 != 0 && _400.Year4 != 0) ? (_400.Year3 - _400.Year4) / Math.Abs(_400.Year4.Value) * 100 : null,
                    Year4 = (_270.Year5 != 0 && _400.Year5 != 0) ? (_400.Year4 - _400.Year5) / Math.Abs(_400.Year5.Value) * 100 : null,
                    Year5 = (_270.Year6 != 0 && _400.Year6 != 0) ? (_400.Year5 - _400.Year6) / Math.Abs(_400.Year6.Value) * 100 : null,
                    Year6 = (_270.Year7 != 0 && _400.Year7 != 0) ? (_400.Year6 - _400.Year7) / Math.Abs(_400.Year7.Value) * 100 : null,
                };
                res.Add(CAE);
                #endregion

                #region Net Sale Growth Rate (%) = IF(DK10<>0,((CK10-DK10)/(DK10)*100,"-")
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Net Sale Growth Rate (%)",
                    Year1 = P10.Year2 != 0 ? (P10.Year1 - P10.Year2) / P10.Year2 * 100 : null,
                    Year2 = P10.Year3 != 0 ? (P10.Year2 - P10.Year3) / P10.Year3 * 100 : null,
                    Year3 = P10.Year4 != 0 ? (P10.Year3 - P10.Year4) / P10.Year4 * 100 : null,
                    Year4 = P10.Year5 != 0 ? (P10.Year4 - P10.Year5) / P10.Year5 * 100 : null,
                    Year5 = P10.Year6 != 0 ? (P10.Year5 - P10.Year6) / P10.Year6 * 100 : null,
                    Year6 = P10.Year7 != 0 ? (P10.Year6 - P10.Year7) / P10.Year7 * 100 : null,
                };
                res.Add(marketableInvestments);

                #endregion

                #region Gross Profit Growth Rate (%) = IF(DK20<>0,((CK20-DK20)/(ABS(DK20))*100,"-")
                var P20 = ReportBase.GetCodeValue(datas, "P20");
                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Gross Profit Growth Rate (%)",
                    Year1 = P20.Year2 != 0 ? (P20.Year1 - P20.Year2) / Math.Abs(P20.Year2.Value) * 100 : null,
                    Year2 = P20.Year3 != 0 ? (P20.Year2 - P20.Year3) / Math.Abs(P20.Year3.Value) * 100 : null,
                    Year3 = P20.Year4 != 0 ? (P20.Year3 - P20.Year4) / Math.Abs(P20.Year4.Value) * 100 : null,
                    Year4 = P20.Year5 != 0 ? (P20.Year4 - P20.Year5) / Math.Abs(P20.Year5.Value) * 100 : null,
                    Year5 = P20.Year6 != 0 ? (P20.Year5 - P20.Year6) / Math.Abs(P20.Year6.Value) * 100 : null,
                    Year6 = P20.Year7 != 0 ? (P20.Year6 - P20.Year7) / Math.Abs(P20.Year7.Value) * 100 : null,
                };
                res.Add(tradeDebtor);

                #endregion

                #region Operating Income Growth Rate (%) = IF(DK30<>0,(CK(20-24-25) - DK(20-24-25))/ (ABS(DK(20-24-25))*100,"-")
                var P30 = ReportBase.GetCodeValue(datas, "P30");
                var P24 = ReportBase.GetCodeValue(datas, "P24");
                var P25 = ReportBase.GetCodeValue(datas, "P25");

                var growthRate1 = P20.Year1 - P24.Year1 - P25.Year1;
                var growthRate2 = P20.Year2 - P24.Year2 - P25.Year2;
                var growthRate3 = P20.Year3 - P24.Year3 - P25.Year3;
                var growthRate4 = P20.Year4 - P24.Year4 - P25.Year4;
                var growthRate5 = P20.Year5 - P24.Year5 - P25.Year5;
                var growthRate6 = P20.Year6 - P24.Year6 - P25.Year6;
                decimal? growthRate7 = 0;

                var inventories = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Operating Income Growth Rate (%)",
                    Year1 = (P30.Year2 != 0 && growthRate2 != 0) ? (growthRate1 - growthRate2) / Math.Abs(growthRate2.Value) * 100 : null,
                    Year2 = (P30.Year3 != 0 && growthRate3 != 0) ? (growthRate2 - growthRate3) / Math.Abs(growthRate3.Value) * 100 : null,
                    Year3 = (P30.Year4 != 0 && growthRate4 != 0) ? (growthRate3 - growthRate4) / Math.Abs(growthRate4.Value) * 100 : null,
                    Year4 = (P30.Year5 != 0 && growthRate5 != 0) ? (growthRate4 - growthRate5) / Math.Abs(growthRate5.Value) * 100 : null,
                    Year5 = (P30.Year6 != 0 && growthRate6 != 0) ? (growthRate5 - growthRate6) / Math.Abs(growthRate6.Value) * 100 : null,
                    Year6 = (P30.Year7 != 0 && growthRate7 != 0) ? (growthRate6 - growthRate7) / Math.Abs(growthRate7.Value) * 100 : null,
                };

                res.Add(inventories);

                #endregion

                #region EBITDA Growth Rate (%) =IF(DK(C52)<>0,(CK(C52)-DK(C52))/DK(C52)*100
                var _2 = ReportBase.GetCodeValue(datas, "2");
                var subebit = ReportBase.GetCodeValue(datas, "223+226+229+242");
                var P23P50 = ReportBase.GetCodeValue(datas, "P23+P50");

                var ebitDa1 = _2.Year1 > 0 ? P23P50.Year1 + _2.Year1 : (subebit.Year2 - subebit.Year1) > 0 ? P23P50.Year1 + subebit.Year2 - subebit.Year1 : P23P50.Year1;
                var ebitDa2 = _2.Year2 > 0 ? P23P50.Year2 + _2.Year2 : (subebit.Year3 - subebit.Year2) > 0 ? P23P50.Year2 + subebit.Year3 - subebit.Year2 : P23P50.Year2;
                var ebitDa3 = _2.Year3 > 0 ? P23P50.Year3 + _2.Year3 : (subebit.Year4 - subebit.Year3) > 0 ? P23P50.Year3 + subebit.Year4 - subebit.Year3 : P23P50.Year3;
                var ebitDa4 = _2.Year4 > 0 ? P23P50.Year4 + _2.Year4 : (subebit.Year5 - subebit.Year4) > 0 ? P23P50.Year4 + subebit.Year5 - subebit.Year4 : P23P50.Year4;
                var ebitDa5 = _2.Year5 > 0 ? P23P50.Year5 + _2.Year5 : (subebit.Year6 - subebit.Year5) > 0 ? P23P50.Year5 + subebit.Year6 - subebit.Year5 : P23P50.Year5;
                var ebitDa6 = _2.Year6 > 0 ? P23P50.Year6 + _2.Year6 : (subebit.Year7 - subebit.Year6) > 0 ? P23P50.Year6 + subebit.Year7 - subebit.Year6 : P23P50.Year6;
                var ebitDa7 = 0;

                var OCA = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "EBITDA Growth Rate (%)",
                    Year1 = ebitDa2 != 0 ? (ebitDa1 - ebitDa2) / ebitDa2 * 100 : null,
                    Year2 = ebitDa3 != 0 ? (ebitDa2 - ebitDa3) / ebitDa3 * 100 : null,
                    Year3 = ebitDa4 != 0 ? (ebitDa3 - ebitDa4) / ebitDa4 * 100 : null,
                    Year4 = ebitDa5 != 0 ? (ebitDa4 - ebitDa5) / ebitDa5 * 100 : null,
                    Year5 = ebitDa6 != 0 ? (ebitDa5 - ebitDa6) / ebitDa6 * 100 : null,
                    Year6 = ebitDa7 != 0 ? (ebitDa6 - ebitDa7) / ebitDa7 * 100 : null,
                };

                res.Add(OCA);
                #endregion

                #region EBIT Growth Rate (%) = IF(DK P30<>0, ((CK(P50+P23) - DK (P50+P23))/(ABS(DK(P50+P23)))*100,"-")
                var NCA = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "EBIT Growth Rate (%)",
                    Year1 = (P30.Year2 != 0 && P23P50.Year2 != 0) ? (P23P50.Year1 - P23P50.Year2) / Math.Abs(P23P50.Year2.Value) * 100 : null,
                    Year2 = (P30.Year3 != 0 && P23P50.Year3 != 0) ? (P23P50.Year2 - P23P50.Year3) / Math.Abs(P23P50.Year3.Value) * 100 : null,
                    Year3 = (P30.Year4 != 0 && P23P50.Year4 != 0) ? (P23P50.Year3 - P23P50.Year4) / Math.Abs(P23P50.Year4.Value) * 100 : null,
                    Year4 = (P30.Year5 != 0 && P23P50.Year5 != 0) ? (P23P50.Year4 - P23P50.Year5) / Math.Abs(P23P50.Year5.Value) * 100 : null,
                    Year5 = (P30.Year6 != 0 && P23P50.Year6 != 0) ? (P23P50.Year5 - P23P50.Year6) / Math.Abs(P23P50.Year6.Value) * 100 : null,
                    Year6 = (P30.Year7 != 0 && P23P50.Year7 != 0) ? (P23P50.Year6 - P23P50.Year7) / Math.Abs(P23P50.Year7.Value) * 100 : null,
                };
                res.Add(NCA);
                #endregion

                #region EBT. Growth Rate (%)= IF(DK P50<>0, ((CK P50-DK P50)/(ABS(DK P50))*100,"-")
                var P50 = ReportBase.GetCodeValue(datas, "P50");

                var PPAE = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "EBT Growth Rate (%)",
                    Year1 = P50.Year2 != 0 ? (P50.Year1 - P50.Year2) / Math.Abs(P50.Year2.Value) * 100 : null,
                    Year2 = P50.Year3 != 0 ? (P50.Year2 - P50.Year3) / Math.Abs(P50.Year3.Value) * 100 : null,
                    Year3 = P50.Year4 != 0 ? (P50.Year3 - P50.Year4) / Math.Abs(P50.Year4.Value) * 100 : null,
                    Year4 = P50.Year5 != 0 ? (P50.Year4 - P50.Year5) / Math.Abs(P50.Year5.Value) * 100 : null,
                    Year5 = P50.Year6 != 0 ? (P50.Year5 - P50.Year6) / Math.Abs(P50.Year6.Value) * 100 : null,
                    Year6 = P50.Year7 != 0 ? (P50.Year6 - P50.Year7) / Math.Abs(P50.Year7.Value) * 100 : null,
                };
                res.Add(PPAE);
                #endregion

                #region Net Income Growth Rate (%) = IF(DK P60<>0,((CKP60-DKP60)/(ABS(DKP60))*100,"-")
                var P60 = ReportBase.GetCodeValue(datas, "P60");
                var IP = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Net Income Growth Rate (%)",
                    Year1 = P60.Year2 != 0 ? (P60.Year1 - P60.Year2) / Math.Abs(P60.Year2.Value) * 100 : null,
                    Year2 = P60.Year3 != 0 ? (P60.Year2 - P60.Year3) / Math.Abs(P60.Year3.Value) * 100 : null,
                    Year3 = P60.Year4 != 0 ? (P60.Year3 - P60.Year4) / Math.Abs(P60.Year4.Value) * 100 : null,
                    Year4 = P60.Year5 != 0 ? (P60.Year4 - P60.Year5) / Math.Abs(P60.Year5.Value) * 100 : null,
                    Year5 = P60.Year6 != 0 ? (P60.Year5 - P60.Year6) / Math.Abs(P60.Year6.Value) * 100 : null,
                    Year6 = P60.Year7 != 0 ? (P60.Year6 - P60.Year7) / Math.Abs(P60.Year7.Value) * 100 : null,
                };

                res.Add(IP);

                #endregion


                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetActivityRatios(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Inventory turnover
                // =P11/IF(AND(CK140>0,DK140>0),(CK140+DK140)/2,CK140)
                var a140 = ReportBase.GetCodeValue(datas, "140");
                var p11 = ReportBase.GetCodeValue(datas, "P11");
                var it_mauso1 = (a140.Year2 > 0 && a140.Year1 > 0) ? (a140.Year1 + a140.Year2) / 2 : a140.Year1;
                var it_mauso2 = (a140.Year3 > 0 && a140.Year2 > 0) ? (a140.Year2 + a140.Year3) / 2 : a140.Year2;
                var it_mauso3 = (a140.Year4 > 0 && a140.Year3 > 0) ? (a140.Year3 + a140.Year4) / 2 : a140.Year3;
                var it_mauso4 = (a140.Year5 > 0 && a140.Year4 > 0) ? (a140.Year4 + a140.Year5) / 2 : a140.Year4;
                var it_mauso5 = (a140.Year6 > 0 && a140.Year5 > 0) ? (a140.Year5 + a140.Year6) / 2 : a140.Year5;

                var inventoryTurnover1 = it_mauso1 != 0 ? p11.Year1 / it_mauso1 : (decimal?)null;
                var inventoryTurnover2 = it_mauso2 != 0 ? p11.Year2 / it_mauso2 : (decimal?)null;
                var inventoryTurnover3 = it_mauso3 != 0 ? p11.Year3 / it_mauso3 : (decimal?)null;
                var inventoryTurnover4 = it_mauso4 != 0 ? p11.Year4 / it_mauso4 : (decimal?)null;
                var inventoryTurnover5 = it_mauso5 != 0 ? p11.Year5 / it_mauso5 : (decimal?)null;

                var inventoryTurnover = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Inventory turnover",
                    Year1 = inventoryTurnover1,
                    Year2 = inventoryTurnover2,
                    Year3 = inventoryTurnover3,
                    Year4 = inventoryTurnover4,
                    Year5 = inventoryTurnover5,
                    AverageIndustryCode = "D1",
                    IndustryAverageGroupId = 2,
                    Comment = "(Cost of goods sold/Average inventory)"
                };
                res.Add(inventoryTurnover);
                #endregion

                #region Receivables from customer turnover
                // =P10/IF(AND(CK131>0,DK131>0),(CK131+DK131)/2,CK131)
                var a131 = ReportBase.GetCodeValue(datas, "131");
                var p10 = ReportBase.GetCodeValue(datas, "P10");
                var rc_mauso1 = (a131.Year2 > 0 && a131.Year1 > 0) ? (a131.Year1 + a131.Year1) / 2 : a131.Year1;
                var rc_mauso2 = (a131.Year3 > 0 && a131.Year2 > 0) ? (a131.Year2 + a131.Year2) / 2 : a131.Year2;
                var rc_mauso3 = (a131.Year4 > 0 && a131.Year3 > 0) ? (a131.Year3 + a131.Year3) / 2 : a131.Year3;
                var rc_mauso4 = (a131.Year5 > 0 && a131.Year4 > 0) ? (a131.Year4 + a131.Year4) / 2 : a131.Year4;
                var rc_mauso5 = (a131.Year6 > 0 && a131.Year5 > 0) ? (a131.Year5 + a131.Year5) / 2 : a131.Year5;


                var customerTurnover1 = rc_mauso1 != 0 ? p10.Year1 / rc_mauso1 : (decimal?)null;
                var customerTurnover2 = rc_mauso2 != 0 ? p10.Year2 / rc_mauso2 : (decimal?)null;
                var customerTurnover3 = rc_mauso3 != 0 ? p10.Year3 / rc_mauso3 : (decimal?)null;
                var customerTurnover4 = rc_mauso4 != 0 ? p10.Year4 / rc_mauso4 : (decimal?)null;
                var customerTurnover5 = rc_mauso5 != 0 ? p10.Year5 / rc_mauso5 : (decimal?)null;

                var customerTurnover = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Receivables from customer turnover",
                    Year1 = customerTurnover1,
                    Year2 = customerTurnover2,
                    Year3 = customerTurnover3,
                    Year4 = customerTurnover4,
                    Year5 = customerTurnover5,
                    AverageIndustryCode = "D2",
                    IndustryAverageGroupId = 2,
                    Comment = "(Net Sales/Average receivables from customers)"
                };
                res.Add(customerTurnover);
                #endregion

                #region Payables to supplier turnover
                // '=(P11+CK140-DK140)/IF(AND(CK312>0,DK312>0),(CK312+DK312)/2,CK312)
                decimal? supplierTurnover1 = null;
                decimal? supplierTurnover2 = null;
                decimal? supplierTurnover3 = null;
                decimal? supplierTurnover4 = null;
                decimal? supplierTurnover5 = null;

                var condi1 = p11.Year1 + a140.Year1 - a140.Year2;
                var condi2 = p11.Year2 + a140.Year2 - a140.Year3;
                var condi3 = p11.Year3 + a140.Year3 - a140.Year4;
                var condi4 = p11.Year4 + a140.Year4 - a140.Year5;
                var condi5 = p11.Year5 + a140.Year5 - a140.Year6;
                var a312 = ReportBase.GetCodeValue(datas, "312");

                if (a312.Year1 > 0 && a312.Year2 > 0)
                    supplierTurnover1 = condi1 / ((a312.Year1 + a312.Year2) / 2);
                else
                    supplierTurnover1 = a312.Year1 != 0 ? condi1 / a312.Year1 : (decimal?)null;

                if (a312.Year2 > 0 && a312.Year3 > 0)
                    supplierTurnover2 = condi2 / ((a312.Year2 + a312.Year3) / 2);
                else
                    supplierTurnover2 = a312.Year2 != 0 ? condi2 / a312.Year2 : (decimal?)null;

                if (a312.Year3 > 0 && a312.Year4 > 0)
                    supplierTurnover3 = condi3 / ((a312.Year3 + a312.Year4) / 2);
                else
                    supplierTurnover3 = a312.Year3 != 0 ? condi3 / a312.Year3 : (decimal?)null;

                if (a312.Year4 > 0 && a312.Year5 > 0)
                    supplierTurnover4 = condi4 / ((a312.Year4 + a312.Year5) / 2);
                else
                    supplierTurnover4 = a312.Year4 != 0 ? condi4 / a312.Year4 : (decimal?)null;

                if (a312.Year5 > 0 && a312.Year6 > 0)
                    supplierTurnover5 = condi5 / ((a312.Year5 + a312.Year6) / 2);
                else
                    supplierTurnover5 = a312.Year5 != 0 ? condi5 / a312.Year5 : (decimal?)null;

                var supplierTurnover = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Payables to supplier turnover",
                    Year1 = supplierTurnover1,
                    Year2 = supplierTurnover2,
                    Year3 = supplierTurnover3,
                    Year4 = supplierTurnover4,
                    Year5 = supplierTurnover5,
                    AverageIndustryCode = "D3",
                    IndustryAverageGroupId = 2,
                    Comment = "(Purchases/Average payables to suppliers)"
                };
                res.Add(supplierTurnover);
                #endregion

                #region Working capital turnover
                // =P10/IF(AND(CK100>0,DK100>0),(CK100-CK310+DK100DK-DK310)/2,CK100-CK310)
                decimal? capitalTurnover1 = null;
                decimal? capitalTurnover2 = null;
                decimal? capitalTurnover3 = null;
                decimal? capitalTurnover4 = null;
                decimal? capitalTurnover5 = null;

                var a100 = ReportBase.GetCodeValue(datas, "100");
                var a310 = ReportBase.GetCodeValue(datas, "310");

                var a1 = (a100.Year1 - a310.Year1 + a100.Year2 - a310.Year2) / 2;
                var a2 = (a100.Year2 - a310.Year2 + a100.Year3 - a310.Year3) / 2;
                var a3 = (a100.Year3 - a310.Year3 + a100.Year4 - a310.Year4) / 2;
                var a4 = (a100.Year4 - a310.Year4 + a100.Year5 - a310.Year5) / 2;
                var a5 = (a100.Year5 - a310.Year5 + a100.Year6 - a310.Year6) / 2;

                //CK100-CK310
                var a1_1 = a100.Year1 - a310.Year1;
                var a2_1 = a100.Year2 - a310.Year2;
                var a3_1 = a100.Year3 - a310.Year3;
                var a4_1 = a100.Year4 - a310.Year4;
                var a5_1 = a100.Year5 - a310.Year5;

                //AND(CK100>0,DK100>0)
                if (a100.Year1 > 0 && a100.Year2 > 0)
                    capitalTurnover1 = a1 != 0 ? p10.Year1 / a1 : (decimal?)null;
                else
                    capitalTurnover1 = a1_1 != 0 ? p10.Year1 / a1_1 : (decimal?)null;

                if (a100.Year2 > 0 && a100.Year3 > 0)
                    capitalTurnover2 = a2 != 0 ? p10.Year2 / a2 : (decimal?)null;
                else
                    capitalTurnover2 = a2_1 != 0 ? p10.Year2 / a2_1 : (decimal?)null;

                if (a100.Year3 > 0 && a100.Year4 > 0)
                    capitalTurnover3 = a3 != 0 ? p10.Year3 / a3 : (decimal?)null;
                else
                    capitalTurnover3 = a3_1 != 0 ? p10.Year3 / a3_1 : (decimal?)null;

                if (a100.Year4 > 0 && a100.Year5 > 0)
                    capitalTurnover4 = a4 != 0 ? p10.Year4 / a4 : (decimal?)null;
                else
                    capitalTurnover4 = a4_1 != 0 ? p10.Year4 / a4_1 : (decimal?)null;

                if (a100.Year5 > 0 && a100.Year6 > 0)
                    capitalTurnover5 = a5 != 0 ? p10.Year5 / a5 : (decimal?)null;
                else
                    capitalTurnover5 = a5_1 != 0 ? p10.Year5 / a5_1 : (decimal?)null;

                var capitalTurnover = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Working capital turnover",
                    Year1 = capitalTurnover1,
                    Year2 = capitalTurnover2,
                    Year3 = capitalTurnover3,
                    Year4 = capitalTurnover4,
                    Year5 = capitalTurnover5,
                    AverageIndustryCode = "D4",
                    IndustryAverageGroupId = 2,
                    Comment = "(Net Sales/Average working capital)"
                };
                res.Add(capitalTurnover);
                #endregion

                #region Fixed asset turnover
                // '=P10/IF(AND(CK220>0,DK220>0),(CK220+DK220)/2,CK220)
                decimal? assetTurnover1 = null;
                decimal? assetTurnover2 = null;
                decimal? assetTurnover3 = null;
                decimal? assetTurnover4 = null;
                decimal? assetTurnover5 = null;

                var a220 = ReportBase.GetCodeValue(datas, "220");
                if (a220.Year1 > 0 && a220.Year2 > 0)
                    assetTurnover1 = p10.Year1 / ((a220.Year1 + a220.Year2) / 2);
                else
                    assetTurnover1 = a220.Year1 != 0 ? p10.Year1 / a220.Year1 : (decimal?)null;

                if (a220.Year2 > 0 && a220.Year3 > 0)
                    assetTurnover2 = p10.Year2 / ((a220.Year2 + a220.Year3) / 2);
                else
                    assetTurnover2 = a220.Year2 != 0 ? p10.Year2 / a220.Year2 : (decimal?)null;

                if (a220.Year3 > 0 && a220.Year4 > 0)
                    assetTurnover3 = p10.Year3 / ((a220.Year3 + a220.Year4) / 2);
                else
                    assetTurnover3 = a220.Year3 != 0 ? p10.Year3 / a220.Year3 : (decimal?)null;

                if (a220.Year4 > 0 && a220.Year5 > 0)
                    assetTurnover4 = p10.Year4 / ((a220.Year4 + a220.Year5) / 2);
                else
                    assetTurnover4 = a220.Year4 != 0 ? p10.Year4 / a220.Year4 : (decimal?)null;

                if (a220.Year5 > 0 && a220.Year6 > 0)
                    assetTurnover5 = p10.Year5 / ((a220.Year5 + a220.Year6) / 2);
                else
                    assetTurnover5 = a220.Year5 != 0 ? p10.Year5 / a220.Year5 : (decimal?)null;

                var assetTurnover = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Fixed asset turnover",
                    Year1 = assetTurnover1,
                    Year2 = assetTurnover2,
                    Year3 = assetTurnover3,
                    Year4 = assetTurnover4,
                    Year5 = assetTurnover5,
                    AverageIndustryCode = "D5",
                    IndustryAverageGroupId = 2,
                    Comment = "(Net Sales/Average fixed assets)"
                };
                res.Add(assetTurnover);
                #endregion

                #region Days of inventory on hand (DOH)
                // =IF(P11>0,365/(P11/IF(AND(CK140>0,DK140>0),(CK140+DK140)/2,CK140)),“-”)
                decimal? dho1 = null;
                decimal? dho2 = null;
                decimal? dho3 = null;
                decimal? dho4 = null;
                decimal? dho5 = null;

                if (p11.Year1 > 0)
                    dho1 = inventoryTurnover1 != 0 ? 365 / inventoryTurnover1 : (decimal?)null;
                if (p11.Year2 > 0)
                    dho2 = inventoryTurnover2 != 0 ? 365 / inventoryTurnover2 : (decimal?)null;
                if (p11.Year3 > 0)
                    dho3 = inventoryTurnover3 != 0 ? 365 / inventoryTurnover3 : (decimal?)null;
                if (p11.Year4 > 0)
                    dho4 = inventoryTurnover4 != 0 ? 365 / inventoryTurnover4 : (decimal?)null;
                if (p11.Year5 > 0)
                    dho5 = inventoryTurnover5 != 0 ? 365 / inventoryTurnover5 : (decimal?)null;

                var dho = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Days of inventory on hand (DOH)",
                    Year1 = dho1,
                    Year2 = dho2,
                    Year3 = dho3,
                    Year4 = dho4,
                    Year5 = dho5,
                    AverageIndustryCode = "D6",
                    IndustryAverageGroupId = 2,
                    Comment = "(Number of days in period/Inventory turnover)",
                    IsHideOnQuarter = true
                };
                res.Add(dho);
                #endregion

                #region Days of sales outstanding (DSO)
                // IF(P10>0, 365/(P10/IF(AND(CK131>0,DK131>0),(CK131+DK131)/2,CK131)), “-”)
                // -> IF(P10>0, 365/customerTurnover , “-”)
                decimal? dso1 = null;
                decimal? dso2 = null;
                decimal? dso3 = null;
                decimal? dso4 = null;
                decimal? dso5 = null;

                if (p10.Year1 > 0)
                    dso1 = customerTurnover1 != 0 ? 365 / customerTurnover1 : (decimal?)null;
                if (p10.Year2 > 0)
                    dso2 = customerTurnover2 != 0 ? 365 / customerTurnover2 : (decimal?)null;
                if (p10.Year3 > 0)
                    dso3 = customerTurnover3 != 0 ? 365 / customerTurnover3 : (decimal?)null;
                if (p10.Year4 > 0)
                    dso4 = customerTurnover4 != 0 ? 365 / customerTurnover4 : (decimal?)null;
                if (p10.Year5 > 0)
                    dso5 = customerTurnover5 != 0 ? 365 / customerTurnover5 : (decimal?)null;

                var dso = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Days of sales outstanding (DSO)",
                    Year1 = dso1,
                    Year2 = dso2,
                    Year3 = dso3,
                    Year4 = dso4,
                    Year5 = dso5,
                    AverageIndustryCode = "D7",
                    IndustryAverageGroupId = 2,
                    Comment = "(Number of days in period/Receivables turnover)",
                    IsHideOnQuarter = true
                };
                res.Add(dso);
                #endregion

                #region Number of days of payables (DPO)
                // IF(P11+CK140-DK140>0, 365/((P11+CK140-DK140)/IF(AND(CK312>0,DK312>0),(CK312+DK312)/2,CK312)), “-”)
                //==> IF(P11+CK140-DK140>0, 365/supplierTurnover, "-")
                decimal? dpo1 = null;
                decimal? dpo2 = null;
                decimal? dpo3 = null;
                decimal? dpo4 = null;
                decimal? dpo5 = null;
                if (condi1 > 0)
                    dpo1 = supplierTurnover1 != 0 ? 365 / (supplierTurnover1) : (decimal?)null;
                if (condi2 > 0)
                    dpo2 = supplierTurnover2 != 0 ? 365 / (supplierTurnover2) : (decimal?)null;
                if (condi3 > 0)
                    dpo3 = supplierTurnover3 != 0 ? 365 / (supplierTurnover3) : (decimal?)null;
                if (condi4 > 0)
                    dpo4 = supplierTurnover4 != 0 ? 365 / (supplierTurnover4) : (decimal?)null;
                if (condi5 > 0)
                    dpo5 = supplierTurnover5 != 0 ? 365 / (supplierTurnover5) : (decimal?)null;

                var dpo = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Number of days of payables (DPO)",
                    Year1 = dpo1,
                    Year2 = dpo2,
                    Year3 = dpo3,
                    Year4 = dpo4,
                    Year5 = dpo5,
                    AverageIndustryCode = "D8",
                    IndustryAverageGroupId = 2,
                    Comment = "(Number of days in period/Payables turnover)",
                    IsHideOnQuarter = true
                };
                res.Add(dpo);
                #endregion

                #region Average Monthly Inventory by monthly net Sales
                //=(IF(AND(CK140>0,DK140>0),(CK140+DK140)/2,CK140))/(P10/12)

                decimal? monthlyNetSales1 = null;
                decimal? monthlyNetSales2 = null;
                decimal? monthlyNetSales3 = null;
                decimal? monthlyNetSales4 = null;
                decimal? monthlyNetSales5 = null;
                if (p10.Year1 != 0)
                {
                    if (a140.Year2 > 0 && a140.Year1 > 0)
                        monthlyNetSales1 = (a140.Year1 + a140.Year2) / 2 / p10.Year1;
                    else
                        monthlyNetSales1 = a140.Year1 / p10.Year1;
                }

                if (p10.Year2 != 0)
                {
                    if (a140.Year3 > 0 && a140.Year2 > 0)
                        monthlyNetSales2 = (a140.Year2 + a140.Year3) / 2 / p10.Year2;
                    else
                        monthlyNetSales2 = a140.Year2 / p10.Year2;
                }

                if (p10.Year3 != 0)
                {
                    if (a140.Year4 > 0 && a140.Year3 > 0)
                        monthlyNetSales3 = (a140.Year3 + a140.Year4) / 2 / p10.Year3;
                    else
                        monthlyNetSales3 = a140.Year3 / p10.Year3;
                }

                if (p10.Year4 != 0)
                {
                    if (a140.Year5 > 0 && a140.Year4 > 0)
                        monthlyNetSales4 = (a140.Year4 + a140.Year5) / 2 / p10.Year4;
                    else
                        monthlyNetSales4 = a140.Year4 / p10.Year4;
                }

                if (p10.Year5 != 0)
                {
                    if (a140.Year6 > 0 && a140.Year5 > 0)
                        monthlyNetSales5 = (a140.Year5 + a140.Year6) / 2 / p10.Year5;
                    else
                        monthlyNetSales5 = a140.Year5 / p10.Year5;
                }

                var monthlyNetSales = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Average Monthly Inventory by monthly net Sales",
                    Year1 = monthlyNetSales1,
                    Year2 = monthlyNetSales2,
                    Year3 = monthlyNetSales3,
                    Year4 = monthlyNetSales4,
                    Year5 = monthlyNetSales5,
                    AverageIndustryCode = "D9",
                    IndustryAverageGroupId = 2,
                    Comment = "(Monthly Inventory/Monthly Net Sales)",
                    IsHideOnQuarter = true
                };
                res.Add(monthlyNetSales);
                #endregion

                #region Total asset turnover
                decimal? totalAssetTurnover1 = null;
                decimal? totalAssetTurnover2 = null;
                decimal? totalAssetTurnover3 = null;
                decimal? totalAssetTurnover4 = null;
                decimal? totalAssetTurnover5 = null;

                var assets = ReportBase.GetCodeValue(datas, "270");

                if (assets.Year1 != 0)
                    totalAssetTurnover1 = assets.Year1 > 0 ? p10.Year1 / ((assets.Year1 + assets.Year2) / 2) : p10.Year1 / assets.Year1;
                if (assets.Year2 != 0)
                    totalAssetTurnover2 = assets.Year2 > 0 ? p10.Year2 / ((assets.Year2 + assets.Year3) / 2) : p10.Year2 / assets.Year2;
                if (assets.Year3 != 0)
                    totalAssetTurnover3 = assets.Year3 > 0 ? p10.Year3 / ((assets.Year3 + assets.Year4) / 2) : p10.Year3 / assets.Year3;
                if (assets.Year4 != 0)
                    totalAssetTurnover4 = assets.Year4 > 0 ? p10.Year4 / ((assets.Year4 + assets.Year5) / 2) : p10.Year4 / assets.Year4;
                if (assets.Year5 != 0)
                    totalAssetTurnover5 = assets.Year5 > 0 ? p10.Year5 / ((assets.Year5 + assets.Year6) / 2) : p10.Year5 / assets.Year5;

                var totalAssetTurnover = new ReportSummaryDTO
                {
                    Index = 10,
                    Name = "Total asset turnover",
                    Year1 = totalAssetTurnover1,
                    Year2 = totalAssetTurnover2,
                    Year3 = totalAssetTurnover3,
                    Year4 = totalAssetTurnover4,
                    Year5 = totalAssetTurnover5,
                    AverageIndustryCode = "D10",
                    IndustryAverageGroupId = 2,
                    Comment = "(Net Sales/Average total assets)"
                };
                res.Add(totalAssetTurnover);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<ReportSummaryDTO> GetCreditRatios(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region EBIT Interest Coverage 
                //'=if(P23>0,(P50+P23)/P23,"-")
                var p23 = ReportBase.GetCodeValue(datas, "P23");
                var p50 = ReportBase.GetCodeValue(datas, "P50");

                decimal? ebitic1 = null;
                decimal? ebitic2 = null;
                decimal? ebitic3 = null;
                decimal? ebitic4 = null;
                decimal? ebitic5 = null;

                if (ReportBase.Compare(p23.Year1, 0)) ebitic1 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { p50.Year1, p23.Year1 }), p23.Year1);
                if (ReportBase.Compare(p23.Year2, 0)) ebitic2 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { p50.Year2, p23.Year2 }), p23.Year2);
                if (ReportBase.Compare(p23.Year3, 0)) ebitic3 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { p50.Year3, p23.Year3 }), p23.Year3);
                if (ReportBase.Compare(p23.Year4, 0)) ebitic4 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { p50.Year4, p23.Year4 }), p23.Year4);
                if (ReportBase.Compare(p23.Year5, 0)) ebitic5 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { p50.Year5, p23.Year5 }), p23.Year5);

                var interestCoverage = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "EBIT Interest Coverage",
                    Year1 = ebitic1,
                    Year2 = ebitic2,
                    Year3 = ebitic3,
                    Year4 = ebitic4,
                    Year5 = ebitic5,
                    AverageIndustryCode = "D31",
                    IndustryAverageGroupId = 3,
                    Comment = "(EBIT/Interest Expense)"
                };
                res.Add(interestCoverage);
                #endregion

                #region EBITDA Interest Coverage
                //==(P50+P23+IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0)))/P23
                decimal? ebitdaic1 = null;
                decimal? ebitdaic2 = null;
                decimal? ebitdaic3 = null;
                decimal? ebitdaic4 = null;
                decimal? ebitdaic5 = null;

                decimal? subebitdaic1 = null;
                decimal? subebitdaic2 = null;
                decimal? subebitdaic3 = null;
                decimal? subebitdaic4 = null;
                decimal? subebitdaic5 = null;

                //P50+P23
                var ts_ic1 = ReportBase.GetCodeValue(datas, "P50+P23").Year1;
                var ts_ic2 = ReportBase.GetCodeValue(datas, "P50+P23").Year2;
                var ts_ic3 = ReportBase.GetCodeValue(datas, "P50+P23").Year3;
                var ts_ic4 = ReportBase.GetCodeValue(datas, "P50+P23").Year4;
                var ts_ic5 = ReportBase.GetCodeValue(datas, "P50+P23").Year5;
                // 223+226+229+242
                var val_ic1 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year1;
                var val_ic2 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year2;
                var val_ic3 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year3;
                var val_ic4 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year4;
                var val_ic5 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year5;
                var val_ic6 = ReportBase.GetCodeValue(datas, "223+226+229+242").Year6;
                var ic2 = ReportBase.GetCodeValue(datas, "2");
                //
                if (ic2.Year1 > 0)
                    subebitdaic1 = (ts_ic1 + ic2.Year1);
                else if ((val_ic2 - val_ic1) > 0)
                    subebitdaic1 = (ts_ic1 + (val_ic2 - val_ic1));
                else
                    subebitdaic1 = ts_ic1;
                //
                if (ic2.Year2 > 0)
                    subebitdaic2 = (ts_ic2 + ic2.Year2);
                else if ((val_ic3 - val_ic2) > 0)
                    subebitdaic2 = (ts_ic2 + (val_ic3 - val_ic2));
                else
                    subebitdaic2 = ts_ic2;
                //
                if (ic2.Year3 > 0)
                    subebitdaic3 = (ts_ic3 + ic2.Year3);
                else if ((val_ic4 - val_ic3) > 0)
                    subebitdaic3 = (ts_ic3 + (val_ic4 - val_ic3));
                else
                    subebitdaic3 = ts_ic3;
                //
                if (ic2.Year4 > 0)
                    subebitdaic4 = (ts_ic4 + ic2.Year5);
                else if ((val_ic5 - val_ic4) > 0)
                    subebitdaic4 = (ts_ic4 + (val_ic5 - val_ic4));
                else
                    subebitdaic4 = ts_ic4;
                //
                if (ic2.Year5 > 0)
                    subebitdaic5 = (ts_ic5 + ic2.Year5);
                else if ((val_ic6 - val_ic5) > 0)
                    subebitdaic5 = (ts_ic5 + (val_ic6 - val_ic5));
                else
                    subebitdaic5 = ts_ic5;

                ebitdaic1 = p23.Year1 != 0 ? subebitdaic1 / p23.Year1 : (decimal?)null;
                ebitdaic2 = p23.Year2 != 0 ? subebitdaic2 / p23.Year2 : (decimal?)null;
                ebitdaic3 = p23.Year3 != 0 ? subebitdaic3 / p23.Year3 : (decimal?)null;
                ebitdaic4 = p23.Year4 != 0 ? subebitdaic4 / p23.Year4 : (decimal?)null;
                ebitdaic5 = p23.Year5 != 0 ? subebitdaic5 / p23.Year5 : (decimal?)null;

                var ebitdaic = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "EBITDA Interest Coverage",
                    Year1 = ebitdaic1,
                    Year2 = ebitdaic2,
                    Year3 = ebitdaic3,
                    Year4 = ebitdaic4,
                    Year5 = ebitdaic5,
                    AverageIndustryCode = "D32",
                    IndustryAverageGroupId = 3,
                    Comment = "(EBITDA / Interest Expense)"
                };
                res.Add(ebitdaic);
                #endregion

                #region Free Operating Cash flow to Debt 
                //=((20+C8)-IF((CK222+CK225+CK241-DK222-DK225-DK241)>(ABS(21)+ABS(C9)),(CK222+CK225+CK241-DK222-DK225-DK241),ABS(21)+ABS(C9)))/IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344)
                //==>((20+C8)- CAPEX)/IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344)

                var i20 = ReportBase.GetCodeValue(datas, "20");
                var i21 = ReportBase.GetCodeValue(datas, "21");
                var c8 = ReportBase.GetCodeValue(datas, "C8");
                var c9 = ReportBase.GetCodeValue(datas, "C9");
                var a270 = ReportBase.GetCodeValue(datas, "270");

                decimal? focfd1 = null;
                decimal? focfd2 = null;
                decimal? focfd3 = null;
                decimal? focfd4 = null;
                decimal? focfd5 = null;
                //311+334+342+344
                var val_foc1 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year1;
                var val_foc2 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year2;
                var val_foc3 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year3;
                var val_foc4 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year4;
                var val_foc5 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year5;
                var val_foc6 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year6;
                #region capex
                decimal? capex1 = null;
                decimal? capex2 = null;
                decimal? capex3 = null;
                decimal? capex4 = null;
                decimal? capex5 = null;

                var ic_1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year1), ReportBase.GetAbs(c9.Year1) });
                var ic_2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year2), ReportBase.GetAbs(c9.Year2) });
                var ic_3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year3), ReportBase.GetAbs(c9.Year3) });
                var ic_4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year4), ReportBase.GetAbs(c9.Year4) });
                var ic_5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year5), ReportBase.GetAbs(c9.Year5) });

                var capval = ReportBase.GetCodeValue(datas, "222+225+241");

                capex1 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year1, -capval.Year2 }), ic_1) ? ReportBase.GetSum(new List<decimal?>() { capval.Year1, -capval.Year2 }) : ic_1;
                capex2 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year2, -capval.Year3 }), ic_2) ? ReportBase.GetSum(new List<decimal?>() { capval.Year2, -capval.Year3 }) : ic_2;
                capex3 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year3, -capval.Year4 }), ic_3) ? ReportBase.GetSum(new List<decimal?>() { capval.Year3, -capval.Year4 }) : ic_3;
                capex4 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year4, -capval.Year5 }), ic_4) ? ReportBase.GetSum(new List<decimal?>() { capval.Year4, -capval.Year5 }) : ic_4;
                capex5 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year5, -capval.Year6 }), ic_5) ? ReportBase.GetSum(new List<decimal?>() { capval.Year5, -capval.Year6 }) : ic_5;
                #endregion

                var ts_foc1 = ReportBase.GetSum(new List<decimal?>() { i20.Year1, c8.Year1, -capex1 });
                var ts_foc2 = ReportBase.GetSum(new List<decimal?>() { i20.Year2, c8.Year2, -capex2 });
                var ts_foc3 = ReportBase.GetSum(new List<decimal?>() { i20.Year3, c8.Year3, -capex3 });
                var ts_foc4 = ReportBase.GetSum(new List<decimal?>() { i20.Year4, c8.Year4, -capex4 });
                var ts_foc5 = ReportBase.GetSum(new List<decimal?>() { i20.Year5, c8.Year5, -capex5 });

                var ms_foc1 = ReportBase.Compare(a270.Year2, 0) ? ReportBase.GetSum(new List<decimal?>() { val_foc1, val_foc2 }) / 2 : val_foc1;
                var ms_foc2 = ReportBase.Compare(a270.Year3, 0) ? ReportBase.GetSum(new List<decimal?>() { val_foc2, val_foc3 }) / 2 : val_foc2;
                var ms_foc3 = ReportBase.Compare(a270.Year4, 0) ? ReportBase.GetSum(new List<decimal?>() { val_foc3, val_foc4 }) / 2 : val_foc3;
                var ms_foc4 = ReportBase.Compare(a270.Year5, 0) ? ReportBase.GetSum(new List<decimal?>() { val_foc4, val_foc5 }) / 2 : val_foc4;
                var ms_foc5 = ReportBase.Compare(a270.Year6, 0) ? ReportBase.GetSum(new List<decimal?>() { val_foc5, val_foc6 }) / 2 : val_foc5;

                focfd1 = ReportBase.GetDiv(ts_foc1, ms_foc1);
                focfd2 = ReportBase.GetDiv(ts_foc2, ms_foc2);
                focfd3 = ReportBase.GetDiv(ts_foc3, ms_foc3);
                focfd4 = ReportBase.GetDiv(ts_foc4, ms_foc4);
                focfd5 = ReportBase.GetDiv(ts_foc5, ms_foc5);

                var focfd = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Free Operating Cash flow to Debt",
                    Year1 = focfd1,
                    Year2 = focfd2,
                    Year3 = focfd3,
                    Year4 = focfd4,
                    Year5 = focfd5,
                    AverageIndustryCode = "D33",
                    IndustryAverageGroupId = 3,
                    Comment = "(CFO-Capital Expenditures/Total Debt)"
                };
                res.Add(focfd);
                #endregion

                #region  Discretionary Cash flow to Debt 
                //= ((20 + C8) - IF((CK222 + CK225 + CK241 - DK222 - DK225 - DK241) > (ABS(21) + ABS(C9)), (CK222 + CK225 + CK241 - DK222 - DK225 - DK241), ABS(21) + ABS(C9)) - ABS(36) - ABS(C22)) /
                //  IF(DK270 > 0, ((CK311 + CK334 + CK342 + CK344) + (DK311 + DK334 + DK342 + DK344)) / 2, CK311 + CK334 + CK342 + CK344)

                var c20 = ReportBase.GetCodeValue(datas, "C20");
                var i34 = ReportBase.GetCodeValue(datas, "34");
                var i36 = ReportBase.GetCodeValue(datas, "36");
                var c22 = ReportBase.GetCodeValue(datas, "C22");

                decimal? dcfd1 = null;
                decimal? dcfd2 = null;
                decimal? dcfd3 = null;
                decimal? dcfd4 = null;
                decimal? dcfd5 = null;

                //var res_if1_1 = capval1 - capval2 > ic_1 ? capval1 - capval2 : ic_1 - Math.Abs(GetFieldValue(i36, 1)) - Math.Abs(GetFieldValue(c22, 1));

                var res_if1_1 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year1, -capval.Year2 }), ic_1) ? ReportBase.GetSum(new List<decimal?>() { capval.Year1, -capval.Year2 }) : ReportBase.GetSum(new List<decimal?>() { ic_1, -ReportBase.GetAbs(i36.Year1), -ReportBase.GetAbs(c22.Year1) });
                var res_if1_2 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year2, -capval.Year3 }), ic_2) ? ReportBase.GetSum(new List<decimal?>() { capval.Year2, -capval.Year3 }) : ReportBase.GetSum(new List<decimal?>() { ic_2, -ReportBase.GetAbs(i36.Year2), -ReportBase.GetAbs(c22.Year2) });
                var res_if1_3 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year3, -capval.Year4 }), ic_3) ? ReportBase.GetSum(new List<decimal?>() { capval.Year3, -capval.Year4 }) : ReportBase.GetSum(new List<decimal?>() { ic_3, -ReportBase.GetAbs(i36.Year3), -ReportBase.GetAbs(c22.Year3) });
                var res_if1_4 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year4, -capval.Year5 }), ic_4) ? ReportBase.GetSum(new List<decimal?>() { capval.Year4, -capval.Year5 }) : ReportBase.GetSum(new List<decimal?>() { ic_4, -ReportBase.GetAbs(i36.Year4), -ReportBase.GetAbs(c22.Year4) });
                var res_if1_5 = ReportBase.Compare(ReportBase.GetSum(new List<decimal?>() { capval.Year5, -capval.Year6 }), ic_5) ? ReportBase.GetSum(new List<decimal?>() { capval.Year5, -capval.Year6 }) : ReportBase.GetSum(new List<decimal?>() { ic_5, -ReportBase.GetAbs(i36.Year5), -ReportBase.GetAbs(c22.Year5) });

                dcfd1 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { i20.Year1, c8.Year1, -res_if1_1 }), ms_foc1);
                dcfd2 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { i20.Year2, c8.Year2, -res_if1_2 }), ms_foc2);
                dcfd3 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { i20.Year3, c8.Year3, -res_if1_3 }), ms_foc3);
                dcfd4 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { i20.Year4, c8.Year4, -res_if1_4 }), ms_foc4);
                dcfd5 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { i20.Year5, c8.Year5, -res_if1_5 }), ms_foc5);

                var dcfd = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Discretionary Cash flow to Debt",
                    Year1 = dcfd1,
                    Year2 = dcfd2,
                    Year3 = dcfd3,
                    Year4 = dcfd4,
                    Year5 = dcfd5,
                    AverageIndustryCode = "D34",
                    IndustryAverageGroupId = 3,
                    Comment = "(CFO-Capital Expenditures-Dividends paid/Total Debt)"
                };
                res.Add(dcfd);
                #endregion

                #region Debt to EBITDA =(20+C8)/(ABS(36)+(ABS(C22)) 
                //=(IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344))/(P50+P23+IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0)))

                decimal? dtEbitda1 = null;
                decimal? dtEbitda2 = null;
                decimal? dtEbitda3 = null;
                decimal? dtEbitda4 = null;
                decimal? dtEbitda5 = null;

                if (subebitdaic1 != 0)
                    dtEbitda1 = ms_foc1 / subebitdaic1;
                if (subebitdaic2 != 0)
                    dtEbitda2 = ms_foc2 / subebitdaic2;
                if (subebitdaic3 != 0)
                    dtEbitda3 = ms_foc3 / subebitdaic3;
                if (subebitdaic4 != 0)
                    dtEbitda4 = ms_foc4 / subebitdaic4;
                if (subebitdaic5 != 0)
                    dtEbitda5 = ms_foc5 / subebitdaic5;

                var dtEbitda = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Debt to EBITDA",
                    Year1 = dtEbitda1,
                    Year2 = dtEbitda2,
                    Year3 = dtEbitda3,
                    Year4 = dtEbitda4,
                    Year5 = dtEbitda5,
                    AverageIndustryCode = "D35",
                    IndustryAverageGroupId = 3,
                    Comment = "(Total Debt/EBITDA)"
                };
                res.Add(dtEbitda);
                #endregion

                #region Total Debt to Total Debt+Equity =(20+C8)/(ABS(30)+(ABS(40)+(ABS(C16)+(ABS(C23))
                //=(311+334+342+344)/(311+334+342+344+400)

                decimal? tdtde1 = null;
                decimal? tdtde2 = null;
                decimal? tdtde3 = null;
                decimal? tdtde4 = null;
                decimal? tdtde5 = null;

                var denominatortotalDebt = ReportBase.GetCodeValue(datas, "311+334+342+344+400");

                if (denominatortotalDebt.Year1 != 0)
                    tdtde1 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year1 / denominatortotalDebt.Year1;
                if (denominatortotalDebt.Year2 != 0)
                    tdtde2 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year2 / denominatortotalDebt.Year2;
                if (denominatortotalDebt.Year3 != 0)
                    tdtde3 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year3 / denominatortotalDebt.Year3;
                if (denominatortotalDebt.Year4 != 0)
                    tdtde4 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year4 / denominatortotalDebt.Year4;
                if (denominatortotalDebt.Year5 != 0)
                    tdtde5 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year5 / denominatortotalDebt.Year5;

                var tdtde = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Total Debt to Total Debt+Equity",
                    Year1 = tdtde1,
                    Year2 = tdtde2,
                    Year3 = tdtde3,
                    Year4 = tdtde4,
                    Year5 = tdtde5,
                    AverageIndustryCode = "D36",
                    IndustryAverageGroupId = 3,
                    Comment = "(Total Debt/Total Debt + Equity)"
                };
                res.Add(tdtde);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<ReportSummaryDTO> GetCoverageRatios(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Debt Coverage
                //'=(20+C8)/(IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344))

                decimal? dc1 = null;
                decimal? dc2 = null;
                decimal? dc3 = null;
                decimal? dc4 = null;
                decimal? dc5 = null;
                var a270 = ReportBase.GetCodeValue(datas, "270");

                decimal? ms_dc1 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year1;
                decimal? ms_dc2 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year2;
                decimal? ms_dc3 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year3;
                decimal? ms_dc4 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year4;
                decimal? ms_dc5 = ReportBase.GetCodeValue(datas, "311+334+342+344").Year5;
                var i20 = ReportBase.GetCodeValue(datas, "20");
                var c8 = ReportBase.GetCodeValue(datas, "C8");

                // (IF(DK270>0,((CK311+CK334+CK342+CK344)+(DK311+DK334+DK342+DK344))/2,CK311+CK334+CK342+CK344))

                if (ReportBase.Compare(a270.Year2, 0)) ms_dc1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetCodeValue(datas, "311+334+342+344").Year1, ReportBase.GetCodeValue(datas, "311+334+342+344").Year2 }) / 2;
                if (ReportBase.Compare(a270.Year3, 0)) ms_dc2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetCodeValue(datas, "311+334+342+344").Year2, ReportBase.GetCodeValue(datas, "311+334+342+344").Year3 }) / 2;
                if (ReportBase.Compare(a270.Year4, 0)) ms_dc3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetCodeValue(datas, "311+334+342+344").Year3, ReportBase.GetCodeValue(datas, "311+334+342+344").Year4 }) / 2;
                if (ReportBase.Compare(a270.Year5, 0)) ms_dc4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetCodeValue(datas, "311+334+342+344").Year4, ReportBase.GetCodeValue(datas, "311+334+342+344").Year5 }) / 2;
                if (ReportBase.Compare(a270.Year6, 0)) ms_dc5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetCodeValue(datas, "311+334+342+344").Year5, ReportBase.GetCodeValue(datas, "311+334+342+344").Year6 }) / 2;

                var vCashflow1 = ReportBase.GetSum(new List<decimal?>() { i20.Year1, c8.Year1 });
                var vCashflow2 = ReportBase.GetSum(new List<decimal?>() { i20.Year2, c8.Year2 });
                var vCashflow3 = ReportBase.GetSum(new List<decimal?>() { i20.Year3, c8.Year3 });
                var vCashflow4 = ReportBase.GetSum(new List<decimal?>() { i20.Year4, c8.Year4 });
                var vCashflow5 = ReportBase.GetSum(new List<decimal?>() { i20.Year5, c8.Year5 });

                dc1 = ReportBase.GetDiv(vCashflow1, ms_dc1);
                dc2 = ReportBase.GetDiv(vCashflow2, ms_dc2);
                dc3 = ReportBase.GetDiv(vCashflow3, ms_dc3);
                dc4 = ReportBase.GetDiv(vCashflow4, ms_dc4);
                dc5 = ReportBase.GetDiv(vCashflow5, ms_dc5);

                var debtCoverage = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Debt Coverage",
                    Year1 = dc1,
                    Year2 = dc2,
                    Year3 = dc3,
                    Year4 = dc4,
                    Year5 = dc5,
                    AverageIndustryCode = "D41",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO/Total Debt)"
                };
                res.Add(debtCoverage);
                #endregion

                #region Interest Coverage 
                // '=((20+C8)+(ABS(13)+(ABS(C4))+(ABS(14)+(ABS(C5)))/(ABS(13)+(ABS(C4))

                var i13 = ReportBase.GetCodeValue(datas, "13");
                var i14 = ReportBase.GetCodeValue(datas, "14");
                var c5 = ReportBase.GetCodeValue(datas, "C5");
                var c4 = ReportBase.GetCodeValue(datas, "C4");

                var vC41 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i13.Year1), ReportBase.GetAbs(c4.Year1) });
                var vC42 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i13.Year2), ReportBase.GetAbs(c4.Year2) });
                var vC43 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i13.Year3), ReportBase.GetAbs(c4.Year3) });
                var vC44 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i13.Year4), ReportBase.GetAbs(c4.Year4) });
                var vC45 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i13.Year5), ReportBase.GetAbs(c4.Year5) });

                decimal? coverage1 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { vCashflow1, vC41, ReportBase.GetAbs(i14.Year1), ReportBase.GetAbs(c5.Year1) }), vC41);
                decimal? coverage2 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { vCashflow2, vC42, ReportBase.GetAbs(i14.Year2), ReportBase.GetAbs(c5.Year2) }), vC42);
                decimal? coverage3 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { vCashflow3, vC43, ReportBase.GetAbs(i14.Year3), ReportBase.GetAbs(c5.Year3) }), vC43);
                decimal? coverage4 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { vCashflow4, vC44, ReportBase.GetAbs(i14.Year4), ReportBase.GetAbs(c5.Year4) }), vC44);
                decimal? coverage5 = ReportBase.GetDiv(ReportBase.GetSum(new List<decimal?>() { vCashflow5, vC45, ReportBase.GetAbs(i14.Year5), ReportBase.GetAbs(c5.Year5) }), vC45);

                var interestCoverage = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Interest Coverage",
                    Year1 = coverage1,
                    Year2 = coverage2,
                    Year3 = coverage3,
                    Year4 = coverage4,
                    Year5 = coverage5,
                    AverageIndustryCode = "D42",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO+Interest Paid+Tax Paid)/Interest Paid)"
                };
                res.Add(interestCoverage);
                #endregion

                #region Reinvestment 
                //'=(20+C8)/(ABS(21)+ABS(C9))
                var i21 = ReportBase.GetCodeValue(datas, "21");
                var c9 = ReportBase.GetCodeValue(datas, "C̣9");
                var i21c1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year1), ReportBase.GetAbs(c9.Year1) });
                var i21c2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year2), ReportBase.GetAbs(c9.Year2) });
                var i21c3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year3), ReportBase.GetAbs(c9.Year3) });
                var i21c4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year4), ReportBase.GetAbs(c9.Year4) });
                var i21c5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year5), ReportBase.GetAbs(c9.Year5) });

                decimal? reinvestment1 = ReportBase.GetDiv(vCashflow1, i21c1);
                decimal? reinvestment2 = ReportBase.GetDiv(vCashflow2, i21c2);
                decimal? reinvestment3 = ReportBase.GetDiv(vCashflow3, i21c3);
                decimal? reinvestment4 = ReportBase.GetDiv(vCashflow4, i21c4);
                decimal? reinvestment5 = ReportBase.GetDiv(vCashflow5, i21c5);

                var reinvestment = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Reinvestment",
                    Year1 = reinvestment1,
                    Year2 = reinvestment2,
                    Year3 = reinvestment3,
                    Year4 = reinvestment4,
                    Year5 = reinvestment5,
                    AverageIndustryCode = "D43",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO/Cash paid for Long-term Assets)"
                };
                res.Add(reinvestment);
                #endregion

                #region  Debt Paymen
                //'=(20+C8)/(ABS(34)+ABS(35)+ABS(C20)+ABS(C21))

                var i35 = ReportBase.GetCodeValue(datas, "35");
                var i34 = ReportBase.GetCodeValue(datas, "35");
                var c21 = ReportBase.GetCodeValue(datas, "C21");
                var c20 = ReportBase.GetCodeValue(datas, "C20");
                decimal? dp1 = null;
                decimal? dp2 = null;
                decimal? dp3 = null;
                decimal? dp4 = null;
                decimal? dp5 = null;
                var abs1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i34.Year1), ReportBase.GetAbs(i35.Year1), ReportBase.GetAbs(c20.Year1), ReportBase.GetAbs(c21.Year1) });
                var abs2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i34.Year2), ReportBase.GetAbs(i35.Year2), ReportBase.GetAbs(c20.Year2), ReportBase.GetAbs(c21.Year2) });
                var abs3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i34.Year3), ReportBase.GetAbs(i35.Year3), ReportBase.GetAbs(c20.Year3), ReportBase.GetAbs(c21.Year3) });
                var abs4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i34.Year4), ReportBase.GetAbs(i35.Year4), ReportBase.GetAbs(c20.Year4), ReportBase.GetAbs(c21.Year4) });
                var abs5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i34.Year5), ReportBase.GetAbs(i35.Year5), ReportBase.GetAbs(c20.Year5), ReportBase.GetAbs(c21.Year5) });

                dp1 = ReportBase.GetDiv(vCashflow1, abs1);
                dp2 = ReportBase.GetDiv(vCashflow2, abs2);
                dp3 = ReportBase.GetDiv(vCashflow3, abs3);
                dp4 = ReportBase.GetDiv(vCashflow4, abs4);
                dp5 = ReportBase.GetDiv(vCashflow5, abs5);

                var debtPaymen = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Debt Payment",
                    Year1 = dp1,
                    Year2 = dp2,
                    Year3 = dp3,
                    Year4 = dp4,
                    Year5 = dp5,
                    AverageIndustryCode = "D44",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO/Cash Paid for Debt Repayment)"
                };
                res.Add(debtPaymen);
                #endregion

                #region  Devidend Payment=(20+C8)/(ABS(36)+(ABS(C22))

                decimal? devidend1 = null;
                decimal? devidend2 = null;
                decimal? devidend3 = null;
                decimal? devidend4 = null;
                decimal? devidend5 = null;
                var i36 = ReportBase.GetCodeValue(datas, "36");
                var c22 = ReportBase.GetCodeValue(datas, "C22");

                var math1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i36.Year1), ReportBase.GetAbs(c22.Year1) });
                var math2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i36.Year2), ReportBase.GetAbs(c22.Year2) });
                var math3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i36.Year3), ReportBase.GetAbs(c22.Year3) });
                var math4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i36.Year4), ReportBase.GetAbs(c22.Year4) });
                var math5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i36.Year5), ReportBase.GetAbs(c22.Year5) });

                devidend1 = ReportBase.GetDiv(vCashflow1, math1);
                devidend2 = ReportBase.GetDiv(vCashflow2, math2);
                devidend3 = ReportBase.GetDiv(vCashflow3, math3);
                devidend4 = ReportBase.GetDiv(vCashflow4, math4);
                devidend5 = ReportBase.GetDiv(vCashflow5, math5);

                var devidend = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Dividend Payment",
                    Year1 = devidend1,
                    Year2 = devidend2,
                    Year3 = devidend3,
                    Year4 = devidend4,
                    Year5 = devidend5,
                    AverageIndustryCode = "D45",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO/Dividends Paid)"
                };
                res.Add(devidend);
                #endregion

                #region  Investing and Financing ===(20+C8)/(-36+(-C22))
                // (20+C8)/(ABS(21)+ABS(23)+ABS(25)+ABS(32)+ABS(34)+ABS(35)+ABS(36)+ABS(C9)+ABS(C11)+ABS(C13)+ABS(C18)+ABS(C20)+ABS(C21)+ABS(C22))
                var i23 = ReportBase.GetCodeValue(datas, "23");
                var i25 = ReportBase.GetCodeValue(datas, "25");
                var i30 = ReportBase.GetCodeValue(datas, "30");
                var i32 = ReportBase.GetCodeValue(datas, "32");

                var c11 = ReportBase.GetCodeValue(datas, "C11");
                var c13 = ReportBase.GetCodeValue(datas, "C13");
                var c18 = ReportBase.GetCodeValue(datas, "C18");
                var i40 = ReportBase.GetCodeValue(datas, "40");
                var c16 = ReportBase.GetCodeValue(datas, "C16");
                var c23 = ReportBase.GetCodeValue(datas, "C23");

                decimal? if1 = null;
                decimal? if2 = null;
                decimal? if3 = null;
                decimal? if4 = null;
                decimal? if5 = null;
                var dif1 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year1), ReportBase.GetAbs(i23.Year1), ReportBase.GetAbs(i25.Year1), ReportBase.GetAbs(i32.Year1), ReportBase.GetAbs(i34.Year1), ReportBase.GetAbs(i35.Year1), ReportBase.GetAbs(i36.Year1), ReportBase.GetAbs(c9.Year1), ReportBase.GetAbs(c11.Year1), ReportBase.GetAbs(c13.Year1), ReportBase.GetAbs(c18.Year1), ReportBase.GetAbs(c20.Year1), ReportBase.GetAbs(c21.Year1) + ReportBase.GetAbs(c22.Year1) });
                var dif2 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year2), ReportBase.GetAbs(i23.Year2), ReportBase.GetAbs(i25.Year2), ReportBase.GetAbs(i32.Year2), ReportBase.GetAbs(i34.Year2), ReportBase.GetAbs(i35.Year2), ReportBase.GetAbs(i36.Year2), ReportBase.GetAbs(c9.Year2), ReportBase.GetAbs(c11.Year2), ReportBase.GetAbs(c13.Year2), ReportBase.GetAbs(c18.Year2), ReportBase.GetAbs(c20.Year2), ReportBase.GetAbs(c21.Year2) + ReportBase.GetAbs(c22.Year2) });
                var dif3 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year3), ReportBase.GetAbs(i23.Year3), ReportBase.GetAbs(i25.Year3), ReportBase.GetAbs(i32.Year3), ReportBase.GetAbs(i34.Year3), ReportBase.GetAbs(i35.Year3), ReportBase.GetAbs(i36.Year3), ReportBase.GetAbs(c9.Year3), ReportBase.GetAbs(c11.Year3), ReportBase.GetAbs(c13.Year3), ReportBase.GetAbs(c18.Year3), ReportBase.GetAbs(c20.Year3), ReportBase.GetAbs(c21.Year3) + ReportBase.GetAbs(c22.Year3) });
                var dif4 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year4), ReportBase.GetAbs(i23.Year4), ReportBase.GetAbs(i25.Year4), ReportBase.GetAbs(i32.Year4), ReportBase.GetAbs(i34.Year4), ReportBase.GetAbs(i35.Year4), ReportBase.GetAbs(i36.Year4), ReportBase.GetAbs(c9.Year4), ReportBase.GetAbs(c11.Year4), ReportBase.GetAbs(c13.Year4), ReportBase.GetAbs(c18.Year4), ReportBase.GetAbs(c20.Year4), ReportBase.GetAbs(c21.Year4) + ReportBase.GetAbs(c22.Year4) });
                var dif5 = ReportBase.GetSum(new List<decimal?>() { ReportBase.GetAbs(i21.Year5), ReportBase.GetAbs(i23.Year5), ReportBase.GetAbs(i25.Year5), ReportBase.GetAbs(i32.Year5), ReportBase.GetAbs(i34.Year5), ReportBase.GetAbs(i35.Year5), ReportBase.GetAbs(i36.Year5), ReportBase.GetAbs(c9.Year5), ReportBase.GetAbs(c11.Year5), ReportBase.GetAbs(c13.Year5), ReportBase.GetAbs(c18.Year5), ReportBase.GetAbs(c20.Year5), ReportBase.GetAbs(c21.Year5) + ReportBase.GetAbs(c22.Year5) });

                // Investing and Financing
                if1 = ReportBase.GetDiv(vCashflow1, dif1);
                if2 = ReportBase.GetDiv(vCashflow2, dif2);
                if3 = ReportBase.GetDiv(vCashflow3, dif3);
                if4 = ReportBase.GetDiv(vCashflow4, dif4);
                if5 = ReportBase.GetDiv(vCashflow5, dif5);

                var investingandFinancing = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Investing and Financing",
                    Year1 = if1,
                    Year2 = if2,
                    Year3 = if3,
                    Year4 = if4,
                    Year5 = if5,
                    AverageIndustryCode = "D46",
                    IndustryAverageGroupId = 4,
                    Comment = "(CFO/Cash outflow for Investing and Financing activities)"
                };
                res.Add(investingandFinancing);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static List<ReportSummaryDTO> GetLiquidityRatios(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Current Ratio =100/310
                decimal? cr1 = null;
                decimal? cr2 = null;
                decimal? cr3 = null;
                decimal? cr4 = null;
                decimal? cr5 = null;
                var a310 = ReportBase.GetCodeValue(datas, "310");
                var a100 = ReportBase.GetCodeValue(datas, "100");
                if (a310.Year1 != 0)
                    cr1 = a100.Year1 / a310.Year1;
                if (a310.Year2 != 0)
                    cr2 = a100.Year2 / a310.Year2;
                if (a310.Year3 != 0)
                    cr3 = a100.Year3 / a310.Year3;
                if (a310.Year4 != 0)
                    cr4 = a100.Year4 / a310.Year4;
                if (a310.Year5 != 0)
                    cr5 = a100.Year5 / a310.Year5;

                var currentRatio = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Current Ratio",
                    Year1 = cr1,
                    Year2 = cr2,
                    Year3 = cr3,
                    Year4 = cr4,
                    Year5 = cr5,
                    AverageIndustryCode = "D11",
                    IndustryAverageGroupId = 1,
                    Comment = "(Current Assets/Current Liabilities)"
                };
                res.Add(currentRatio);
                #endregion

                #region Quick Ratio = 110+120+130/310
                decimal? qr1 = null;
                decimal? qr2 = null;
                decimal? qr3 = null;
                decimal? qr4 = null;
                decimal? qr5 = null;
                var quickRatioData = ReportBase.GetCodeValue(datas, "110+120+130");
                if (a310.Year1 != 0)
                    qr1 = quickRatioData.Year1 / a310.Year1;
                if (a310.Year2 != 0)
                    qr2 = quickRatioData.Year2 / a310.Year2;
                if (a310.Year3 != 0)
                    qr3 = quickRatioData.Year3 / a310.Year3;
                if (a310.Year4 != 0)
                    qr4 = quickRatioData.Year4 / a310.Year4;
                if (a310.Year5 != 0)
                    qr5 = quickRatioData.Year5 / a310.Year5;

                var quickRatio = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Quick Ratio",
                    Year1 = qr1,
                    Year2 = qr2,
                    Year3 = qr3,
                    Year4 = qr4,
                    Year5 = qr5,
                    AverageIndustryCode = "D12",
                    IndustryAverageGroupId = 1,
                    Comment = "{(Cash + ST marketable inv.+ ST receivables)/Current liabilities)}"
                };
                res.Add(quickRatio);
                #endregion

                #region Cash Ratio =(110+120)/310
                decimal? car1 = null;
                decimal? car2 = null;
                decimal? car3 = null;
                decimal? car4 = null;
                decimal? car5 = null;
                var quickCashRatio = ReportBase.GetCodeValue(datas, "110+120");
                if (a310.Year1 != 0)
                    car1 = quickCashRatio.Year1 / a310.Year1;
                if (a310.Year2 != 0)
                    car2 = quickCashRatio.Year2 / a310.Year2;
                if (a310.Year3 != 0)
                    car3 = quickCashRatio.Year3 / a310.Year3;
                if (a310.Year4 != 0)
                    car4 = quickCashRatio.Year4 / a310.Year4;
                if (a310.Year5 != 0)
                    car5 = quickCashRatio.Year5 / a310.Year5;

                var cashRatio = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Cash Ratio",
                    Year1 = car1,
                    Year2 = car2,
                    Year3 = car3,
                    Year4 = car4,
                    Year5 = car5,
                    AverageIndustryCode = "D13",
                    IndustryAverageGroupId = 1,
                    Comment = "{(Cash + ST marketable inv.)/Current liabilities}"
                };
                res.Add(cashRatio);
                #endregion

                #region Defensive Interval Ratio (110+120+131)/((P11+P24+P25-P23)/365)
                //=IF(100DK>0,
                //(CK110+CK120+CK130+DK110+DK120+DK130)/2,
                // CK110+CK120+CK130)/((P11+P24+P25-IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0)))/365)
                decimal? dir1 = null;
                decimal? dir2 = null;
                decimal? dir3 = null;
                decimal? dir4 = null;
                decimal? dir5 = null;

                //110+120+130
                var value = ReportBase.GetCodeValue(datas, "110+120+130");

                //223+226+229+242
                var val = ReportBase.GetCodeValue(datas, "223+226+229+242");

                //P11+P24+P25
                var p = ReportBase.GetCodeValue(datas, "P11+P24+P25");

                var val_i2 = ReportBase.GetCodeValue(datas, "2");
                decimal? ts_dir1 = 0;
                decimal? ts_dir2 = 0;
                decimal? ts_dir3 = 0;
                decimal? ts_dir4 = 0;
                decimal? ts_dir5 = 0;

                decimal? ms_dir1 = null;
                decimal? ms_dir2 = null;
                decimal? ms_dir3 = null;
                decimal? ms_dir4 = null;
                decimal? ms_dir5 = null;

                //tu so
                if (a100.Year2 > 0) ts_dir1 = (value.Year1 + value.Year2) / 2;
                else ts_dir1 = value.Year1;
                if (a100.Year3 > 0) ts_dir2 = (value.Year2 + value.Year3) / 2;
                else ts_dir2 = value.Year2;
                if (a100.Year4 > 0) ts_dir3 = (value.Year3 + value.Year4) / 2;
                else ts_dir3 = value.Year3;
                if (a100.Year5 > 0) ts_dir4 = (value.Year4 + value.Year5) / 2;
                else ts_dir4 = value.Year4;
                if (a100.Year6 > 0) ts_dir5 = (value.Year5 + value.Year6) / 2;
                else ts_dir5 = value.Year5;
                //mau so
                if (val_i2.Year1 > 0)
                    ms_dir1 = (p.Year1 - val_i2.Year1) / 365;
                else if ((val.Year2 - val.Year1) > 0)
                    ms_dir1 = (p.Year1 - (val.Year2 - val.Year1)) / 365;
                else
                    ms_dir1 = (p.Year1 - 0) / 365;

                if (val_i2.Year2 > 0)
                    ms_dir2 = (p.Year2 - val_i2.Year2) / 365;
                else if ((val.Year3 - val.Year2) > 0)
                    ms_dir2 = (p.Year2 - (val.Year3 - val.Year2)) / 365;
                else
                    ms_dir2 = (p.Year2 - 0) / 365;

                if (val_i2.Year3 > 0)
                    ms_dir3 = (p.Year3 - val_i2.Year3) / 365;
                else if ((val.Year4 - val.Year3) > 0)
                    ms_dir3 = (p.Year3 - (val.Year4 - val.Year3)) / 365;
                else
                    ms_dir3 = (p.Year3 - 0) / 365;

                if (val_i2.Year4 > 0)
                    ms_dir4 = (p.Year4 - val_i2.Year4) / 365;
                else if ((val.Year5 - val.Year4) > 0)
                    ms_dir4 = (p.Year4 - (val.Year5 - val.Year4)) / 365;
                else
                    ms_dir4 = (p.Year4 - 0) / 365;

                if (val_i2.Year5 > 0)
                    ms_dir5 = (p.Year5 - val_i2.Year5) / 365;
                else if ((val.Year6 - val.Year5) > 0)
                    ms_dir5 = (p.Year5 - (val.Year6 - val.Year5)) / 365;
                else
                    ms_dir5 = (p.Year5 - 0) / 365;

                if (ms_dir1 != 0) dir1 = ts_dir1 / ms_dir1;
                if (ms_dir2 != 0) dir2 = ts_dir2 / ms_dir2;
                if (ms_dir3 != 0) dir3 = ts_dir3 / ms_dir3;
                if (ms_dir4 != 0) dir4 = ts_dir4 / ms_dir4;
                if (ms_dir5 != 0) dir5 = ts_dir5 / ms_dir5;

                var defensiveIntervalRatio = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Defensive Interval Ratio",
                    Year1 = dir1,
                    Year2 = dir2,
                    Year3 = dir3,
                    Year4 = dir4,
                    Year5 = dir5,
                    AverageIndustryCode = "D14",
                    IndustryAverageGroupId = 1,
                    Comment = "{(Cash + ST marketable inv.+ Receivables)/Daily cash expenditures}",
                    IsHideOnQuarter = true
                };
                res.Add(defensiveIntervalRatio);
                #endregion

                #region Cash Conversion Cycle (days) (DOH + DSO – DPO)

                var cashConversionCycle = GetCCC(datas);
                cashConversionCycle.Index = 5;
                cashConversionCycle.AverageIndustryCode = "D15";
                cashConversionCycle.IndustryAverageGroupId = 1;
                cashConversionCycle.Comment = "(DOH + DSO – DPO)";
                cashConversionCycle.IsHideOnQuarter = true;

                res.Add(cashConversionCycle);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ReportSummaryDTO> GetPerformance(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Cash flow to Sales = (20+C8)/P10
                var _20C8 = ReportBase.GetCodeValue(datas, "20+C8");
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Cash flow to Sales",
                    Year1 = ReportBase.GetDiv(_20C8.Year1, P10.Year1),
                    Year2 = ReportBase.GetDiv(_20C8.Year2, P10.Year2),
                    Year3 = ReportBase.GetDiv(_20C8.Year3, P10.Year3),
                    Year4 = ReportBase.GetDiv(_20C8.Year4, P10.Year4),
                    Year5 = ReportBase.GetDiv(_20C8.Year5, P10.Year5),
                    Year6 = ReportBase.GetDiv(_20C8.Year6, P10.Year6),
                    AverageIndustryCode = "D37",
                    Comment = "(CFO/Net Sales)"
                };
                res.Add(CurrentAssets);
                #endregion

                #region Cash return on Assets = IF(DK270>0,(20+C8)/((CK270+DK270)/2),(20+C8)/CK270)
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var CAE = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Cash return on Assets",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(_20C8.Year1, (_270.Year1 + _270.Year2) / 2) : ReportBase.GetDiv(_20C8.Year1, _270.Year1),
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(_20C8.Year2, (_270.Year2 + _270.Year3) / 2) : ReportBase.GetDiv(_20C8.Year2, _270.Year2),
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(_20C8.Year3, (_270.Year3 + _270.Year4) / 2) : ReportBase.GetDiv(_20C8.Year3, _270.Year3),
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(_20C8.Year4, (_270.Year4 + _270.Year5) / 2) : ReportBase.GetDiv(_20C8.Year4, _270.Year4),
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(_20C8.Year5, (_270.Year5 + _270.Year6) / 2) : ReportBase.GetDiv(_20C8.Year5, _270.Year5),
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(_20C8.Year6, (_270.Year6 + _270.Year7) / 2) : ReportBase.GetDiv(_20C8.Year6, _270.Year6),
                    AverageIndustryCode = "D38",
                    Comment = "(CFO/Average total Assets)"
                };
                res.Add(CAE);
                #endregion

                #region Cash return on Equity = IF((CK400+DK400)>0,(20+C8)/IF(DK400DK'=0,CK400,(CK400+DK400)/2),"-")
                var _400 = ReportBase.GetCodeValue(datas, "400");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Cash return on Equity",
                    Year1 = (_400.Year1 + _400.Year2) > 0 ? ReportBase.GetDiv(_20C8.Year1, _400.Year2 == 0 ? _400.Year1 : (_400.Year1 + _400.Year2) / 2) : null,
                    Year2 = (_400.Year2 + _400.Year3) > 0 ? ReportBase.GetDiv(_20C8.Year2, _400.Year3 == 0 ? _400.Year2 : (_400.Year2 + _400.Year3) / 2) : null,
                    Year3 = (_400.Year3 + _400.Year4) > 0 ? ReportBase.GetDiv(_20C8.Year3, _400.Year4 == 0 ? _400.Year3 : (_400.Year3 + _400.Year4) / 2) : null,
                    Year4 = (_400.Year4 + _400.Year5) > 0 ? ReportBase.GetDiv(_20C8.Year4, _400.Year5 == 0 ? _400.Year4 : (_400.Year4 + _400.Year5) / 2) : null,
                    Year5 = (_400.Year5 + _400.Year6) > 0 ? ReportBase.GetDiv(_20C8.Year5, _400.Year6 == 0 ? _400.Year5 : (_400.Year5 + _400.Year6) / 2) : null,
                    Year6 = (_400.Year6 + _400.Year7) > 0 ? ReportBase.GetDiv(_20C8.Year6, _400.Year7 == 0 ? _400.Year6 : (_400.Year6 + _400.Year7) / 2) : null,
                    AverageIndustryCode = "D39",
                    Comment = "(CFO/Avg Owners' Equity)"
                };
                res.Add(marketableInvestments);

                #endregion

                #region Cash to Income = (20+C8)/(P20-P24-P25)
                var P20 = ReportBase.GetCodeValue(datas, "P20");
                var P24P25 = ReportBase.GetCodeValue(datas, "P24+P25");

                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Cash to Income",
                    Year1 = ReportBase.GetDiv(_20C8.Year1, (P20.Year1 - P24P25.Year1)),
                    Year2 = ReportBase.GetDiv(_20C8.Year2, (P20.Year2 - P24P25.Year2)),
                    Year3 = ReportBase.GetDiv(_20C8.Year3, (P20.Year3 - P24P25.Year3)),
                    Year4 = ReportBase.GetDiv(_20C8.Year4, (P20.Year4 - P24P25.Year4)),
                    Year5 = ReportBase.GetDiv(_20C8.Year5, (P20.Year5 - P24P25.Year5)),
                    Year6 = ReportBase.GetDiv(_20C8.Year6, (P20.Year6 - P24P25.Year6)),
                    AverageIndustryCode = "D40",
                    Comment = "(CFO/Operating Income)"
                };
                res.Add(tradeDebtor);

                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetReturnOnInvestment(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Operating ROA (%) = IF(DK270>0,(P20-P24-P25)/((CK270+DK270)/2)*100,(P20-P24-P25)/CK270)*100
                var P24P25 = ReportBase.GetCodeValue(datas, "P24+P25");
                var P20 = ReportBase.GetCodeValue(datas, "P20");
                var _270 = ReportBase.GetCodeValue(datas, "270");

                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Operating ROA (%)",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(P20.Year1 - P24P25.Year1, (_270.Year1 + _270.Year2) / 2) * 100 : ReportBase.GetDiv(P20.Year1 - P24P25.Year1, _270.Year1) * 100,
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(P20.Year2 - P24P25.Year2, (_270.Year2 + _270.Year3) / 2) * 100 : ReportBase.GetDiv(P20.Year2 - P24P25.Year2, _270.Year2) * 100,
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(P20.Year3 - P24P25.Year3, (_270.Year3 + _270.Year4) / 2) * 100 : ReportBase.GetDiv(P20.Year3 - P24P25.Year3, _270.Year3) * 100,
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(P20.Year4 - P24P25.Year4, (_270.Year4 + _270.Year5) / 2) * 100 : ReportBase.GetDiv(P20.Year4 - P24P25.Year4, _270.Year4) * 100,
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(P20.Year5 - P24P25.Year5, (_270.Year5 + _270.Year6) / 2) * 100 : ReportBase.GetDiv(P20.Year5 - P24P25.Year5, _270.Year5) * 100,
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(P20.Year6 - P24P25.Year6, (_270.Year6 + _270.Year7) / 2) * 100 : ReportBase.GetDiv(P20.Year6 - P24P25.Year6, _270.Year6) * 100,
                    AverageIndustryCode = "D27",
                    Comment = "(Operating Income/Average total Assets) * 100"
                };
                res.Add(CurrentAssets);
                #endregion

                #region ROA (%) = IF(DK270>0,P60/((CK270+DK270)/2)*100,(P60)/CK270)*100
                var P60 = ReportBase.GetCodeValue(datas, "P60");

                var CAE = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "ROA (%)",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(P60.Year1, (_270.Year1 + _270.Year2) / 2) * 100 : ReportBase.GetDiv(P60.Year1, _270.Year1) * 100,
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(P60.Year2, (_270.Year2 + _270.Year3) / 2) * 100 : ReportBase.GetDiv(P60.Year2, _270.Year2) * 100,
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(P60.Year3, (_270.Year3 + _270.Year4) / 2) * 100 : ReportBase.GetDiv(P60.Year3, _270.Year3) * 100,
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(P60.Year4, (_270.Year4 + _270.Year5) / 2) * 100 : ReportBase.GetDiv(P60.Year4, _270.Year4) * 100,
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(P60.Year5, (_270.Year5 + _270.Year6) / 2) * 100 : ReportBase.GetDiv(P60.Year5, _270.Year5) * 100,
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(P60.Year6, (_270.Year6 + _270.Year7) / 2) * 100 : ReportBase.GetDiv(P60.Year6, _270.Year6) * 100,
                    AverageIndustryCode = "D28",
                    Comment = "(Net Income/Average total Assets) * 100"
                };
                res.Add(CAE);
                #endregion

                #region Cash return on Equity = ((P20-P24-P25)/IF(DK270>0,((CK311+CK334+CK342+CK344+CK400)+(DK311+DK334+DK342+DK344+DK400))/2,CK311+CK334+CK342+CK344+CK400))*100 
                var subCROE = ReportBase.GetCodeValue(datas, "311+334+342+344+400");

                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "ROIC (Return on invested capital) (%)",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(P20.Year1 - P24P25.Year1, (subCROE.Year1 + subCROE.Year2) / 2) * 100 : ReportBase.GetDiv(P20.Year1 - P24P25.Year1, subCROE.Year1) * 100,
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(P20.Year2 - P24P25.Year2, (subCROE.Year2 + subCROE.Year3) / 2) * 100 : ReportBase.GetDiv(P20.Year2 - P24P25.Year2, subCROE.Year2) * 100,
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(P20.Year3 - P24P25.Year3, (subCROE.Year3 + subCROE.Year4) / 2) * 100 : ReportBase.GetDiv(P20.Year3 - P24P25.Year3, subCROE.Year3) * 100,
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(P20.Year4 - P24P25.Year4, (subCROE.Year4 + subCROE.Year5) / 2) * 100 : ReportBase.GetDiv(P20.Year4 - P24P25.Year4, subCROE.Year4) * 100,
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(P20.Year5 - P24P25.Year5, (subCROE.Year5 + subCROE.Year6) / 2) * 100 : ReportBase.GetDiv(P20.Year5 - P24P25.Year5, subCROE.Year5) * 100,
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(P20.Year6 - P24P25.Year6, (subCROE.Year6 + subCROE.Year7) / 2) * 100 : ReportBase.GetDiv(P20.Year6 - P24P25.Year6, subCROE.Year6) * 100,
                    AverageIndustryCode = "D29",
                    Comment = "(Operating Income - Income tax)/Invested Capital) * 100"
                };
                res.Add(marketableInvestments);

                #endregion

                #region ROE (%) = IF((CK400+DK400)>0,(P60/IF(DK400'=0,CK400,(CK400+DK400)/2))*100,“-”)
                var _400 = ReportBase.GetCodeValue(datas, "400");

                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "ROE (%)",
                    Year1 = (_400.Year1 + _400.Year2) > 0 ? ReportBase.GetDiv(P60.Year1, _400.Year2 == 0 ? _400.Year1 : (_400.Year1 + _400.Year2) / 2) * 100 : null,
                    Year2 = (_400.Year2 + _400.Year3) > 0 ? ReportBase.GetDiv(P60.Year2, _400.Year3 == 0 ? _400.Year2 : (_400.Year2 + _400.Year3) / 2) * 100 : null,
                    Year3 = (_400.Year3 + _400.Year4) > 0 ? ReportBase.GetDiv(P60.Year3, _400.Year4 == 0 ? _400.Year3 : (_400.Year3 + _400.Year4) / 2) * 100 : null,
                    Year4 = (_400.Year4 + _400.Year5) > 0 ? ReportBase.GetDiv(P60.Year4, _400.Year5 == 0 ? _400.Year4 : (_400.Year4 + _400.Year5) / 2) * 100 : null,
                    Year5 = (_400.Year5 + _400.Year6) > 0 ? ReportBase.GetDiv(P60.Year5, _400.Year6 == 0 ? _400.Year5 : (_400.Year5 + _400.Year6) / 2) * 100 : null,
                    Year6 = (_400.Year6 + _400.Year7) > 0 ? ReportBase.GetDiv(P60.Year6, _400.Year7 == 0 ? _400.Year6 : (_400.Year6 + _400.Year7) / 2) * 100 : null,
                    AverageIndustryCode = "D30",
                    Comment = "(Net Income/Average total Equity) * 100"
                };
                res.Add(tradeDebtor);

                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetReturnOnNetSales(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Gross Profit Margin (%) = (P20/P10)*100
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var P20 = ReportBase.GetCodeValue(datas, "P20");

                var GPM = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Gross Profit Margin (%)",
                    Year1 = ReportBase.GetDiv(P20.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P20.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P20.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P20.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P20.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P20.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D21",
                    Comment = "(Gross Profit/Net Sales) * 100"
                };
                res.Add(GPM);
                #endregion

                #region Operating Income Margin (%) = ((P20-P24-P25)/P10)*100
                var P24P25 = ReportBase.GetCodeValue(datas, "P24+P25");

                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Operating Income Margin (%)",
                    Year1 = ReportBase.GetDiv(P20.Year1 - P24P25.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P20.Year2 - P24P25.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P20.Year3 - P24P25.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P20.Year4 - P24P25.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P20.Year5 - P24P25.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P20.Year6 - P24P25.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D22",
                    Comment = "{(Gross Profit - Operating Expenses)/Net Sales} * 100"
                };
                res.Add(CurrentAssets);
                #endregion

                #region EBITDA Margin (%) = ebitDa/p10*100
                var ebitDa = GetEBITDA(datas);

                var CAE = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "EBITDA Margin (%)",
                    Year1 = ReportBase.GetDiv(ebitDa.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(ebitDa.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(ebitDa.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(ebitDa.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(ebitDa.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(ebitDa.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D23",
                    Comment = "(EBITDA/Net Sales) * 100",
                    InitIndex = 5
                };
                res.Add(CAE);
                #endregion

                #region EBIT Margin (%) = ((P50+P23)/P10)*100
                var P50P23 = ReportBase.GetCodeValue(datas, "P50+P23");

                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "EBIT Margin (%)",
                    Year1 = ReportBase.GetDiv(P50P23.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P50P23.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P50P23.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P50P23.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P50P23.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P50P23.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D24",
                    Comment = "(EBIT/Net Sales) * 100",
                    InitIndex = 6
                };
                res.Add(marketableInvestments);

                #endregion

                #region EBT Margin (%) = (P50/P10)*100
                var P50 = ReportBase.GetCodeValue(datas, "P50");

                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "EBT Margin (%)",
                    Year1 = ReportBase.GetDiv(P50.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P50.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P50.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P50.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P50.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P50.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D25",
                    Comment = "(EBT/Net Sales) * 100"
                };
                res.Add(tradeDebtor);

                #endregion

                #region Net Income Margin (%) = (P60/P10)*100
                var P60 = ReportBase.GetCodeValue(datas, "P60");

                var NIM = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Net Income Margin (%)",
                    Year1 = ReportBase.GetDiv(P60.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P60.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P60.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P60.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P60.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P60.Year6, P10.Year6) * 100,
                    AverageIndustryCode = "D26",
                    Comment = "(Net Income/Net Sales) * 100"
                };
                res.Add(NIM);

                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetSolvencyRatios(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Debt to Assets Ratio (%) = ((311+334+342+344)/270)*100
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var sub = ReportBase.GetCodeValue(datas, "311+334+342+344");

                var GPM = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Debt to Assets Ratio (%)",
                    Year1 = ReportBase.GetDiv(sub.Year1, _270.Year1) * 100,
                    Year2 = ReportBase.GetDiv(sub.Year2, _270.Year2) * 100,
                    Year3 = ReportBase.GetDiv(sub.Year3, _270.Year3) * 100,
                    Year4 = ReportBase.GetDiv(sub.Year4, _270.Year4) * 100,
                    Year5 = ReportBase.GetDiv(sub.Year5, _270.Year5) * 100,
                    Year6 = ReportBase.GetDiv(sub.Year6, _270.Year6) * 100,
                    AverageIndustryCode = "D16",
                    Comment = "(Total Debt/Total Assets) * 100"
                };
                res.Add(GPM);
                #endregion

                #region Debt to Equity Ratio (%) = ((311+334+342+344)/400)*100
                var _400 = ReportBase.GetCodeValue(datas, "400");

                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Debt to Equity Ratio (%)",
                    Year1 = ReportBase.GetDiv(sub.Year1, _400.Year1) * 100,
                    Year2 = ReportBase.GetDiv(sub.Year2, _400.Year2) * 100,
                    Year3 = ReportBase.GetDiv(sub.Year3, _400.Year3) * 100,
                    Year4 = ReportBase.GetDiv(sub.Year4, _400.Year4) * 100,
                    Year5 = ReportBase.GetDiv(sub.Year5, _400.Year5) * 100,
                    Year6 = ReportBase.GetDiv(sub.Year6, _400.Year6) * 100,
                    AverageIndustryCode = "D17",
                    Comment = "(Total Debt/Total Owners’ Equity) * 100"
                };
                res.Add(CurrentAssets);
                #endregion

                #region Financial Leverage Ratio (%) = IF(DK270>0,((CK270+DK270)/2)/((CK400+DK400)/2),CK270/CK400)*100

                var CAE = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Financial Leverage Ratio (%)",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(_270.Year1 + _270.Year2, _400.Year1 + _400.Year2) * 100 : ReportBase.GetDiv(_270.Year1, _400.Year1) * 100,
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(_270.Year2 + _270.Year3, _400.Year2 + _400.Year3) * 100 : ReportBase.GetDiv(_270.Year2, _400.Year2) * 100,
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(_270.Year3 + _270.Year4, _400.Year3 + _400.Year4) * 100 : ReportBase.GetDiv(_270.Year3, _400.Year3) * 100,
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(_270.Year4 + _270.Year5, _400.Year4 + _400.Year5) * 100 : ReportBase.GetDiv(_270.Year4, _400.Year4) * 100,
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(_270.Year5 + _270.Year6, _400.Year5 + _400.Year6) * 100 : ReportBase.GetDiv(_270.Year5, _400.Year5) * 100,
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(_270.Year6 + _270.Year7, _400.Year6 + _400.Year7) * 100 : ReportBase.GetDiv(_270.Year6, _400.Year6) * 100,
                    AverageIndustryCode = "D18",
                    Comment = "(Average total Assets/Average total Owner's Equity) * 100"
                };
                res.Add(CAE);
                #endregion

                #region Liabilities by Total Assets (%) = (300/270)*100
                var _300 = ReportBase.GetCodeValue(datas, "300");

                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Liabilities by Total Assets (%)",
                    Year1 = ReportBase.GetDiv(_300.Year1, _270.Year1) * 100,
                    Year2 = ReportBase.GetDiv(_300.Year2, _270.Year2) * 100,
                    Year3 = ReportBase.GetDiv(_300.Year3, _270.Year3) * 100,
                    Year4 = ReportBase.GetDiv(_300.Year4, _270.Year4) * 100,
                    Year5 = ReportBase.GetDiv(_300.Year5, _270.Year5) * 100,
                    Year6 = ReportBase.GetDiv(_300.Year6, _270.Year6) * 100,
                    AverageIndustryCode = "D19",
                    Comment = "(Liabilities/Total Assets) * 100"
                };
                res.Add(marketableInvestments);

                #endregion

                #region Liabilities by Owner's Equity (%) = (300/400)*100

                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Liabilities by Owner's Equity (%)",
                    Year1 = ReportBase.GetDiv(_300.Year1, _400.Year1) * 100,
                    Year2 = ReportBase.GetDiv(_300.Year2, _400.Year2) * 100,
                    Year3 = ReportBase.GetDiv(_300.Year3, _400.Year3) * 100,
                    Year4 = ReportBase.GetDiv(_300.Year4, _400.Year4) * 100,
                    Year5 = ReportBase.GetDiv(_300.Year5, _400.Year5) * 100,
                    Year6 = ReportBase.GetDiv(_300.Year6, _400.Year6) * 100,
                    AverageIndustryCode = "D20",
                    Comment = "(Liabilities /Owner's Equity) * 100"
                };
                res.Add(tradeDebtor);

                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetFinancialAnalysis(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Cash and cash equivalents = 110
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var _110 = ReportBase.GetCodeValue(datas, "110");
                var CAE = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Cash and cash equivalents",
                    Year1 = ReportBase.GetDiv(_110.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_110.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_110.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_110.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_110.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_110.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(CAE);
                #endregion

                #region Short-term marketable investments = 120
                var _120 = ReportBase.GetCodeValue(datas, "120");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Short-term marketable investments",
                    Year1 = ReportBase.GetDiv(_120.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_120.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_120.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_120.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_120.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_120.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(marketableInvestments);
                #endregion

                #region Trade debts = 131
                var _131 = ReportBase.GetCodeValue(datas, "131");
                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Trade debts",
                    Year1 = ReportBase.GetDiv(_131.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_131.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_131.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_131.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_131.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_131.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(tradeDebtor);

                #endregion

                #region Inventories = 140
                var _140 = ReportBase.GetCodeValue(datas, "140");
                var inventories = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Inventories",
                    Year1 = ReportBase.GetDiv(_140.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_140.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_140.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_140.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_140.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_140.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(inventories);
                #endregion

                #region Other Current Assets =100-110-120-131-140
                var _100 = ReportBase.GetCodeValue(datas, "100");
                var subOCA = ReportBase.GetCodeValue(datas, "110+120+131+140");
                var OCA = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Other Current Assets",
                    Year1 = ReportBase.GetDiv(_100.Year1 - subOCA.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_100.Year2 - subOCA.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_100.Year3 - subOCA.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_100.Year4 - subOCA.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_100.Year5 - subOCA.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_100.Year6 - subOCA.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(OCA);
                #endregion

                #region Property, plant, and equipment = 221+224
                var subPPAE = ReportBase.GetCodeValue(datas, "221+224");
                var PPAE = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Property, plant, and equipment",
                    Year1 = ReportBase.GetDiv(subPPAE.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(subPPAE.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(subPPAE.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(subPPAE.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(subPPAE.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(subPPAE.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(PPAE);
                #endregion

                #region Investment property = 240
                var _240 = ReportBase.GetCodeValue(datas, "240");
                var IP = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Investment property",
                    Year1 = ReportBase.GetDiv(_240.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_240.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_240.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_240.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_240.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_240.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(IP);
                #endregion

                #region Long-term investments = 250
                var _250 = ReportBase.GetCodeValue(datas, "250");
                var LTI = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Long-term investments",
                    Year1 = ReportBase.GetDiv(_250.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_250.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_250.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_250.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_250.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_250.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(LTI);
                #endregion

                #region Other Non- Current Assets  = 200-221-224-240-250
                var subONCA = ReportBase.GetCodeValue(datas, "221+224+240+250");
                var _200 = ReportBase.GetCodeValue(datas, "200");
                var ONCA = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "Other Non- Current Assets ",
                    Year1 = ReportBase.GetDiv(_200.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_200.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_200.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_200.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_200.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_200.Year6, _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(ONCA);
                #endregion

                #region Trade creditor = 312
                var _312 = ReportBase.GetCodeValue(datas, "312");
                var TC = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Trade creditor",
                    Year1 = ReportBase.GetDiv(_312.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_312.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_312.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_312.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_312.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_312.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(TC);
                #endregion

                #region Short-term loans = 311
                var _311 = ReportBase.GetCodeValue(datas, "311");
                var STL = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Short-term loans",
                    Year1 = ReportBase.GetDiv(_311.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_311.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_311.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_311.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_311.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_311.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(STL);
                #endregion

                #region Other current liabilities = 310-312-311
                var subOCL = ReportBase.GetCodeValue(datas, "312+311");
                var _310 = ReportBase.GetCodeValue(datas, "310");
                var OCL = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Other current liabilities",
                    Year1 = ReportBase.GetDiv(_310.Year1 - subOCL.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_310.Year2 - subOCL.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_310.Year3 - subOCL.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_310.Year4 - subOCL.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_310.Year5 - subOCL.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_310.Year6 - subOCL.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(OCL);
                #endregion

                #region Long-term loans  = 334
                var _334 = ReportBase.GetCodeValue(datas, "334");

                var LTL = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Long-term loans ",
                    Year1 = ReportBase.GetDiv(_334.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_334.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_334.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_334.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_334.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_334.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(LTL);
                #endregion

                #region Other non-current liabilities  = 330-334
                var _330 = ReportBase.GetCodeValue(datas, "330");
                var ONCL = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Other non-current liabilities",
                    Year1 = ReportBase.GetDiv(_330.Year1 - _334.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_330.Year2 - _334.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_330.Year3 - _334.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_330.Year4 - _334.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_330.Year5 - _334.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_330.Year6 - _334.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(ONCL);
                #endregion

                #region  Owner's investment capital  = 411
                var _411 = ReportBase.GetCodeValue(datas, "411");
                var OIC = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "Owner's investment capital",
                    Year1 = ReportBase.GetDiv(_411.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_411.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_411.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_411.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_411.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_411.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(OIC);
                #endregion

                #region Retained earnings = 420
                var _420 = ReportBase.GetCodeValue(datas, "420");
                var RE = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Retained earnings",
                    Year1 = ReportBase.GetDiv(_420.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(_420.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(_420.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(_420.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(_420.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(_420.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(RE);
                #endregion

                #region Other owner’s equity = 410-411-420+430+434
                var subOOE1 = ReportBase.GetCodeValue(datas, "410+430+434");
                var subOOE2 = ReportBase.GetCodeValue(datas, "411+420");
                var OOE = new ReportSummaryDTO
                {
                    Index = 8,
                    Name = "Other owner’s equity",
                    Year1 = ReportBase.GetDiv(subOOE1.Year1 - subOOE2.Year1, _270.Year1),
                    Year2 = ReportBase.GetDiv(subOOE1.Year2 - subOOE2.Year2, _270.Year2),
                    Year3 = ReportBase.GetDiv(subOOE1.Year3 - subOOE2.Year3, _270.Year3),
                    Year4 = ReportBase.GetDiv(subOOE1.Year4 - subOOE2.Year4, _270.Year4),
                    Year5 = ReportBase.GetDiv(subOOE1.Year5 - subOOE2.Year5, _270.Year5),
                    Year6 = ReportBase.GetDiv(subOOE1.Year6 - subOOE2.Year6, _270.Year6),
                    IndustryAverageGroupId = 2
                };
                res.Add(OOE);
                #endregion

                #region  Net Sales
                var netSales = GetNetSales(datas);
                netSales.Index = 1;
                netSales.IndustryAverageGroupId = 3;
                res.Add(netSales);
                #endregion

                #region Gross profit
                var p20 = ReportBase.GetCodeValue(datas, "P20");
                var grossProfit = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Gross profit",
                    Year1 = p20.Year1,
                    Year2 = p20.Year2,
                    Year3 = p20.Year3,
                    Year4 = p20.Year4,
                    Year5 = p20.Year5,
                    Year6 = p20.Year6,
                    IndustryAverageGroupId = 3
                };
                res.Add(grossProfit);
                #endregion

                #region EBITDA
                var ebitda = GetEBITDA(datas);
                ebitda.Index = 3;
                ebitda.IndustryAverageGroupId = 3;
                res.Add(ebitda);
                #endregion

                #region EBIT

                var p10 = ReportBase.GetCodeValue(datas, "P10");
                var editData = ReportBase.GetCodeValue(datas, "P50+P23");
                var edit = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "EBIT",
                    Year1 = ReportBase.GetDiv(editData.Year1, p10.Year1),
                    Year2 = ReportBase.GetDiv(editData.Year2, p10.Year2),
                    Year3 = ReportBase.GetDiv(editData.Year3, p10.Year3),
                    Year4 = ReportBase.GetDiv(editData.Year4, p10.Year4),
                    Year5 = ReportBase.GetDiv(editData.Year5, p10.Year5),
                    Year6 = ReportBase.GetDiv(editData.Year6, p10.Year6),
                    IndustryAverageGroupId = 3
                };
                res.Add(edit);
                #endregion

                #region EBT
                var getListIncome = GetIncomeStatement(datas);
                var ebt = getListIncome.Where(x => x.Name.Equals("EBT")).FirstOrDefault();
                if (ebt != null)
                {
                    var ebt1 = ReportBase.GetDiv(ebt.Year1, p10.Year1);
                    var ebt2 = ReportBase.GetDiv(ebt.Year2, p10.Year2);
                    var ebt3 = ReportBase.GetDiv(ebt.Year3, p10.Year3);
                    var ebt4 = ReportBase.GetDiv(ebt.Year4, p10.Year4);
                    var ebt5 = ReportBase.GetDiv(ebt.Year5, p10.Year5);
                    var ebt6 = ReportBase.GetDiv(ebt.Year6, p10.Year6);
                    ebt.Year1 = ebt1;
                    ebt.Year2 = ebt2;
                    ebt.Year3 = ebt3;
                    ebt.Year4 = ebt4;
                    ebt.Year5 = ebt5;
                    ebt.Year6 = ebt6;

                    ebt.Index = 5;
                    ebt.IndustryAverageGroupId = 3;
                    res.Add(ebt);
                }
                #endregion

                #region Net Income
                var netIncome = GetNetIncome(datas);
                var netIncome1 = ReportBase.GetDiv(netIncome.Year1, p10.Year1);
                var netIncome2 = ReportBase.GetDiv(netIncome.Year2, p10.Year2);
                var netIncome3 = ReportBase.GetDiv(netIncome.Year3, p10.Year3);
                var netIncome4 = ReportBase.GetDiv(netIncome.Year4, p10.Year4);
                var netIncome5 = ReportBase.GetDiv(netIncome.Year5, p10.Year5);
                var netIncome6 = ReportBase.GetDiv(netIncome.Year6, p10.Year6);
                netIncome.Year1 = netIncome1;
                netIncome.Year2 = netIncome2;
                netIncome.Year3 = netIncome3;
                netIncome.Year4 = netIncome4;
                netIncome.Year5 = netIncome5;
                netIncome.Year6 = netIncome6;
                netIncome.Index = 6;
                netIncome.IndustryAverageGroupId = 3;
                res.Add(netIncome);
                #endregion

                #region Net Cash Flow
                //'=if((50+C24)<>0,(50+C24),"-")
                var ncf = ReportBase.GetCodeValue(datas, "50+C24");
                var ncfModel = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Net Cash Flow",
                    Year1 = ncf.Year1 == 0 ? null : ncf.Year1,
                    Year2 = ncf.Year2 == 0 ? null : ncf.Year2,
                    Year3 = ncf.Year3 == 0 ? null : ncf.Year3,
                    Year4 = ncf.Year4 == 0 ? null : ncf.Year4,
                    Year5 = ncf.Year5 == 0 ? null : ncf.Year5,
                    Year6 = ncf.Year6 == 0 ? null : ncf.Year6,
                    IndustryAverageGroupId = 4
                };
                res.Add(ncfModel);
                #endregion

                #region CFF (Net cash flows from financing activities)
                // '=if((40+C23)<>0,(40+C23),"-")
                var cff = ReportBase.GetCodeValue(datas, "40+C23");
                var cffModel = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "CFF (Net cash flows from financing activities)",
                    Year1 = cff.Year1 == 0 ? null : cff.Year1,
                    Year2 = cff.Year2 == 0 ? null : cff.Year2,
                    Year3 = cff.Year3 == 0 ? null : cff.Year3,
                    Year4 = cff.Year4 == 0 ? null : cff.Year4,
                    Year5 = cff.Year5 == 0 ? null : cff.Year5,
                    Year6 = cff.Year6 == 0 ? null : cff.Year6,
                    IndustryAverageGroupId = 4
                };
                res.Add(cffModel);
                #endregion

                #region CFI (Net cash flows from investing activities)
                // '=if((30+C16)<>0,(30+C16),"-")
                var cfi = ReportBase.GetCodeValue(datas, "30+C16");
                var cfiModel = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "CFI (Net cash flows from investing activities)",
                    Year1 = cfi.Year1 == 0 ? null : cfi.Year1,
                    Year2 = cfi.Year2 == 0 ? null : cfi.Year2,
                    Year3 = cfi.Year3 == 0 ? null : cfi.Year3,
                    Year4 = cfi.Year4 == 0 ? null : cfi.Year4,
                    Year5 = cfi.Year5 == 0 ? null : cfi.Year5,
                    Year6 = cfi.Year6 == 0 ? null : cfi.Year6,
                    IndustryAverageGroupId = 4
                };
                res.Add(cfiModel);
                #endregion

                #region CFO (Net cash flows from operating activities)    
                // '=IF(('20+C8)<>0,('20+C8),"-")
                var cfo = ReportBase.GetCodeValue(datas, "20+C8");
                var cfoModel = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "CFO (Net cash flows from operating activities)",
                    Year1 = cfo.Year1 == 0 ? null : cfo.Year1,
                    Year2 = cfo.Year2 == 0 ? null : cfo.Year2,
                    Year3 = cfo.Year3 == 0 ? null : cfo.Year3,
                    Year4 = cfo.Year4 == 0 ? null : cfo.Year4,
                    Year5 = cfo.Year5 == 0 ? null : cfo.Year5,
                    Year6 = cfo.Year6 == 0 ? null : cfo.Year6,
                    IndustryAverageGroupId = 4
                };
                res.Add(cfoModel);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ReportSummaryDTO> GetDataMargin(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Gross profit
                //(P20)
                var p20 = ReportBase.GetCodeValue(datas, "P20");
                var grossProfit = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Gross profit",
                    Year1 = p20.Year1,
                    Year2 = p20.Year2,
                    Year3 = p20.Year3,
                    Year4 = p20.Year4,
                    Year5 = p20.Year5,
                    Year6 = p20.Year6,
                    IndustryAverageGroupId = 1
                };
                res.Add(grossProfit);
                #endregion

                #region Gross Profit Margin (%)
                var grossProfitMargin = GetGrossProfitMargin(datas);
                if (grossProfitMargin != null)
                {
                    grossProfitMargin.Index = 2;
                    grossProfitMargin.IndustryAverageGroupId = 1;
                    res.Add(grossProfitMargin);
                }
                #endregion

                #region Operating Income
                var gaExpenses = ReportBase.GetCodeValue(datas, "P24+P25");
                var operatingIncome = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Operating Income",
                    Year1 = p20.Year1 - gaExpenses.Year1,
                    Year2 = p20.Year2 - gaExpenses.Year2,
                    Year3 = p20.Year3 - gaExpenses.Year3,
                    Year4 = p20.Year4 - gaExpenses.Year4,
                    Year5 = p20.Year5 - gaExpenses.Year5,
                    Year6 = p20.Year6 - gaExpenses.Year6,
                    IndustryAverageGroupId = 2
                };
                res.Add(operatingIncome);
                #endregion

                #region Operating Income Margin (%) = ((P20-P24-P25)/P10)*100
                var P24P25 = ReportBase.GetCodeValue(datas, "P24+P25");
                var p10 = ReportBase.GetCodeValue(datas, "P10");
                var CurrentAssets = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Operating Income Margin (%)",
                    Year1 = ReportBase.GetDiv(p20.Year1 - P24P25.Year1, p10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(p20.Year2 - P24P25.Year2, p10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(p20.Year3 - P24P25.Year3, p10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(p20.Year4 - P24P25.Year4, p10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(p20.Year5 - P24P25.Year5, p10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(p20.Year6 - P24P25.Year6, p10.Year6) * 100,
                    AverageIndustryCode = "D22",
                    IndustryAverageGroupId = 2
                };
                res.Add(CurrentAssets);
                #endregion

                #region EBIT
                var editData = ReportBase.GetCodeValue(datas, "P50+P23");
                var edit = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "EBIT",
                    Year1 = editData.Year1,
                    Year2 = editData.Year2,
                    Year3 = editData.Year3,
                    Year4 = editData.Year4,
                    Year5 = editData.Year5,
                    Year6 = editData.Year6,
                    IndustryAverageGroupId = 3
                };
                res.Add(edit);
                #endregion

                #region  EBIT Margin (%)
                var eBITMargin = GetEBITMargin(datas);
                if (eBITMargin != null)
                {
                    eBITMargin.Index = 6;
                    eBITMargin.IndustryAverageGroupId = 3;
                    res.Add(eBITMargin);
                }
                #endregion

                #region EBT
                var financialIncomeData = ReportBase.GetCodeValue(datas, "P21+P53");
                var p22 = ReportBase.GetCodeValue(datas, "P22");
                var p40 = ReportBase.GetCodeValue(datas, "P40");
                var ebt = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "EBT",
                    Year1 = p20.Year1 - gaExpenses.Year1 + financialIncomeData.Year1 - p22.Year1 + p40.Year1,
                    Year2 = p20.Year2 - gaExpenses.Year2 + financialIncomeData.Year2 - p22.Year2 + p40.Year2,
                    Year3 = p20.Year3 - gaExpenses.Year3 + financialIncomeData.Year3 - p22.Year3 + p40.Year3,
                    Year4 = p20.Year4 - gaExpenses.Year4 + financialIncomeData.Year4 - p22.Year4 + p40.Year4,
                    Year5 = p20.Year5 - gaExpenses.Year5 + financialIncomeData.Year5 - p22.Year5 + p40.Year5,
                    Year6 = p20.Year6 - gaExpenses.Year6 + financialIncomeData.Year6 - p22.Year6 + p40.Year6,
                    IndustryAverageGroupId = 4
                };
                res.Add(ebt);
                #endregion

                #region  EBT Margin (%)
                var ebtMargin = GetEBTMargin(datas);
                if (ebtMargin != null)
                {
                    ebtMargin.Index = 8;
                    ebtMargin.IndustryAverageGroupId = 4;
                    res.Add(ebtMargin);
                }
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ReportSummaryDTO> GetDupontAnalysis(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Tax burden (PAT/PBT) = P60/P50
                var P60 = ReportBase.GetCodeValue(datas, "P60");
                var P50 = ReportBase.GetCodeValue(datas, "P50");
                var CAE = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Tax burden (PAT/PBT)",
                    Year1 = ReportBase.GetDiv(P60.Year1, P50.Year1),
                    Year2 = ReportBase.GetDiv(P60.Year2, P50.Year2),
                    Year3 = ReportBase.GetDiv(P60.Year3, P50.Year3),
                    Year4 = ReportBase.GetDiv(P60.Year4, P50.Year4),
                    Year5 = ReportBase.GetDiv(P60.Year5, P50.Year5),
                    Year6 = ReportBase.GetDiv(P60.Year6, P50.Year6),
                };
                res.Add(CAE);
                #endregion

                #region Interest expense rate (EBT/EBIT) = P50/(P50+P23)
                var P50P23 = ReportBase.GetCodeValue(datas, "P50+P23");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Interest expense rate (EBT/EBIT)",
                    Year1 = ReportBase.GetDiv(P50.Year1, P50P23.Year1),
                    Year2 = ReportBase.GetDiv(P50.Year2, P50P23.Year2),
                    Year3 = ReportBase.GetDiv(P50.Year3, P50P23.Year3),
                    Year4 = ReportBase.GetDiv(P50.Year4, P50P23.Year4),
                    Year5 = ReportBase.GetDiv(P50.Year5, P50P23.Year5),
                    Year6 = ReportBase.GetDiv(P50.Year6, P50P23.Year6),
                };
                res.Add(marketableInvestments);
                #endregion

                #region Operating profit margin (EBIT/Net Sales) (%) = ((P50+P23)/P10)*100
                var P10 = ReportBase.GetCodeValue(datas, "P10");
                var tradeDebtor = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Operating profit margin (EBIT/Net Sales) (%)",
                    Year1 = ReportBase.GetDiv(P50P23.Year1, P10.Year1) * 100,
                    Year2 = ReportBase.GetDiv(P50P23.Year2, P10.Year2) * 100,
                    Year3 = ReportBase.GetDiv(P50P23.Year3, P10.Year3) * 100,
                    Year4 = ReportBase.GetDiv(P50P23.Year4, P10.Year4) * 100,
                    Year5 = ReportBase.GetDiv(P50P23.Year5, P10.Year5) * 100,
                    Year6 = ReportBase.GetDiv(P50P23.Year6, P10.Year6) * 100,
                };
                res.Add(tradeDebtor);

                #endregion

                #region Assets turnover (Net Sales/Avg. Assets) = =IF(DK270>0,P10/((CK270+DK270)/2),P10/CK270)
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var inventories = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = "Assets turnover (Net Sales/Avg. Assets)",
                    Year1 = ReportBase.GetDiv(P10.Year1, _270.Year2 > 0 ? (_270.Year1 + _270.Year2) / 2 : _270.Year1),
                    Year2 = ReportBase.GetDiv(P10.Year2, _270.Year3 > 0 ? (_270.Year2 + _270.Year3) / 2 : _270.Year2),
                    Year3 = ReportBase.GetDiv(P10.Year3, _270.Year4 > 0 ? (_270.Year3 + _270.Year4) / 2 : _270.Year3),
                    Year4 = ReportBase.GetDiv(P10.Year4, _270.Year5 > 0 ? (_270.Year4 + _270.Year5) / 2 : _270.Year4),
                    Year5 = ReportBase.GetDiv(P10.Year5, _270.Year6 > 0 ? (_270.Year5 + _270.Year6) / 2 : _270.Year5),
                    Year6 = ReportBase.GetDiv(P10.Year6, _270.Year7 > 0 ? (_270.Year6 + _270.Year7) / 2 : _270.Year6),
                    IndustryAverageGroupId = 1
                };
                res.Add(inventories);
                #endregion

                #region Financial Leverage (Avg. Assets/Avg Equity) = IF(DK270>0,((CK270+DK270)/2)/((CK400+DK400)/2),CK270/CK400)*100
                var _400 = ReportBase.GetCodeValue(datas, "400");
                var subOCA = ReportBase.GetCodeValue(datas, "110+120+131+140");
                var OCA = new ReportSummaryDTO
                {
                    Index = 5,
                    Name = "Financial Leverage (Avg. Assets/Avg Equity)",
                    Year1 = _270.Year2 > 0 ? ReportBase.GetDiv(_270.Year1 + _270.Year2, _400.Year1 + _400.Year2) : ReportBase.GetDiv(_270.Year1, _400.Year1),
                    Year2 = _270.Year3 > 0 ? ReportBase.GetDiv(_270.Year2 + _270.Year3, _400.Year2 + _400.Year3) : ReportBase.GetDiv(_270.Year2, _400.Year2),
                    Year3 = _270.Year4 > 0 ? ReportBase.GetDiv(_270.Year3 + _270.Year4, _400.Year3 + _400.Year4) : ReportBase.GetDiv(_270.Year3, _400.Year3),
                    Year4 = _270.Year5 > 0 ? ReportBase.GetDiv(_270.Year4 + _270.Year5, _400.Year4 + _400.Year5) : ReportBase.GetDiv(_270.Year4, _400.Year4),
                    Year5 = _270.Year6 > 0 ? ReportBase.GetDiv(_270.Year5 + _270.Year6, _400.Year5 + _400.Year6) : ReportBase.GetDiv(_270.Year5, _400.Year5),
                    Year6 = _270.Year7 > 0 ? ReportBase.GetDiv(_270.Year6 + _270.Year7, _400.Year6 + _400.Year7) : ReportBase.GetDiv(_270.Year6, _400.Year6),
                };
                res.Add(OCA);
                #endregion

                #region ROE (%)=(C91*C92*C93*C94*C95)*100
                var ROE = new ReportSummaryDTO
                {
                    Index = 6,
                    Name = "ROE (%)",
                    Year1 = CAE.Year1 * marketableInvestments.Year1 * tradeDebtor.Year1 * inventories.Year1 * OCA.Year1 * 100,
                    Year2 = CAE.Year1 * marketableInvestments.Year2 * tradeDebtor.Year2 * inventories.Year2 * OCA.Year2 * 100,
                    Year3 = CAE.Year1 * marketableInvestments.Year3 * tradeDebtor.Year3 * inventories.Year3 * OCA.Year3 * 100,
                    Year4 = CAE.Year1 * marketableInvestments.Year4 * tradeDebtor.Year4 * inventories.Year4 * OCA.Year4 * 100,
                    Year5 = CAE.Year1 * marketableInvestments.Year5 * tradeDebtor.Year5 * inventories.Year5 * OCA.Year5 * 100,
                    Year6 = CAE.Year1 * marketableInvestments.Year6 * tradeDebtor.Year6 * inventories.Year6 * OCA.Year6 * 100,
                };
                res.Add(ROE);
                #endregion


                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<ReportSummaryDTO> GetTop10Companies(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                var BalanceSheet = GetBalanceSheet(datas);
                var IncomeStatement = GetIncomeStatement(datas);
                var Other = GetOther(datas);
                var LiquidityRatios = GetLiquidityRatios(datas);
                var ActivityRatios = GetActivityRatios(datas);
                var CreditRatios = GetCreditRatios(datas);
                var CoverageRatios = GetCoverageRatios(datas);
                var SolvencyRatios = GetSolvencyRatios(datas);
                var ReturnOnNetSales = GetReturnOnNetSales(datas);
                var ReturnOnInvestme = GetReturnOnInvestment(datas);
                var Performance = GetPerformance(datas);
                var GrowthRateRatios = GetGrowthRateRatios(datas);
                res.AddRange(BalanceSheet);
                res.AddRange(IncomeStatement);
                res.AddRange(Other);
                res.AddRange(LiquidityRatios);
                res.AddRange(ActivityRatios);
                res.AddRange(CreditRatios);
                res.AddRange(CoverageRatios);
                res.AddRange(SolvencyRatios);
                res.AddRange(ReturnOnNetSales);
                res.AddRange(ReturnOnInvestme);
                res.AddRange(Performance);
                res.AddRange(GrowthRateRatios);
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ReportSummaryDTO> GetOther(List<ReportsDTO> datas)
        {
            try
            {
                var res = new List<ReportSummaryDTO>();

                #region Operating Assets =270-(110+120+426+135+428+159+158+444+218+230+240+245+250+268+448+269)
                var _270 = ReportBase.GetCodeValue(datas, "270");
                var sub = ReportBase.GetCodeValue(datas, "110+120+426+135+428+159+158+444+218+230+240+245+250+268+448+269");
                var CAE = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Operating Assets",
                    Year1 = _270.Year1 - sub.Year1,
                    Year2 = _270.Year2 - sub.Year2,
                    Year3 = _270.Year3 - sub.Year3,
                    Year4 = _270.Year4 - sub.Year4,
                    Year5 = _270.Year5 - sub.Year5,
                    Year6 = _270.Year6 - sub.Year6,
                };
                res.Add(CAE);
                #endregion

                #region Non-Operating Assets =110+120+426+135+428+159+158+444+218+230+240+245+250+268+448+269
                var _120 = ReportBase.GetCodeValue(datas, "120");
                var marketableInvestments = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "Non-Operating Assets",
                    Year1 = sub.Year1,
                    Year2 = sub.Year2,
                    Year3 = sub.Year3,
                    Year4 = sub.Year4,
                    Year5 = sub.Year5,
                    Year6 = sub.Year6,
                };
                res.Add(marketableInvestments);
                #endregion

                #region Working Capital = 100-310
                var tradeDebtor = GetWorkingCapital(datas);
                res.Add(tradeDebtor);

                #endregion

                #region Interest Bearing Debts
                var IBDs = GetInterestBearingDebts(datas);

                res.Add(IBDs);
                #endregion

                #region Tangible Net Worth
                var OCA = GetTangibleNetWorth(datas);
                res.Add(OCA);
                #endregion

                #region Operating Liabilities
                var PPAE = GetOperatingLiabilities(datas);
                res.Add(PPAE);
                #endregion

                #region  Non-Operating Liabilities = 319+311+324+333+334+342+344+321
                var subNon = ReportBase.GetCodeValue(datas, "319+311+324+333+334+342+344+321");
                var IP = new ReportSummaryDTO
                {
                    Index = 7,
                    Name = "Non-Operating Liabilities",
                    Year1 = subNon.Year1,
                    Year2 = subNon.Year2,
                    Year3 = subNon.Year3,
                    Year4 = subNon.Year4,
                    Year5 = subNon.Year5,
                    Year6 = subNon.Year6,
                };
                res.Add(IP);
                #endregion

                #region CAPEX (Capital expendicture)
                var LTI = GetCAPEX(datas);
                res.Add(LTI);
                #endregion

                #region region CFO (Net cash flows from operating activities) =IF(('20+C8)<>0,('20+C8),"-")
                var _20 = ReportBase.GetCodeValue(datas, "20");
                var C8 = ReportBase.GetCodeValue(datas, "C8");
                var ONCA = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "CFO (Net cash flows from operating activities)",
                    Year1 = _20.Year1 + C8.Year1 != 0 ? _20.Year1 + C8.Year1 : null,
                    Year2 = _20.Year2 + C8.Year2 != 0 ? _20.Year2 + C8.Year2 : null,
                    Year3 = _20.Year3 + C8.Year3 != 0 ? _20.Year3 + C8.Year3 : null,
                    Year4 = _20.Year4 + C8.Year4 != 0 ? _20.Year4 + C8.Year4 : null,
                    Year5 = _20.Year5 + C8.Year5 != 0 ? _20.Year5 + C8.Year5 : null,
                    Year6 = _20.Year6 + C8.Year6 != 0 ? _20.Year6 + C8.Year6 : null,
                };
                res.Add(ONCA);
                #endregion

                #region CFI (Net cash flows from investing activities) =if((30+C16)<>0,(30+C16),"-")
                var _30 = ReportBase.GetCodeValue(datas, "30");
                var C16 = ReportBase.GetCodeValue(datas, "C16");
                var CFI = new ReportSummaryDTO
                {
                    Index = 9,
                    Name = "CFI (Net cash flows from investing activities)",
                    Year1 = _30.Year1 + C16.Year1 != 0 ? _30.Year1 + C16.Year1 : null,
                    Year2 = _30.Year2 + C16.Year2 != 0 ? _30.Year2 + C16.Year2 : null,
                    Year3 = _30.Year3 + C16.Year3 != 0 ? _30.Year3 + C16.Year3 : null,
                    Year4 = _30.Year4 + C16.Year4 != 0 ? _30.Year4 + C16.Year4 : null,
                    Year5 = _30.Year5 + C16.Year5 != 0 ? _30.Year5 + C16.Year5 : null,
                    Year6 = _30.Year6 + C16.Year6 != 0 ? _30.Year6 + C16.Year6 : null,
                };
                res.Add(CFI);
                #endregion

                #region CFF (Net cash flows from financing activities) =if((40+C23)<>0,(40+C23),"-")
                var _40 = ReportBase.GetCodeValue(datas, "40");
                var C23 = ReportBase.GetCodeValue(datas, "C23");
                var STL = new ReportSummaryDTO
                {
                    Index = 2,
                    Name = "CFF (Net cash flows from financing activities)",
                    Year1 = _40.Year1 + C23.Year1 != 0 ? _40.Year1 + C23.Year1 : null,
                    Year2 = _40.Year2 + C23.Year2 != 0 ? _40.Year2 + C23.Year2 : null,
                    Year3 = _40.Year3 + C23.Year3 != 0 ? _40.Year3 + C23.Year3 : null,
                    Year4 = _40.Year4 + C23.Year4 != 0 ? _40.Year4 + C23.Year4 : null,
                    Year5 = _40.Year5 + C23.Year5 != 0 ? _40.Year5 + C23.Year5 : null,
                    Year6 = _40.Year6 + C23.Year6 != 0 ? _40.Year6 + C23.Year6 : null,
                };
                res.Add(STL);
                #endregion

                #region Net Cash Flow =if((50+C24)<>0,(50+C24),"-")
                var _50 = ReportBase.GetCodeValue(datas, "50");
                var C24 = ReportBase.GetCodeValue(datas, "C24");
                var OCL = new ReportSummaryDTO
                {
                    Index = 3,
                    Name = "Net Cash Flow",
                    Year1 = _50.Year1 + C24.Year1 != 0 ? _50.Year1 + C24.Year1 : null,
                    Year2 = _50.Year2 + C24.Year2 != 0 ? _50.Year2 + C24.Year2 : null,
                    Year3 = _50.Year3 + C24.Year3 != 0 ? _50.Year3 + C24.Year3 : null,
                    Year4 = _50.Year4 + C24.Year4 != 0 ? _50.Year4 + C24.Year4 : null,
                    Year5 = _50.Year5 + C24.Year5 != 0 ? _50.Year5 + C24.Year5 : null,
                    Year6 = _50.Year6 + C24.Year6 != 0 ? _50.Year6 + C24.Year6 : null,
                };
                res.Add(OCL);
                #endregion

                #region Daily Cash Operating Expenses  = (P11+P24+P25-IF(2>0,2,IF(((DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242))>0,(DK223+DK226+DK229+DK242)-(CK223+CK226+CK229+CK242),0)))/365
                var P11P24P25 = ReportBase.GetCodeValue(datas, "P11+P24+P25");
                var DaA = GetDepreciationAndAmortization(datas);

                var LTL = new ReportSummaryDTO
                {
                    Index = 4,
                    Name = " Daily Cash Operating Expenses",
                    Year1 = (P11P24P25.Year1 - DaA.Year1) / 365,
                    Year2 = (P11P24P25.Year2 - DaA.Year2) / 365,
                    Year3 = (P11P24P25.Year3 - DaA.Year3) / 365,
                    Year4 = (P11P24P25.Year4 - DaA.Year4) / 365,
                    Year5 = (P11P24P25.Year5 - DaA.Year5) / 365,
                    Year6 = (P11P24P25.Year6 - DaA.Year6) / 365,
                };
                res.Add(LTL);
                #endregion

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}