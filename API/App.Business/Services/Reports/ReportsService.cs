﻿using App.Common.Base;
using App.Data.Models.VCIS;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{

    public class ReportsService : IReportsService
    {
        private VCIS4Context _vcisContext;
        public IConfiguration Configuration { get; }
        public IBusinessService _businessService { get; }
        public ReportsService(VCIS4Context vcisContext, IConfiguration configuration, IBusinessService businessService)
        {
            _vcisContext = vcisContext;
            Configuration = configuration;
            _businessService = businessService;
        }

        public ResualtFSData GetFSData(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var res = new List<ReportsDTO>();
                var proc = AppConst.Proc_FSData;

                var FS = GetFS(bid, FSYear, Quarter, Type);
                var id1 = FS.FS1! != null ? FS.FS1.Id : 0;
                var id2 = FS.FS2! != null ? FS.FS2.Id : 0;
                var id3 = FS.FS3! != null ? FS.FS3.Id : 0;
                var id4 = FS.FS4! != null ? FS.FS4.Id : 0;
                var id5 = FS.FS5! != null ? FS.FS5.Id : 0;
                var id6 = FS.FS6! != null ? FS.FS6.Id : 0;

                List<SqlParameter> parms = new List<SqlParameter>
                {
                    new SqlParameter("@id1",id1),
                    new SqlParameter("@id2",id2),
                    new SqlParameter("@id3",id3),
                    new SqlParameter("@id4",id4),
                    new SqlParameter("@id5",id5),
                    new SqlParameter("@id6",id6),
                    new SqlParameter("@type",Type)
                };

                using (var command = _vcisContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = proc;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parms.ToArray());
                    _vcisContext.Database.OpenConnection();
                    var result = command.ExecuteReader();
                    while (result.Read())
                    {
                        res.Add(new ReportsDTO
                        {
                            AccountCode = result.GetString("AccountCode"),
                            Year1 = result.IsDBNull(3) ? null : result.GetDecimal(3),
                            Year2 = result.IsDBNull(4) ? null : result.GetDecimal(4),
                            Year3 = result.IsDBNull(5) ? null : result.GetDecimal(5),
                            Year4 = result.IsDBNull(6) ? null : result.GetDecimal(6),
                            Year5 = result.IsDBNull(7) ? null : result.GetDecimal(7),
                            Year6 = result.IsDBNull(8) ? null : result.GetDecimal(8),
                            Name = result.IsDBNull("Name") ? null : result.GetString("Name"),
                            Category = result.IsDBNull("Category") ? null : result.GetString("Category"),
                            Report = result.IsDBNull("Report") ? null : result.GetString("Report"),
                            Level = result.IsDBNull("Level") ? null : result.GetByte("Level"),
                            Parent = result.IsDBNull("Parent") ? null : result.GetString("Parent"),
                        });
                    }
                    command.Connection.Close();
                }

                return new ResualtFSData
                {
                    datas = res,
                    fs = FS
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReportSummaryDTO> GetReportSummary(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var res = new List<ReportSummaryDTO>();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;

                #region Total Assets 
                var TotalAssets = BusinessReportsService.GetTotalAssets(datas);
                res.Add(TotalAssets);

                #endregion

                #region Operating Assets 
                var OperatingAssets = BusinessReportsService.GetOperatingAssets(datas);
                res.Add(OperatingAssets);


                #endregion

                #region Owner's Equity
                var OwnerEquity = BusinessReportsService.GetOwnerEquity(datas);
                res.Add(OwnerEquity);
                #endregion

                #region Net Sales
                var NetSales = BusinessReportsService.GetNetSales(datas);
                res.Add(NetSales);
                #endregion

                #region Net Income
                var NetIncome = BusinessReportsService.GetNetIncome(datas);
                res.Add(NetIncome);
                #endregion

                #region Operating Liabilities
                var OperatingLiabilities = BusinessReportsService.GetOperatingLiabilities(datas);
                res.Add(OperatingLiabilities);
                #endregion

                #region Interest Bearing Debts
                var InterestBearingDebts = BusinessReportsService.GetInterestBearingDebts(datas);
                res.Add(InterestBearingDebts);
                #endregion

                #region Tangible Net Worth 
                var TangibleNetWorth = BusinessReportsService.GetTangibleNetWorth(datas);
                res.Add(TangibleNetWorth);

                #endregion

                #region Working Capital
                var WorkingCapital = BusinessReportsService.GetWorkingCapital(datas);
                res.Add(WorkingCapital);

                #endregion

                #region EBITDA 
                var EBITDA = BusinessReportsService.GetEBITDA(datas);
                res.Add(EBITDA);

                #endregion

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResualReportDT0 GetSummaryFSOverView(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var res = new List<ReportSummaryDTO>();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;

                #region Tangible Net Worth 
                var TangibleNetWorth = BusinessReportsService.GetTangibleNetWorth(datas);
                res.Add(TangibleNetWorth);
                #endregion

                #region Net Sales
                var NetSales = BusinessReportsService.GetNetSales(datas);
                res.Add(NetSales);
                #endregion

                #region Working Capital
                var WorkingCapital = BusinessReportsService.GetWorkingCapital(datas);
                res.Add(WorkingCapital);

                var GrossWorkingCapital = BusinessReportsService.GetGrossWorkingCapital(datas);
                res.Add(GrossWorkingCapital);

                #endregion

                #region DebtToEquity 
                var DebtToEquity = BusinessReportsService.GetDebtToEquity(datas);
                res.Add(DebtToEquity);
                #endregion

                #region GetDebtToAssets 
                var DebtToAssets = BusinessReportsService.GetDebtToAssets(datas);
                res.Add(DebtToAssets);
                #endregion

                #region DebtCoverage 
                var DebtCoverage = BusinessReportsService.GetDebtCoverage(datas);
                res.Add(DebtCoverage);
                #endregion

                #region CurrentRatio 
                var CurrentRatio = BusinessReportsService.GetCurrentRatio(datas);
                res.Add(CurrentRatio);
                #endregion

                #region ROA 
                var ROA = BusinessReportsService.GetROA(datas);
                res.Add(ROA);
                #endregion

                #region ROE 
                var ROE = BusinessReportsService.GetROE(datas);
                res.Add(ROE);
                #endregion

                #region InterestBearingDebts 
                var InterestBearingDebts = BusinessReportsService.GetInterestBearingDebts(datas);
                res.Add(InterestBearingDebts);
                #endregion

                #region NetIncomeMargin 
                var NetIncomeMargin = BusinessReportsService.GetNetIncomeMargin(datas);
                res.Add(NetIncomeMargin);
                #endregion

                #region EBTMargin 
                var EBTMargin = BusinessReportsService.GetEBTMargin(datas);
                res.Add(EBTMargin);
                #endregion

                #region EBITMargin 
                var EBITMargin = BusinessReportsService.GetEBITMargin(datas);
                res.Add(EBITMargin);
                #endregion

                #region GrossProfitMargin 
                var GrossProfitMargin = BusinessReportsService.GetGrossProfitMargin(datas);
                res.Add(GrossProfitMargin);
                #endregion

                #region Account 
                var ReceivableAccount = BusinessReportsService.GetReceivableAccount(datas);
                res.Add(ReceivableAccount);

                var PayableAccount = BusinessReportsService.GetPayableAccount(datas);
                res.Add(PayableAccount);
                #endregion

                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion

                var fs = FS.fs;
                var header = ReportBase.GetHeader(fs, Type, false, false, false, false, true);

                var resualt = new ResualReportDT0();
                resualt.data = res;
                resualt.header = header;

                return resualt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResualReportDT0 GetBalanceSheet(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetBalanceSheet(datas);
                var header = ReportBase.GetHeader(fs, Type, false, true);

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResualReportDT0 GetIncomeStatement(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetIncomeStatement(datas);
                var header = ReportBase.GetHeader(fs, Type, true, true, true, true, false, "PL");

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetCashflowDirect(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                //Direct method
                var datas = FS.datas.Where(x => x.Category == AppConst.CategoryCashFlowDirect && x.Report == AppConst.ReportCashFlowDirect)
                    .Select(y => new ReportSummaryDTO
                    {
                        Name = y.Name,
                        Year1 = y.Year1,
                        Year2 = y.Year2,
                        Year3 = y.Year3,
                        Year4 = y.Year4,
                        Year5 = y.Year5,
                        Parent = y.Parent,
                        Level = y.Level

                    }).ToList();
                var fs = FS.fs;
                var header = ReportBase.GetHeader(fs, Type, true, true, true, true, false, "CF");

                resualt.data = datas;
                resualt.header = header;
                return resualt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResualReportDT0 GetCashflowInDirect(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                //Direct method
                var FinancingActivities = datas.Where(x => x.Category == AppConst.CategoryFinancingActivities && x.Report == AppConst.ReportCashflow).ToList();
                var InvestingActivities = datas.Where(x => x.Category == AppConst.CategoryInvestingActivities && x.Report == AppConst.ReportCashflow).ToList();
                var OperatingActivities = datas.Where(x => x.Category == AppConst.CategoryOperatingActivities && x.Report == AppConst.ReportCashflow).ToList();
                var Summaries = datas.Where(x => x.Category == AppConst.CategorySummary && x.Report == AppConst.ReportCashflow).ToList();

                List<ReportsDTO> lst = new List<ReportsDTO>();

                if (OperatingActivities.Count > 0)
                {
                    lst.Add(new ReportsDTO { Name = "I. Cash flows from operating activities", Level = 1 });
                    if (OperatingActivities.Any(x => x.AccountCode == "1"))
                    {
                        OperatingActivities.Add(new ReportsDTO());
                        var item = OperatingActivities.FirstOrDefault(x => x.AccountCode == "1");
                        var index = OperatingActivities.IndexOf(item);
                        var pos = index + 1;

                        for (var i = OperatingActivities.Count - 1; i >= pos; i--)
                        {
                            OperatingActivities[i] = OperatingActivities[i - 1];
                        }
                        OperatingActivities[pos] = new ReportsDTO { Name = "2. Adjustments", Level = 2 };
                    }
                    lst.AddRange(OperatingActivities);
                }
                if (InvestingActivities.Count > 0)
                {
                    lst.Add(new ReportsDTO { Name = "II. Cash flows from investing activities", Level = 1 });
                    lst.AddRange(InvestingActivities);
                }
                if (FinancingActivities.Count > 0)
                {
                    lst.Add(new ReportsDTO { Name = "III. Cash flows from financing activities", Level = 1 });
                    lst.AddRange(FinancingActivities);
                }
                if (Summaries.Count > 0)
                {
                    lst.AddRange(Summaries);
                }
                var res = new List<ReportSummaryDTO>();
                var indirect = lst.Select(y => new ReportSummaryDTO
                {
                    Name = y.Name,
                    Year1 = y.Year1,
                    Year2 = y.Year2,
                    Year3 = y.Year3,
                    Year4 = y.Year4,
                    Year5 = y.Year5,
                    Parent = y.Parent,
                    Level = y.Level

                }).ToList();
                var fs = FS.fs;
                var header = ReportBase.GetHeader(fs, Type, true, true, true, true, false, "CF");
                res.AddRange(indirect);

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ResualReportDT0 GetGrowthRateRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetGrowthRateRatios(datas);
                var header = ReportBase.GetHeaderRatio(fs, Type, false);

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetLiquidityRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetLiquidityRatios(datas);
                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion
                var header = ReportBase.GetHeaderRatio(fs, Type, false);
                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetActivityRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetActivityRatios(datas);
                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion
                var header = ReportBase.GetHeaderRatio(fs, Type, false);
                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetCreditRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetCreditRatios(datas);
                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion
                var header = ReportBase.GetHeaderRatio(fs, Type, false);
                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResualReportDT0 GetCoverageRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetCoverageRatios(datas);
                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion
                var header = ReportBase.GetHeaderRatio(fs, Type, false);
                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetPerformance(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetPerformance(datas);
                var header = ReportBase.GetHeaderRatio(fs, Type, false);

                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResualReportDT0 GetReturnOnInvestment(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetReturnOnInvestment(datas);
                var header = ReportBase.GetHeaderRatio(fs, Type, false);

                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResualReportDT0 GetReturnOnNetSales(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetReturnOnNetSales(datas);
                var header = ReportBase.GetHeaderRatio(fs, Type, false);

                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResualReportDT0 GetSolvencyRatios(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var res = BusinessReportsService.GetSolvencyRatios(datas);
                var header = ReportBase.GetHeaderRatio(fs, Type, false);

                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion

                resualt.data = res;
                resualt.header = header;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResualReportDT0 GetFinancialAnalysis(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;
                var header = ReportBase.GetHeader(fs, Type, false, false, true, true, true, "PL");
                var res = BusinessReportsService.GetFinancialAnalysis(datas);
                resualt.header = header;
                resualt.data = res;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ResualReportDT0 GetDataMargin(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;
                var fs = FS.fs;

                var res = BusinessReportsService.GetDataMargin(datas);
                var header = ReportBase.GetHeader(fs, Type, false, false, true, true, true, "PL");
                #region Tinhtoan average
                var averageIndustries = _vcisContext.Set<AverageIndustry>().Where(x => x.Deleted != true).ToList();
                var IndustryId = GetIndustryId(bid);
                var RatioYear = int.Parse(Configuration["Configuration:FinancialRatioYear"]);

                foreach (var item in res)
                {
                    if (!String.IsNullOrEmpty(item.AverageIndustryCode))
                    {
                        var obj = averageIndustries.FirstOrDefault(x => x.AverageIndustryCode == item.AverageIndustryCode);
                        if (obj != null)
                        {
                            item.AverageType = obj.AverageUnit;
                            item.IndustryAverageGroupId = obj.IndustryAverageGroupId;
                            if (obj.IndustryAverageValues != null)
                            {
                                var ave = obj.IndustryAverageValues.FirstOrDefault(a => a.BusinessLineId == IndustryId && a.IndustryEverageYear == RatioYear);
                                if (ave != null)
                                    item.AverageValue = ave.AverageValue;
                            }
                        }
                    }
                }

                #endregion
                resualt.header = header;
                resualt.data = res;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public ResualReportDT0 GetCashConversionCycle(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var resualt = new ResualReportDT0();

                var res = new List<ReportSummaryDTO>();
                var FS = GetFSData(bid, FSYear, Quarter, Type);
                var datas = FS.datas;

                #region DSO
                var DSO = BusinessReportsService.GetDSO(datas);
                DSO.Index = 1;
                DSO.Name = "DSO";
                res.Add(DSO);
                #endregion

                #region DOH
                var DOH = BusinessReportsService.GetDOH(datas);
                DOH.Index = 2;
                DOH.Name = "DOH";
                res.Add(DOH);
                #endregion

                #region DPO
                var DPO = BusinessReportsService.GetDPO(datas);
                DPO.Index = 3;
                DPO.Name = "DPO";
                res.Add(DPO);
                #endregion

                #region ccc
                var CCC = BusinessReportsService.GetCCC(datas);
                CCC.Index = 4;
                CCC.Name = "CCC";
                res.Add(CCC);
                #endregion

                var fs = FS.fs;
                var header = ReportBase.GetHeader(fs, Type, false, false, true, true, true, "PL");
               //  var header = ReportBase.GetHeaderRatio(fs, Type, false);

                resualt.header = header;
                resualt.data = res;
                return resualt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetIndustryId(int businessId)
        {
            var activity = _vcisContext.Activities.FirstOrDefault(a => a.IsMain == true && a.IsActive == true && a.BusinessId == businessId);
            if (activity != null)
            {
                if (activity.BusinessLine != null)
                {
                    if (activity.BusinessLine.BusinessLineCode.Length >= 5)
                    {
                        var line = _vcisContext.BusinessLines.FirstOrDefault(b => !b.Deleted && b.BusinessLineCode == activity.BusinessLine.BusinessLineCode.Substring(0, 5));
                        if (line != null)
                            return line.Id;
                        return 0;
                    }
                    return 0;
                }
                return 0;
            }
            return 0;

        }

        public FSDTO GetFS(int bid, int FSYear, int? Quarter, string Type = "1")
        {
            try
            {
                var FS = _vcisContext.Set<FinancialReport>().Where(x => x.BusinessId == bid && x.IsPrioritize == true);
                if (Type == "4")
                {
                    FS = FS.Where(x => x.IsProfit == true || x.IsCashFlow == true);
                }
                var yq = ReportBase.GetYearQuarter(FSYear, Quarter, Type);

                var fs1 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year1 && x.QuarterNumber == yq.Quarter1);
                var fs2 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year2 && x.QuarterNumber == yq.Quarter2);
                var fs3 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year3 && x.QuarterNumber == yq.Quarter3);
                var fs4 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year4 && x.QuarterNumber == yq.Quarter4);
                var fs5 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year5 && x.QuarterNumber == yq.Quarter5);
                var fs6 = FS.FirstOrDefault(x => x.FiscalYear == yq.Year6 && x.QuarterNumber == yq.Quarter6);

                return new FSDTO
                {
                    FS1 = fs1,
                    FS2 = fs2,
                    FS3 = fs3,
                    FS4 = fs4,
                    FS5 = fs5,
                    FS6 = fs6
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ResualDADTO> GetDupontAnalysis(DupontAnalysisDTO dto)
        {
            try
            {
                var res = new List<ResualDADTO>();
                
                foreach (var id in dto.ids)
                {
                    var name = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(id)?.Name;
                    var fs = GetFSData(id, dto.FSYear, dto.Quarter, dto.Type);
                    var DupontAnalysis = BusinessReportsService.GetDupontAnalysis(fs.datas);
                    DupontAnalysis.ForEach(x => {
                        x.BusinessName = name;
                        x.BusinessId = id;
                    });
                    var header = ReportBase.GetHeader(fs.fs, dto.Type, false, false, false, false, false, "");
                    res.Add(new ResualDADTO
                    {
                        header = header,
                        datas = DupontAnalysis
                    });
                }
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ResualDADTO> GetTop10Companies(DupontAnalysisDTO dto)
        {
            try
            {
                var res = new List<ResualDADTO>();

                if (dto.IsInit == true && dto.id != null)
                {
                    var ids = _businessService.GetTop10Companies_Ids(dto.id.Value, dto.FSYear);
                    dto.ids = ids;
                }

                foreach (var id in dto.ids)
                {
                    string name = null;
                    int? totalEmploy = 0;

                    var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(id);
                    if (business != null)
                    {
                        name = business.Name;
                        totalEmploy = business.TotalEmployees;
                    }
                    var fs = GetFSData(id, dto.FSYear, dto.Quarter, dto.Type);
                    var Top10Companies = BusinessReportsService.GetTop10Companies(fs.datas);
                    Top10Companies.ForEach(x => {
                        x.BusinessName = name;
                        x.BusinessId = id;
                        x.TotalEmployees = totalEmploy;
                    });
                    res.Add(new ResualDADTO
                    {
                        datas = Top10Companies
                    });
                }
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
