﻿using App.Common.Base;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class ReportSummaryDTO
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public decimal? Year1 { get; set; }
        public decimal? Year2 { get; set; }
        public decimal? Year3 { get; set; }
        public decimal? Year4 { get; set; }
        public decimal? Year5 { get; set; }
        public decimal? Year6 { get; set; }
        public decimal? AverageValue { get; set; }
        public string AverageIndustryCode { get; set; }
        public int? IndustryAverageGroupId { get; set; }
        public string AverageType { get; set; }
        public string Comment { get; set; }
        public bool? IsBold { get; set; }
        public bool? IsHideOnQuarter { get; set; }
        public string Parent { get; set; }
        public byte? Level { get; set; }
        public decimal? AddPercent1 { get; set; }
        public decimal? AddPercent2 { get; set; }
        public decimal? AddPercent3 { get; set; }

        public decimal? Percent
        {
            get
            {
                return (Year1 != null && Year2 != null && Year2 != 0) ? (decimal)((Year1 - Year2) / Math.Abs(Year2.Value)) : null;
            }
        }
        public decimal? Percent2
        {
            get
            {
                return (Year2 != null && Year3 != null && Year3 != 0) ? (decimal)((Year2 - Year3) / Math.Abs(Year3.Value)) : null;
            }
        }
        public decimal? Percent3
        {
            get
            {
                return (Year3 != null && Year4 != null && Year4 != 0) ? (decimal)((Year3 - Year4) / Math.Abs(Year4.Value)) : null;
            }
        }
        public bool Isup
        {
            get
            {
                return (Year1 != null && Year2 != null && Year2 != 0) ? Year1 > Year2 : false;
            }
        }

        // use for top10
        public string BusinessName { get; set; }
        public int? BusinessId { get; set; }
        public int? TotalEmployees { get; set; }
        public int? InitIndex { get; set; } 
        //
    }

    public class ResualReportDT0
    {
        public HeaderDTO header { get; set; }
        public List<ReportSummaryDTO> data { get; set; }
    }

    public class ResualtFSData
    {
        public List<ReportsDTO> datas { get; set; }
        public FSDTO fs { get; set; }
    }

    public class DupontAnalysisDTO
    {
        public List<int> ids { get; set; }
        public int FSYear { get; set; }
        public int? Quarter { get; set; }
        public string Type { get; set; }
        public bool? IsInit { get; set; }
        public int? id { get; set; }
    }

    public class ResualDADTO
    {
        public List<ReportSummaryDTO> datas { get; set; }
        public HeaderDTO header { get; set; }
    }
}
