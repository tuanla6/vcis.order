﻿using App.Common.Base;
using App.Data.Models.VCIS;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{

    public static class BankerReportsService
    {
        public static ReportSummaryDTO GetTotalAssets(List<ReportsDTO> datas)
        {
            try
            {
                #region Total Assets = BK43
                var BK43 = ReportBase.GetCodeValue(datas, "BK43");
                var TotalAssets = new ReportSummaryDTO
                {
                    Index = 1,
                    Name = "Total Assets",
                    Year1 = BK43.Year1,
                    Year2 = BK43.Year2,
                    Year3 = BK43.Year3,
                    Year4 = BK43.Year4,
                    Year5 = BK43.Year5,
                    Year6 = BK43.Year6,
                };

                #endregion

                return TotalAssets;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
