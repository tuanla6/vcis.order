﻿using App.Business.Base;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;

namespace App.Business.Services
{
    public interface INegativeNewsesService : IGenericService<NegativeNews>
    {
        List<NegativeNewsesDTO> GetNews(int bid);
    }
}
