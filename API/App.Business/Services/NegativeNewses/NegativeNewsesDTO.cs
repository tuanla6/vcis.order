﻿using AutoMapper;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class NegativeNewsesDTO
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public string Content { get; set; }
        public bool Deleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? DateOfNews { get; set; }
        public short? Type { get; set; }
    }
    public class NegativeNewsesProfile : Profile
    {
        public NegativeNewsesProfile()
        {
            CreateMap<NegativeNews, NegativeNewsesDTO>();
        }
    }
}
