﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class NegativeNewsesService : GenericService<NegativeNews>, INegativeNewsesService
    {
        public NegativeNewsesService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<NegativeNewsesProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<NegativeNews> QueryBuilder(IQueryable<NegativeNews> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

        public List<NegativeNewsesDTO> GetNews(int bid)
        {
            try
            {
                var res = new List<NegativeNewsesDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var news = business.NegativeNews.ToList();
                    if (news.Count > 0)
                    {
                        res = _mapper.Map<List<NegativeNews>, List<NegativeNewsesDTO>>(news);
                    }
                }

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
