﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class LitigationService : GenericService<LitigationRecord>, ILitigationService
    {
        public LitigationService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<LitigationProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<LitigationRecord> QueryBuilder(IQueryable<LitigationRecord> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

        public List<LitigationListDTO> GetLitigation(int bid)
        {
            try
            {
                var res = new List<LitigationListDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var Litigation = business.LitigationRecordBusinesses;
                    if (Litigation.Any())
                    {
                        var list = Litigation.ToList();
                        res = _mapper.Map<List<LitigationRecord>, List<LitigationListDTO>>(list);
                    }
                }

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
