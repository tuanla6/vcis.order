﻿using App.Business.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Business.Services
{
    public class LitigationListDTO
    {
        public int Id { get; set; }
        public DateTime? DateOfAcceptance { get; set; }
        public short? TypeOfcase { get; set; }
        public int? PlaintiffBusinessId { get; set; }
        public int? PlaintiffPersonId { get; set; }
        public int? RelatedPartiesBusinessId { get; set; }

        public string TypeOfcaseName {
            get
            {
                return TypeOfcase == 1 ? "Commercial business" :
                    TypeOfcase == 2 ? "Civil" :
                    TypeOfcase == 3 ? "Criminal" :
                    TypeOfcase == 4 ? "Administrative" : "";
            }
        }
        public string PlaintiffBusinessName { get; set; }
        public string PlaintiffPersonName { get; set; }
        public string RelatedPartiesBusinessName { get; set; }

        public string Status { get; set; }
    }
    public class LitigationProfile : Profile
    {
        public LitigationProfile()
        {
            CreateMap<LitigationRecord, LitigationListDTO>(MemberList.None)
                .ForMember(x => x.PlaintiffBusinessName, opt => opt.MapFrom(x => x.PlaintiffBusiness.Name))
                .ForMember(x => x.PlaintiffPersonName, opt => opt.MapFrom(x =>x.PlaintiffPerson.FirstName + " " + x.PlaintiffPerson.LastName))
                .ForMember(x => x.RelatedPartiesBusinessName, opt => opt.MapFrom(x => x.RelatedPartiesBusiness.Name))
                ;
        }
    }
}



