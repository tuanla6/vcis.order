﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class BoardOfDirectorService : GenericService<Staff>, IBoardOfDirectorService
    {
        public BoardOfDirectorService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<BoardOfDirectorProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<Staff> QueryBuilder(IQueryable<Staff> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
                string positionName = filter.positionName;
                if (!String.IsNullOrEmpty(positionName))
                {
                    query = query.Where(x => x.JobPosition.Name.Contains(positionName));
                }
                bool? chair = filter.chair;
                if (chair == true)
                {
                    query = query.Where(x => x.JobPosition.Name.Contains("chair") && !x.JobPosition.Name.Contains("former"));
                }
                bool? formerChair = filter.formerChair;
                if (formerChair == true)
                {
                    query = query.Where(x => x.JobPosition.Name.Contains("chair") && x.JobPosition.Name.Contains("former"));
                }
                bool? director = filter.director;
                if (director == true)
                {
                    query = query.Where(x => x.JobPosition.Name.Contains("director") && !x.JobPosition.Name.Contains("former"));
                }
                bool? formerDirector = filter.formerDirector;
                if (formerDirector == true)
                {
                    query = query.Where(x => x.JobPosition.Name.Contains("director") && x.JobPosition.Name.Contains("former"));
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }
    }
}
