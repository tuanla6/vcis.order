﻿using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class BoardOfDirectorListDTO
    {
        public int Id { get; set; }
        public int? BusinessId { get; set; }
        public string Name { get; set; }
        public string VNName { get; set; }
        public string PositionName { get; set; }
        public string IdNumber { get; set; }
        public string Nationality { get; set; }
        public DateTime? DOB { get; set; }
    }
    public class BoardOfDirectorProfile : Profile
    {
        public BoardOfDirectorProfile()
        {
            CreateMap<Staff, BoardOfDirectorListDTO>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Person.FullName))
                .ForMember(x => x.VNName, opt => opt.MapFrom(x => x.Person.VietnameseName))
                .ForMember(x => x.PositionName, opt => opt.MapFrom(x => x.JobPosition.Name))
                .ForMember(x => x.IdNumber, opt => opt.MapFrom(x => x.Person.Idnumber))
                .ForMember(x => x.Nationality, opt => opt.MapFrom(x => x.Person.Nationality))
                .ForMember(x => x.DOB, opt => opt.MapFrom(x => x.Person.Birthday));
        }
    }
}
