﻿using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class NegativePaymentDTO
    {
        public string Title { get; set; }
        public int Type { get; set; }
        public List<NegativePaymentDataDTO> ListData { get; set; }
    }
    public class NegativePaymentDataDTO
    {
        public decimal? Value { get; set; }
        public string Year { get; set; }
        public DateTime? Date { get; set; }
        public string DateShow {get; set;}
    }
}
