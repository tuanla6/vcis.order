﻿using App.Business.Base;
using App.Common.Base;
using App.Data.Models.VCIS;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class NegativePaymentService : GenericService<NegativePayment>, INegativePaymentService
    {
        public IConfiguration Configuration { get; }
        public NegativePaymentService(VCIS4Context dbContext, IConfiguration configuration)
           : base(dbContext)
        {
            Configuration = configuration;
        }
        public List<NegativePaymentDTO> GetNagativePayment(int businessId)
        {
            try
            {
                var model = new List<NegativePaymentDTO>();

                var outstandingTaxDateRange = Configuration["Configuration:OutstandingTaxDateRange"];

                var negativePayments = _vcisContext.Set<NegativePaymentRecord>().Where(n => !n.Deleted && n.BusinessId == businessId).ToList();
                if (negativePayments.Any())
                {
                    var dates = outstandingTaxDateRange.Split(";");
                    var firstDate = DateTime.Parse(dates.First());
                    // List Tax Liabilities
                    var taxLiabilities = negativePayments.Where(x => (x.Type == (int)CommonEnums.NegativePaymentRecords.OutstandingTaxLiabili || x.Type == null) && x.RecordDate >= firstDate).OrderByDescending(y => y.RecordDate).ToList();
                    if(taxLiabilities.Any())
                    {
                        var outstandingTaxLiabili = new NegativePaymentDTO() { 
                            Type = (int)CommonEnums.NegativePaymentRecords.OutstandingTaxLiabili
                        };
                        var outstandingTaxLiabilies = new List<NegativePaymentDataDTO>();
                        foreach (var payment in taxLiabilities)
                        {
                            var objNegativePayment = new NegativePaymentDataDTO()
                            {
                                Date = payment.RecordDate,
                                Value = payment.RecordValue,
                                Year = payment.RecordYear.ToString(),
                                DateShow = payment.RecordDate.Value.ToString("dd MMM yyyy")
                            };
                            outstandingTaxLiabilies.Add(objNegativePayment);
                        }
                        //add date if some object missing outstanding tax
                        foreach (string s in dates)
                        {
                            if (!outstandingTaxLiabilies.Exists(n => n.Date.HasValue && n.Date.Value.ToString("MM/dd/yyyy") == s))
                            {
                                var date = DateTime.Parse(s); 
                                var objNegativePayment = new NegativePaymentDataDTO()
                                {
                                    Date = date,
                                    Value = 0,
                                    Year = date.Year.ToString(),
                                    DateShow = date.ToString("dd MMM yyyy")
                                };
                                outstandingTaxLiabilies.Add(objNegativePayment);
                            }
                        }
                        var listTaxLiabilities = outstandingTaxLiabilies.OrderByDescending(x => x.Date).Take(9).ToList();
                        if (listTaxLiabilities.Any())
                        {
                            var objTax = listTaxLiabilities.OrderByDescending(n => n.Date).FirstOrDefault();
                            outstandingTaxLiabili.Title = "Total outstanding tax liabilities as at " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty) + ": (VND) " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty);
                            outstandingTaxLiabili.ListData = listTaxLiabilities.OrderBy(x => x.Date).ToList();
                            model.Add(outstandingTaxLiabili);
                        }
                    }
                    var socialInsuranceOverdues = negativePayments.Where(x => (x.Type == (int)CommonEnums.NegativePaymentRecords.OverduePaymentToSocialInsuranceFund)).ToList();
                    if (socialInsuranceOverdues.Any())
                    {
                        var outstandingTaxLiabili = new NegativePaymentDTO()
                        {
                            Type = (int)CommonEnums.NegativePaymentRecords.OverduePaymentToSocialInsuranceFund
                        };
                        var outstandingTaxLiabilies = new List<NegativePaymentDataDTO>();
                        foreach (var payment in socialInsuranceOverdues)
                        {
                            var objNegativePayment = new NegativePaymentDataDTO()
                            {
                                Date = payment.RecordDate,
                                Value = payment.RecordValue,
                                Year = payment.RecordYear.ToString(),
                                DateShow = payment.RecordDate.Value.ToString("dd MMM yyyy")
                            };
                            outstandingTaxLiabilies.Add(objNegativePayment);
                        }
                        var listTaxLiabilities = outstandingTaxLiabilies.OrderByDescending(x => x.Date).Take(9).ToList();
                        if (listTaxLiabilities.Any())
                        {
                            var objTax = listTaxLiabilities.OrderByDescending(n => n.Date).FirstOrDefault();
                            outstandingTaxLiabili.Title = "Total overdue payment to Social insurance fund as at " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty) + ": (VND) " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty);
                            outstandingTaxLiabili.ListData = listTaxLiabilities.OrderBy(x => x.Date).ToList();
                            model.Add(outstandingTaxLiabili);
                        }
                    }
                    var otherOverdues = negativePayments.Where(x => (x.Type == (int)CommonEnums.NegativePaymentRecords.OverduePaymentToClientAndOther)).ToList();
                    if (otherOverdues.Any())
                    {
                        var outstandingTaxLiabili = new NegativePaymentDTO()
                        {
                            Type = (int)CommonEnums.NegativePaymentRecords.OverduePaymentToClientAndOther
                        };
                        var outstandingTaxLiabilies = new List<NegativePaymentDataDTO>();
                        foreach (var payment in otherOverdues)
                        {
                            var objNegativePayment = new NegativePaymentDataDTO()
                            {
                                Date = payment.RecordDate,
                                Value = payment.RecordValue,
                                Year = payment.RecordYear.ToString(),
                                DateShow = payment.RecordDate.Value.ToString("dd MMM yyyy")
                            };
                            outstandingTaxLiabilies.Add(objNegativePayment);
                        }
                        var listTaxLiabilities = outstandingTaxLiabilies.OrderByDescending(x => x.Date).Take(9).ToList();
                        if (listTaxLiabilities.Any())
                        {
                            var objTax = listTaxLiabilities.OrderByDescending(n => n.Date).FirstOrDefault();
                            outstandingTaxLiabili.Title = "Total overdue payment to supplier and other as at " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty) + ": (VND) " + (objTax.Date.HasValue ? objTax.Date.Value.ToString("dd MMM yyyy") : string.Empty);
                            outstandingTaxLiabili.ListData = listTaxLiabilities.OrderBy(x => x.Date).ToList();
                            model.Add(outstandingTaxLiabili);
                        }
                    }
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
