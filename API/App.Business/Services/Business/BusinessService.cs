﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Common.Library;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class BusinessService : GenericService<App.Data.Models.VCIS.Business>, IBusinessService
    {
        public BusinessService(VCIS4Context dbContext)
           : base(dbContext)
        {
            _repo.Include(x => x.BusinessType);

            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<BusinessProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();
        }

        protected override IQueryable<App.Data.Models.VCIS.Business> QueryBuilder(IQueryable<App.Data.Models.VCIS.Business> query, dynamic filter, string search)
        {
            if (filter != null)
            {

            }
            if (search != null && search != "")
            {

            }
            return query;
        }
        protected override void BeforeMapper<TDto>(bool isNew, ref TDto dto)
        {
            //check trùng dữ liệu
            if (typeof(TDto) == typeof(BusinessDTO))
            {
                BusinessDTO businessDTO = (BusinessDTO)(object)dto;
            }
        }
        protected override void BeforeAddOrUpdate<TDto>(bool isNew, ref TDto dto, ref App.Data.Models.VCIS.Business entity)
        {
            if (typeof(TDto) != typeof(BusinessDTO))
            {
                return;
            }
            BusinessDTO businessDTO = (BusinessDTO)(object)dto;
        }

        protected override void AfferAddOrUpdate<TDto>(bool isNew, ref TDto dto, ref App.Data.Models.VCIS.Business entity)
        {
            if (typeof(TDto) != typeof(BusinessDTO))
            {
                return;
            }
            BusinessDTO businessDTO = (BusinessDTO)(object)dto;
            if (isNew)
            {
            }
        }

        public async Task<OverViewDTO> GetCompanyProfile(int id, int FSYear, int? Quarter)
        {
            try
            {
                var info = new OverViewDTO();
                var entity = await _repo.FindAsync(id);
                if (entity != null)
                {
                    info = _mapper.Map<OverViewDTO>(entity);

                    var businessAddress = entity.BusinessAddresses.FirstOrDefault(x => x.AddressTypeId == AppConst.HeadOffice);
                    if (businessAddress != null)
                    {
                        var street = businessAddress.Street;
                        var district = businessAddress.District != null ? businessAddress.District.DistrictName : string.Empty;
                        var province = businessAddress.Province != null ? businessAddress.Province.ProvinceName : string.Empty;
                        var country = businessAddress.Country != null ? businessAddress.Country.CountryName : string.Empty;
                        var category = businessAddress.Province != null ? businessAddress.Province.Category : string.Empty;
                        info.Address = StringUtilities.GenerateAddress(street, district, province, country, category,
                            businessAddress.District != null ? businessAddress.District.Category : string.Empty);
                    }

                    var FS = _vcisContext.Set<FinancialReport>().FirstOrDefault(x => x.BusinessId == id && x.FiscalYear == FSYear && x.QuarterNumber == Quarter && x.IsPrioritize == true);
                    if (FS != null)
                    {
                        info.FSYearEnd = FS.ReportedDate;
                        info.AuditStatus = FS.AuditStatus;
                    }

                    return info;
                }

                return info;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CompetitorDTO> GetCompetitor(int id, int FSYear)
        {
            try
            {
                var proc = AppConst.Proc_Competitor;
                List<SqlParameter> parms = new List<SqlParameter>
                {
                    new SqlParameter("@businessID", id),
                    new SqlParameter("@year", FSYear),
                    new SqlParameter("@auditStatus", "all"),
                    new SqlParameter("@consolidatedStatus", "all")
                };

                var res = new List<CompetitorDTO>();
                var business = _repo.Find(id);
                if (business != null && business.Bankers.Any())
                {
                    proc = AppConst.Proc_Competitor_Banker;
                }

                using (var command = _vcisContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = proc;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(parms.ToArray());
                    _vcisContext.Database.OpenConnection();
                    var result = command.ExecuteReader();
                    while (result.Read())
                    {
                        res.Add(new CompetitorDTO
                        {
                            Id = result.GetInt32(0),
                            Name = result.GetString(1),
                            Value = result.GetDecimal(3)
                        });
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HistoricalimelineDTO> GetHistoricalTimeline(int id)
        {
            try
            {
                var model = new List<HistoricalimelineDTO>();
                var businessLogs = _vcisContext.Set<BusinessLog>().Where(x => x.BusinessId == id && !x.Deleted).ToList();
                var listCapitals = businessLogs.Where(h => h.ColumnName.Equals("Charter capital increase") || h.ColumnName.Equals("Charter capital decrease") || h.ColumnName.Equals("Charter capital")).OrderByDescending(y => y.CreatedDate).ToList();

                if (listCapitals != null && listCapitals.Any())
                {
                    foreach (var cap in listCapitals)
                    {
                        var businessLogModel = new HistoricalimelineDTO()
                        {
                            Created = cap.Created,
                            NewValue = cap.NewValue,
                            OldValue = cap.OldValue,
                            Type = 2,
                            OnlyYear = cap.OnlyYear,
                            Date = cap.OnlyYear.HasValue ? (cap.Created.HasValue ? cap.Created.Value.Year.ToString() : "N/A") : (cap.Created.HasValue ? cap.Created.Value.ToString("dd MMM yyyy") : "N/A")
                        };
                        model.Add(businessLogModel);
                    }
                }
                var listCompanyName = businessLogs.Where(h => h.ColumnName.Equals("Registered English Name") || h.ColumnName.Equals("Registered Vietnamese Name ")).ToList();
                if (listCompanyName != null && listCompanyName.Any())
                {
                    foreach (var item in listCompanyName)
                    {
                        var businessLogModel = new HistoricalimelineDTO()
                        {
                            Created = item.Created,
                            NewValue = item.NewValue,
                            OldValue = item.OldValue,
                            Type = 1,
                            OnlyYear = item.OnlyYear,
                            ColumnName = item.ColumnName,
                            Date = item.OnlyYear.HasValue ? (item.Created.HasValue ? item.Created.Value.Year.ToString() : "N/A") : (item.Created.HasValue ? item.Created.Value.ToString("dd MMM yyyy") : "N/A")
                        };
                        model.Add(businessLogModel);
                    }
                }

                var listBusinessRegistrationNo = businessLogs.Where(h => h.ColumnName.Equals("Business Registration No")).ToList();
                if (listBusinessRegistrationNo != null && listBusinessRegistrationNo.Any())
                {
                    foreach (var item in listBusinessRegistrationNo)
                    {
                        var businessLogModel = new HistoricalimelineDTO()
                        {
                            Created = item.Created,
                            NewValue = item.NewValue,
                            OldValue = item.OldValue,
                            Type = 3,
                            OnlyYear = item.OnlyYear,
                            ColumnName = item.ColumnName,
                            Date = item.OnlyYear.HasValue ? (item.Created.HasValue ? item.Created.Value.Year.ToString() : "N/A") : (item.Created.HasValue ? item.Created.Value.ToString("dd MMM yyyy") : "N/A")
                        };
                        model.Add(businessLogModel);
                    }
                }

                var listContactAddress = businessLogs.Where(h => h.ColumnName.Equals("Address")).ToList();
                if (listBusinessRegistrationNo != null && listBusinessRegistrationNo.Any())
                {
                    foreach (var item in listBusinessRegistrationNo)
                    {
                        var businessLogModel = new HistoricalimelineDTO()
                        {
                            Created = item.Created,
                            NewValue = item.NewValue,
                            OldValue = item.OldValue,
                            Type = 4,
                            OnlyYear = item.OnlyYear,
                            ColumnName = item.ColumnName,
                            Date = item.OnlyYear.HasValue ? (item.Created.HasValue ? item.Created.Value.Year.ToString() : "N/A") : (item.Created.HasValue ? item.Created.Value.ToString("dd MMM yyyy") : "N/A")
                        };
                        model.Add(businessLogModel);
                    }
                }

                var financial = _vcisContext.Set<FinancialReport>().Where(x => x.BusinessId == id && !x.Deleted).OrderByDescending(x => x.FiscalYear).ThenByDescending(y => y.ReportedDate).FirstOrDefault();
                if (financial != null)
                {
                    var dateNext = "25 " + "Apr " + (DateTime.Now.Year + 1).ToString();
                    var businessLogModel = new HistoricalimelineDTO()
                    {
                        Created = null,
                        NewValue = dateNext,
                        OldValue = financial.CreatedDate.Value.ToString("dd MMM yyyy"),
                        Type = 5,
                        OnlyYear = null
                    };
                    model.Add(businessLogModel);
                }
                // report.UpdatedDate.HasValue ? report.UpdatedDate : (report.CreatedDate.HasValue ? report.CreatedDate : report.CompleteDate);
                var ratings = _vcisContext.Set<Report>().Where(x => x.BusinessId == id && !x.Deleted).OrderByDescending(x => x.CompleteDate).Take(2).ToList();
                if (ratings != null && ratings.Any())
                {
                    var ratingFirst = ratings.FirstOrDefault();
                    var dateCurrentRating = ratingFirst.UpdatedDate.HasValue ? ratingFirst.UpdatedDate : (ratingFirst.CreatedDate.HasValue ? ratingFirst.CreatedDate : ratingFirst.CompleteDate);

                    var businessLogModel = new HistoricalimelineDTO()
                    {
                        Created = null,
                        NewValue = dateCurrentRating.Value.ToString("dd MMM yyyy"),
                        Type = 6,
                        OnlyYear = null
                    };
                    DateTime? dateLastRating = null;
                    if (ratings.Count > 1)
                    {
                        var ratingLast = ratings.OrderBy(x => x.CompleteDate).FirstOrDefault();
                        dateLastRating = ratingLast.UpdatedDate.HasValue ? ratingLast.UpdatedDate : (ratingLast.CreatedDate.HasValue ? ratingLast.CreatedDate : ratingLast.CompleteDate);
                        businessLogModel.OldValue = dateLastRating.HasValue ? dateLastRating.Value.ToString("dd MMM yyyy") : "";
                    }
                    model.Add(businessLogModel);
                }
                return model.OrderByDescending(x => x.Created).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BusinessAddressesDTO> GetBusinessAddress(int id)
        {
            try
            {
                var model = new List<BusinessAddressesDTO>();
                var addresses = _vcisContext.Set<BusinessAddress>().Where(x => x.BusinessId == id && !x.Deleted).ToList();
                foreach (var item in addresses)
                {
                    var obj = new BusinessAddressesDTO();
                    obj.AddressTypeName = item.AddressType != null ? (item.AddressType.TypeName != AppConst.HeadOfficeText
                    ? item.AddressType.TypeName + (item.Province != null ? " in " + item.Province.ProvinceName : string.Empty) : item.AddressType.TypeName) : string.Empty;
                    var street = item.Street;
                    var district = item.District != null ? item.District.DistrictName : string.Empty;
                    var province = item.Province != null ? item.Province.ProvinceName : string.Empty;
                    var country = item.Country != null ? item.Country.CountryName : string.Empty;
                    var category = item.Province != null ? item.Province.Category : string.Empty;
                    obj.FullAddress = StringUtilities.GenerateAddress(street, district, province, country, category, item.District != null ? item.District.Category : string.Empty);
                    obj.Email = item.Email;
                    obj.Phone = item.Phone;
                    model.Add(obj);
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<YearFSDTO> getYearFS(int bid, string type = "1")
        {
            try
            {
                var res = new List<YearFSDTO>();
                var fs = _vcisContext.Set<FinancialReport>().Where(x => x.Deleted != true && x.BusinessId == bid && x.IsPrioritize == true);

                if (type == "1")
                {
                    fs = fs.Where(x => x.QuarterNumber == null || x.QuarterNumber == 0);
                }
                if (type == "2" || type == "3")
                {
                    fs = fs.Where(x => x.QuarterNumber != null && x.QuarterNumber != 0);
                }
                if (type == "4")
                {
                    fs = fs.Where(x => x.QuarterNumber == 2 && (x.IsProfit == true || x.IsCashFlow == true));
                }

                if (fs.Any())
                {
                    res = fs.OrderByDescending(y => y.FiscalYear).ThenByDescending(z => z.QuarterNumber).Select(x => new YearFSDTO
                    {
                        year = x.FiscalYear,
                        quarter = x.QuarterNumber
                    }).ToList();
                }

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Top10DTO> GetTop10Companies(int id, int FSYear)
        {
            try
            {
                var res = new List<Top10DTO>();
                var proc = AppConst.Proc_Top10Companies;
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().FirstOrDefault(x => x.Id == id && !x.Deleted);
                if (business != null)
                {
                    var activities = business.Activities.Where(a => a.IsActive == true && !a.Deleted).OrderByDescending(a => a.IsMain).ThenBy(a => a.Id).ThenByDescending(a => a.UpdatedDate).FirstOrDefault();
                    if (activities != null)
                    {
                        var businessLineVsic = activities.BusinessLine?.Vsic;
                        if (business.Bankers.Any())
                        {
                            proc = AppConst.Proc_Top10Companies_Banker;
                        }
                        List<SqlParameter> parms = new List<SqlParameter>
                        {
                            new SqlParameter("@businessID", id),
                            new SqlParameter("@year", FSYear),
                            new SqlParameter("@auditStatus", "all"),
                            new SqlParameter("@consolidatedStatus", "all"),
                            new SqlParameter("@vsic", businessLineVsic)
                        };

                        using (var command = _vcisContext.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = proc;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddRange(parms.ToArray());
                            _vcisContext.Database.OpenConnection();
                            var result = command.ExecuteReader();
                            while (result.Read())
                            {
                                res.Add(new Top10DTO
                                {
                                    Id = result.GetInt32(0),
                                    Name = result.GetString(1),
                                    TotalEmployees = result.GetInt32(2),
                                    NetSale = result.GetDecimal(3),
                                    EbitData = result.GetDecimal(4)
                                });
                            }
                        }
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> GetTop10Companies_Ids(int id, int FSYear)
        {
            try
            {
                var res = new List<int>();
                var proc = AppConst.Proc_Top10_NetSales;
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().FirstOrDefault(x => x.Id == id && !x.Deleted);
                if (business != null)
                {
                    var activities = business.Activities.Where(a => a.IsActive == true && !a.Deleted).OrderByDescending(a => a.IsMain).ThenBy(a => a.Id).ThenByDescending(a => a.UpdatedDate).FirstOrDefault();
                    if (activities != null)
                    {
                        var businessLineVsic = activities.BusinessLine?.Vsic;
                        if (business.Bankers.Any())
                        {
                            proc = AppConst.Proc_SP_Top10_Netinterest;
                        }
                        List<SqlParameter> parms = new List<SqlParameter>
                        {
                            new SqlParameter("@businessID", id),
                            new SqlParameter("@year", FSYear),
                            new SqlParameter("@auditStatus", "all"),
                            new SqlParameter("@consolidatedStatus", "all"),
                            new SqlParameter("@vsic", businessLineVsic)
                        };

                        using (var command = _vcisContext.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = proc;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddRange(parms.ToArray());
                            _vcisContext.Database.OpenConnection();
                            var result = command.ExecuteReader();
                            while (result.Read())
                            {
                                res.Add(result.GetInt32(0));
                            }
                            command.Connection.Close();
                        }
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
