﻿using App.Business.Base;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public interface IBusinessService : IGenericService<App.Data.Models.VCIS.Business>
    {
        Task<OverViewDTO> GetCompanyProfile(int id, int FSYear, int? Quarter);
        List<CompetitorDTO> GetCompetitor(int id, int FSYear);
        List<HistoricalimelineDTO> GetHistoricalTimeline(int id);
        List<BusinessAddressesDTO> GetBusinessAddress(int id);
        List<YearFSDTO> getYearFS(int bid, string type = "1");
        List<Top10DTO> GetTop10Companies(int id, int FSYear);
        List<int> GetTop10Companies_Ids(int id, int FSYear);
    }
}
