﻿using App.Business.Base;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Business.Services
{
    public class BusinessDTO
    {
        public int Id { get; set; }
    }
    public class BusinessListDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Crid { get; set; }
        public string TaxCode { get; set; }
        public string RegistrationNumber { get; set; }
        public string Address { get; set; }
    }
    public class OverViewDTO
    {
        public int Id { get; set; }
        public string Crid { get; set; }
        public string Name { get; set; }
        public string Vnname { get; set; }
        public string TradeName { get; set; }
        public string EnglishName { get; set; }
        public string TypeName { get; set; }
        public string RegistrationNumber { get; set; }
        public string TaxCode { get; set; }
        public string Status { get; set; }
        public decimal? RegisteredCapital { get; set; }
        public string Currency { get; set; }
        public int? TotalEmployees { get; set; }
        public string Website { get; set; }
        public string DetailedInfo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? FoundDate { get; set; }
        public int? BusinessTypeId { get; set; }
        public string StatusBonds { get; set; }
        public decimal? PaidUpCapital { get; set; }
        public string CurrencyPaidUpCapital { get; set; }
        public SubIndustryDTO Industrys { get; set; }
        public string LegalRepresentative { get; set; }
        public DateTime? FSYearEnd { get; set; }
        public string AuditStatus { get; set; }
        public string StatusName {
            get {
                return Status == "A" ? "Active" : Status == "C" ? "Closed" : Status == "T" ? "Temporarily closed" : Status == "D" ? "Dissolution" : "-";
            }
        }
        public string Address { get; set; }
    }

    public class StaffDTO
    {
        public string PositionName { get; set; }
        public string PersonName { get; set; }
    }
    public class InterviewDTO
    {
        public DateTime? InterviewDate { get; set; }
        public string Interviewer { get; set; }
        public string Interviewees { get; set; }
    }
    public class HeadOfficeDTO
    {
        public string street { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string provi_category { get; set; }
        public string distr_category { get; set; }

    }

    public class SubIndustryDTO
    {
        public string vcis { get; set; }
        public string nace { get; set; }
        public string naics { get; set; }
    }

    public class Top10DTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalEmployees { get; set; }
        public decimal NetSale { get; set; }
        public decimal EbitData { get; set; }
    }
    public class CompetitorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
    public class BusinessProfile : Profile
    {
        public BusinessProfile()
        {
            CreateMap<App.Data.Models.VCIS.Business, BusinessListDTO>()
                .ForMember(x => x.Address, otp => otp.Ignore());

            CreateMap<App.Data.Models.VCIS.Business, OverViewDTO>(MemberList.None)
                .ForMember(x => x.TypeName, otp => otp.MapFrom(y => y.BusinessType.TypeName))
                .ForMember(x => x.LegalRepresentative, otp => otp.MapFrom(y => y.staff.Any(z =>
                    (z.JobPositionId == AppConst.LegalRepresentativeId) && y.staff.FirstOrDefault(z => z.JobPositionId == AppConst.LegalRepresentativeId).Person != null) ?
                    y.staff.FirstOrDefault(z => z.JobPositionId == AppConst.LegalRepresentativeId).Person.FirstName : null
                 ))
                .ForMember(x => x.Industrys, otp => otp.MapFrom(y => y.Activities.Where(x => x.IsMain == true && x.Deleted != true).Select(y => y.BusinessLine != null ? new SubIndustryDTO {
                    vcis = y.BusinessLine.Vsic + " " + y.BusinessLine.Name,
                    nace = y.BusinessLine.Nace + " " + y.BusinessLine.Nacedescription,
                    naics = y.BusinessLine.Naics + " " + y.BusinessLine.Naicsdescription
                } : new SubIndustryDTO()).FirstOrDefault()));

        }
    }
    public class HistoricalimelineDTO
    {
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int Type { get; set; }
        public DateTime? Created { get; set; }
        public bool? OnlyYear { get; set; }
        public string Date { get; set; }
        public string ColumnName { get; set; }
    }
    public class BusinessAddressesDTO
    {
        public string AddressTypeName { get; set; }
        public string FullAddress { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class YearFSDTO
    {
        public int year { get; set; }
        public int? quarter { get; set; }
    }

}



