﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class BranchService : GenericService<IndependentBranch>, IBranchService
    {
        public BranchService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<BranchProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<IndependentBranch> QueryBuilder(IQueryable<IndependentBranch> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
                bool? isParent = filter.isParent;
               
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

        public List<BranchDTO> GetBranch(int bid)
        {
            try
            {
                var res = new List<BranchDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var branch = business.IndependentBranches.ToList();
                    if (branch.Count > 0)
                    {
                        res = _mapper.Map<List<IndependentBranch>, List<BranchDTO>>(branch);
                    }
                }

                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
