﻿using App.Common.Base;
using App.Common.Library;
using App.Data.Models.VCIS;
using AutoMapper;
using System.Linq;

namespace App.Business.Services
{
    public class BranchDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BRN { get; set; }
        public AddressDTO AddressObj { get; set; }
        public string Address
        {
            get
            {
                return StringUtilities.GenerateAddress(AddressObj);
            }
        }

    }
    public class BranchProfile : Profile
    {
        public BranchProfile()
        {
            CreateMap<IndependentBranch, BranchDTO>(MemberList.None)
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Branch.Name))
                .ForMember(x => x.BRN, opt => opt.MapFrom(x => x.Branch.RegistrationNumber))
                .ForMember(x => x.AddressObj, opt => opt.MapFrom(x =>
                    x.Branch.BusinessAddresses.Any(y => y.AddressTypeId == AppConst.HeadOffice) ?
                        x.Branch.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => new AddressDTO
                        {
                            Street = z.Street,
                            District = z.District != null ? z.District.DistrictName : string.Empty,
                            Province = z.Province != null ? z.Province.ProvinceName : string.Empty,
                            Country = z.Country != null ? z.Country.CountryName : string.Empty,
                            ProvinceCategory = z.Province != null ? z.Province.Category : string.Empty,
                            DistrictCategory = z.District != null ? z.District.Category : string.Empty,
                        }).FirstOrDefault() : new AddressDTO()))
                ;
        }
    }
}
