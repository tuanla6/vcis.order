﻿using App.Common.Base;
using App.Common.Library;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class ShareholderDTO
    {
        public int Id { get; set; }
        public int BusinessId { get; set; }
        public int? ShareHolderBusinessId { get; set; }
        public int? ShareHolderPersonId { get; set; }
        public decimal? SharedCapital { get; set; }
        public double? SharedPercentage { get; set; }
        public long? SharedStocks { get; set; }
        public string ShareHolderOther { get; set; }
        public string Currency { get; set; }
        public string Notes { get; set; }
        public string BusinessRepresentative { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Deleted { get; set; }
        public int? RshareHolderId { get; set; }
        public DateTime? SharedPercentageDate { get; set; }
        public int? SubSharedUserId { get; set; }
        public double? SubPercentage { get; set; }
        public DateTime? SubPercentageDate { get; set; }
    }
    public class ShareholderListDTO
    {
        public int Id { get; set; }
        public double? SharedPercentage { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public string IDNumber { get; set; }
        public decimal? SharedCapital { get; set; }
        public string Currency { get; set; }
        public AddressDTO AddressObj { get; set; }
        public string Address
        {
            get
            {
                return StringUtilities.GenerateAddress(AddressObj);
            }
        }

    }

    public class SubsidiaryCompanyDTO
    {
        public string Name { get; set; }
        public DateTime? FoundDate { get; set; }
        public string RegistrationNumber { get; set; }
        public double? SharedPercentage { get; set; }
        public AddressDTO AddressObj { get; set; }
        public string Address
        {
            get
            {
                return StringUtilities.GenerateAddress(AddressObj);
            }
        }

        public string RelationStatus { get; set; }
        public string Status { get; set; }
        public string OperationStatus {
            get
            {
                return Status == "A" ? "Active" : Status == "C" ? "Closed" : Status == "T" ? "Temporarily closed" : Status == "D" ? "Dissolution" : "-";
            }
        }
    }

    public class ShareholderProfile : Profile
    {
        public ShareholderProfile()
        {
            CreateMap<ShareHolder, ShareholderListDTO>(MemberList.None)
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.ShareHolderBusinessId != null ? x.ShareHolderBusiness.Name :
                                                                x.ShareHolderPersonId != null ? x.ShareHolderPerson.FirstName + " " + x.ShareHolderPerson.LastName : x.ShareHolderOther))
                .ForMember(x => x.Date, opt => opt.MapFrom(x => x.ShareHolderBusinessId != null ? x.ShareHolderBusiness.FoundDate :
                                                                x.ShareHolderPersonId != null ? x.ShareHolderPerson.Birthday : null))
                .ForMember(x => x.IDNumber, opt => opt.MapFrom(x => x.ShareHolderBusinessId != null ? x.ShareHolderBusiness.RegistrationNumber :
                                                                x.ShareHolderPersonId != null ? x.ShareHolderPerson.Idnumber : null))
                .ForMember(x => x.Type, opt => opt.MapFrom(x => x.ShareHolderBusinessId != null ? "Business" : x.ShareHolderPersonId != null ? "Person" : "Other"))
                .ForMember(x => x.AddressObj, opt => opt.MapFrom(x =>
                    x.ShareHolderBusinessId != null && x.ShareHolderBusiness.BusinessAddresses.Any(y => y.AddressTypeId == AppConst.HeadOffice) ?
                        x.ShareHolderBusiness.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => new AddressDTO
                        {
                            Street = z.Street,
                            District = z.District != null ? z.District.DistrictName : string.Empty,
                            Province = z.Province != null ? z.Province.ProvinceName : string.Empty,
                            Country = z.Country != null ? z.Country.CountryName : string.Empty,
                            ProvinceCategory = z.Province != null ? z.Province.Category : string.Empty,
                            DistrictCategory = z.District != null ? z.District.Category : string.Empty,
                        }).FirstOrDefault() :
                    x.ShareHolderPersonId != null && x.ShareHolderPerson.PersonAddresses.Any() ?
                        x.ShareHolderPerson.PersonAddresses
                        .OrderBy(y => y.AddressTypeId == AppConst.CurrentResidence ? 1 : y.AddressTypeId == AppConst.PermanentAddress ? 2 : y.AddressTypeId == AppConst.Resident ? 3 : 4)
                        .Select(z => new AddressDTO
                        {
                            Street = z.Street,
                            District = z.District != null ? z.District.DistrictName : string.Empty,
                            Province = z.Province != null ? z.Province.ProvinceName : string.Empty,
                            Country = z.Country != null ? z.Country.CountryName : string.Empty,
                            ProvinceCategory = z.Province != null ? z.Province.Category : string.Empty,
                            DistrictCategory = z.District != null ? z.District.Category : string.Empty,
                        }).FirstOrDefault() : new AddressDTO()))
                ;

            CreateMap<ShareHolder, SubsidiaryCompanyDTO>(MemberList.None)
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Business.Name))
                .ForMember(x => x.FoundDate, opt => opt.MapFrom(x => x.Business.FoundDate))
                .ForMember(x => x.RegistrationNumber, opt => opt.MapFrom(x => x.Business.RegistrationNumber))
                .ForMember(x => x.Status, opt => opt.MapFrom(x => x.Business.Status))
                .ForMember(x => x.AddressObj, opt => opt.MapFrom(x =>
                    x.Business.BusinessAddresses.Any(y => y.AddressTypeId == AppConst.HeadOffice) ?
                        x.Business.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => new AddressDTO
                        {
                            Street = z.Street,
                            District = z.District != null ? z.District.DistrictName : string.Empty,
                            Province = z.Province != null ? z.Province.ProvinceName : string.Empty,
                            Country = z.Country != null ? z.Country.CountryName : string.Empty,
                            ProvinceCategory = z.Province != null ? z.Province.Category : string.Empty,
                            DistrictCategory = z.District != null ? z.District.Category : string.Empty,
                        }).FirstOrDefault() : new AddressDTO()))
                ;

            CreateMap<Staff, SubsidiaryCompanyDTO>(MemberList.None)
               .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Business.Name))
               .ForMember(x => x.FoundDate, opt => opt.MapFrom(x => x.Business.FoundDate))
               .ForMember(x => x.RegistrationNumber, opt => opt.MapFrom(x => x.Business.RegistrationNumber))
               .ForMember(x => x.Status, opt => opt.MapFrom(x => x.Business.Status))
               .ForMember(x => x.AddressObj, opt => opt.MapFrom(x =>
                   x.Business.BusinessAddresses.Any(y => y.AddressTypeId == AppConst.HeadOffice) ?
                       x.Business.BusinessAddresses.Where(y => y.AddressTypeId == AppConst.HeadOffice).Select(z => new AddressDTO
                       {
                           Street = z.Street,
                           District = z.District != null ? z.District.DistrictName : string.Empty,
                           Province = z.Province != null ? z.Province.ProvinceName : string.Empty,
                           Country = z.Country != null ? z.Country.CountryName : string.Empty,
                           ProvinceCategory = z.Province != null ? z.Province.Category : string.Empty,
                           DistrictCategory = z.District != null ? z.District.Category : string.Empty,
                       }).FirstOrDefault() : new AddressDTO()))
               ;
        }
    }
}
