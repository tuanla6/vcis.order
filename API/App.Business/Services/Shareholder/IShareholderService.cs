﻿using App.Business.Base;
using App.Common.Library;
using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;

namespace App.Business.Services
{
    public interface IShareholderService : IGenericService<ShareHolder>
    {
        List<ShareholderListDTO> GetShareholder(int bid);
        List<SubsidiaryCompanyDTO> GetSubsidiaryCompany(int bid);
        List<SubsidiaryCompanyDTO> GetRelatedCompany(int bid);
    }
}
