﻿using App.Business.Base;
using App.Business.Utils;
using App.Common.Base;
using App.Common.Library;
using App.Data;
using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class ShareholderService : GenericService<ShareHolder>, IShareholderService
    {
        public ShareholderService(VCIS4Context dbContext)
           : base(dbContext)
        {
            ///Khởi tạo mapperconfiuration
            _mapperCfg = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<BaseProfile>();
                cfg.AddProfile<ShareholderProfile>();
            });
            _mapper = _mapperCfg.CreateMapper();
            _mapperCfg.AssertConfigurationIsValid();

        }

        protected override IQueryable<ShareHolder> QueryBuilder(IQueryable<ShareHolder> query, dynamic filter, string search)
        {
            if (filter != null)
            {
                int? businessID = filter.businessID;
                if (businessID != null)
                {
                    query = query.Where(x => x.BusinessId == businessID);
                }
                bool? isParent = filter.isParent;
                // percent >= 51% và ko phải cá nhân
                if (isParent == true)
                {
                    query = query.Where(x => x.SharedPercentage >= 51 && x.ShareHolderPersonId == null);
                }
            }
            if (search != null && search != "")
            {

            }
            return query;
        }

       
        public List<ShareholderListDTO> GetShareholder(int bid)
        {
            try
            {
                var res = new List<ShareholderListDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var share = business.ShareHolderBusinesses.Where(x => x.Deleted != true).ToList();
                    if (share.Count > 0)
                    {
                        res = _mapper.Map<List<ShareHolder>, List<ShareholderListDTO>>(share);
                    }
                }
               
                return res;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SubsidiaryCompanyDTO> GetSubsidiaryCompany(int bid)
        {
            try
            {
                var res = new List<SubsidiaryCompanyDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);
                if (business != null)
                {
                    var entities = business.ShareHolderShareHolderBusinesses.Where(x => x.Deleted != true && x.SharedPercentage >= 51);
                    if (entities.Any())
                    {
                        var list = entities.ToList();
                        res = _mapper.Map<List<ShareHolder>, List<SubsidiaryCompanyDTO>>(list);
                    }
                }

                //var res = new List<SubsidiaryCompanyDTO>();

                //var entities = _repo.Where(x => x.ShareHolderBusinessId == bid && x.Deleted != true && x.SharedPercentage >= 51);
                //if (entities.Any())
                //{
                //    var list = entities.ToList();
                //    res = _mapper.Map<List<ShareHolder>, List<SubsidiaryCompanyDTO>>(list);
                //}

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SubsidiaryCompanyDTO> GetRelatedCompany(int bid)
        {
            try
            {
                var res = new List<SubsidiaryCompanyDTO>();
                var business = _vcisContext.Set<App.Data.Models.VCIS.Business>().Find(bid);

                if (business != null)
                {
                    var eRelated = business.ShareHolderShareHolderBusinesses.Where(x => x.Deleted != true && x.SharedPercentage <= 50);
                    if (eRelated.Any())
                    {
                        var list = eRelated.ToList();
                        var Related = _mapper.Map<List<ShareHolder>, List<SubsidiaryCompanyDTO>>(list);
                        res.AddRange(Related);
                    }
                }
                
                // other related
                var personIds = _repo.Where(x => !x.Deleted && x.BusinessId == bid && x.ShareHolderPersonId != null).Select(y => y.ShareHolderPersonId).ToList();
                var sharePerson = _repo.Where(x => !x.Deleted && personIds.Contains(x.ShareHolderPersonId) && x.BusinessId != bid);
                if (sharePerson.Any())
                {
                    var list = sharePerson.ToList();
                    var Related = _mapper.Map<List<ShareHolder>, List<SubsidiaryCompanyDTO>>(list);
                    res.AddRange(Related);
                }

                var personStaffIds = _vcisContext.Set<Staff>().Where(x => !x.Deleted && x.BusinessId == bid && x.PersonId != null).Select(y => y.PersonId).ToList();
                var staff = _vcisContext.Set<Staff>().Where(x => !x.Deleted && personStaffIds.Contains(x.PersonId) && x.BusinessId != bid);
                if (staff.Any())
                {
                    var list = staff.ToList();
                    var Related = _mapper.Map<List<Staff>, List<SubsidiaryCompanyDTO>>(list);
                    res.AddRange(Related);
                }


                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }


}
