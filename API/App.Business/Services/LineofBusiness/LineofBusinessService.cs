﻿using App.Business.Base;
using App.Common.Base;
using App.Data.Models.VCIS;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;


namespace App.Business.Services
{
    public class LineofBusinessService : GenericService<BusinessLine>, ILineofBusinessService
    {
        public IConfiguration Configuration { get; }
        public LineofBusinessService(VCIS4Context dbContext, IConfiguration configuration)
           : base(dbContext)
        {
            Configuration = configuration;
        }
        public LineofBusinessDTO GetBusinessActivities(int businessId)
        {
            try
            {
                var model = new LineofBusinessDTO();
                var activities = _vcisContext.Set<Activity>().Where(n => !n.Deleted && n.BusinessId == businessId && n.IsActive == true).ToList();
                var activitymain = activities.FirstOrDefault(x => x.IsMain == true);
                if (activitymain != null)
                {
                    var businessLineId = activitymain.BusinessLineId;
                    if(activitymain.BusinessLine != null)
                    {
                       var listMainBusiness = BusinessLineRecursive(activitymain.BusinessLine.ParentId, new List<ActivityDataDTO>());
                        var objMain = new ActivityDataDTO()
                        {
                            BusinessLevel = activitymain.BusinessLine.BusinessLevel,
                            BusinessLineCode = activitymain.BusinessLine.BusinessLineCode,
                            BusinessName  = activitymain.BusinessLine.Name
                        };
                        listMainBusiness.Add(objMain);
                        model.MainBusiness = new ActivityMainDTO();
                        model.MainBusiness.ListMainBusiness = new List<ActivityDataDTO>(listMainBusiness);
                        model.MainBusiness.NACE = string.Format("{0} {1}", activitymain.BusinessLine.Nace, activitymain.BusinessLine.Nacedescription);
                        model.MainBusiness.NAICS = string.Format("{0} {1}", activitymain.BusinessLine.Naics, activitymain.BusinessLine.Naicsdescription);
                    }
                    var creditRating = _vcisContext.Set<Report>().Where(n => !n.Deleted && n.BusinessId == businessId).OrderByDescending(x => x.CompleteDate).FirstOrDefault();
                    if(creditRating != null)
                        model.MainBusiness.ProductsAndServices = creditRating.ActivityInterpretation;
                }
                var registeredBusinessActivities = activities.Where(x => x.IsMain == false);
                var listRegisteredBusiness = new List<ActivityDataDTO>();
                foreach (var item in registeredBusinessActivities)
                {
                    var obj = new ActivityDataDTO()
                    {
                        BusinessLevel = item.BusinessLine?.BusinessLevel,
                        BusinessLineCode = item.BusinessLine?.BusinessLineCode,
                        BusinessName = item.BusinessLine?.Name,
                    };
                    listRegisteredBusiness.Add(obj);
                }
                model.ListRegisteredBusiness = new List<ActivityDataDTO>(listRegisteredBusiness);
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<ActivityDataDTO> BusinessLineRecursive(string parentID, List<ActivityDataDTO> model)
        {
            try
            {
                var businessLine = _vcisContext.Set<BusinessLine>().FirstOrDefault(x => x.BusinessLineCode.Equals(parentID));
                if (!string.IsNullOrEmpty(parentID)  && businessLine != null && !string.IsNullOrEmpty(businessLine.ParentId))
                {
                    var item = new ActivityDataDTO() 
                    {
                        BusinessLevel = businessLine.BusinessLevel,
                        BusinessLineCode = businessLine.BusinessLineCode,
                        BusinessName = businessLine.Name
                    };
                    model.Add(item);
                    return BusinessLineRecursive(businessLine.ParentId, model);
                }
                return model;

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
