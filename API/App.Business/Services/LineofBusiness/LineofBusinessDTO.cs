﻿using App.Data.Models.VCIS;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Business.Services
{
    public class LineofBusinessDTO
    {
        public ActivityMainDTO MainBusiness { get; set; }
        public List<ActivityDataDTO> ListRegisteredBusiness { get; set; }
    }
    public class ActivityMainDTO
    {
        public string NACE { get; set; }
        public string NAICS { get; set; }
        public string ProductsAndServices { get; set; }
        public List<ActivityDataDTO> ListMainBusiness { get; set; }
    }
    public class ActivityDataDTO
    {
        public int? BusinessLevel { get; set; }
        public string BusinessLineCode { get; set; }
        public string BusinessName { get; set; }
    }
}
