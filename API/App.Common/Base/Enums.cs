﻿namespace App.Common.Base
{
    public static class CommonEnums
    {
        public enum NegativePaymentRecords
        {
            OutstandingTaxLiabili = 1,
            OverduePaymentToSocialInsuranceFund = 2,
            OverduePaymentToClientAndOther = 3
        }
    }
}
