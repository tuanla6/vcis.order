﻿namespace App.Common.Base
{
    public static class AppConst
    {
        public const string NAMESPACE_DATA = "App.Data";
        public const string ASSEMBLY_DATA = "App.Data";
        public const string PRIMARY_KEY_NAME = "id";
        public const string ID = "id";
        public const string NGUOI_TAO_ID = "nguoi_tao_id";
        public const string NGAY_TAO = "ngay_tao";
        public const string NGAY_CHINH_SUA = "ngay_chinh_sua";
        public const string NGUOI_CHINH_SUA_ID = "nguoi_chinh_sua_id";

        public const string TEMPLATES_PATH = @"\Templates\";
        public const string TEMP_PATH = @"\temp\";

        public const int LegalRepresentativeId = 342;

        public const string Proc_Competitor = "SP_Competitor_Analysis";
        public const string Proc_Competitor_Banker = "SP_Competitor_Analysis_Banker";
        public const string Proc_FSData = "SP_FINANCIAL_DATA_ORDERNEW";
        public const string Proc_Top10Companies = "SP_Top10_Companies";
        public const string Proc_Top10Companies_Banker = "SP_Top10_Companies_Banker";
        public const string Proc_Top10_NetSales = "SP_Top10_NetSales";
        public const string Proc_SP_Top10_Netinterest = "SP_Top10_Netinterest";

        public const string HeadOfficeText = "Head Office";
        public const string OnlyNumbers = @"^[0-9]+$";
        public const int HeadOffice = 5;
        public const int CurrentResidence = 1;
        public const int PermanentAddress = 3;
        public const int Resident = 4;

        public const string CategoryAssets = "Assets";
        public const string CategoryLiabilities = "Liabilities";
        public const string CategoryProfitLose = "Profit and lose statements";
        public const string CategoryFactors = "Financial factors";
        public const string CategoryHIGHLIGHTS = "FinancialHighLights";
        public const string CategoryFinancingActivities = "Financing activities";
        public const string CategoryInvestingActivities = "Investing activities";
        public const string CategoryOffBalancesheet = "Off- balance sheet items";
        public const string CategoryOperatingActivities = "Operating activities";
        public const string CategorySummary = "Summary";
        public const string CategoryCashFlowDirect = "CashFlow Direct";
        public const string CategoryCashFlowIndirect = "CashFlow Indirect";

        public const string ReportBalancesheet = "BalanceSheet";
        public const string ReportCashflow = "CashFlow";
        public const string ReportFactors = "FinancialFactors";
        public const string ReportHighlights = "FinancialHighLights";
        public const string HIGHLIGHTS = "HighLights";
        public const string ReportProfitLose = "ProfitLose";
        public const string ReportFinancialBanking = "Financial_Banking";
        public const string ReportCashFlowDirectBanking = "CashFlow_Direct_Banking";
        public const string ReportCashFlowIndirectBanking = "CashFlow_Indirect_Banking";
        public const string ReportCashFlowDirect = "CashFlow_Direct";
        public const string ReportCashFlowIndirect = "CashFlow_Indirect";
    }
}
