﻿using App.Data.Models.VCIS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Base
{
    public static class ReportBase
    {
        public static ReportsDTO GetCodeValue(List<ReportsDTO> financials, string accountCodes)
        {
            var res = new ReportsDTO
            {
                Year1 = 0,
                Year2 = 0,
                Year3 = 0,
                Year4 = 0,
                Year5 = 0,
                Year6 = 0,
            };

            var codes = accountCodes.Split('+');
            var lst = financials.Where(f => codes.Contains(f.AccountCode));

            if (lst.Any(x => x.Year1 != null))
                res.Year1 = lst.Sum(x => x.Year1 ?? 0);
            if (lst.Any(x => x.Year2 != null))
                res.Year2 = lst.Sum(x => x.Year2 ?? 0);
            if (lst.Any(x => x.Year3 != null))
                res.Year3 = lst.Sum(x => x.Year3 ?? 0);
            if (lst.Any(x => x.Year4 != null))
                res.Year4 = lst.Sum(x => x.Year4 ?? 0);
            if (lst.Any(x => x.Year5 != null))
                res.Year5 = lst.Sum(x => x.Year5 ?? 0);
            if (lst.Any(x => x.Year6 != null))
                res.Year6 = lst.Sum(x => x.Year6 ?? 0);

            return res;
        }
        public static decimal? GetDiv(decimal? num1, decimal? num2)
        {
            if (num1 == null || num2 == null || num2 == 0)
                return null;
            else
                return num1 / num2;
        }

        public static bool Compare(decimal? num1, decimal? num2)
        {
            if (num1 != null && num2 != null)
                return num1 > num2;
            if (num1 != null || (num1 == null && num2 == null))
                return true;
            return false;
        }
        public static decimal? GetSum(List<decimal?> lst)
        {
            var check = lst.FirstOrDefault(x => x != null);
            if (check != null)
            {
                var res = lst.Sum(x => x ?? 0);
                return res;
            }
            else
                return (decimal?)null;
        }
        public static decimal? GetAbs(decimal? num)
        {
            if (num != null)
                return Math.Abs(num.Value);
            return null;
        }
        public static string GetDateString(int year, int? q, DateTime? reportedDate, bool from_to, bool fye, bool same_endding = true, bool isCum = false, bool isChart = false)
        {
            reportedDate ??= new DateTime(year, 12, 31);
            if (q != null)
            {
                return GetQuarterDate(year, q.Value, reportedDate, from_to, isCum);
            }
            else
            {
                if (fye)
                {
                    return "FYE " + reportedDate.Value.ToString("dd MMM yyyy");
                }
                else
                {
                    if ((reportedDate.Value.ToString("MM") == "12" && same_endding == true) || isChart == true)
                    {
                        return year.ToString();
                    }
                    else
                    {
                        return reportedDate.Value.ToString("dd MMM yyyy");
                    }
                }
            }
        }
        public static string GetCashFlowDate(int year, int? q, DateTime? reportedDate, bool from_to, bool fye, bool isCum = false)
        {
            reportedDate ??= new DateTime(year, 12, 31);
            if (year > 0)
                if (q > 0)
                {
                    return GetDateString(year, q.Value, reportedDate, from_to, isCum);
                }
                else
                {
                    if (fye)
                    {
                        return "FYE " + reportedDate.Value.ToString("dd MMM yyyy");
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "12")
                        {
                            return year.ToString();
                        }
                        else
                        {
                            return reportedDate.Value.ToString("dd MMM yyyy");
                        }
                    }
                }

            return "Unavaible";
        }

        public static string GetQuarterDate(int y, int q, DateTime? reportedDate, bool from_to, bool? isCum = false)
        {
            switch (q)
            {
                case 1:
                    if (from_to)
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "01 Apr " + (y - 1) + " - 30 Jun " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "01 Jul " + (y - 1) + " - 30 Sep " + (y - 1);
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "01 Oct " + (y - 1) + " - 31 Dec " + (y - 1);
                        else
                            return "01 Jan " + y + " - 31 Mar " + y;
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "30 Jun " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "30 Sep " + (y - 1);
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "31 Dec " + (y - 1);
                        else
                            return "31 Mar " + y;
                    }
                case 2:
                    if (from_to)
                    {
                        if (isCum == true)
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Apr " + (y - 1) + " - 30 Sep " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Jul " + (y - 1) + " - 31 Dec " + (y - 1);
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Oct " + (y - 1) + " - 31 Mar " + y;
                            else
                                return "01 Jan " + y + " - 30 Jun " + y;
                        }
                        else
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Jul " + (y - 1) + " - 30 Sep " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Oct " + (y - 1) + " - 31 Dec " + (y - 1);
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Jan " + y + " - 31 Mar " + y;
                            else
                                return "01 Apr " + y + " - 30 Jun " + y;
                        }
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "30 Sep " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "31 Dec " + (y - 1);
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "31 Mar " + y;
                        else
                            return "30 Jun " + y;
                    }
                case 3:
                    if (from_to)
                    {
                        if (isCum == true)
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Apr " + (y - 1) + " - 31 Dec " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Jul " + (y - 1) + " - 31 Mar " + y;
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Oct " + (y - 1) + " - 30 Jun " + y;
                            else
                                return "01 Jan " + y + " - 30 Sep " + y;
                        }
                        else
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Oct " + (y - 1) + " - 31 Dec " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Jan " + y + " - 31 Mar " + y;
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Apr " + y + " - 30 Jun " + y;
                            else
                                return "01 Jul " + y + " - 30 Sep " + y;
                        }
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "31 Dec " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "31 Mar " + y;
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "30 Jun " + y;
                        else
                            return "30 Sep " + y;
                    }
                case 4:
                    if (from_to)
                    {
                        if (isCum == true)
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Apr " + (y - 1) + " - 31 Mar " + y;
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Jul " + (y - 1) + " - 30 Jun " + y;
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Oct " + (y - 1) + " - 30 Sep " + y;
                            else
                                return "01 Jan " + y + " - 31 Dec " + y;
                        }
                        else
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Jan " + y + " - 31 Mar " + y;
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Apr " + y + " - 30 Jun " + y;
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Jul " + y + " - 30 Sep " + y;
                            else
                                return "01 Oct " + y + " - 31 Dec " + y;
                        }
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "31 Mar " + y;
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "30 Jun " + y;
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "30 Sep " + y;
                        else
                            return "31 Dec " + y;
                    }
                case 5:
                    if (from_to)
                    {
                        if (isCum == true)
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Apr " + (y - 1) + " - 30 Sep " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Jul " + (y - 1) + " - 31 Dec " + (y - 1);
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Oct " + y + " - 31 Mar " + y;
                            else
                                return "01 Jan " + y + " - 30 Jun " + y;
                        }
                        else
                        {
                            if (reportedDate.Value.ToString("MM") == "03")
                                return "01 Jul " + (y - 1) + " - 30 Sep " + (y - 1);
                            else if (reportedDate.Value.ToString("MM") == "06")
                                return "01 Oct " + (y - 1) + " - 31 Dec " + (y - 1);
                            if (reportedDate.Value.ToString("MM") == "09")
                                return "01 Jan " + y + " - 31 Mar " + y;
                            else
                                return "01 Apr " + y + " - 30 Jun " + y;
                        }
                    }
                    else
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "30 Sep " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "31 Jan " + y;
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "31 Mar " + y;
                        else
                            return "30 Jun " + y;
                    }
                default:
                    return string.Empty;
            }
        }

        public static YearQuarterDTO GetYearQuarter(int y, int? q, string type)
        {
            int y1 = y - 0; int? q1 = null;
            int y2 = y - 1; int? q2 = null;
            int y3 = y - 2; int? q3 = null;
            int y4 = y - 3; int? q4 = null;
            int y5 = y - 4; int? q5 = null;
            int y6 = y - 5; int? q6 = null;

            if (type == "2")
            {
                if (q == 4)
                {
                    y1 = y2 = y3 = y4 = y;
                    y5 = y6 = y - 1;
                    q1 = 4; q2 = 3; q3 = 2; q4 = 1; q5 = 4; q6 = 3;
                }
                if (q == 3)
                {
                    y1 = y2 = y3 = y;
                    y4 = y5 = y6 = y - 1;
                    q1 = 3; q2 = 2; q3 = 1; q4 = 4; q5 = 3; q6 = 2;
                }
                if (q == 2)
                {
                    y1 = y2 = y;
                    y3 = y4 = y5 = y6 = y - 1;
                    q1 = 2; q2 = 1; q3 = 4; q4 = 3; q5 = 2; q6 = 1;
                }
                if (q == 1)
                {
                    y1 = y;
                    y2 = y3 = y4 = y5 = y - 1;
                    y6 = y - 2;
                    q1 = 1; q2 = 4; q3 = 3; q4 = 2; q5 = 1; q6 = 4;
                }

            }

            if (type == "3")
            {
                q1 = q2 = q3 = q4 = q5 = q6 = q;
            }
            if (type == "4")
            {
                q1 = q2 = q3 = q4 = q5 = q6 = 2;
            }

            return new YearQuarterDTO
            {
                Quarter1 = q1,
                Quarter2 = q2,
                Quarter3 = q3,
                Quarter4 = q4,
                Quarter5 = q5,
                Quarter6 = q6,

                Year1 = y1,
                Year2 = y2,
                Year3 = y3,
                Year4 = y4,
                Year5 = y5,
                Year6 = y6
            };
        }

        public static HeaderDTO GetHeader(FSDTO fs, string Type, bool from_to, bool fye, bool same_endding = true, bool isCum = false, bool isChart = false, string typeCum = "")
        {
            var res = new HeaderDTO() {
                Type = Type
            };
            var fs1 = fs.FS1;
            var fs2 = fs.FS2;
            var fs3 = fs.FS3;
            var fs4 = fs.FS4;
            var fs5 = fs.FS5;
            var enddings = new List<string>();
            var endding1 = fs1?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding1))
                enddings.Add(endding1);
            var endding2 = fs2?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding2))
                enddings.Add(endding2);
            var endding3 = fs3?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding3))
                enddings.Add(endding3);
            var endding4 = fs4?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding4))
                enddings.Add(endding4);
            var endding5 = fs5?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding5))
                enddings.Add(endding5);
            same_endding = enddings.Distinct().Count() == 1;

            bool isCumYear1 = false;
            bool isCumYear2 = false;
            bool isCumYear3 = false;
            bool isCumYear4 = false;
            bool isCumYear5 = false;
           
            if (fs1 != null)
            {
                if (isCum == true && !string.IsNullOrEmpty(typeCum) && (typeCum.Contains("PL") || typeCum.Contains("CF")))
                {
                    if (typeCum.Contains("PL"))
                    {
                        isCumYear1 = fs1.IsProfit == true;
                        if (Type == "2" || Type == "3")
                            isCumYear1 = false;
                        if (Type == "4" || Type == "3")
                            isCumYear1 = true;
                    }
                        
                    else if (typeCum.Contains("CF"))
                    {
                        isCumYear1 = fs1.IsCashFlow == true;
                        bool IsProfit = fs1.IsProfit != null && fs1.IsProfit.Value;
                        if (Type == "1" && IsProfit == false) isCumYear1 = false;
                        if (Type == "2" || Type == "3") isCumYear1 = false;
                    } 
                }
                res.Year1 = typeCum.Contains("CF") ? GetCashFlowDate(fs1.FiscalYear, fs1.QuarterNumber, fs1.ReportedDate, from_to, fye, isCumYear1) : GetDateString(fs1.FiscalYear, fs1.QuarterNumber, fs1.ReportedDate, from_to, fye, same_endding, isCumYear1, isChart);
                res.AuditStatus1 = fs1.AuditStatus;
                res.ConsolidatedStatus1 = fs1.ConsolidatedStatus;
                res.Source1 = fs1.Source;
                res.Numyear = 1;
                if (fs2 != null)
                {
                    if (isCum == true && !string.IsNullOrEmpty(typeCum) && (typeCum.Contains("PL") || typeCum.Contains("CF")))
                    {
                        if (typeCum.Contains("PL"))
                        {
                            isCumYear2 = fs2.IsProfit == true;
                            if (Type == "2" || Type == "3")
                                isCumYear2 = false;
                            if (Type == "4" || Type == "3")
                                isCumYear2 = true;
                        }
                        else if (typeCum.Contains("CF"))
                        {
                            isCumYear2 = fs2.IsCashFlow == true;
                            bool IsProfit = fs2.IsProfit != null && fs2.IsProfit.Value;
                            if (Type == "1" && IsProfit == false) isCumYear2 = false;
                            if (Type == "2" || Type == "3") isCumYear2 = false;
                        }
                    }
                    res.Year2 = typeCum.Contains("CF") ? GetCashFlowDate(fs2.FiscalYear, fs2.QuarterNumber, fs2.ReportedDate, from_to, fye, isCumYear2) : GetDateString(fs2.FiscalYear, fs2.QuarterNumber, fs2.ReportedDate, from_to, fye, same_endding, isCumYear2, isChart);
                    res.AuditStatus2 = fs2.AuditStatus;
                    res.ConsolidatedStatus2 = fs2.ConsolidatedStatus;
                    res.Source2 = fs2.Source;
                    res.Numyear = 2;
                    if (fs3 != null)
                    {
                        if (isCum == true && !string.IsNullOrEmpty(typeCum) && (typeCum.Contains("PL") || typeCum.Contains("CF")))
                        {
                            if (typeCum.Contains("PL"))
                            {
                                isCumYear3 = fs3.IsProfit == true;
                                if (Type == "2" || Type == "3")
                                    isCumYear3 = false;
                                if (Type == "4" || Type == "3")
                                    isCumYear3 = true;
                            }
                            else if (typeCum.Contains("CF"))
                            {
                                isCumYear3 = fs3.IsCashFlow == true;
                                bool IsProfit = fs3.IsProfit != null && fs3.IsProfit.Value;
                                if (Type == "1" && IsProfit == false) isCumYear3 = false;
                                if (Type == "2" || Type == "3") isCumYear3 = false;
                            }
                        }
                        res.Year3 = typeCum.Contains("CF") ? GetCashFlowDate(fs3.FiscalYear, fs3.QuarterNumber, fs3.ReportedDate, from_to, fye, isCumYear3) : GetDateString(fs3.FiscalYear, fs3.QuarterNumber, fs3.ReportedDate, from_to, fye, same_endding, isCumYear3, isChart);
                        res.AuditStatus3 = fs3.AuditStatus;
                        res.ConsolidatedStatus3 = fs3.ConsolidatedStatus;
                        res.Source3 = fs3.Source;
                        res.Numyear = 3;
                        if (fs4 != null)
                        {
                            if (isCum == true && !string.IsNullOrEmpty(typeCum) && (typeCum.Contains("PL") || typeCum.Contains("CF")))
                            {
                                if (typeCum.Contains("PL"))
                                {
                                    isCumYear4 = fs4.IsProfit == true;
                                    if (Type == "2" || Type == "3")
                                        isCumYear4 = false;
                                    if (Type == "4" || Type == "3")
                                        isCumYear4 = true;
                                }
                                else if (typeCum.Contains("CF"))
                                {
                                    isCumYear4 = fs4.IsCashFlow == true;
                                    bool IsProfit = fs4.IsProfit != null && fs4.IsProfit.Value;
                                    if (Type == "1" && IsProfit == false) isCumYear4 = false;
                                    if (Type == "2" || Type == "3") isCumYear4 = false;
                                }
                            }
                            res.Year4 = typeCum.Contains("CF") ? GetCashFlowDate(fs1.FiscalYear, fs4.QuarterNumber, fs4.ReportedDate, from_to, fye, isCumYear4) : GetDateString(fs4.FiscalYear, fs4.QuarterNumber, fs4.ReportedDate, from_to, fye, same_endding, isCumYear4, isChart);
                            res.Numyear = 4;
                            if (fs5 != null)
                            {
                                if (isCum == true && !string.IsNullOrEmpty(typeCum) && (typeCum.Contains("PL") || typeCum.Contains("CF")))
                                {
                                    if (typeCum.Contains("PL"))
                                    {
                                        isCumYear5 = fs5.IsProfit == true;
                                        if (Type == "2" || Type == "3")
                                            isCumYear5 = false;
                                        if (Type == "4" || Type == "3")
                                            isCumYear5 = true;
                                    }
                                    else if (typeCum.Contains("CF"))
                                    {
                                        isCumYear5 = fs5.IsCashFlow == true;
                                        bool IsProfit = fs5.IsProfit != null && fs5.IsProfit.Value;
                                        if (Type == "1" && IsProfit == false) isCumYear5 = false;
                                        if (Type == "2" || Type == "3") isCumYear5 = false;
                                    }
                                }
                                res.Year5 = typeCum.Contains("CF") ? GetCashFlowDate(fs5.FiscalYear, fs5.QuarterNumber, fs5.ReportedDate, from_to, fye, isCumYear5) : GetDateString(fs5.FiscalYear, fs5.QuarterNumber, fs5.ReportedDate, from_to, fye, same_endding, isCumYear5, isChart);
                                res.Numyear = 5;
                            }
                        }
                    }
                }
            }

            return res;
        }
        public static HeaderDTO GetHeaderRatio(FSDTO fs, string Type, bool same_endding = false)
        {
            var res = new HeaderDTO() {
                Type = Type
            };

            var fs1 = fs.FS1;
            var fs2 = fs.FS2;
            var fs3 = fs.FS3;
            var fs4 = fs.FS4;
            var fs5 = fs.FS5;

            var enddings = new List<string>();
            var endding1 = fs1?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding1))
                enddings.Add(endding1);
            var endding2 = fs2?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding2))
                enddings.Add(endding2);
            var endding3 = fs3?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding3))
                enddings.Add(endding3);
            var endding4 = fs4?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding4))
                enddings.Add(endding4);
            var endding5 = fs5?.ReportedDate?.ToString("MM");
            if (!string.IsNullOrEmpty(endding5))
                enddings.Add(endding5);
            same_endding = enddings.Distinct().Count() == 1;

            if (fs1 != null)
            {
                bool isProfit1 = fs1.IsProfit != null && fs1.IsProfit.Value;
                if (Type == "2")
                    isProfit1 = false;
                if (Type == "4" || Type == "3")
                    isProfit1 = true;
                res.Year1 = GetFSRatiosYear(fs1.FiscalYear, fs1.QuarterNumber, fs1.ReportedDate, same_endding, isProfit1);
                res.AuditStatus1 = fs1.AuditStatus;
                res.ConsolidatedStatus1 = fs1.ConsolidatedStatus;
                res.Source1 = fs1.Source;
                res.Numyear = 1;

                if (fs2 != null)
                {
                    bool isProfit2 = fs2.IsProfit != null && fs2.IsProfit.Value;
                    if (Type == "2")
                        isProfit2 = false;
                    if (Type == "4" || Type == "3")
                        isProfit2 = true;
                    res.Year2 = GetFSRatiosYear(fs2.FiscalYear, fs2.QuarterNumber, fs2.ReportedDate, same_endding, isProfit2);
                    res.AuditStatus2 = fs2.AuditStatus;
                    res.ConsolidatedStatus2 = fs2.ConsolidatedStatus;
                    res.Source2 = fs2.Source;
                    res.Numyear = 2;
                    if (fs3 != null)
                    {
                        bool isProfit3 = fs3.IsProfit != null && fs3.IsProfit.Value;
                        if (Type == "2")
                            isProfit3 = false;
                        if (Type == "4" || Type == "3")
                            isProfit3 = true;
                        res.Year3 = GetFSRatiosYear(fs3.FiscalYear, fs3.QuarterNumber, fs3.ReportedDate, same_endding, isProfit3);
                        res.AuditStatus3 = fs3.AuditStatus;
                        res.ConsolidatedStatus3 = fs3.ConsolidatedStatus;
                        res.Source3 = fs3.Source;
                        res.Numyear = 3;
                        if (fs4 != null)
                        {
                            bool isProfit4 = fs4.IsProfit != null && fs4.IsProfit.Value;
                            if (Type == "2")
                                isProfit4 = false;
                            if (Type == "4" || Type == "3")
                                isProfit4 = true;
                            res.Year4 = GetFSRatiosYear(fs4.FiscalYear, fs4.QuarterNumber, fs4.ReportedDate, same_endding, isProfit4);
                            res.Numyear = 4;
                            if (fs5 != null)
                            {
                                bool isProfit5 = fs5.IsProfit != null && fs5.IsProfit.Value;
                                if (Type == "2")
                                    isProfit5 = false;
                                if (Type == "4" || Type == "3")
                                    isProfit5 = true;
                                res.Year5 = GetFSRatiosYear(fs5.FiscalYear, fs5.QuarterNumber, fs5.ReportedDate, same_endding, isProfit5);
                                res.Numyear = 5;
                            }
                        }
                    }
                }
            }
            return res;
        }

        public static string GetFSRatiosYear(int year, int? q, DateTime? reportedDate, bool same_endding = true, bool isCum = false)
        {
            reportedDate ??= new DateTime(year, 12, 31);
            int y = year;

            if (year > 0)
                if (q == 1)
                {
                    if (isCum == true)
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "01 Apr " + (y - 1) + " - 30 Jun " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "01 Jul " + (y - 1) + " - 30 Sep " + (y - 1);
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "01 Oct " + (y - 1) + " - 31 Dec " + (y - 1);
                        else
                            return "01 Jan " + y + " - 31 Mar " + y;
                    }
                    else
                    {
                        return "Q1<br/>(FYE " + reportedDate.Value.ToString("dd MMM yyyy") + ")";
                    }

                }
                else if (q == 2)
                {
                    if (isCum == true)
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "01 Apr " + (y - 1) + " - 30 Sep " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "01 Jul " + (y - 1) + " - 31 Dec " + (y - 1);
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "01 Oct " + (y - 1) + " - 31 Mar " + y;
                        else
                            return "01 Jan " + y + " - 30 Jun " + y;
                    }
                    else
                    {
                        return "Q2<br/>(FYE " + reportedDate.Value.ToString("dd MMM yyyy") + ")";
                    }

                }
                else if (q == 3)
                {
                    if (isCum == true)
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "01 Apr " + (y - 1) + " - 31 Dec " + (y - 1);
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "01 Jul " + (y - 1) + " - 31 Mar " + y;
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "01 Oct " + (y - 1) + " - 30 Jun " + y;
                        else
                            return "01 Jan " + y + " - 30 Sep " + y;
                    }
                    else
                    {
                        return "Q3<br/>(FYE " + reportedDate.Value.ToString("dd MMM yyyy") + ")";
                    }

                }
                else if (q == 4)
                {
                    if (isCum == true)
                    {
                        if (reportedDate.Value.ToString("MM") == "03")
                            return "01 Apr " + (y - 1) + " - 31 Mar " + y;
                        else if (reportedDate.Value.ToString("MM") == "06")
                            return "01 Jul " + (y - 1) + " - 30 Jun " + y;
                        if (reportedDate.Value.ToString("MM") == "09")
                            return "01 Oct " + (y - 1) + " - 30 Sep " + y;
                        else
                            return "01 Jan " + y + " - 31 Dec " + y;
                    }
                    else
                    {
                        return "Q4<br/>(FYE " + reportedDate.Value.ToString("dd MMM yyyy") + ")";
                    }

                }
                else if (q == 5)
                {
                    return "Half-year<br/>(FYE " + reportedDate.Value.ToString("dd MMM yyyy") + ")";
                }
                else
                {
                    if (reportedDate.Value.ToString("MM") == "12" && same_endding == true)
                    {
                        return year.ToString();
                    }
                    else
                    {
                        return reportedDate.Value.ToString("dd MMM yyyy");
                    }
                }

            return "Unavaible";
        }
    }

    public class ReportsDTO
    {
        public int? Index { get; set; }
        public string AccountCode { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Report { get; set; }
        public string Parent { get; set; }
        public byte? Level { get; set; }
        public decimal? Year1 { get; set; }
        public decimal? Year2 { get; set; }
        public decimal? Year3 { get; set; }
        public decimal? Year4 { get; set; }
        public decimal? Year5 { get; set; }
        public decimal? Year6 { get; set; }
        public decimal? Year7
        {
            get
            {
                return 0;
            }
        }
    }

    public class YearQuarterDTO
    {
        public int Year1 { get; set; }
        public int Year2 { get; set; }
        public int Year3 { get; set; }
        public int Year4 { get; set; }
        public int Year5 { get; set; }
        public int Year6 { get; set; }

        public int? Quarter1 { get; set; }
        public int? Quarter2 { get; set; }
        public int? Quarter3 { get; set; }
        public int? Quarter4 { get; set; }
        public int? Quarter5 { get; set; }
        public int? Quarter6 { get; set; }
    }

    public class HeaderDTO
    {
        public string Year1 { get; set; }
        public string Year2 { get; set; }
        public string Year3 { get; set; }
        public string Year4 { get; set; }
        public string Year5 { get; set; }
        public string Type { get; set; }
        public int? Numyear { get; set; }

        public string AuditStatus1 { get; set; }
        public string AuditStatus2 { get; set; }
        public string AuditStatus3 { get; set; }

        public string ConsolidatedStatus1 { get; set; }
        public string ConsolidatedStatus2 { get; set; }
        public string ConsolidatedStatus3 { get; set; }

        public string Source1 { get; set; }
        public string Source2 { get; set; }
        public string Source3 { get; set; }
    }

    public class FSDTO
    {
        public FinancialReport FS1 { get; set; }
        public FinancialReport FS2 { get; set; }
        public FinancialReport FS3 { get; set; }
        public FinancialReport FS4 { get; set; }
        public FinancialReport FS5 { get; set; }
        public FinancialReport FS6 { get; set; }
    }
}

