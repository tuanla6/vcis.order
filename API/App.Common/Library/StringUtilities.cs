﻿using App.Common.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
namespace App.Common.Library
{
    public static class StringUtilities
    {
        /// <summary>
        /// Split string to list string
        /// </summary>
        /// <param name="input"></param>
        /// <param name="separator"></param>
        /// <param name="splitOptions"></param>
        /// <returns></returns>
        public static string[] Split(this string input, string separator, StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries)
        {
            return input.Split(new[] { separator }, splitOptions);
        }

        public static bool IsIn(this string str, IEnumerable<string> array)
        {
            return array.Contains(str);
        }

        public static string Replace(string pObject, string key = "_", string value = " ")
        {
            return pObject.Replace(key, value);
        }

        /// <summary>
        /// convert to html tags
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ConvertToHtmlTags(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = text.Replace("\n", "<br>");
                //text = text.Replace("\t", "<pre>");
                text = text.Replace("\r\n", "<br>");
                text = text.Replace("<br> ", "<br>&nbsp;");
                var index = text.IndexOf("&nbsp; ", StringComparison.Ordinal);

                while (index > 0)
                {
                    text = text.Replace("&nbsp; ", "&nbsp;&nbsp;");
                    index = text.IndexOf("&nbsp; ", StringComparison.Ordinal);
                }
            }
            return text;
        }

        public static string HtmlEncode(string input)
        {
            return HttpUtility.HtmlEncode(input);
        }

        public static string HtmlDecode(string input)
        {
            return HttpUtility.HtmlDecode(input);
        }

        public static string ReplaceCharacter(string find, string replace, string str)
        {
            return str.Replace(find, replace);
        }

        /// <summary>
        /// Replace - characters by space
        /// Uppercase first character of each word
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string UppercaseFirstCharacter(string input)
        {
            input = input.Replace("-", " ");
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }

        public static string AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;

            var newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        public static void GenerateName(this string fullName, out string firstName, out string lastName)
        {
            firstName = string.Empty;
            lastName = string.Empty;
            if (string.IsNullOrEmpty(fullName))
                return;
            var nameParts = fullName.Split(' ');
            // Full name more than 1 part
            if (nameParts.Count() >= 2)
            {
                lastName = nameParts.Last();
                firstName = string.Join(" ", nameParts.Take(nameParts.Count() - 1));
            }
            // Only 1 part
            else
            {
                firstName = fullName;
            }
        }

        public static string RemoveHtmlTags(string input)
        {
            return Regex.Replace(input, "<.*?>", "");
        }

        public static string GenerateAddress(string street, string district, string province, string country, string category, string districtCategory)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(street))
                result = street;
            if (!string.IsNullOrEmpty(district))
            {
                if (IsNumber(district.Trim()))
                {
                    if (!string.IsNullOrEmpty(districtCategory))
                        result = !string.IsNullOrEmpty(result) ? $"{result}, {districtCategory} {district.Trim()}" : $"{districtCategory} {district.Trim()}";
                    else
                        result = !string.IsNullOrEmpty(result) ? $"{result}, District {district.Trim()}" : $"District {district.Trim()}";
                }
                else
                {
                    if (!string.IsNullOrEmpty(districtCategory))
                        result = !string.IsNullOrEmpty(result) ? $"{result}, {district.Trim()} {districtCategory}" : $"{district.Trim()} {districtCategory}";
                    else
                        result = !string.IsNullOrEmpty(result) ? $"{result}, {district.Trim()} District" : $"{district.Trim()} District";
                }
            }

            if (!string.IsNullOrEmpty(province))
            {
                result = !string.IsNullOrEmpty(result) ? $"{result}, {province}" : province;
                if (!string.IsNullOrEmpty(category))
                    result = $"{result} {category}";
            }

            if (!string.IsNullOrEmpty(country))
                result = !string.IsNullOrEmpty(result) ? $"{result}, {country}" : country;

            return result;
        }

        public static string GetContentType(string extension)
        {
            switch (extension.ToLower())
            {
                case ".doc":
                    return "application/msword";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".csv":
                    return "text/csv";
                case ".pdf":
                    return "application/pdf";
                case ".xls":
                    return "application/vnd.ms-excel";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xml":
                    return "application/xml";
                case ".json":
                    return "application/json";
                case ".txt":
                    return "application/plain";
                case ".png":
                    return "application/png";
                case ".bin":
                    return "application/png";
                case ".rar":
                    return "application/x-rar-compressed";
                case ".zip":
                    return "application/zip";
                case ".7z":
                    return "application/x-7z-compressed";
                case ".gif":
                    return "application/gif";
                case ".jpeg":
                case ".jpg":
                    return "application/jpeg";
                case ".htm":
                case ".html":
                    return "application/html";
                case ".ico":
                    return "application/x-icon";
                case ".js":
                    return "application/javascript";
                case ".css":
                    return "application/css";
            }

            return string.Empty;
        }

        public static bool IsImage(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return false;
            fileName = fileName.ToLower();
            return fileName.EndsWith(".png") || fileName.EndsWith(".jpg") || fileName.EndsWith(".gif") ||
                   fileName.EndsWith(".jpeg") || fileName.EndsWith(".bmp") || fileName.EndsWith(".tif");
        }
        public static bool IsNumber(string input)
        {
            return Regex.IsMatch(input, AppConst.OnlyNumbers);
        }
        public static string GenerateAddress(AddressDTO dto)
        {
            if (dto != null)
            {
                string street = dto.Street;
                string district = dto.District;
                string province = dto.Province;
                string country = dto.Country;
                string category = dto.DistrictCategory;
                string districtCategory = dto.ProvinceCategory;

                var result = string.Empty;
                if (!string.IsNullOrEmpty(street))
                    result = street;
                if (!string.IsNullOrEmpty(district))
                {
                    if (IsNumber(district.Trim()))
                    {
                        if (!string.IsNullOrEmpty(districtCategory))
                            result = !string.IsNullOrEmpty(result) ? $"{result}, {districtCategory} {district.Trim()}" : $"{districtCategory} {district.Trim()}";
                        else
                            result = !string.IsNullOrEmpty(result) ? $"{result}, District {district.Trim()}" : $"District {district.Trim()}";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(districtCategory))
                            result = !string.IsNullOrEmpty(result) ? $"{result}, {district.Trim()} {districtCategory}" : $"{district.Trim()} {districtCategory}";
                        else
                            result = !string.IsNullOrEmpty(result) ? $"{result}, {district.Trim()} District" : $"{district.Trim()} District";
                    }
                }

                if (!string.IsNullOrEmpty(province))
                {
                    result = !string.IsNullOrEmpty(result) ? $"{result}, {province}" : province;
                    if (!string.IsNullOrEmpty(category))
                        result = $"{result} {category}";
                }

                if (!string.IsNullOrEmpty(country))
                    result = !string.IsNullOrEmpty(result) ? $"{result}, {country}" : country;

                return result;
            }
            return null;
        }
    }


    public class AddressDTO
    {
        public string Street { get; set; }
        public string District { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string ProvinceCategory { get; set; }
        public string DistrictCategory { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}
