﻿
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace App.Common.Library
{
    public static class UrlUtilities
    {
        private static readonly Regex InvalidUrlCharacter = new Regex(@"\W+",
            RegexOptions.Compiled | RegexOptions.Singleline |
            RegexOptions.IgnoreCase);


        /// <summary>
        /// Check if url is local
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsLocalUrl(this string url)
        {
            if (String.IsNullOrEmpty(url))
            {
                return false;
            }
            return ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
        }


        /// <summary>
        /// To the URL string.
        /// </summary>
        /// <param name="s">The s.</param>
        /// <returns></returns>
        public static string ToUrlString(this string s)
        {
            if (s == null)
            {
                return null;
            }
            s = s.ToLower();
            var sb = new StringBuilder(s.Length);
            foreach (var c in s)
            {
                var m = RemapInternationalCharToAscii(c);
                if (string.IsNullOrEmpty(m)) sb.Append(c);
                else sb.Append(m);
            }
            var str = sb.ToString().Trim();
            str = !string.IsNullOrEmpty(str) ? InvalidUrlCharacter.Replace(str, "-") : str;

            return str.ToLower();
        }

        /// <summary>
        /// Remap all international char to Ascii
        /// </summary>
        /// <param name="c">the input string</param>
        /// <returns></returns>
        public static string RemapInternationalCharToAscii(char c)
        {
            var l = Convert.ToString(c).ToLowerInvariant();

            if ("àáạảãäąăååắằặẳẵâấầậẩẩ".Contains(l))
            {
                return "a";
            }
            if ("èéẹẻẽêếềệểễëę".Contains(l))
            {
                return "e";
            }
            if ("ìíịỉĩîïı".Contains(l))
            {
                return "i";
            }
            if ("òóọỏõôồốộổỗơờớợởỡöøőð".Contains(l))
            {
                return "o";
            }
            if ("ùúụủũûüŭůưứừựửữ".Contains(l))
            {
                return "u";
            }
            if ("çćčĉ".Contains(l))
            {
                return "c";
            }
            if ("żźž".Contains(l))
            {
                return "z";
            }
            if ("śşšŝ".Contains(l))
            {
                return "s";
            }
            if ("ñń".Contains(l))
            {
                return "n";
            }
            if ("ýỳỵỷỹÿ".Contains(l))
            {
                return "y";
            }
            if ("ğĝ".Contains(l))
            {
                return "g";
            }
            switch (c)
            {
                case 'ř':
                    return "r";
                case 'ł':
                    return "l";
                case 'đ':
                    return "d";
                case 'ß':
                    return "ss";
                case 'Þ':
                    return "th";
                case 'ĥ':
                    return "h";
                case 'ĵ':
                    return "j";
                default:
                    return "";
            }
        }

        public static string UnChangeToSlug(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : value.Replace("+", " ");
        }

        public static string ChangeToSlug(this string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            return value.Replace(" ", "+");
        }

        public static string ToUrlParam(this string value, int startIndex = 0)
        {
            if (string.IsNullOrWhiteSpace(value)) return "";
            var index = value.LastIndexOf("?", StringComparison.Ordinal);
            if (index < 0) return "";

            return startIndex == 0 ? value.Substring(startIndex, index) : value.Substring(index);
        }

        public static string ToReplaceSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : value.Replace(" ", "");
        }

        public static string ToFirstLetter(string firstName, string lastName, int? maxLength = 20)
        {
            var fullName = $"{firstName} {lastName}";
            if (string.IsNullOrWhiteSpace(fullName) || fullName.Length < maxLength)
                return fullName;

            return string.Format("{0}{1}", string.IsNullOrWhiteSpace(firstName) ? new char() : firstName.FirstOrDefault(),
                string.IsNullOrWhiteSpace(lastName) ? new char() : lastName.FirstOrDefault());
        }

        public static string ToGetNameEmail(this string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : value.Substring(0, value.IndexOf("@"));
        }

        public static string RemoveVietnameseTone(this string text)
        {
            string result = text.ToLower();
            result = Regex.Replace(result, "à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|/g", "a");
            result = Regex.Replace(result, "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|/g", "e");
            result = Regex.Replace(result, "ì|í|ị|ỉ|ĩ|/g", "i");
            result = Regex.Replace(result, "ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|/g", "o");
            result = Regex.Replace(result, "ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|/g", "u");
            result = Regex.Replace(result, "ỳ|ý|ỵ|ỷ|ỹ|/g", "y");
            result = Regex.Replace(result, "đ", "d");
            return result;
        }
    }
}
