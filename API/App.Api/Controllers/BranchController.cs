﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers
{
    /// <summary>
    /// branch
    /// </summary>
    [Route("api/vcis-business-branch")]
    [AuthorizeApi]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly IBranchService _service;
        public BranchController(IBranchService service)
        {
            _service = service;
        }


        [HttpGet("")]
        public ActionResult GetBranch(int businessId)
        {
            try
            {
                var result = _service.GetBranch(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }


    }
}
