﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers.VCIS
{
    /// <summary>
    /// negativenew
    /// </summary>
    [Route("api/vcis-business-negativenew")]
    [AuthorizeApi]
    [ApiController]
    public class NegativeNewsController : ControllerBase
    {
        private readonly INegativeNewsesService _service;
        public NegativeNewsController(INegativeNewsesService service)
        {
            _service = service;
        }
        [HttpGet("")]
        public ActionResult GetNews(int businessId)
        {
            try
            {
                var result = _service.GetNews(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
    }
}
