﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;

namespace App.Api.Controllers
{
    /// <summary>
    /// NegativePaymentController
    /// </summary>
    [Route("api/vcis-business-negativepayment")]
   // [AuthorizeApi]
    [ApiController]
    public class NegativePaymentController : ControllerBase
    {
        private readonly INegativePaymentService _service;
        public NegativePaymentController(INegativePaymentService service)
        {
            _service = service;
        }
        [Route("getnegativepayment")]
        public ActionResult GetNegativePayment(int id)
        {
            try
            {
                var result = _service.GetNagativePayment(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
    }
}
