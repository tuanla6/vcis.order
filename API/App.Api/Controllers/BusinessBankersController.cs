﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers.VCIS
{
    /// <summary>
    /// business-bankers
    /// </summary>
    [Route("api/vcis-business-bankers")]
    //[AuthorizeApi]
    [ApiController]
    public class BusinessBankersController : ControllerBase
    {
        private readonly IBusinessBankersService _service;
        public BusinessBankersController(IBusinessBankersService service)
        {
            _service = service;
        }

        [HttpGet("")]
        public ActionResult GetBusinessBanker(int businessId)
        {
            try
            {
                var result = _service.GetBusinessBanker(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
    }
}
