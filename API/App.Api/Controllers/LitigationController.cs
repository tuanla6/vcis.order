﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers
{
    /// <summary>
    /// litigation
    /// </summary>
    [Route("api/vcis-business-litigation")]
    //[AuthorizeApi]
    [ApiController]
    public class LitigationController : ControllerBase
    {
        private readonly ILitigationService _service;
        public LitigationController(ILitigationService service)
        {
            _service = service;
        }

        [HttpGet("")]
        public ActionResult GetLitigation(int businessId)
        {
            try
            {
                var result = _service.GetLitigation(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
    }
}
