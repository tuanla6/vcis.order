﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;

namespace App.Api.Controllers
{
    /// <summary>
    /// NegativePaymentController
    /// </summary>
    [Route("api/vcis-business-lineofbusiness")]
   // [AuthorizeApi]
    [ApiController]
    public class LineofBusinessController : ControllerBase
    {
        private readonly ILineofBusinessService _service;
        public LineofBusinessController(ILineofBusinessService service)
        {
            _service = service;
        }
        [Route("")]
        public ActionResult GetBusinessActivities(int id)
        {
            try
            {
                var result = _service.GetBusinessActivities(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
    }
}
