﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers
{
    /// <summary>
    /// shareholders
    /// </summary>
    [Route("api/vcis-business-shareholders")]
    [AuthorizeApi]
    [ApiController]
    public class ShareholderController : ControllerBase
    {
        private readonly IShareholderService _service;
        public ShareholderController(IShareholderService service)
        {
            _service = service;
        }

        [HttpGet("")]
        public ActionResult GetShareholder(int businessId)
        {
            try
            {
                var result =  _service.GetShareholder(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }


        [HttpGet("subsidiary-company")]
        public ActionResult GetSubsidiary(int businessId)
        {
            try
            {
                var result = _service.GetSubsidiaryCompany(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("related-company")]
        public ActionResult GetRelated(int businessId)
        {
            try
            {
                var result = _service.GetRelatedCompany(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

    }
}
