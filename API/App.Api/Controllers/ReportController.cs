﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers
{
    /// <summary>
    /// report
    /// </summary>
    [Route("api/vcis-report")]
    // [AuthorizeApi]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportsService _service;
        public ReportController(IReportsService service)
        {
            _service = service;
        }


        [HttpGet("")]
        public ActionResult GetReportSummary(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetReportSummary(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("summaryfs-overview")]
        public ActionResult GetSummaryFSOverView(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetSummaryFSOverView(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("balacesheet")]
        public ActionResult GetBalanceSheet(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetBalanceSheet(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        [HttpGet("incomestatement")]
        public ActionResult GetIncomeStatement(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetIncomeStatement(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("cashflow-direct")]
        public ActionResult GetCashflowDirect(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetCashflowDirect(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        [HttpGet("cashflow-indirect")]
        public ActionResult GetCashflowInDirect(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetCashflowInDirect(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("growth-rate-ratios")]
        public ActionResult GetGrowthRateRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetGrowthRateRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("liquidityratios")]
        public ActionResult GetLiquidityRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetLiquidityRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        [HttpGet("activityratios")]
        public ActionResult GetActivityRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetActivityRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("creditratios")]
        public ActionResult GetCreditRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetCreditRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("coverageratios")]
        public ActionResult GetCoverageRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetCoverageRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        [HttpGet("performance")]
        public ActionResult GetPerformance(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetPerformance(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("return-on-investment")]
        public ActionResult GetReturnOnInvestment(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetReturnOnInvestment(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("return-on-netsales")]
        public ActionResult GetReturnOnNetSales(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetReturnOnNetSales(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("solvency-ratios")]
        public ActionResult GetSolvencyRatios(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetSolvencyRatios(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        
        [HttpGet("financial-analysis")]
        public ActionResult GetFinancialAnalysis(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetFinancialAnalysis(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        [HttpGet("data-margin")]
        public ActionResult GetDataMargin(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetDataMargin(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpGet("cash-conversion-cycle")]
        public ActionResult GetCashConversionCycle(int businessId, int year, int? q, string type = "1")
        {
            try
            {
                var result = _service.GetCashConversionCycle(businessId, year, q, type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

        [HttpPost("DupontAnalysis")]
        public ActionResult GetDupontAnalysis(DupontAnalysisDTO dto)
        {
            try
            {
                var result = _service.GetDupontAnalysis(dto);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }


        [HttpPost("top10companies")]
        public ActionResult GetTop10Companies(DupontAnalysisDTO dto)
        {
            try
            {
                var result = _service.GetTop10Companies(dto);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

    }
}
