﻿using App.Business.Services;
using App.Common.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Api.Controllers
{
    /// <summary>
    /// importexport
    /// </summary>
    [Route("api/vcis-business-importexport")]
    [AuthorizeApi]
    [ApiController]
    public class ImportExportsController : ControllerBase
    {
        private readonly IImportExportsService _service;
        public ImportExportsController(IImportExportsService service)
        {
            _service = service;
        }

        [HttpGet("")]
        public ActionResult GetImportExports(int businessId)
        {
            try
            {
                var result = _service.GetImportExports(businessId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }

    }
}
