﻿using App.Business.Services;
using App.Common.Base;
using App.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using static App.Business.Services.BusinessesDTO;

namespace App.Api.Controllers
{
    /// <summary>
    /// QTHT - Người dùng
    /// </summary>
    [Route("api/Businesses")]
    //[AuthorizeApi]
    [ApiController]
    public class BusinessesController : ControllerBase
    {
        private readonly IBusinessesService _businessesService;
        public BusinessesController(IBusinessesService businessesService)
        {
            _businessesService = businessesService;
        }

        #region GetAll 
        /// <summary>
        /// Lấy danh sách người dùng
        /// </summary>
        /// <param name="page">default = 1, lựa chọn page hiển thị</param>
        /// <param name="page_size"> cấu hình số dòng trả ra trong 1 page </param>
        /// <param name="sort">  sort = { field:value }</param>
        /// <param name="filter"> filter = { field:value }</param>
        /// <param name="search">Từ khóa tìm kiếm </param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public async Task<ActionResult> Get(int page = 1, int page_size = 0, string sort = null, string filter = null, string search = null, bool is_select_data = false)
        {
            try
            {
                dynamic filterObj = new object();
                var query = new SearchConditionsViewModel();
                try
                {
                    if (filter != null)
                    {
                        filterObj = JsonConvert.DeserializeObject<dynamic>(filter);
                    }
                    else
                    {
                        filterObj = null;
                    }
                }
                catch (Exception)
                {
                    ErrorCtr.Reject(HttpStatusCode.BadRequest, "invalid_argument", "tham số sort truyền vào không đúng");
                }

                if (filterObj != null)
                {
                    string name = filterObj.name;
                    if (!String.IsNullOrEmpty(name))
                    {
                        query.BusinessName = name;
                    }
                    string crid = filterObj.crid;
                    if (!String.IsNullOrEmpty(crid))
                    {
                        query.CRID = crid;
                    }
                    string tax_brn = filterObj.tax_brn;
                    if (!String.IsNullOrEmpty(tax_brn))
                    {
                        query.TaxCodeOrRegistrationNumber = tax_brn;
                    }
                }

                if (!String.IsNullOrEmpty(search))
                {
                    query.Keyword = search;
                }

                var result = _businessesService.SearchBusiness(query, page, page_size);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var err = ErrorCtr.ExtractErrorInfo(ex);
                return StatusCode(Convert.ToInt32(err.statusCode), err);
            }
        }
        #endregion
    }
}
