using Microsoft.Extensions.DependencyInjection;

namespace App.Business
{
    public static class IServiceCollectionExtension
    {
        public static IServiceCollection AddAppLibrary(this IServiceCollection services)
        {

            services.AddAppBusinessLibrary();
            return services;
        }
    }
}
