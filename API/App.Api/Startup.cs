﻿using App.Business;
using App.Common.Base;
using App.Data;
using App.Data.Models.VCIS;
using Identity;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServerHost.Quickstart.UI;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace App.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // csdl
            var migrationsAssembly = AppConst.ASSEMBLY_DATA;
            var connection_UseSqlServer = this.Configuration.GetConnectionString("sqlServer");
            services.AddDbContext<APPContext>
                (builder =>
                builder.UseSqlServer(connection_UseSqlServer,
                sql => sql.MigrationsAssembly(migrationsAssembly))
                );

            //Register Vcis Entity Framework  
            var vcisConnection = Configuration.GetConnectionString("VcisConnection");
            services.AddDbContext<VCIS4Context>(options => options.UseSqlServer(vcisConnection));

            services.AddHttpContextAccessor();
            ConfigureIdentityServerServices(services, connection_UseSqlServer, migrationsAssembly);
            services.AddAppLibrary();
            services.AddMvc();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            InitializeDatabase(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add(
                    "Content-Security-Policy",
                    "script-src 'self' 'unsafe-inline' https://code.iconify.design https://api.iconify.design/; " +
                    "style-src 'self' 'unsafe-inline'; " +
                    "img-src 'self'  data:;");

                await next();
            });
            app.UseStaticFiles();
            app.UseRouting();
            //cấu hình cors
            ConfigureCors(app);
            app.UseIdentityServer();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var PersistedContext = serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>();
            var ConfigContext = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            var AppContext = serviceScope.ServiceProvider.GetRequiredService<APPContext>();
            PersistedContext.Database.Migrate();
            ConfigContext.Database.Migrate();
            AppContext.Database.Migrate();


            if (!ConfigContext.Clients.Any())
            {
                foreach (var client in Config.Clients)
                {
                    ConfigContext.Clients.Add(client.ToEntity());
                }
                ConfigContext.SaveChanges();
            }

            if (!ConfigContext.IdentityResources.Any())
            {
                foreach (var resource in Config.Ids)
                {
                    ConfigContext.IdentityResources.Add(resource.ToEntity());
                }
                ConfigContext.SaveChanges();
            }

            if (!ConfigContext.ApiResources.Any())
            {
                foreach (var resource in Config.Apis)
                {
                    ConfigContext.ApiResources.Add(resource.ToEntity());
                }
                ConfigContext.SaveChanges();
            }
        }

        private void ConfigureIdentityServerServices(IServiceCollection services, string connectionStr, string migrationsAssembly)
        {
            var Authority = this.Configuration.GetSection("ApplicationSettings:Authority").Get<string>();

            var builder = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;
            })
              //.AddTestUsers(TestUsers.Users)
              // this adds the config data from DB (clients, resources, CORS)
              .AddConfigurationStore(options =>
              {
                  options.ConfigureDbContext = builder => builder.UseSqlServer(connectionStr,
                      sql => sql.MigrationsAssembly(migrationsAssembly));
              })
              // this adds the operational data from DB (codes, tokens, consents)
              .AddOperationalStore(options =>
              {
                  options.ConfigureDbContext = builder => builder.UseSqlServer(connectionStr,
                      sql => sql.MigrationsAssembly(migrationsAssembly));

                  // this enables automatic token cleanup. this is optional.
                  options.EnableTokenCleanup = true;
              })
              .AddCustomUserStore();

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential();

            services.AddAuthentication
            (options =>
            {
                options.DefaultScheme = "oidc";
                options.DefaultAuthenticateScheme = "oidc";
                options.DefaultChallengeScheme = "oidc";
            })
             .AddCookie("oidc", options =>
             {
                 options.CookieManager = new ChunkingCookieManager();
             })
             .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
             {
                 options.Authority = Authority;
                 options.RequireHttpsMetadata = false;
                 options.BackchannelHttpHandler = new HttpClientHandler { ServerCertificateCustomValidationCallback = delegate { return true; } };
                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateAudience = false

                 };
                 options.SaveToken = true;
                 options.Events = new JwtBearerEvents
                 {
                     OnChallenge = context =>
                     {
                         // Skip the default logic.
                         context.HandleResponse();

                         var payload = new JObject
                         {
                             ["error"] = context.Error,
                             ["error_description"] = context.ErrorDescription,
                             ["error_uri"] = context.ErrorUri
                         };

                         context.Response.ContentType = "application/json";
                         context.Response.StatusCode = 401;

                         return context.Response.WriteAsync(payload.ToString());
                     }
                 };
             });
        }

        private void ConfigureCors(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var ConfigContext = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
            var AllowOrigins = ConfigContext.ClientCorsOrigins.Select(x => x.Origin).ToArray();
            app.UseCors(builder =>
            {
                builder
                .WithOrigins(AllowOrigins)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
            });
        }
    }
}
