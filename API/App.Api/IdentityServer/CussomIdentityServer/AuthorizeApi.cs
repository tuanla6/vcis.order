﻿using Microsoft.AspNetCore.Authorization;

namespace App.Api
{
    public class AuthorizeApi : AuthorizeAttribute

    {

        public AuthorizeApi() : base()
        {
            this.AuthenticationSchemes = "oidc,Bearer";
        }
    }
}
