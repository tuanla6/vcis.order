﻿using App.Business.Services;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace IdentityServerHost.Quickstart.UI
{
    public class CustomProfileService : IProfileService
    {

        protected readonly INguoiDungService _nguoiDungService;

        public CustomProfileService(
            INguoiDungService nguoiDungService
            )
        {
            _nguoiDungService = nguoiDungService;
        }


        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var userID = Int32.Parse(context.Subject.GetSubjectId());

            //var permission_user = _nguoiDungService.GetPermissionUser(userID);
            var user = await _nguoiDungService.GetByIdAsync<NguoiDungDTO>(userID);

            var claims = new List<Claim>
            {
                new Claim("id", user.Id.ToString()),
                new Claim("tai_khoan", user.tai_khoan),
                new Claim("ten", user.ten),
                new Claim("email", user.email),
            };

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var userID = Int32.Parse(context.Subject.GetSubjectId());
            var user = await _nguoiDungService.GetByIdAsync<NguoiDungDTO>(userID);
            context.IsActive = user != null;
        }
    }
}