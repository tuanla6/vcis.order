export const API_URL='https://localhost:5001/';
export const AS_URL=`${API_URL}connect/`;
export const CLIENT = {
    client_id: 'VCIS.ORDER',
    client_secret: 'Q2jGsMahbmcgdHLDrG5oIGtob2EgaOG7jWMgdsOgIGPDtG5nIG5naOG7hyBxdeG7kWMgZ2lh',
    scope:'offline_access openid',
    redirect_uri:`${window.location.protocol}//${window.location.host}/oauth-callback`,
    logout_redirect_uri:`${window.location.protocol}//${window.location.host}/logout`
};
export const AUTHORIZATION_BASE = `Basic ${btoa(CLIENT.client_id + ':' + CLIENT.client_secret)}`;