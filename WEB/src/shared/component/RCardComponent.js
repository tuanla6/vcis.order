import React from 'react';
import PropTypes from 'prop-types';
import {
    CCard,
    CCardBody,
    CCardHeader,
} from '@coreui/react';
import './style.scss';
import { RLoading } from 'src/shared/component';
class RCardComponent extends React.PureComponent {
    static propTypes = {
        children: PropTypes.any,
        title: PropTypes.string,
        className: PropTypes.any,
    };
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        const { children, title, className, loading } = this.props;
        return (
            <CCard className={['rcard', className].join(' ')}>
                 <RLoading loading={loading} />
                <CCardHeader>{title} </CCardHeader>
                <CCardBody>
                    {children}
                </CCardBody>
            </CCard >
        );
    }

}

export { RCardComponent };