import React from 'react';
import PropTypes from 'prop-types';
import { Card, Collapse } from 'react-bootstrap';
import { RLoading } from 'src/shared/component';

class CardToogleComponent extends React.PureComponent {
    static propTypes = {
        children: PropTypes.any,
        defaultShow: PropTypes.bool,
        title: PropTypes.string,
        className: PropTypes.any,
        title_icon: PropTypes.string,
    };
    static defaultProps = {
        defaultShow: false
    }
    constructor(props) {
        super(props);
        this.state = {
            isShow: props.defaultShow,
        };

    }
    render() {
        const { children, title, className, loading } = this.props;
        return (
            <Card className={['shadow-none card-toogle', className].join(' ')}>
                <RLoading loading={loading} />
                <Card.Header
                    type="button"
                    className={this.state.isShow ? 'active' : ''}
                >
                    <h3 className="card-title">{title}</h3>
                </Card.Header>
                <Collapse in={this.state.isShow}>
                    {children}
                </Collapse>
            </Card>

        );
    }

}

export { CardToogleComponent };