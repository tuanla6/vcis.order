import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { AS_URL, CLIENT } from 'src/app-setting';


class RouteGuard extends React.PureComponent {
    static propTypes = {
        component: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
        oauth: PropTypes.object
    };
    isAuthenticated() {
        return true; //!!this.props.oauth.access_token;
    }
    redirectToAS(){
        window.location.href = `${AS_URL}authorize?client_id=${CLIENT.client_id}&scope=${CLIENT.scope}&response_type=code&redirect_uri=${CLIENT.redirect_uri}`;
    }
    render() {
        const { component: Component, ...rest } = this.props;
        const renderRoute = props => {
            if (this.isAuthenticated()) {
                return (
                    <Component {...props} />
                );
            }
            this.redirectToAS();
            
        };
        return (
            <Route {...rest} render={renderRoute} />

        );
    }
}
const mapStateToProps = (state) => {
    return {
        oauth: state.oauth
    };
};
const RouteGuardComponent = connect(mapStateToProps, {})(RouteGuard);
export { RouteGuardComponent };
