import React from 'react';

class RNoRecord extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
        };

    }
    render() {
        const { text } = this.props;
        return (
            <table className="table table-hover">
                <tbody>
                    <tr>
                        <td colSpan="6" style={{borderTop: 0}}>
                            <div className="text-center my-5">
                                <h2>{text || 'No records'}</h2>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }

}

export { RNoRecord };