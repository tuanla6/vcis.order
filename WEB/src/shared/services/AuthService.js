import { tap,map } from 'rxjs/operators';
import { http ,jsonToUrlencoded} from 'src/shared/utils';
import { AUTHORIZATION_BASE,CLIENT } from 'src/app-setting';
import { store } from 'src/redux/store';
import { OauthAction } from 'src/redux/actions';
class AuthService {
    constructor(_store, { setToken, removeToken }) {
        this.store = _store;
        this.dispatch=_store.dispatch;
        this.action = {
            setToken, removeToken
        };
    }
    login(data) {
        
        const body = jsonToUrlencoded({
            grant_type: 'password',
            username: data.username,
            password: data.password
        });
        return http.post('connect/token',body,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': AUTHORIZATION_BASE
            }
        }).pipe(tap((res) => {
            this.dispatch(this.action.setToken(res));
        }));
    }
    loginWithAuthCode(data){
        const body = jsonToUrlencoded({
            grant_type: 'authorization_code',
            code: data.code,
            redirect_uri: CLIENT.redirect_uri,
        });
        return http.post('connect/token',body,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': AUTHORIZATION_BASE
            }
        }).pipe(tap((res) => {
            console.log('res=', res);
            this.dispatch(this.action.setToken(res));
        }));
    }
    logout() {
        this.dispatch(this.action.removeToken());
    }
    refreshToken(refresh_token){
        const body = jsonToUrlencoded({
            grant_type: 'refresh_token',
            refresh_token: refresh_token
        });
        return http.post('connect/token', body, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': AUTHORIZATION_BASE
            }
        }).pipe(
            map(res => {
                const newToken = res;
                this.dispatch(this.action.setToken(newToken));
                return newToken;
            })
        );
    }
}
const authService = new AuthService(store, OauthAction);
export { authService, AuthService };
