import { http } from '../utils';

class BaseService {
    constructor(props = {}) {
        this.url = props ? props.url: '';
        this.http=http;
    }
    getMany(props) {
        const { page, page_size, sort, filter,search } = props;
        const params = Object.assign({}, {
            page: page,
            page_size: page_size,
            sort: JSON.stringify(sort),
            filter: JSON.stringify(filter),
            search:search
        }, filter);
        return http.get(`${this.url}`, { params: params });
    }
    getById(id) {
        return http.get(`${this.url}/${id}`);
    }
    create(obj) {
        return http.post(`${this.url}`, obj);
    }
    update(obj, id) {
        return http.put(`${this.url}/${id}`, obj);
    }
    del(id) {
        return http.delete(`${this.url}/${id}`);
    }
    deletes(listId = []) {
        return this.http.post(this.url + '/deletes', listId);
    }
    dels(listId = [], id) {
        return http.post(`${this.url}/${id}`, listId);
    }
}

export { BaseService };
