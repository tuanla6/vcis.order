import { API_URL, AUTHORIZATION_BASE } from 'src/app-setting';
import HttpClient from './http/httpClient';
import { jsonToUrlencoded } from './http/htppUtils';

//utils
export { jsonToUrlencoded };
export const http = new HttpClient({
  apiUrl: API_URL,
  authorizationBase: AUTHORIZATION_BASE,
});

export {
  currencyFormat,
  formatDate,
  getLastDayOfQuarter,
  getDayChartRatios,
  getDistanceToTop,
  currencyFormatBilion,
  currencyFormatToFixed3,
} from './format';
