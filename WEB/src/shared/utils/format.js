import { format, parseISO } from 'date-fns';

function currencyFormat(num) {
  if (num) {
    num = parseFloat(num);
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  } else return '';
}
function currencyFormatToFixed3(num) {
  if (num) {
    num = parseFloat(num);
    return num.toFixed(2);
  } else return '';
}
function currencyFormatBilion(num) {
  if (num) {
    num = parseFloat(num / 1000000000);
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  } else return '';
}

function formatDate(value, type) {
  if (value) {
    const result = format(parseISO(value.replace('Z', '')), type ? type : 'dd MMM yyyy');
    return result;
  } else {
    return '';
  }
}

function getLastDayOfQuarter(y, q, m_end) {
  const q_endings = [
    [3, 31],
    [6, 30],
    [9, 30],
    [12, 31],
  ];
  const m_endings = [3, 6, 9, 12];
  const idx = m_endings.indexOf(m_end);

  let quater = q - 1 - (4 - (idx + 1));
  if (quater < 0) {
    y = y - 1;
    quater = 4 + quater;
  }
  const res = new Date(y, q_endings[quater][0] - 1, q_endings[quater][1]);
  return format(res, 'dd MMM yyyy');
}

function getDayChartRatios(dateString, type) {
  try {
    if (dateString) {
      if (type === '1') {
        return format(new Date(dateString), 'yyyy');
      }
      if (type === '2' || type === '3') {
        //"Q4<br/>(FYE 31 Dec 2020)"
        let quarter = parseInt(dateString.split('<br/>(FYE ')[0].split('')[1]);
        dateString = dateString.split('<br/>(FYE ')[1].split(')')[0];
        const year = parseInt(format(new Date(dateString), 'yyyy'));
        const month_end = parseInt(format(new Date(dateString), 'MM'));
        return getLastDayOfQuarter(year, quarter, month_end);
      }
      if (type === '4') {
        //01 Jan 2020 - 30 Jun 2020
        return dateString.split(' - ')[1];
      }
    }
  } catch {
    return dateString;
  }
}

function getDistanceToTop(el) {
  let div = document.querySelector(el);
  let windown = window.innerHeight;
  if (div) {
    let distanceToTop = div.getBoundingClientRect().top;
    return windown - distanceToTop;
  }
  return windown;
}

export {
  formatDate,
  currencyFormat,
  getLastDayOfQuarter,
  getDayChartRatios,
  getDistanceToTop,
  currencyFormatBilion,
  currencyFormatToFixed3,
};
