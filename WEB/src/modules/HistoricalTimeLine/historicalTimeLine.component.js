import React from 'react';
import { Card } from 'react-bootstrap';
import { CCol, CRow, } from '@coreui/react';
import { RCard } from 'src/shared/component';
import './style.scss';
import { historicalTimeService } from './service';
class HistoricalTimeLine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listHistoricalTimeline: [],
            id: this.props.id,
            loadingHistoricalTimeline: false,
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.getHistoricalTimeLine();
    }
    componentWillUnmount() {
    }
    getHistoricalTimeLine() {
        let id = this.state.id;
        this.setState({ loadingHistoricalTimeline: true });
        if (id) {
            this.subscriptions['getHistoricalTimeLine'] = historicalTimeService.getHistoricalTimeline(id).subscribe(res => {
                this.setState({
                    listHistoricalTimeline: res,
                    loadingHistoricalTimeline: false
                });
            },
                err => {
                    console.log(err);
                    this.setState({ loadingHistoricalTimeline: false });
                }
            );
        }
    }
    render() {
        return (
            <div className="content-historical">
                <CRow>
                    <CCol xs="12" lg="6">
                        <RCard title="Company name" className="card card-danger" loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 1).map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <Card.Text className="card-body-title">
                                                {item.date}
                                            </Card.Text>
                                            <Card.Text className="card-body-title">
                                                Former  {item.columnName}:
                                            </Card.Text>
                                            <Card.Text>
                                                &nbsp;  &nbsp;  {item.oldValue}
                                            </Card.Text>
                                            <Card.Text className="card-body-title">
                                                Changed to:
                                            </Card.Text>
                                            <Card.Text>
                                                &nbsp; &nbsp; {item.newValue}
                                            </Card.Text>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                    <CCol xs="12" lg="6">
                        <RCard title="Charter capital" className="card card-danger" loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 2).map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <Card.Title className="card-body-title">
                                                Charter capital increase: <span className="text-date"> &nbsp; {item.date}</span>
                                            </Card.Title>

                                            <Card.Title className="card-body-title">
                                                From:
                                            </Card.Title>
                                            <Card.Text>
                                                &nbsp;  &nbsp; {item.oldValue} to:  {item.newValue}
                                            </Card.Text>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                </CRow>

                <CRow>
                    <CCol xs="12" lg="6">
                        <RCard title="Business registration number" className="card card-danger" loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 3).map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <Card.Text className="card-body-title">
                                                Former {item.columnName}:
                                            </Card.Text>
                                            <Card.Text>
                                                &nbsp; &nbsp; {item.oldValue}
                                            </Card.Text>
                                            <Card.Text className="card-body-title">
                                                Changed to:
                                            </Card.Text>
                                            <Card.Text>
                                                &nbsp; &nbsp; {item.newValue}
                                            </Card.Text>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                    <CCol xs="12" lg="6">
                        <RCard title="Contact address" className="card card-danger" defaultShow={true} loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 4).map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <Card.Text className="card-body-title">
                                                {item.date}
                                            </Card.Text>
                                            <Card.Title className="card-body-title">
                                                Former Address:
                                            </Card.Title>
                                            <Card.Text>
                                                &nbsp; &nbsp; {item.oldValue}
                                            </Card.Text>

                                            <Card.Title className="card-body-title">
                                                Changed to:
                                            </Card.Title>
                                            <Card.Text>
                                                &nbsp;  &nbsp; {item.newValue}
                                            </Card.Text>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                </CRow>

                <CRow>
                    <CCol xs="12" lg="6">
                        <RCard title="Financial statements" className="card card-danger" defaultShow={true} loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 5).map((item, index) => {
                                    return (
                                        <div key={index}>

                                            <Card.Text className="card-body-title">
                                                Financial statements last update: <span className="text-date"> &nbsp; {item.oldValue}</span>
                                            </Card.Text>
                                            <Card.Text className="card-body-title">
                                                Financial statements next update:<span className="text-date"> &nbsp; {item.newValue}</span>
                                            </Card.Text>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                    <CCol xs="12" lg="6">
                        <RCard title="Quick rating" className="card card-danger"  defaultShow={true} loading={this.state.loadingHistoricalTimeline}>
                            {
                                this.state.listHistoricalTimeline.filter((e) => e.type === 5).map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <Card.Title className="card-body-title">
                                                Current rating: <span className="text-date"> &nbsp; {item.newValue}</span>
                                            </Card.Title>

                                            <Card.Title className="card-body-title">
                                                Last rating: <span className="text-date"> &nbsp; {item.oldValue}</span>
                                            </Card.Title>
                                        </div>
                                    );
                                })
                            }
                        </RCard>
                    </CCol>
                </CRow>

            </div>
        )
    }
}

export { HistoricalTimeLine };