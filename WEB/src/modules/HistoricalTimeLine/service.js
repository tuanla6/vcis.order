import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class HistoricalTimeService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
    getHistoricalTimeline(id) {
        let url = 'api/vcis-business/historicaltimeline?id=' + id;
        return http.get(url);
    }
}
const historicalTimeService = new HistoricalTimeService();
export { historicalTimeService, HistoricalTimeService };