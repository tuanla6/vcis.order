import React from 'react';
import PropTypes from 'prop-types';
import {
    CCard,
    CCol,
    CRow,
    // CPagination
} from '@coreui/react';
import { Row, Col, Tabs, Tab } from 'react-bootstrap';
import { LiquidityRatios } from './LiquidityRatios';
import { ActivityRatios } from './ActivityRatios';
import { GrowthRateRatios } from './growthRateRatios';
import { Performance } from './performance';
import { ReturnOnInvestment } from './returnOnInvestment';
import { CreditRatios } from './CreditRatios';
import { CoverageRatio } from './CoverageRatio';
import { SolvencyRatios } from './SolvencyRatios';
import { ReturnOnNetSales } from './ReturnOnNetSales'

class BusinessFinancialRatios extends React.Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    };
    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            type: props.type,
            year: props.year,
            quarter: props.quarter,
        };
    }
    componentDidMount() {
    }
    componentDidUpdate() {
        if (this.props.type !== this.state.type) {
            this.setState({ type: this.props.type })
        }
        if (this.props.year !== this.state.year) {
            this.setState({ year: this.props.year })
        }
        if (this.props.quarter !== this.state.quarter) {
            this.setState({ quarter: this.props.quarter })
        }
    }
    componentWillUnmount() {

    }

    render() {
        return (
            <CRow>
                <CCol xl={12}>
                    <CCard className="p30">
                        <Row>
                            <Col lg={12} md={12} className="info-ratio">
                                <Tabs id="controlled-tab-example"
                                    defaultActiveKey="Liquidity"
                                    mountOnEnter={true}
                                    unmountOnExit={true}>
                                    <Tab eventKey="Liquidity" title={
                                        <React.Fragment> Liquidity Ratios
                                        </React.Fragment>} >
                                        <LiquidityRatios
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Activity" title={
                                        <React.Fragment> Activity Ratios
                                        </React.Fragment>} >
                                        <ActivityRatios
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Credit" title={
                                        <React.Fragment> Credit Ratios
                                        </React.Fragment>} >
                                        <CreditRatios
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Coverage" title={
                                        <React.Fragment> Coverage Ratios
                                        </React.Fragment>} >
                                        <CoverageRatio
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Solvency" title={
                                        <React.Fragment> Solvency Ratios
                                        </React.Fragment>} >
                                        <SolvencyRatios
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="NSales" title={
                                        <React.Fragment> Return on Net Sales
                                        </React.Fragment>} >
                                        <ReturnOnNetSales
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Investment" title={
                                        <React.Fragment> Return on Investment
                                        </React.Fragment>} >
                                        <ReturnOnInvestment
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Performance" title={
                                        <React.Fragment> Performance
                                        </React.Fragment>} >
                                        <Performance
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                    <Tab eventKey="Growth" title={
                                        <React.Fragment> Growth Rate Ratios
                                        </React.Fragment>} >
                                        <GrowthRateRatios
                                            id={this.state.id}
                                            type={this.state.type}
                                            year={this.state.year}
                                            quarter={this.state.quarter}
                                        />
                                    </Tab>
                                </Tabs>
                            </Col>

                        </Row>
                    </CCard>
                </CCol>
            </CRow>
        )
    }
}

export { BusinessFinancialRatios };
