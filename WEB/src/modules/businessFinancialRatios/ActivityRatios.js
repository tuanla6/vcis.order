import React from 'react';
import { CCol, CRow } from '@coreui/react';
import { Row, Table } from 'react-bootstrap';
import {
  Chart,
  Tooltip,
  Axis,
  Legend,
  Geom,
  Coordinate,
  Point,
  Line,
  Area,
} from 'bizcharts';
import DataSet from '@antv/data-set';
import './style.scss';
import { financialRatiosService } from './service';
import {
  currencyFormat,
  getDayChartRatios,
  getDistanceToTop,
} from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class ActivityRatios extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingActivityRatios: false,
      data: [],
      header: {},
      numyear: null,
      chartData1: [],
      chartData2: [],
      chartData3: [],
      chartData4: [],
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({
      heightLoading: getDistanceToTop('#Activity-Ratios'),
    });
    this.getActivityRatios();
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getActivityRatios();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getActivityRatios();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getActivityRatios();
      });
    }
  }
  getActivityRatios() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      this.setState({ loadingActivityRatios: true });

      this.subscriptions['getActivityRatios'] =
        financialRatiosService
          .getActivityRatios(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let header1 = getDayChartRatios(
                header.year1,
                type
              );
              let header2 = getDayChartRatios(
                header.year2,
                type
              );
              let header3 = getDayChartRatios(
                header.year3,
                type
              );
              let header4 = getDayChartRatios(
                header.year4,
                type
              );
              let header5 = getDayChartRatios(
                header.year5,
                type
              );

              let inventoryTurnover = res.data.find(
                (x) => x.index === 1
              );
              let workingCapitalTurnover = res.data.find(
                (x) => x.index === 4
              );
              let payablestoSupplierTurnover =
                res.data.find((x) => x.index === 3);
              let chartData1 = [];
              let chartData2 = [];
              let chartData3 = [];
              let chartData4 = [];
              let receivablesfromCustomerTurnover =
                res.data.find((x) => x.index === 2);
              let fixedAssetTurnover = res.data.find(
                (x) => x.index === 5
              );

              if (inventoryTurnover != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Inventory turnover',
                    value: inventoryTurnover.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Inventory turnover',
                    value: inventoryTurnover.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Inventory turnover',
                    value: inventoryTurnover.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Inventory turnover',
                    value: inventoryTurnover.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'Inventory turnover',
                    value: inventoryTurnover.year1,
                  });
                  chartData3.push({
                    item: 'Inventory turnover',
                    'Average industry':
                      inventoryTurnover.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      inventoryTurnover.year1?.toFixed(2),
                  });
                }
              }

              if (receivablesfromCustomerTurnover != null) {
                if (header.numyear >= 1) {
                  chartData3.push({
                    item: 'Receivables from customer turnover',
                    'Average industry':
                      receivablesfromCustomerTurnover.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      receivablesfromCustomerTurnover.year1?.toFixed(
                        2
                      ),
                  });
                }
              }

              if (workingCapitalTurnover != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Working capital turnover',
                    value: workingCapitalTurnover.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Working capital turnover',
                    value: workingCapitalTurnover.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Working capital turnover',
                    value: workingCapitalTurnover.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Working capital turnover',
                    value: workingCapitalTurnover.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'Working capital turnover',
                    value: workingCapitalTurnover.year1,
                  });
                  chartData3.push({
                    item: 'Working capital turnover',
                    'Average industry':
                      workingCapitalTurnover.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      workingCapitalTurnover.year1?.toFixed(
                        2
                      ),
                  });
                }
              }
              if (payablestoSupplierTurnover != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Payables to supplier turnover',
                    value: payablestoSupplierTurnover.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Payables to supplier turnover',
                    value: payablestoSupplierTurnover.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Payables to supplier turnover',
                    value: payablestoSupplierTurnover.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Payables to supplier turnover',
                    value: payablestoSupplierTurnover.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'Payables to supplier turnover',
                    value: payablestoSupplierTurnover.year1,
                  });
                  chartData3.push({
                    item: 'Payables to supplier turnover',
                    'Average industry':
                      payablestoSupplierTurnover.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      payablestoSupplierTurnover.year1?.toFixed(
                        2
                      ),
                  });
                }
              }

              if (fixedAssetTurnover != null) {
                if (header.numyear >= 1) {
                  chartData3.push({
                    item: 'Fixed asset turnover',
                    'Average industry':
                      fixedAssetTurnover.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      fixedAssetTurnover.year1?.toFixed(2),
                  });
                }
              }
              let doh = res.data.find((x) => x.index === 6);
              let dso = res.data.find((x) => x.index === 7);
              let dpo = res.data.find((x) => x.index === 8);

              if (doh != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'DOH (Days)',
                    value: doh.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'DOH (Days)',
                    value: doh.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'DOH (Days)',
                    value: doh.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'DOH (Days)',
                    value: doh.year2,
                  });
                if (header.numyear >= 1) {
                  chartData2.push({
                    year: header1,
                    type: 'DOH (Days)',
                    value: doh.year1,
                  });
                  chartData4.push({
                    item: 'Days of inventory on hand (DOH)',
                    'Average industry':
                      doh.averageValue?.toFixed(2),
                    'Subject company':
                      doh.year1?.toFixed(2),
                  });
                }
              }
              if (dso != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'DSO (Days)',
                    value: dso.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'DSO (Days)',
                    value: dso.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'DSO (Days)',
                    value: dso.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'DSO (Days)',
                    value: dso.year2,
                  });
                if (header.numyear >= 1) {
                  chartData2.push({
                    year: header1,
                    type: 'DSO (Days)',
                    value: dso.year1,
                  });
                  chartData4.push({
                    item: 'Days of sales outstanding (DSO)',
                    'Average industry':
                      dso.averageValue?.toFixed(2),
                    'Subject company':
                      dso.year1?.toFixed(2),
                  });
                }
              }
              if (dpo != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'DPO (Days)',
                    value: dpo.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'DPO (Days)',
                    value: dpo.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'DPO (Days)',
                    value: dpo.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'DPO (Days)',
                    value: dpo.year2,
                  });
                if (header.numyear >= 1) {
                  chartData2.push({
                    year: header1,
                    type: 'DPO (Days)',
                    value: dpo.year1,
                  });
                  chartData4.push({
                    item: 'Number of days of payables (DPO)',
                    'Average industry':
                      dpo.averageValue?.toFixed(2),
                    'Subject company':
                      dpo.year1?.toFixed(2),
                  });
                }
              }
              this.setState({
                loadingActivityRatios: false,
                data: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
                chartData3: chartData3,
                chartData4: chartData4,
              });
            },
            (err) => {
              console.log(err);
              this.setState({
                loadingActivityRatios: false,
              });
            }
          );
    }
  }

  render() {
    const { numyear, header, loadingActivityRatios } =
      this.state;
    const { DataView } = DataSet;
    const dv1 = new DataView().source(
      this.state.chartData3
    );
    dv1.transform({
      type: 'fold',
      fields: ['Average industry', 'Subject company'], // 展开字段集
      key: 'user', // key字段
      value: 'score', // value字段
    });

    const dv2 = new DataView().source(
      this.state.chartData4
    );
    dv2.transform({
      type: 'fold',
      fields: ['Average industry', 'Subject company'], // 展开字段集
      key: 'user', // key字段
      value: 'score', // value字段
    });

    const cols = {
      year: {
        range: [0, 1],
      },
    };
    return (
      <div
        className="content-business-per"
        id="Activity-Ratios"
      >
        {loadingActivityRatios ? (
          <CRow>
            <CCol
              xs="12"
              lg="12"
              style={{ height: this.state.heightLoading }}
            >
              <RLoading loading={true} />
            </CCol>
          </CRow>
        ) : (
          <React.Fragment>
            <CRow>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData1}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>

              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData2}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
            </CRow>
            <CRow className="chart-ratio">
              <CCol xs="12" lg="6">
                <Chart
                  height={220}
                  data={dv1.rows}
                  autoFit
                  interactions={['legend-highlight']}
                >
                  <Coordinate type="polar" radius={0.8} />
                  <Tooltip shared />
                  <Point
                    position="item*score"
                    color="user"
                    shape="circle"
                  />
                  <Line
                    position="item*score"
                    color="user"
                    size="2"
                  />
                  <Area
                    position="item*score"
                    color="user"
                  />

                  <Axis
                    name="score"
                    grid={{ line: { type: 'line' } }}
                  />

                  <Axis name="item" line={false} />
                </Chart>
              </CCol>

              <CCol xs="12" lg="6">
                <Chart
                  height={220}
                  data={dv2.rows}
                  autoFit
                  interactions={['legend-highlight']}
                >
                  <Coordinate type="polar" radius={0.8} />
                  <Tooltip shared />
                  <Point
                    position="item*score"
                    color="user"
                    shape="circle"
                  />
                  <Line
                    position="item*score"
                    color="user"
                    size="2"
                  />
                  <Area
                    position="item*score"
                    color="user"
                  />

                  <Axis
                    name="score"
                    grid={{ line: { type: 'line' } }}
                  />

                  <Axis name="item" line={false} />
                </Chart>
              </CCol>
            </CRow>

            <Row className="content-data-detail">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center">
                      Description
                    </th>
                    {this.state.type === '1' && (
                      <th className="text-center">
                        Industry Average
                      </th>
                    )}
                    <th className="text-center">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: header.year1,
                        }}
                      />
                    </th>
                    {numyear >= 2 && (
                      <th className="text-center">
                        {' '}
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year2,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 3 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year3,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 4 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year4,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 5 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year5,
                          }}
                        />
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {this.state.data.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <span>{item.name}</span>
                          <p className="data-comment">
                            {item.comment}
                          </p>
                        </td>

                        {this.state.type === '1' && (
                          <td className="text-right">
                            {currencyFormat(
                              item.averageValue
                            )}
                          </td>
                        )}
                        <td className="text-right">
                          {currencyFormat(item.year1)}
                        </td>
                        {numyear >= 2 && (
                          <td className="text-right">
                            {currencyFormat(item.year2)}
                          </td>
                        )}
                        {numyear >= 3 && (
                          <td className="text-right">
                            {currencyFormat(item.year3)}
                          </td>
                        )}
                        {numyear >= 4 && (
                          <td className="text-right">
                            {currencyFormat(item.year4)}
                          </td>
                        )}
                        {numyear >= 5 && (
                          <td className="text-right">
                            {currencyFormat(item.year5)}
                          </td>
                        )}
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Row>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { ActivityRatios };
