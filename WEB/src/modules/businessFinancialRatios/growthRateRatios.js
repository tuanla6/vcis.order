import React from 'react';
import { CCol, CRow } from '@coreui/react';
import { Row, Table } from 'react-bootstrap';
import {
  Chart,
  Tooltip,
  Axis,
  Legend,
  Geom,
} from 'bizcharts';
import { financialRatiosService } from './service';
import {
  currencyFormat,
  getDayChartRatios,
  getDistanceToTop,
} from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
import './style.scss';

class GrowthRateRatios extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData1: [],
      chartData2: [],
      datatable: [],
      header: {},
      numyear: null,
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({
      heightLoading: getDistanceToTop('#GrowthRate-Ratios'),
    });
    this.GetGrowthRateRatios();
  }

  componentWillUnmount() {}
  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.GetGrowthRateRatios();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.GetGrowthRateRatios();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.GetGrowthRateRatios();
      });
    }
  }

  GetGrowthRateRatios() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      this.setState({ loading: true });

      this.subscriptions['GetGrowthRateRatios'] =
        financialRatiosService
          .GetGrowthRateRatios(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let header1 = getDayChartRatios(
                header.year1,
                type
              );
              let header2 = getDayChartRatios(
                header.year2,
                type
              );
              let header3 = getDayChartRatios(
                header.year3,
                type
              );
              let header4 = getDayChartRatios(
                header.year4,
                type
              );
              let header5 = getDayChartRatios(
                header.year5,
                type
              );

              let TotalAssets = res.data.find(
                (x) => x.index === 1
              );
              let OwnersEquity = res.data.find(
                (x) => x.index === 2
              );

              let EBITDA = res.data.find(
                (x) => x.index === 6
              );
              let EBIT = res.data.find(
                (x) => x.index === 7
              );
              let NetIncome = res.data.find(
                (x) => x.index === 9
              );
              let chartData1 = [];
              let chartData2 = [];
              if (TotalAssets != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Total Accets Growth Rate',
                    value: TotalAssets.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Total Accets Growth Rate',
                    value: TotalAssets.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Total Accets Growth Rate',
                    value: TotalAssets.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Total Accets Growth Rate',
                    value: TotalAssets.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: 'Total Accets Growth Rate',
                    value: TotalAssets.year1,
                  });
              }
              if (OwnersEquity != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: "Owner's Equity Growth Rate",
                    value: OwnersEquity.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: "Owner's Equity Growth Rate",
                    value: OwnersEquity.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: "Owner's Equity Growth Rate",
                    value: OwnersEquity.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: "Owner's Equity Growth Rate",
                    value: OwnersEquity.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: "Owner's Equity Growth Rate",
                    value: OwnersEquity.year1,
                  });
              }

              if (EBITDA != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'EBITDA Growth Rate',
                    value: EBITDA.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'EBITDA Growth Rate',
                    value: EBITDA.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'EBITDA Growth Rate',
                    value: EBITDA.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'EBITDA Growth Rate',
                    value: EBITDA.year2,
                  });
                if (header.numyear >= 1)
                  chartData2.push({
                    year: header1,
                    type: 'EBITDA Growth Rate',
                    value: EBITDA.year1,
                  });
              }
              if (EBIT != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'EBIT Growth Rate',
                    value: EBIT.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'EBIT Growth Rate',
                    value: EBIT.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'EBIT Growth Rate',
                    value: EBIT.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'EBIT Growth Rate',
                    value: EBIT.year2,
                  });
                if (header.numyear >= 1)
                  chartData2.push({
                    year: header1,
                    type: 'EBIT Growth Rate',
                    value: EBIT.year1,
                  });
              }
              if (NetIncome != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'Net Income Growth Rate',
                    value: NetIncome.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'Net Income Growth Rate',
                    value: NetIncome.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'Net Income Growth Rate',
                    value: NetIncome.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'Net Income Growth Rate',
                    value: NetIncome.year2,
                  });
                if (header.numyear >= 1)
                  chartData2.push({
                    year: header1,
                    type: 'Net Income Growth Rate',
                    value: NetIncome.year1,
                  });
              }

              this.setState({
                loading: false,
                datatable: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBalanceSheet: false });
            }
          );
    }
  }

  render() {
    const { numyear, header, loading } = this.state;
    const cols = {
      year: {
        range: [0, 1],
      },
    };
    return (
      <div
        className="content-business-pay"
        id="GrowthRate-Ratios"
      >
        {loading ? (
          <CRow>
            <CCol
              xs="12"
              lg="12"
              style={{ height: this.state.heightLoading }}
            >
              <RLoading loading={true} />
            </CCol>
          </CRow>
        ) : (
          <React.Fragment>
            <CRow>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData1}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                  ></Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData2}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                  ></Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
            </CRow>
            <Row className="content-data-detail">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center">
                      Description
                    </th>
                    <th className="text-center">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: header.year1,
                        }}
                      />
                    </th>
                    {numyear >= 2 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year2,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 3 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year3,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 4 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year4,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 5 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year5,
                          }}
                        />
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {this.state.datatable.map(
                    (item, index) => {
                      return (
                        <tr key={index}>
                          <td>
                            <span>{item.name}</span>
                            <p className="data-comment">
                              {item.comment}
                            </p>
                          </td>
                          <td className="text-right">
                            {currencyFormat(item.year1)}
                          </td>
                          {numyear >= 2 && (
                            <td className="text-right">
                              {currencyFormat(item.year2)}
                            </td>
                          )}
                          {numyear >= 3 && (
                            <td className="text-right">
                              {currencyFormat(item.year3)}
                            </td>
                          )}
                          {numyear >= 4 && (
                            <td className="text-right">
                              {currencyFormat(item.year4)}
                            </td>
                          )}
                          {numyear >= 5 && (
                            <td className="text-right">
                              {currencyFormat(item.year5)}
                            </td>
                          )}
                        </tr>
                      );
                    }
                  )}
                </tbody>
              </Table>
            </Row>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { GrowthRateRatios };
