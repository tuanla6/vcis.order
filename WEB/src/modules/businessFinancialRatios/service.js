
import { http } from 'src/shared/utils';

class FinancialRatiosService {

    GetGrowthRateRatios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/growth-rate-ratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    getLiquidityRatios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/liquidityratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    getActivityRatios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/activityratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    getCreditratios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/creditratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    getCoverageRatios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/coverageratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    
    GetPerformance(businessId, year, quarter, type) {
        let url = 'api/vcis-report/performance?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }

    GetReturnOnInvestment(businessId, year, quarter, type) {
        let url = 'api/vcis-report/return-on-investment?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    GetReturnOnNetSales(businessId, year, quarter, type) {
        let url = 'api/vcis-report/return-on-netsales?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
    GetSolvencyRatios(businessId, year, quarter, type) {
        let url = 'api/vcis-report/solvency-ratios?businessId=' + businessId + '&year=' + year;
        if (quarter) {
            url += '&q=' + quarter;
        } else {
            url += '&q='
        }
        if (type) {
            url += '&type=' + type;
        } else {
            url += '&type='
        }
        return http.get(url);
    }
}
const financialRatiosService = new FinancialRatiosService();
export { financialRatiosService, FinancialRatiosService };


