import React from 'react';
import { CCol, CRow } from '@coreui/react';
import {
  Chart,
  Tooltip,
  Axis,
  Legend,
  Geom,
  Coordinate,
  Point,
  Line,
  Area,
} from 'bizcharts';
import { financialRatiosService } from './service';
import { Table } from 'react-bootstrap';
import DataSet from '@antv/data-set';
import './style.scss';
import {
  currencyFormat,
  getDayChartRatios,
  getDistanceToTop,
} from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class ReturnOnInvestment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData1: [],
      chartData2: [],
      datatable: [],
      header: {},
      numyear: null,
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({
      heightLoading: getDistanceToTop('#Investment'),
    });
    this.getData();
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getData();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getData();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getData();
      });
    }
  }

  getData() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      this.setState({ loading: true });

      this.subscriptions['GetReturnOnInvestment'] =
        financialRatiosService
          .GetReturnOnInvestment(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let header1 = getDayChartRatios(
                header.year1,
                type
              );
              let header2 = getDayChartRatios(
                header.year2,
                type
              );
              let header3 = getDayChartRatios(
                header.year3,
                type
              );
              let header4 = getDayChartRatios(
                header.year4,
                type
              );
              let header5 = getDayChartRatios(
                header.year5,
                type
              );

              //#region chart
              let OperatingROA = res.data.find(
                (x) => x.index === 1
              );
              let ROA = res.data.find((x) => x.index === 2);
              let ROE = res.data.find((x) => x.index === 3);

              let chartData1 = [];
              let chartData2 = [];
              if (OperatingROA != null) {
                chartData2.push({
                  item: 'Operating ROA (%)',
                  'Average industry':
                    OperatingROA.averageValue,
                  'Subject company': OperatingROA.year1,
                });
              }
              if (ROA != null) {
                chartData2.push({
                  item: 'ROA (%)',
                  'Average industry': ROA.averageValue,
                  'Subject company': ROA.year1,
                });
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'ROA (%)',
                    value: ROA.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'ROA (%)',
                    value: ROA.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'ROA (%)',
                    value: ROA.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'ROA (%)',
                    value: ROA.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: 'ROA (%)',
                    value: ROA.year1,
                  });
              }
              if (ROE != null) {
                chartData2.push({
                  item: 'ROE (%)',
                  'Average industry': ROE.averageValue,
                  'Subject company': ROE.year1,
                });
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'ROE (%)',
                    value: ROE.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'ROE (%)',
                    value: ROE.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'ROE (%)',
                    value: ROE.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'ROE (%)',
                    value: ROE.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: 'ROE (%)',
                    value: ROE.year1,
                  });
              }
              //#endregion

              this.setState({
                loading: false,
                datatable: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBalanceSheet: false });
            }
          );
    }
  }

  render() {
    const { numyear, header, loading } = this.state;
    const { DataView } = DataSet;
    const dv = new DataView().source(this.state.chartData2);
    dv.transform({
      type: 'fold',
      fields: ['Average industry', 'Subject company'], // 展开字段集
      key: 'user', // key字段
      value: 'score', // value字段
    });
    const cols = {
      year: {
        range: [0, 1],
      },
    };
    return (
      <div className="content-business-per" id="Investment">
        {loading ? (
          <CRow>
            <CCol
              xs="12"
              lg="12"
              style={{ height: this.state.heightLoading }}
            >
              <RLoading loading={true} />
            </CCol>
          </CRow>
        ) : (
          <React.Fragment>
            <CRow>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData1}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  ></Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="6">
                <Chart
                  height={220}
                  data={dv.rows}
                  autoFit
                  scale={{}}
                  interactions={['legend-highlight']}
                >
                  <Coordinate type="polar" radius={0.8} />
                  <Tooltip shared />
                  <Point
                    position="item*score"
                    color="user"
                    shape="circle"
                  />
                  <Line
                    position="item*score"
                    color="user"
                    size="2"
                  />
                  <Area
                    position="item*score"
                    color="user"
                  />

                  <Axis
                    name="score"
                    grid={{ line: { type: 'line' } }}
                  />

                  <Axis name="item" line={false} />
                </Chart>
              </CCol>
            </CRow>
            <CRow className="content-data-detail">
              <CCol
                xs="12"
                lg="12"
                style={{ marginTop: 50 }}
              >
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th className="text-center">
                        Description
                      </th>
                      {this.state.type === '1' && (
                        <th className="text-center">
                          Industry Average
                        </th>
                      )}
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year1,
                          }}
                        />
                      </th>
                      {numyear >= 2 && (
                        <th className="text-center">
                          <p
                            dangerouslySetInnerHTML={{
                              __html: header.year2,
                            }}
                          />
                        </th>
                      )}
                      {numyear >= 3 && (
                        <th className="text-center">
                          <p
                            dangerouslySetInnerHTML={{
                              __html: header.year3,
                            }}
                          />
                        </th>
                      )}
                      {numyear >= 4 && (
                        <th className="text-center">
                          <p
                            dangerouslySetInnerHTML={{
                              __html: header.year4,
                            }}
                          />
                        </th>
                      )}
                      {numyear >= 5 && (
                        <th className="text-center">
                          <p
                            dangerouslySetInnerHTML={{
                              __html: header.year5,
                            }}
                          />
                        </th>
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.datatable.map(
                      (item, index) => {
                        return (
                          <tr key={index}>
                            <td>
                              <span>{item.name}</span>
                              <p className="data-comment">
                                {item.comment}
                              </p>
                            </td>
                            {this.state.type === '1' && (
                              <td className="text-right">
                                {currencyFormat(
                                  item.averageValue
                                )}
                              </td>
                            )}
                            <td className="text-right">
                              {currencyFormat(item.year1)}
                            </td>
                            {numyear >= 2 && (
                              <td className="text-right">
                                {currencyFormat(item.year2)}
                              </td>
                            )}
                            {numyear >= 3 && (
                              <td className="text-right">
                                {currencyFormat(item.year3)}
                              </td>
                            )}
                            {numyear >= 4 && (
                              <td className="text-right">
                                {currencyFormat(item.year4)}
                              </td>
                            )}
                            {numyear >= 5 && (
                              <td className="text-right">
                                {currencyFormat(item.year5)}
                              </td>
                            )}
                          </tr>
                        );
                      }
                    )}
                  </tbody>
                </Table>
              </CCol>
            </CRow>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { ReturnOnInvestment };
