import React from 'react';
import { CCol, CRow } from '@coreui/react';
import { Row, Table } from 'react-bootstrap';
import {
  Chart,
  Tooltip,
  Axis,
  Legend,
  Geom,
  Coordinate,
  Point,
  Line,
  Area,
} from 'bizcharts';
import { financialRatiosService } from './service';
import DataSet from '@antv/data-set';
import './style.scss';
import {
  currencyFormat,
  getDayChartRatios,
  getDistanceToTop,
} from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class ReturnOnNetSales extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData1: [],
      chartData2: [],
      chartData3: [],
      datatable: [],
      header: {},
      numyear: null,
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({
      heightLoading: getDistanceToTop('#NetSales'),
    });
    this.getData();
  }

  componentWillUnmount() {}
  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getData();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getData();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getData();
      });
    }
  }
  getData() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      this.setState({ loading: true });

      this.subscriptions['GetReturnOnNetSales'] =
        financialRatiosService
          .GetReturnOnNetSales(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let header1 = getDayChartRatios(
                header.year1,
                type
              );
              let header2 = getDayChartRatios(
                header.year2,
                type
              );
              let header3 = getDayChartRatios(
                header.year3,
                type
              );
              let header4 = getDayChartRatios(
                header.year4,
                type
              );
              let header5 = getDayChartRatios(
                header.year5,
                type
              );

              //#region chart
              let GrossProfitMargin = res.data.find(
                (x) => x.index === 1
              );
              let OperatingIncomeMargin = res.data.find(
                (x) => x.index === 2
              );
              let EBITDAMargin = res.data.find(
                (x) => x.index === 3
              );
              let EBITMargin = res.data.find(
                (x) => x.index === 4
              );
              let EBTMargin = res.data.find(
                (x) => x.index === 5
              );
              let NetIncomeMargin = res.data.find(
                (x) => x.index === 6
              );

              let chartData1 = [];
              let chartData2 = [];
              let chartData3 = [];

              if (GrossProfitMargin != null) {
                chartData3.push({
                  item: 'Gross Profit Margin (%)',
                  'Average industry':
                    GrossProfitMargin.averageValue,
                  'Subject company':
                    GrossProfitMargin.year1,
                });
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Gross Profit Margin',
                    value: GrossProfitMargin.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Gross Profit Margin',
                    value: GrossProfitMargin.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Gross Profit Margin',
                    value: GrossProfitMargin.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Gross Profit Margin',
                    value: GrossProfitMargin.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: 'Gross Profit Margin',
                    value: GrossProfitMargin.year1,
                  });
              }

              if (OperatingIncomeMargin != null) {
                chartData3.push({
                  item: 'Operating Income Margin (%)',
                  'Average industry':
                    OperatingIncomeMargin.averageValue,
                  'Subject company':
                    OperatingIncomeMargin.year1,
                });
              }

              if (EBITDAMargin != null) {
                chartData3.push({
                  item: 'EBITDA Margin (%)',
                  'Average industry':
                    EBITDAMargin.averageValue,
                  'Subject company': EBITDAMargin.year1,
                });

                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'EBITDA Margin',
                    value: EBITDAMargin.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'EBITDA Margin',
                    value: EBITDAMargin.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'EBITDA Margin',
                    value: EBITDAMargin.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'EBITDA Margin',
                    value: EBITDAMargin.year2,
                  });
                if (header.numyear >= 1)
                  chartData1.push({
                    year: header1,
                    type: 'EBITDA Margin',
                    value: EBITDAMargin.year1,
                  });
              }

              if (EBITMargin != null) {
                chartData3.push({
                  item: 'EBIT Margin (%)',
                  'Average industry':
                    EBITMargin.averageValue,
                  'Subject company': EBITMargin.year1,
                });

                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'EBIT Margin',
                    value: EBITMargin.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'EBIT Margin',
                    value: EBITMargin.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'EBIT Margin',
                    value: EBITMargin.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'EBIT Margin',
                    value: EBITMargin.year2,
                  });
                if (header.numyear >= 1)
                  chartData2.push({
                    year: header1,
                    type: 'EBIT Margin',
                    value: EBITMargin.year1,
                  });
              }

              if (EBTMargin != null) {
                chartData3.push({
                  item: 'EBT Margin (%)',
                  'Average industry':
                    EBTMargin.averageValue,
                  'Subject company': EBTMargin.year1,
                });
              }

              if (NetIncomeMargin != null) {
                chartData3.push({
                  item: 'Net Income Margin (%)',
                  'Average industry':
                    NetIncomeMargin.averageValue,
                  'Subject company': NetIncomeMargin.year1,
                });

                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'Net Income Margin',
                    value: NetIncomeMargin.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'Net Income Margin',
                    value: NetIncomeMargin.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'Net Income Margin',
                    value: NetIncomeMargin.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'Net Income Margin',
                    value: NetIncomeMargin.year2,
                  });
                if (header.numyear >= 1)
                  chartData2.push({
                    year: header1,
                    type: 'Net Income Margin',
                    value: NetIncomeMargin.year1,
                  });
              }
              //#endregion

              this.setState({
                loading: false,
                datatable: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
                chartData3: chartData3,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBalanceSheet: false });
            }
          );
    }
  }

  render() {
    const { numyear, header, loading } = this.state;
    const { DataView } = DataSet;
    const dv = new DataView().source(this.state.chartData3);
    dv.transform({
      type: 'fold',
      fields: ['Average industry', 'Subject company'],
      key: 'user',
      value: 'score',
    });
    const cols = {
      year: {
        range: [0, 1],
      },
    };
    return (
      <div className="content-business-per" id="NetSales">
        {loading ? (
          <CRow>
            <CCol
              xs="12"
              lg="12"
              style={{ height: this.state.heightLoading }}
            >
              <RLoading loading={true} />
            </CCol>
          </CRow>
        ) : (
          <React.Fragment>
            <CRow>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData1}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="6">
                <Chart
                  height={180}
                  data={this.state.chartData2}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="12">
                <Chart
                  height={220}
                  data={dv.rows}
                  autoFit
                  interactions={['legend-highlight']}
                >
                  <Coordinate type="polar" radius={0.8} />
                  <Tooltip shared />
                  <Point
                    position="item*score"
                    color="user"
                    shape="circle"
                  />
                  <Line
                    position="item*score"
                    color="user"
                    size="2"
                  />
                  <Area
                    position="item*score"
                    color="user"
                  />

                  <Axis
                    name="score"
                    grid={{ line: { type: 'line' } }}
                  />

                  <Axis name="item" line={false} />
                </Chart>
              </CCol>
            </CRow>

            <Row className="content-data-detail">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center">
                      Description
                    </th>
                    {this.state.type === '1' && (
                      <th className="text-center">
                        Industry Average
                      </th>
                    )}
                    <th className="text-center">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: header.year1,
                        }}
                      />
                    </th>
                    {numyear >= 2 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year2,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 3 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year3,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 4 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year4,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 5 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year5,
                          }}
                        />
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {this.state.datatable.map(
                    (item, index) => {
                      return (
                        <tr key={index}>
                          <td>
                            <span>{item.name}</span>
                            <p className="data-comment">
                              {item.comment}
                            </p>
                          </td>
                          {this.state.type === '1' && (
                            <td className="text-right">
                              {currencyFormat(
                                item.averageValue
                              )}
                            </td>
                          )}
                          <td className="text-right">
                            {currencyFormat(item.year1)}
                          </td>
                          {numyear >= 2 && (
                            <td className="text-right">
                              {currencyFormat(item.year2)}
                            </td>
                          )}
                          {numyear >= 3 && (
                            <td className="text-right">
                              {currencyFormat(item.year3)}
                            </td>
                          )}
                          {numyear >= 4 && (
                            <td className="text-right">
                              {currencyFormat(item.year4)}
                            </td>
                          )}
                          {numyear >= 5 && (
                            <td className="text-right">
                              {currencyFormat(item.year5)}
                            </td>
                          )}
                        </tr>
                      );
                    }
                  )}
                </tbody>
              </Table>
            </Row>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { ReturnOnNetSales };
