import React from 'react';
import { CCol, CRow } from '@coreui/react';
import { Row, Table } from 'react-bootstrap';
import {
  Chart,
  Tooltip,
  Axis,
  Legend,
  Geom,
  Coordinate,
  Point,
  Line,
  Area,
} from 'bizcharts';
import DataSet from '@antv/data-set';
import './style.scss';
import { financialRatiosService } from './service';
import {
  currencyFormat,
  getDayChartRatios,
  getDistanceToTop,
} from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class CreditRatios extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingCreditRatios: false,
      data: [],
      header: {},
      numyear: null,
      chartData1: [],
      chartData2: [],
      chartData3: [],
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({
      heightLoading: getDistanceToTop('#Credit-Ratios'),
    });
    this.getCreditRatios();
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getCreditRatios();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getCreditRatios();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getCreditRatios();
      });
    }
  }

  getCreditRatios() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      this.setState({ loadingCreditRatios: true });

      this.subscriptions['getCreditRatios'] =
        financialRatiosService
          .getCreditratios(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let header1 = getDayChartRatios(
                header.year1,
                type
              );
              let header2 = getDayChartRatios(
                header.year2,
                type
              );
              let header3 = getDayChartRatios(
                header.year3,
                type
              );
              let header4 = getDayChartRatios(
                header.year4,
                type
              );
              let header5 = getDayChartRatios(
                header.year5,
                type
              );

              let eBITInterestCoverage = res.data.find(
                (x) => x.index === 1
              );
              let eBITDAInterestCoverage = res.data.find(
                (x) => x.index === 2
              );
              let freeOperatingCashflowtoDebt =
                res.data.find((x) => x.index === 3);
              let discretionaryCashflowtoDebt =
                res.data.find((x) => x.index === 4);
              let debttoEBITDA = res.data.find(
                (x) => x.index === 5
              );
              let totalDebt = res.data.find(
                (x) => x.index === 6
              );
              let chartData1 = [];
              let chartData2 = [];
              let chartData3 = [];

              if (eBITInterestCoverage != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'EBIT Interest Coverage',
                    value: eBITInterestCoverage.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'EBIT Interest Coverage',
                    value: eBITInterestCoverage.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'EBIT Interest Coverage',
                    value: eBITInterestCoverage.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'EBIT Interest Coverage',
                    value: eBITInterestCoverage.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'EBIT Interest Coverage',
                    value: eBITInterestCoverage.year1,
                  });
                  chartData3.push({
                    item: 'EBIT Interest Coverage',
                    'Average industry':
                      eBITInterestCoverage.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      eBITInterestCoverage.year1?.toFixed(
                        2
                      ),
                  });
                }
              }
              if (eBITDAInterestCoverage != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'EBITDA Interest Coverage',
                    value: eBITDAInterestCoverage.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'EBITDA Interest Coverage',
                    value: eBITDAInterestCoverage.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'EBITDA Interest Coverage',
                    value: eBITDAInterestCoverage.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'EBITDA Interest Coverage',
                    value: eBITDAInterestCoverage.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'EBITDA Interest Coverage',
                    value: eBITDAInterestCoverage.year1,
                  });
                  chartData3.push({
                    item: 'EBITDA Interest Coverage',
                    'Average industry':
                      eBITDAInterestCoverage.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      eBITDAInterestCoverage.year1?.toFixed(
                        2
                      ),
                  });
                }
              }
              if (freeOperatingCashflowtoDebt != null) {
                if (header.numyear >= 5)
                  chartData1.push({
                    year: header5,
                    type: 'Free Operating Cash flow to Debt',
                    value:
                      freeOperatingCashflowtoDebt.year5,
                  });
                if (header.numyear >= 4)
                  chartData1.push({
                    year: header4,
                    type: 'Free Operating Cash flow to Debt',
                    value:
                      freeOperatingCashflowtoDebt.year4,
                  });
                if (header.numyear >= 3)
                  chartData1.push({
                    year: header3,
                    type: 'Free Operating Cash flow to Debt',
                    value:
                      freeOperatingCashflowtoDebt.year3,
                  });
                if (header.numyear >= 2)
                  chartData1.push({
                    year: header2,
                    type: 'Free Operating Cash flow to Debt',
                    value:
                      freeOperatingCashflowtoDebt.year2,
                  });
                if (header.numyear >= 1) {
                  chartData1.push({
                    year: header1,
                    type: 'Free Operating Cash flow to Debt',
                    value:
                      freeOperatingCashflowtoDebt.year1,
                  });
                  chartData3.push({
                    item: 'Free Operating Cash flow to Debt',
                    'Average industry':
                      freeOperatingCashflowtoDebt.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      freeOperatingCashflowtoDebt.year1?.toFixed(
                        2
                      ),
                  });
                }
              }

              if (debttoEBITDA != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'Debt to EBITDA',
                    value: debttoEBITDA.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'Debt to EBITDA',
                    value: debttoEBITDA.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'Debt to EBITDA',
                    value: debttoEBITDA.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'Debt to EBITDA',
                    value: debttoEBITDA.year2,
                  });
                if (header.numyear >= 1) {
                  chartData2.push({
                    year: header1,
                    type: 'Debt to EBITDA',
                    value: debttoEBITDA.year1,
                  });
                  chartData3.push({
                    item: 'Debt to EBITDA',
                    'Average industry':
                      debttoEBITDA.averageValue?.toFixed(2),
                    'Subject company':
                      debttoEBITDA.year1?.toFixed(2),
                  });
                }
              }
              if (totalDebt != null) {
                if (header.numyear >= 5)
                  chartData2.push({
                    year: header5,
                    type: 'Total Debt to Total Debt+Equity',
                    value: totalDebt.year5,
                  });
                if (header.numyear >= 4)
                  chartData2.push({
                    year: header4,
                    type: 'Total Debt to Total Debt+Equity',
                    value: totalDebt.year4,
                  });
                if (header.numyear >= 3)
                  chartData2.push({
                    year: header3,
                    type: 'Total Debt to Total Debt+Equity',
                    value: totalDebt.year3,
                  });
                if (header.numyear >= 2)
                  chartData2.push({
                    year: header2,
                    type: 'Total Debt to Total Debt+Equity',
                    value: totalDebt.year2,
                  });
                if (header.numyear >= 1) {
                  chartData2.push({
                    year: header1,
                    type: 'Total Debt to Total Debt+Equity',
                    value: totalDebt.year1,
                  });
                  chartData3.push({
                    item: 'Total Debt to Total Debt+Equity',
                    'Average industry':
                      totalDebt.averageValue?.toFixed(2),
                    'Subject company':
                      totalDebt.year1?.toFixed(2),
                  });
                }
              }
              if (discretionaryCashflowtoDebt != null) {
                if (header.numyear >= 1) {
                  chartData3.push({
                    item: 'Discretionary Cash flow to Debt',
                    'Average industry':
                      discretionaryCashflowtoDebt.averageValue?.toFixed(
                        2
                      ),
                    'Subject company':
                      discretionaryCashflowtoDebt.year1?.toFixed(
                        2
                      ),
                  });
                }
              }
              this.setState({
                loadingCreditRatios: false,
                data: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
                chartData3: chartData3,
              });
            },
            (err) => {
              console.log(err);
              this.setState({
                loadingLiquidityRatios: false,
              });
            }
          );
    }
  }

  render() {
    const { DataView } = DataSet;
    const dv = new DataView().source(this.state.chartData3);
    dv.transform({
      type: 'fold',
      fields: ['Average industry', 'Subject company'], // 展开字段集
      key: 'user', // key字段
      value: 'score', // value字段
    });
    const cols = {
      year: {
        range: [0, 1],
      },
    };
    const { numyear, header, loadingCreditRatios } =
      this.state;
    return (
      <div
        className="content-business-per"
        id="Credit-Ratios"
      >
        {loadingCreditRatios ? (
          <CRow>
            <CCol
              xs="12"
              lg="12"
              style={{ height: this.state.heightLoading }}
            >
              <RLoading loading={true} />
            </CCol>
          </CRow>
        ) : (
          <React.Fragment>
            <CRow>
              <CCol xs="12" lg="6">
                <Chart
                  height={200}
                  data={this.state.chartData1}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="6">
                <Chart
                  height={200}
                  data={this.state.chartData2}
                  scale={cols}
                  autoFit
                >
                  <Legend />
                  <Axis name="year" />
                  <Axis name="value" />
                  <Tooltip
                    crosshairs={{
                      type: 'y',
                    }}
                    itemTpl={
                      '<tr data-index={index}><td>{value}</td></tr>'
                    }
                  >
                    {(title, items) => {
                      return (
                        <table>
                          <tbody className="g2-tooltip-list"></tbody>
                        </table>
                      );
                    }}
                  </Tooltip>
                  <Geom
                    type="point"
                    position="year*value"
                    size={4}
                    shape={'circle'}
                    color={'type'}
                    style={{
                      stroke: '#fff',
                      lineWidth: 1,
                    }}
                  />
                  <Geom
                    type="line"
                    position="year*value"
                    size={2}
                    color={'type'}
                    shape={'smooth'}
                  />
                </Chart>
              </CCol>
              <CCol xs="12" lg="12">
                <Chart
                  height={220}
                  data={dv.rows}
                  autoFit
                  interactions={['legend-highlight']}
                >
                  <Coordinate type="polar" radius={0.8} />
                  <Tooltip shared />
                  <Point
                    position="item*score"
                    color="user"
                    shape="circle"
                  />
                  <Line
                    position="item*score"
                    color="user"
                    size="2"
                  />
                  <Area
                    position="item*score"
                    color="user"
                  />

                  <Axis
                    name="score"
                    grid={{ line: { type: 'line' } }}
                  />

                  <Axis name="item" line={false} />
                </Chart>
              </CCol>
            </CRow>

            <Row className="content-data-detail">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center">
                      Description
                    </th>
                    {this.state.type === '1' && (
                      <th className="text-center">
                        Industry Average
                      </th>
                    )}
                    <th className="text-center">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: header.year1,
                        }}
                      />
                    </th>
                    {numyear >= 2 && (
                      <th className="text-center">
                        {' '}
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year2,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 3 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year3,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 4 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year4,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 5 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year5,
                          }}
                        />
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {this.state.data.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <span>{item.name}</span>
                          <p className="data-comment">
                            {item.comment}
                          </p>
                        </td>

                        {this.state.type === '1' && (
                          <td className="text-right">
                            {currencyFormat(
                              item.averageValue
                            )}
                          </td>
                        )}
                        <td className="text-right">
                          {currencyFormat(item.year1)}
                        </td>
                        {numyear >= 2 && (
                          <td className="text-right">
                            {currencyFormat(item.year2)}
                          </td>
                        )}
                        {numyear >= 3 && (
                          <td className="text-right">
                            {currencyFormat(item.year3)}
                          </td>
                        )}
                        {numyear >= 4 && (
                          <td className="text-right">
                            {currencyFormat(item.year4)}
                          </td>
                        )}
                        {numyear >= 5 && (
                          <td className="text-right">
                            {currencyFormat(item.year5)}
                          </td>
                        )}
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </Row>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { CreditRatios };
