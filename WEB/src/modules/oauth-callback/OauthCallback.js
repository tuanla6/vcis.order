import React from 'react';
import PropTypes from 'prop-types';
import { authService } from 'src/shared/services';
import { connect } from 'react-redux';

class OauthCallbackComponent extends React.PureComponent {
    static propTypes = {
        location: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
        history: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    };
    constructor(props) {
        super(props);
        this.state = {           
            loading: false,
            errorMessage: ''
        };
    }
    componentDidMount(){
        let authCode=new URLSearchParams(this.props.location.search).get('code');
        authService.loginWithAuthCode({code:authCode}).subscribe(() => {
            this.setState({ loading: false, errorMessage: '' });
             
            const { from } = this.props.location.state || { from: { pathname: '/' } };
            this.props.history.push(from);
        }, (err) => { 
            if (err.status === 400) {
                this.setState({ loading: false, errorMessage: 'Tài khoản hoặc mật khẩu không chính xác, vui lòng kiểm tra lại' });
            } else {
                this.setState({ loading: false, errorMessage: 'Có lỗi xảy ra trong quá trình login' });
            }
        });
    }
    render() {
        return (
            <h1>Đang xử lý thông tin đăng nhập..</h1>
        );
    }
}
OauthCallbackComponent.propTypes = {
    onToggle: PropTypes.func,
    type: PropTypes.string,
    user: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        user: state.oauth
    };
};
const OauthCallback = connect(mapStateToProps, {})(OauthCallbackComponent);
export default OauthCallback;