import React from 'react';
import {
    CCol,
    CDataTable,
    CRow
} from '@coreui/react';
import './style.scss';
import { ligionService } from './service';
import { formatDate } from 'src/shared/utils'
import { RCard } from 'src/shared/component';
const Fields = [
    { key: 'items', label: 'No.', _style: { width: '5%' } },
    { key: 'dateOfAcceptance', label: 'Date of Acceptance', _style: { width: '15%' } },
    { key: 'typeOfcaseName', label: 'Case type', _style: { width: '20%' } },
    { key: 'plaintiffBusinessName', label: 'Plaintiff', _style: { width: '25%' } },
    { key: 'relatedPartiesBusinessName', label: 'Defendant', _style: { width: '25%' } },
    'status'
];

class BusinessLigion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            LigionData: [],
            id: this.props.id,
            noItemMessage:'Loading... '
        };
        this.subscriptions = {}
    }

    componentDidMount() {
        this.getLitigation();
    }
    componentWillUnmount() {

    }

    getLitigation() {
        let id = this.state.id;
        this.setState({ loading: true });
        this.subscriptions['getLitigation'] = ligionService.getLitigation(id).subscribe(res => {
            res.forEach(x => {
                x.dateOfAcceptance = x.dateOfAcceptance ? formatDate(x.dateOfAcceptance) : '';
                x.typeOfcaseName = x.typeOfcaseName ? x.typeOfcaseName : '';
                x.plaintiffBusinessName = x.plaintiffBusinessName ? x.plaintiffBusinessName : '';
                x.plaintiffPersonName = x.plaintiffPersonName ? x.plaintiffPersonName : '';
                x.relatedPartiesBusinessName = x.relatedPartiesBusinessName ? x.relatedPartiesBusinessName : '';
                x.status = x.status ? x.status : '';
            });

            this.setState({
                LigionData: res,
                loading: false,
                noItemMessage: 'No records'
            })
        }, err => {
            console.log(err);
            this.setState({
                loading: false
            })
        });
    }


    render() {
        return (
            <div className="content-business-group">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Court cases & Administrative sanctions" className="card card-danger">
                            <CDataTable
                                items={this.state.LigionData}
                                fields={Fields}
                                itemsPerPage={15}
                                pagination
                                hover
                                scopedSlots = {{
                                    'items':
                                      (item, index)=>(
                                        <td>
                                          {index+1}
                                        </td>
                                      ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
            </div>
        )
    }
}

export { BusinessLigion };