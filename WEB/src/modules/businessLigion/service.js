
import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class LigionService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-litigation' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getLitigation(businessId) {
      return http.get('api/vcis-business-litigation?businessId='+businessId);
  }
}
const ligionService = new LigionService();

export { ligionService, LigionService };


