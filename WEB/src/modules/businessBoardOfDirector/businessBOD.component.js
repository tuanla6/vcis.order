import React from 'react';
import {
    CCol,
    CDataTable,
    CRow
} from '@coreui/react';
import './style.scss';
import { bodService } from './service';
import { RCard } from 'src/shared/component';
import { formatDate } from 'src/shared/utils';
const fields = ['name', 'positionName', 'idNumber', 'dob', 'nationality']
const meta = {
    page: 1,
    page_size: 0,
    sort: {},
    search: '',
    filter: {}
};

class BusinessBOD extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listDirectors: [],
            listFormerDirectors: [],
            listManagement: [],
            listFormerManagement: [],
            id: this.props.id,
            loadingDirectors: false,
            loadingFormerDirectors: false,
            loadingManagement: false,
            loadingFormerManagement: false,
            noItemMessageBOD:'Loading... ',
            noItemMessageBOM:'Loading... ',
            noItemMessageFBOD:'Loading... ',
            noItemMessageFBOM:'Loading... '
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.getDirectors();
        this.getFormerDirectors();
        this.getManagement();
        this.getFormerManagement();
    }
    componentWillUnmount() {

    }

    getDirectors() {
        let id = this.state.id;
        meta.filter = { businessID: id, chair: true };
        this.setState({ loadingDirectors: true });
        this.subscriptions['getDirectors'] = bodService.getMany(meta).subscribe(res => {
            res.data.map(x => x.dob = formatDate(x.dob));
            this.setState({
                listDirectors: res.data,
                loadingDirectors: false,
                noItemMessageBOD: 'No record'
            })
        },err => {
            console.log(err);
            this.setState({
                loadingDirectors: false
            })
        });
    }

    getFormerDirectors() {
        let id = this.state.id;
        meta.filter = { businessID: id, formerChair: true };
        this.setState({ loadingFormerDirectors: true });
        this.subscriptions['getFormerDirectors'] = bodService.getMany(meta).subscribe(res => {
            res.data.map(x => x.dob = formatDate(x.dob));
            this.setState({
                listFormerDirectors: res.data,
                loadingFormerDirectors: false,
                noItemMessageFBOD: 'No record'
            })
        },err => {
            console.log(err);
            this.setState({
                loadingFormerDirectors: false
            })
        });
    }

    getManagement() {
        let id = this.state.id;
        meta.filter = { businessID: id, director: true };
        this.setState({ loadingManagement: true });
        this.subscriptions['getManagement'] = bodService.getMany(meta).subscribe(res => {
            res.data.map(x => x.dob = formatDate(x.dob));
            this.setState({
                listManagement: res.data,
                loadingManagement: false,
                noItemMessageBOM: 'No record'
            })
        },err => {
            console.log(err);
            this.setState({
                loadingManagement: false
            })
        });
    }

    getFormerManagement() {
        let id = this.state.id;
        meta.filter = { businessID: id, formerDirector: true };
        this.setState({ loadingFormerManagement: true });
        this.subscriptions['getFormerManagement'] = bodService.getMany(meta).subscribe(res => {
            res.data.map(x => x.dob = formatDate(x.dob));
            this.setState({
                listFormerManagement: res.data,
                loadingFormerManagement: false,
                noItemMessageFBOM: 'No record'
            })
        },err => {
            console.log(err);
            this.setState({
                loadingFormerManagement: false
            })
        });
    }


    render() {
        return (
            <div className="content-business-bod">
                <CRow>
                    <CCol xs="12" lg="6">
                        <RCard title="Board of Directors" className="card card-danger" loading={this.state.loadingDirectors}>
                            <CDataTable
                                items={this.state.listDirectors}
                                fields={fields}
                                itemsPerPage={15}
                                pagination
                                hover
                                noItemsView={{ noItems: this.state.noItemMessageBOD }}
                            />
                        </RCard>
                    </CCol>

                    <CCol xs="12" lg="6">
                        <RCard title="Board of Management" className="card card-danger" loading={this.state.loadingFormerDirectors}>
                            <CDataTable
                                items={this.state.listFormerDirectors}
                                fields={fields}
                                itemsPerPage={15}
                                pagination
                                hover
                                noItemsView={{ noItems: this.state.noItemMessageBOM }}
                            />
                        </RCard>
                    </CCol>
                </CRow>
                <CRow>
                    <CCol xs="12" lg="6">
                        <RCard title="Former Board of Directors" className="card card-danger" loading={this.state.loadingManagement}>
                            <CDataTable
                                items={this.state.listManagement}
                                fields={fields}
                                itemsPerPage={15}
                                pagination
                                hover
                                noItemsView={{ noItems: this.state.noItemMessageFBOD }}
                            />
                        </RCard>
                    </CCol>

                    <CCol xs="12" lg="6">
                        <RCard title=" Former Board of Management" className="card card-danger" loading={this.state.loadingFormerManagement}>
                            <CDataTable
                                items={this.state.listFormerManagement}
                                fields={fields}
                                itemsPerPage={15}
                                pagination
                                hover
                                noItemsView={{ noItems: this.state.noItemMessageFBOM }}
                            />
                        </RCard>

                    </CCol>
                </CRow>
            </div>
        )
    }
}

export { BusinessBOD };