import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
class BODService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-staff' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
}
const bodService = new BODService();
export { bodService, BODService };