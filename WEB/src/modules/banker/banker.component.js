import React from 'react';
import { Table } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import { RCard, RNoRecord } from 'src/shared/component';
import './style.scss';
import { bankerService } from './service'

class Banker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bankers: [],
            id: this.props.id,
        };
        this.subscriptions = {}
    }

    componentDidMount() {
        this.getBankers();
    }
    componentWillUnmount() {

    }

    getBankers() {
        let id = this.state.id;
        this.setState({ loading: true });
        this.subscriptions['getBankers'] = bankerService.getBankers(id).subscribe(res => {
            this.setState({
                bankers: res,
                loading: false,
            })
        }, err => {
            console.log(err);
            this.setState({
                loading: false
            })
        });
    }

    render() {
        return (
            <div className="content-banker">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Credit funding and business transaction banks" className="card card-danger" loading={this.state.loading} >
                            {
                                this.state.bankers.length > 0 ?
                                this.state.bankers.map((x, i) => {
                                    return (
                                        <Table bordered key={i}>
                                            <tbody>
                                                <tr style={{backgroundColor: 'rgba(0, 0, 21, 0.075)'}}>
                                                    <td colSpan="2" className="banker-name">{(i+1)+'. ' + x.bankerName}</td>
                                                </tr>
                                                <tr>
                                                    <td className='column-title'>Address</td>
                                                    <td>{x.address} </td>
                                                </tr>
                                                <tr>
                                                    <td className='column-title'>Telephone </td>
                                                    <td>{x.addressObj?.phone} </td>
                                                </tr>
                                                <tr>
                                                    <td className='column-title'>Fax  </td>
                                                    <td>{x.addressObj?.fax} </td>
                                                </tr>
                                            </tbody>
                                        </Table>

                                    )
                                }) : !this.state.loading && <RNoRecord text='The company does not have bankers ' />
                            }

                            {/* <Table bordered hover className="table-suppliers">
                                <thead>
                                    <tr>
                                        <th>Credit type</th>
                                        <th>Outstanding balance (VND)</th>
                                        <th>Outstanding balance (USD)</th>
                                        <th>Loan classification</th>
                                        <th>As at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Short-term loan</td>
                                        <td>169,493,000,000</td>
                                        <td>0</td>
                                        <td>Pass</td>
                                        <td>31 May 2021</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>169,493,000,000</td>
                                        <td>0</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>

                                <thead className="collaterals-description">
                                    <tr>
                                        <th>Collaterals description</th>
                                        <th>Securing parties </th>
                                        <th>Secured parties</th>
                                        <th>Value (VND)  </th>
                                        <th>Date of Records </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>35 assets, including machineries and equipment  </td>
                                        <td>Sample Company Limited</td>
                                        <td>VIETNAM PUBLIC JOINT STOCK COMMERCIAL BANK</td>
                                        <td>114,790,000,000</td>
                                        <td>29 Apr 2021</td>
                                    </tr>
                                </tbody>
                            </Table> */}
                        </RCard>
                    </CCol>
                </CRow>
            </div>
        )
    }
}

export { Banker };