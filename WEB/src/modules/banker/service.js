import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class BankerService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-bankers' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getBankers(businessId) {
      return http.get('api/vcis-business-bankers?businessId='+businessId);
  }
}
const bankerService = new BankerService();

export { bankerService, BankerService };

