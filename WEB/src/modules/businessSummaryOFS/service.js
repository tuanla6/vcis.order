
import { http } from 'src/shared/utils';

class SummaryOFSService {

  getOverView(businessId, year, quarter, type) {
    let url = 'api/vcis-report/summaryfs-overview?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q='
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type='
    }
    return http.get(url);
  }

  getBalanceSheet(businessId, year, quarter, type) {
    let url = 'api/vcis-report/balacesheet?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q='
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type='
    }
    return http.get(url);
  }
  getCashflowDirect(businessId, year, quarter, type) {
    let url = 'api/vcis-report/cashflow-direct?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q='
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type='
    }
    return http.get(url);
  }
  getCashflowInDirect(businessId, year, quarter, type) {
    let url = 'api/vcis-report/cashflow-indirect?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q='
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type='
    }
    return http.get(url);
  }
}
const summaryOFSService = new SummaryOFSService();

export { summaryOFSService, SummaryOFSService };
