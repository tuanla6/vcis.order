import React from "react";
import PropTypes from "prop-types";
import {
  CCard,
  CCol,
  CRow,
  // CPagination
} from "@coreui/react";
import "./style.scss";
import { Row, Col, Tabs, Tab } from "react-bootstrap";
import { SummaryOFSOverView } from "./overview.component";
import { BalanceSheet } from "src/modules/SummaryofFinancialStatements/BalanceSheet.component";
import { IncomeStatement } from "src/modules/SummaryofFinancialStatements/IncomeStatement.component";
import { CashflowStatement } from "src/modules/SummaryofFinancialStatements/CashflowStatement.component";
import "./style.scss";

class BusinessSummaryOFS extends React.Component {
  static propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    setData: PropTypes.func,
    setMeta: PropTypes.func,
    data: PropTypes.array,
    meta: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
    };
  }
  componentDidMount() {}
  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter });
    }
  }
  componentWillUnmount() {}

  render() {
    return (
      <CRow>
        <CCol xl={12}>
          <CCard className="p30">
            <Row>
              <Col lg={12} md={12}>
                <Tabs
                  id="controlled-tab-example"
                  className="tab-business-summary"
                  defaultActiveKey="Overview"
                  mountOnEnter={true}
                  unmountOnExit={true}
                >
                  <Tab
                    eventKey="Overview"
                    title={<React.Fragment> Overview</React.Fragment>}
                  >
                    <SummaryOFSOverView
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                  <Tab
                    eventKey="BalanceSheet"
                    title={<React.Fragment> Balance Sheet</React.Fragment>}
                  >
                    <BalanceSheet
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                  <Tab
                    eventKey="IS"
                    title={<React.Fragment> Income Statement</React.Fragment>}
                  >
                    <IncomeStatement
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                  <Tab
                    eventKey="CS"
                    title={<React.Fragment> Cashflow Statement</React.Fragment>}
                  >
                    <CashflowStatement
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                </Tabs>
              </Col>
            </Row>
          </CCard>
        </CCol>
      </CRow>
    );
  }
}

export { BusinessSummaryOFS };
