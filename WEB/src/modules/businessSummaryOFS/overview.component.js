import React from 'react';
import { Col, Form } from 'react-bootstrap';

// import './style.scss';
import { summaryOFSService } from './service';
import { RCard, RLoading } from 'src/shared/component';
import {
  Chart,
  Interval,
  Tooltip,
  Coordinate,
  Line,
  Point,
  Area,
  Axis,
  Legend,
  Geom,
} from 'bizcharts';
import DataSet from '@antv/data-set';
import { getDistanceToTop } from 'src/shared/utils';

class SummaryOFSOverView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TangibleData: [],
      listSummary: [],
      InterestData: [],
      NetIncomeData: [],
      NetSalesData: [],
      AccountData: [],
      WorkingData: [],
      tyle: 1000000000,

      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      heightLoading: null,
      loading: false,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({ heightLoading: getDistanceToTop('#overview-loading') });
    this.getOverView();
  }

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getOverView();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getOverView();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getOverView();
      });
    }
  }

  getOverView() {
    let { id, type, year, quarter } = this.state;
    this.setState({ loading: true });
    if (id && year) {
      this.subscriptions['getOverView'] = summaryOFSService
        .getOverView(id, year, quarter, type)
        .subscribe(
          (res) => {
            let header = res.header;
            let data = res.data;

            //#region Tangible Net Worth
            let TNW = data.find((x) => x.name === 'Tangible Net Worth');
            let TangibleData = [];
            if (TNW !== null) {
              if (header.year5)
                TangibleData.push({ year: header.year5, sales: TNW.year5 / this.state.tyle });
              if (header.year4)
                TangibleData.push({ year: header.year4, sales: TNW.year4 / this.state.tyle });
              if (header.year3)
                TangibleData.push({ year: header.year3, sales: TNW.year3 / this.state.tyle });
              if (header.year2)
                TangibleData.push({ year: header.year2, sales: TNW.year2 / this.state.tyle });
              if (header.year1)
                TangibleData.push({ year: header.year1, sales: TNW.year1 / this.state.tyle });
            }
            //#endregion

            //#region Interest Bearing Debts
            let IBD = data.find((x) => x.name === 'Interest Bearing Debts');
            let InterestData = [];
            if (IBD !== null) {
              if (header.year5)
                InterestData.push({ year: header.year5, sales: IBD.year5 / this.state.tyle });
              if (header.year4)
                InterestData.push({ year: header.year4, sales: IBD.year4 / this.state.tyle });
              if (header.year3)
                InterestData.push({ year: header.year3, sales: IBD.year3 / this.state.tyle });
              if (header.year2)
                InterestData.push({ year: header.year2, sales: IBD.year2 / this.state.tyle });
              if (header.year1)
                InterestData.push({ year: header.year1, sales: IBD.year1 / this.state.tyle });
            }
            //#endregion

            //#region Net Income Margin
            let NetIM = data.find((x) => x.name === 'Net Income Margin (%)');
            let EBTM = data.find((x) => x.name === 'EBT Margin (%)');
            let EBITM = data.find((x) => x.name === 'EBIT Margin (%)');
            let GrossPM = data.find((x) => x.name === 'Gross Profit Margin (%)');

            let NetIncomeData = [];

            if (NetIM !== null) {
              NetIncomeData.push({
                item: 'Net Income Margin(%)',
                a: NetIM.averageValue,
                b: NetIM.year1,
              });
            }
            if (EBTM !== null) {
              NetIncomeData.push({ item: 'EBT Margin(%)', a: EBTM.averageValue, b: EBTM.year1 });
            }
            if (EBITM !== null) {
              NetIncomeData.push({
                item: 'EBIT Margin (%)',
                a: EBITM.averageValue,
                b: EBITM.year1,
              });
            }
            if (GrossPM !== null) {
              NetIncomeData.push({
                item: 'Gross Profit Margin (%)',
                a: GrossPM.averageValue,
                b: GrossPM.year1,
              });
            }

            //#endregion

            //#region Net Sales
            let NS = data.find((x) => x.name === 'Net Sales');
            let NetSalesData = [];
            if (NS !== null) {
              if (header.year5)
                NetSalesData.push({ year: header.year5, sales: NS.year5 / this.state.tyle });
              if (header.year4)
                NetSalesData.push({ year: header.year4, sales: NS.year4 / this.state.tyle });
              if (header.year3)
                NetSalesData.push({ year: header.year3, sales: NS.year3 / this.state.tyle });
              if (header.year2)
                NetSalesData.push({ year: header.year2, sales: NS.year2 / this.state.tyle });
              if (header.year1)
                NetSalesData.push({ year: header.year1, sales: NS.year1 / this.state.tyle });
            }
            //#endregion

            //#region Working Capital
            let NetWoC = data.find((x) => x.name === 'Working Capital');
            let GrossWoC = data.find((x) => x.name === 'Gross Working Capital');
            let NetWoCData = [];
            let GrossWoCData = [];
            let WorkingData = [];
            if (NetWoC !== null) {
              if (header.year5)
                NetWoCData.push({
                  year: header.year5,
                  value: NetWoC.year5 / this.state.tyle,
                  type: 'Net Working Capital',
                });
              if (header.year4)
                NetWoCData.push({
                  year: header.year4,
                  value: NetWoC.year4 / this.state.tyle,
                  type: 'Net Working Capital',
                });
              if (header.year3)
                NetWoCData.push({
                  year: header.year3,
                  value: NetWoC.year3 / this.state.tyle,
                  type: 'Net Working Capital',
                });
              if (header.year2)
                NetWoCData.push({
                  year: header.year2,
                  value: NetWoC.year2 / this.state.tyle,
                  type: 'Net Working Capital',
                });
              if (header.year1)
                NetWoCData.push({
                  year: header.year1,
                  value: NetWoC.year1 / this.state.tyle,
                  type: 'Net Working Capital',
                });
              WorkingData = WorkingData.concat(NetWoCData);
            }
            if (GrossWoC !== null) {
              if (header.year5)
                GrossWoCData.push({
                  year: header.year5,
                  value: GrossWoC.year5 / this.state.tyle,
                  type: 'Gross Working Capital',
                });
              if (header.year4)
                GrossWoCData.push({
                  year: header.year4,
                  value: GrossWoC.year4 / this.state.tyle,
                  type: 'Gross Working Capital',
                });
              if (header.year3)
                GrossWoCData.push({
                  year: header.year3,
                  value: GrossWoC.year3 / this.state.tyle,
                  type: 'Gross Working Capital',
                });
              if (header.year2)
                GrossWoCData.push({
                  year: header.year2,
                  value: GrossWoC.year2 / this.state.tyle,
                  type: 'Gross Working Capital',
                });
              if (header.year1)
                GrossWoCData.push({
                  year: header.year1,
                  value: GrossWoC.year1 / this.state.tyle,
                  type: 'Gross Working Capital',
                });

              WorkingData = WorkingData.concat(GrossWoCData);
            }
            //#endregion

            //#region Account Receivable and Payable
            let ReAcc = data.find((x) => x.name === 'Receivable Account');
            let PayAcc = data.find((x) => x.name === 'Payable Account');
            let ReAccData = [];
            let PayAccCData = [];
            let AccountData = [];
            if (ReAcc !== null) {
              if (header.year5)
                ReAccData.push({
                  year: header.year5,
                  value: ReAcc.year5 / this.state.tyle,
                  name: 'Receivable Account',
                });
              if (header.year4)
                ReAccData.push({
                  year: header.year4,
                  value: ReAcc.year4 / this.state.tyle,
                  name: 'Receivable Account',
                });
              if (header.year3)
                ReAccData.push({
                  year: header.year3,
                  value: ReAcc.year3 / this.state.tyle,
                  name: 'Receivable Account',
                });
              if (header.year2)
                ReAccData.push({
                  year: header.year2,
                  value: ReAcc.year2 / this.state.tyle,
                  name: 'Receivable Account',
                });
              if (header.year1)
                ReAccData.push({
                  year: header.year1,
                  value: ReAcc.year1 / this.state.tyle,
                  name: 'Receivable Account',
                });
              AccountData = AccountData.concat(ReAccData);
            }
            if (PayAcc !== null) {
              if (header.year5)
                PayAccCData.push({
                  year: header.year5,
                  value: PayAcc.year5 / this.state.tyle,
                  name: 'Payable Account',
                });
              if (header.year4)
                PayAccCData.push({
                  year: header.year4,
                  value: PayAcc.year4 / this.state.tyle,
                  name: 'Payable Account',
                });
              if (header.year3)
                PayAccCData.push({
                  year: header.year3,
                  value: PayAcc.year3 / this.state.tyle,
                  name: 'Payable Account',
                });
              if (header.year2)
                PayAccCData.push({
                  year: header.year2,
                  value: PayAcc.year2 / this.state.tyle,
                  name: 'Payable Account',
                });
              if (header.year1)
                PayAccCData.push({
                  year: header.year1,
                  value: PayAcc.year1 / this.state.tyle,
                  name: 'Payable Account',
                });
              AccountData = AccountData.concat(PayAccCData);
            }
            //#endregion

            this.setState({
              TangibleData: TangibleData,
              InterestData: InterestData,
              NetSalesData: NetSalesData,
              WorkingData: WorkingData,
              AccountData: AccountData,
              NetIncomeData: NetIncomeData,
              loading: false,
            });
          },
          (err) => {
            console.log(err);
            this.setState({
              loading: false,
            });
          }
        );
    }
  }

  render() {
    const { DataView } = DataSet;
    const dv = new DataView().source(this.state.NetIncomeData);
    dv.transform({
      type: 'fold',
      fields: ['a', 'b'], // 展开字段集
      key: 'user', // key字段
      value: 'score', // value字段
    });

    const cols = {
      year: {
        range: [0, 1],
      },
    };

    return (
      <div className="content b-info" id="overview-loading">
        {this.state.loading ? (
          <Form.Row>
            <Col xs="12" lg="12" style={{ height: this.state.heightLoading }}>
              <RLoading loading={true} />
            </Col>
          </Form.Row>
        ) : (
          <React.Fragment>
            <Form.Row>
              <Col sm={12} md={7}>
                <div className="mb-2 slide scrollbar form-row">
                  {this.state.listSummary.map((item, index) => {
                    return (
                      <div className="col-lg-3 item_slide" key={index}>
                        <div className="card card-dashboard-two">
                          <div className="card-header">
                            <h6>
                              {item.value}
                              {item.isup ? (
                                <span className="iconify" data-icon="mdi:trending-up"></span>
                              ) : (
                                <span className="iconify" data-icon="mdi:trending-down"></span>
                              )}
                              <small>{item.per}</small>
                            </h6>
                            <p>{item.title}</p>
                          </div>
                          <div className="card-body">
                            <p className="type">VND Billion</p>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </Col>
              <Col sm={12} md={5}>
                <RCard title="Tangible Net Worth" className="card card-danger">
                  <Chart
                    height={250}
                    autoFit
                    data={this.state.TangibleData}
                    interactions={['active-region']}
                  >
                    <Interval
                      position="year*sales"
                      size={15}
                      opacity={0.3}
                      fill={{
                        fillOpacity: 0.8,
                      }}
                      style={[
                        'sales',
                        (val) => {
                          return {
                            lineWidth: 1,
                            strokeOpacity: 1,
                            fillOpacity: 0.3,
                            opacity: 0.65,
                            // stroke: colorMap[val],
                          };
                        },
                      ]}
                    />
                    <Tooltip shared />
                  </Chart>
                </RCard>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col xs="12" lg="3" style={{ paddingRight: 30 }}>
                <RCard title="Interest Bearing Debts" className="card card-danger">
                  <Chart height={250} data={this.state.InterestData} autoFit>
                    <Coordinate transpose />
                    <Interval size={15} position="year*sales" />
                  </Chart>
                </RCard>
              </Col>
              <Col xs="12" lg="4" style={{ paddingRight: 30 }}>
                <RCard title="Net Income Margin (%)" className="card card-danger">
                  <Chart
                    height={250}
                    data={dv.rows}
                    autoFit
                    scale={{
                      score: {
                        min: 0,
                        max: 80,
                      },
                    }}
                    interactions={['legend-highlight']}
                  >
                    <Coordinate type="polar" radius={0.8} />
                    <Tooltip shared />
                    <Point position="item*score" color="user" shape="circle" />
                    <Line position="item*score" color="user" size="2" />
                    <Area position="item*score" color="user" />

                    <Axis name="score" grid={{ line: { type: 'line' } }} />

                    <Axis name="item" line={false} />
                  </Chart>
                </RCard>
              </Col>
              <Col sm={12} md={5}>
                <RCard title="Net sales" className="card card-danger">
                  <Chart
                    height={250}
                    autoFit
                    data={this.state.NetSalesData}
                    interactions={['active-region']}
                  >
                    <Interval size={15} position="year*sales" />
                    <Tooltip shared />
                  </Chart>
                </RCard>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col xs="12" lg="7" style={{ paddingRight: 30 }}>
                <RCard title="Working Capital" className="card card-danger">
                  <Chart height={250} data={this.state.WorkingData} scale={cols} autoFit>
                    <Legend />
                    <Axis name="year" />
                    <Axis name="value" />
                    <Tooltip
                      crosshairs={{
                        type: 'y',
                      }}
                      itemTpl={'<tr data-index={index}><td>{value}</td></tr>'}
                    ></Tooltip>
                    <Geom
                      type="point"
                      position="year*value"
                      size={4}
                      shape={'circle'}
                      color={'type'}
                      style={{
                        stroke: '#fff',
                        lineWidth: 1,
                      }}
                    />
                    <Geom
                      type="line"
                      position="year*value"
                      size={2}
                      color={'type'}
                      shape={'smooth'}
                    />
                  </Chart>
                </RCard>
              </Col>

              <Col sm={12} md={5}>
                <RCard title="Account Receivable and Payable" className="card card-danger">
                  <Chart height={250} padding="auto" data={this.state.AccountData} autoFit>
                    <Interval
                      size={15}
                      adjust={[
                        {
                          type: 'dodge',
                          marginRatio: 0,
                        },
                      ]}
                      color="name"
                      position="year*value"
                    />
                    <Tooltip shared />
                    <Legend position="bottom" />
                  </Chart>
                </RCard>
              </Col>
            </Form.Row>
          </React.Fragment>
        )}
      </div>
    );
  }
}

export { SummaryOFSOverView };
