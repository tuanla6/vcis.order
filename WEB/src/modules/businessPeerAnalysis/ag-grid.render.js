import React from 'react';
import PropTypes from 'prop-types';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import './style.scss';

class HeaderRender extends React.Component {
  static propTypes = {
    displayName: PropTypes.string,
    style: PropTypes.object,
    showIcon: PropTypes.bool,
    callback: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { displayName, style, showIcon, callback } = this.props;
    return (
      <div className="agrid-header" style={style}>
        {displayName}
        {showIcon && (
          <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Remove column</Tooltip>}>
            <div onClick={callback}>
              <span
                className="iconify fa"
                data-icon="mdi:close"
                data-inline="false"
                style={{ marginLeft: 10, fontSize: 15, cursor: 'pointer' }}
              ></span>
            </div>
          </OverlayTrigger>
        )}
      </div>
    );
  }
}

class ActionRender extends React.Component {
  static propTypes = {
    actions: PropTypes.array,
    style: PropTypes.any,
  };
  constructor(props) {
    super(props);
  }
  render() {
    const { actions, style } = this.props;
    return (
      <div className="action" style={style}>
        {actions &&
          actions.map((x, i) => {
            return (
              x.show && (
                <OverlayTrigger
                  key={i}
                  overlay={<Tooltip id="tooltip-disabled">{x.tooltip}</Tooltip>}
                >
                  <div onClick={x.callback}>
                    <span
                      className="iconify icon"
                      data-icon={x.icon}
                      data-inline="false"
                      style={{ fontSize: 15 }}
                    ></span>
                  </div>
                </OverlayTrigger>
              )
            );
          })}
      </div>
    );
  }
}

class NumberRender extends React.Component {
  static propTypes = {
    value: PropTypes.any,
    styleNumber: PropTypes.any,
    show: PropTypes.bool,
  };
  static defaultProps = {
    show: true,
  };
  constructor(props) {
    super(props);
  }
  currencyFormat(num) {
    if (num) {
      if (Number(num) === num && num % 1 === 0) {
        return num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
      }
      return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
  }

  render() {
    const { value, styleNumber, show } = this.props;
    return (
      show && (
        <div style={{ textAlign: 'right' }}>
          <span style={styleNumber}>{value && this.currencyFormat(value)}</span>
        </div>
      )
    );
  }
}

export { HeaderRender, ActionRender, NumberRender };
