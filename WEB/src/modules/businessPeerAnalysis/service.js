import { http } from 'src/shared/utils';

class PeerAnalysisService {
  getPeerAnalysis(obj) {
    return http.post('api/vcis-report/top10companies', obj);
  }
}
const peerAnalysisService = new PeerAnalysisService();

export { peerAnalysisService, PeerAnalysisService };

const chitieus = [
  {
    category: 'Balance Sheet',
    datas: [
      { id: 19, name: 'Current Assets' },
      { id: 13, name: 'Cash and cash equivalents' },
      { id: 104, name: 'Short-term marketable investments' },
      { id: 113, name: 'Trade debts' },
      { id: 57, name: 'Inventories' },
      { id: 84, name: 'Other Current Assets' },
      { id: 72, name: 'Non- Current Assets' },
      { id: 94, name: 'Property, plant, and equipment' },
      { id: 60, name: 'Investment property' },
      { id: 64, name: 'Long-term investments' },
      { id: 85, name: 'Other Non- Current Assets' },
      { id: 106, name: 'Total Assets' },
      { id: 21, name: 'Current liabilities' },
      { id: 112, name: 'Trade creditor' },
      { id: 103, name: 'Short-term loans' },
      { id: 86, name: 'Other current liabilities' },
      { id: 73, name: 'Non-Current liabilities' },
      { id: 65, name: 'Long-term loans' },
      { id: 88, name: 'Other non-current liabilities' },
      { id: 109, name: 'Total Liabilities' },
      { id: 91, name: "Owner's investment capital" },
      { id: 101, name: 'Retained earnings' },
      { id: 89, name: 'Other owner’s equity' },
      { id: 90, name: "Owner's Equity" },
      { id: 110, name: 'Total Liabilities & Owner’s Equity' },
    ],
  },
  {
    category: 'Income Statement',
    datas: [
      { id: 1, name: 'Net Sales', selected: true },
      { id: 18, name: 'Costs of goods sold' },
      { id: 53, name: 'Gross profit' },
      { id: 102, name: 'Sales and G&A Expenses' },
      { id: 83, name: 'Operating expenses' },
      { id: 78, name: 'Operating Income' },
      { id: 48, name: 'Financial income (expense)' },
      { id: 47, name: 'Financial expense' },
      { id: 87, name: 'Other income (expense)' },
      { id: 43, name: 'EBT' },
      { id: 54, name: 'Income tax' },
      { id: 31, name: 'Deferred income tax' },
      { id: 2, name: 'Net Income', selected: true },
      { id: 63, name: 'Loan interest expenses' },
      { id: 35, name: 'EBIT' },
      { id: 32, name: 'Depreciation and Amortization' },
      { id: 39, name: 'EBITDA' },
    ],
  },
  {
    category: 'Other Financial Statements',
    datas: [
      { id: 77, name: 'Operating Assets' },
      { id: 74, name: 'Non-Operating Assets' },
      { id: 114, name: 'Working Capital' },
      { id: 55, name: 'Interest Bearing Debts' },
      { id: 105, name: 'Tangible Net Worth' },
      { id: 81, name: 'Operating Liabilities' },
      { id: 75, name: 'Non-Operating Liabilities' },
      { id: 8, name: 'CAPEX (Capital expenditures)' },
      { id: 10, name: 'CFO (Net cash flows from operating activities)' },
      { id: 10, name: 'CFI (Net cash flows from investing activities)' },
      { id: 9, name: 'CFF (Net cash flows from financing activities)' },
      { id: 66, name: 'Net Cash Flow' },
      { id: 22, name: 'Daily Cash Operating Expenses' },
    ],
  },

  {
    category: 'Liquidity Ratios',
    datas: [
      { id: 20, name: 'Current Ratio' },
      { id: 95, name: 'Quick Ratio' },
      { id: 12, name: 'Cash Ratio' },
      { id: 30, name: 'Defensive Interval Ratio' },
      { id: 11, name: 'Cash Conversion Cycle' },
    ],
  },

  {
    category: 'Activity Ratios',
    datas: [
      { id: 58, name: 'Inventory turnover' },
      { id: 99, name: 'Receivables from customer turnover' },
      { id: 93, name: 'Payables to supplier turnover' },
      { id: 115, name: 'Working capital turnover' },
      { id: 49, name: 'Fixed asset turnover' },
      { id: 23, name: 'Days of inventory on hand (DOH)' },
      { id: 24, name: 'Days of sales outstanding (DSO)' },
      { id: 76, name: 'Number of days of payables (DPO)' },
      { id: 7, name: 'Average Monthly Inventory by monthly net Sales' },
      { id: 111, name: 'Total asset turnover' },
    ],
  },

  {
    category: 'Credit Ratios',
    datas: [
      { id: 37, name: 'EBIT Interest Coverage' },
      { id: 41, name: 'EBITDA Interest Coverage' },
      { id: 50, name: 'Free Operating Cash flow to Debt' },
      { id: 33, name: 'Discretionary Cash flow to Debt' },
      { id: 28, name: 'Debt to EBITDA' },
      { id: 108, name: 'Total Debt to Total Debt+Equity' },
    ],
  },

  {
    category: 'Coverage Ratios',
    datas: [
      { id: 25, name: 'Debt Coverage' },
      { id: 56, name: 'Interest Coverage' },
      { id: 100, name: 'Reinvestment' },
      { id: 26, name: 'Debt Payment' },
      { id: 34, name: 'Dividend Payment' },
      { id: 59, name: 'Investing and Financing' },
    ],
  },

  {
    category: 'Solvency Ratios',
    datas: [
      { id: 27, name: 'Debt to Assets Ratio (%)' },
      { id: 29, name: 'Debt to Equity Ratio (%)' },
      { id: 46, name: 'Financial Leverage Ratio (%)' },
      { id: 62, name: 'Liabilities by Total Assets (%)' },
      { id: 61, name: "Liabilities by Owner's Equity (%)" },
    ],
  },
  {
    category: 'Return on Net Sales',
    datas: [
      { id: 52, name: 'Gross Profit Margin (%)' },
      { id: 80, name: 'Operating Income Margin (%)' },
      { id: 5, name: 'EBITDA Margin (%)', selected: true },
      { id: 6, name: 'EBIT Margin (%)', selected: true },
      { id: 38, name: 'EBT Margin (%)' },
      { id: 69, name: 'Net Income Margin (%)' },
    ],
  },
  {
    category: 'Return on Investment',
    datas: [
      { id: 82, name: 'Operating ROA (%)' },
      { id: 3, name: 'ROA (%)', selected: true },
      { id: 98, name: 'ROIC (Return on invested capital) (%)' },
      { id: 4, name: 'ROE (%)', selected: true },
    ],
  },

  {
    category: 'Performance',
    datas: [
      { id: 14, name: 'Cash flow to Sales' },
      { id: 15, name: 'Cash return on Assets' },
      { id: 16, name: 'Cash return on Equity' },
      { id: 17, name: 'Cash to Income' },
    ],
  },

  {
    category: 'Growth Rate Ratios',
    datas: [
      { id: 107, name: 'Total Assets Growth Rate(%)' },
      { id: 92, name: 'Owners Equity Growth Rate (%)' },
      { id: 70, name: 'Net Sale Growth Rate (%)' },
      { id: 51, name: 'Gross Profit Growth Rate (%)' },
      { id: 79, name: 'Operating Income Growth Rate (%)' },
      { id: 40, name: 'EBITDA Growth Rate (%)' },
      { id: 36, name: 'EBIT Growth Rate (%)' },
      { id: 44, name: 'EBT Growth Rate (%)' },
      { id: 68, name: 'Net Income Growth Rate (%)' },
    ],
  },
];

export { chitieus };
