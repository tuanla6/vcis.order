import React from 'react';
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import './style.scss';
import { peerAnalysisService, chitieus } from './service';
import { getDistanceToTop } from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
import { BusinessModalComponent } from '../businessDupontAnalysis/modal-business';
import { ChiTieuModalComponent } from './modal-chitieus';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { HeaderRender, ActionRender, NumberRender } from './ag-grid.render';

class PeerAnalysis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      show: false,
      ids: [],
      isInit: false,
      chitieus: [],

      columnDefs: [],
      defaultColDef: {
        resizable: true,
        wrapText: true,
        autoHeight: true,
      },
      rowData: [],
      frameworkComponents: {
        agColumnHeader: HeaderRender,
        actionRender: ActionRender,
        numberRender: NumberRender,
      },
      disabled: false,
      getRowNodeId: function (data) {
        return data.businessId;
      },
    };
    this.subscriptions = {};
  }

  getcolumnDefs(props) {
    const that = this;
    return [
      // {
      //   colKey: 'stt',
      //   headerName: 'STT',
      //   pinned: 'left',
      //   field: 'stt',
      //   maxWidth: 50,
      //   cellRenderer: function (params) {
      //     return params.node.rowIndex + 1;
      //   },
      // },
      {
        headerName: 'Company name',
        field: 'businessName',
        pinned: 'left',
        flex: 1,
        minWidth: 350,
        wrapText: true,
        autoHeight: true,
      },
      {
        headerName: 'Employee',
        field: 'totalEmployees',
        cellRenderer: 'numberRender',
        flex: 1,
        minWidth: 150,
      },
      {
        headerName: 'Net Sales',
        cellRenderer: 'numberRender',
        field: 'Net Sales',
        flex: 1,
        minWidth: 150,
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: 'Net Income',
        cellRenderer: 'numberRender',
        field: 'Net Income',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: 'ROA (%)',
        cellRenderer: 'numberRender',
        field: 'ROA (%)',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: 'ROE (%)',
        cellRenderer: 'numberRender',
        field: 'ROE (%)',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: 'EBITDA Margin (%)',
        cellRenderer: 'numberRender',
        field: 'EBITDA Margin (%)',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: 'EBIT Margin (%)',
        cellRenderer: 'numberRender',
        field: 'EBIT Margin (%)',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      },
      {
        headerName: '',
        field: 'action',
        editable: false,
        width: 50,
        pinned: 'right',
        cellRenderer: 'actionRender',
        cellRendererParams: function (e) {
          return {
            actions: [
              {
                tooltip: 'Remove',
                show: e.data.businessName !== 'Mean' && e.data.businessName !== 'Median',
                icon: 'mdi:trash-can-outline',
                callback: () => that.remove(e),
              },
            ],
          };
        },
        headerComponent: 'actionRender',
        headerComponentParams: function () {
          return {
            actions: [
              {
                tooltip: 'Thêm mới chỉ tiêu',
                show: true,
                icon: 'mdi:plus',
                callback: () => that.addChiTieu(),
              },
            ],
          };
        },
      },
    ];
  }

  componentDidMount() {
    this.setState({ heightLoading: getDistanceToTop('#PeerAnalysis') });
    let col = this.getcolumnDefs(this.props);
    this.setState({
      columnDefs: col,
    });
    const ids = [this.state.id];
    let ds_ct = [];
    chitieus.map((x) => (ds_ct = ds_ct.concat(x.datas)));
    let ct = ds_ct.filter((y) => y.selected).map((x) => x.name);
    this.setState({ isInit: true, chitieus: ct }, () => {
      this.getData(ids);
    });
  }

  setColumnDefs(cols) {
    this.setState({ loadingNew: true, chitieus: cols });
    let arr = this.state.columnDefs || [];
    cols = cols.filter((x) => !arr.map((y) => y.headerName).includes(x));
    cols.forEach((element) => {
      arr.push({
        headerName: element,
        field: element,
        wrapText: true,
        autoHeight: true,
        cellRenderer: 'numberRender',
        headerComponentParams: function (e) {
          return {
            showIcon: true,
            callback: () => that.removeColumn(e),
          };
        },
      });
    });
    const that = this;
    setTimeout(function () {
      that.setState({ loadingNew: false });
    }, 700);
    this.setState({ columnDefs: arr }, () => {
      this.gridApi.setColumnDefs(arr);
    });
  }

  onGridReady = (params) => {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // this.gridApi.setPinnedBottomRowData(this.state.rowsPind);
  };

  remove(node) {
    let deletedRow = node.data;
    let ids = this.state.ids;
    ids = ids.filter((x) => x !== deletedRow.businessId);
    this.gridApi.refreshCells({ force: true });
    this.gridApi.applyTransaction({ remove: [deletedRow] });
    this.setState({ disabled: false, ids: ids });

    var rowNodeMedian = this.gridApi.getRowNode('Median');
    var rowNodeMean = this.gridApi.getRowNode('Mean');
    let rowDataRemove = this.state.rowDataRemove;
    rowDataRemove = rowDataRemove.filter(
      (x) =>
        x.businessName !== 'Mean' &&
        x.businessName !== 'Median' &&
        x.businessId !== deletedRow.businessId
    );
    this.setState({ rowDataRemove: rowDataRemove });
    let median = this.setMedian(rowDataRemove);
    let mean = this.setMean(rowDataRemove);

    rowNodeMedian.setData(median);
    rowNodeMean.setData(mean);
  }

  addChiTieu() {
    this.setState({ showChiTieu: true });
  }

  removeColumn(e) {
    const that = this;
    that.setState({ loadingNew: true });

    setTimeout(function () {
      let columnDefs = that.state.columnDefs;
      columnDefs = columnDefs.filter((x) => x.headerName !== e.displayName);
      that.setState({ columnDefs: columnDefs });
      that.gridApi.setColumnDefs(columnDefs);
      that.setState({ loadingNew: false });
    }, 500);
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type, isInit: false }, () => {
        this.getData(this.state.ids);
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year, isInit: false }, () => {
        this.getData(this.state.ids);
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter, isInit: false }, () => {
        this.getData(this.state.ids);
      });
    }
  }

  addNew() {
    this.setShow(true);
  }

  setShow(show) {
    this.setState({
      show: show,
    });
  }

  getData(ids) {
    this.setState({ ids: ids });
    let { id, type, year, quarter } = this.state;
    const obj = {
      ids: ids,
      FSYear: year,
      Quarter: quarter,
      Type: type,
      id: id,
      IsInit: this.state.isInit,
    };
    this.setState({ loading: true });
    this.subscriptions['getData'] = peerAnalysisService.getPeerAnalysis(obj).subscribe(
      (res) => {
        let arr = [];
        res.forEach((element) => {
          let bname = element.datas[0].businessName;
          let totalEmployees = element.datas[0].totalEmployees;
          let businessId = element.datas[0].businessId;
          let obj = {
            businessName: bname,
            totalEmployees: totalEmployees,
            businessId: businessId,
          };
          element.datas.forEach((it) => {
            obj[it.name.trim()] = it.year1;
          });
          arr.push(obj);
        });

        let ids = arr.map((x) => x.businessId);
        this.setState({ ids: ids });

        if (arr.length >= 10) {
          this.setState({ disabled: true });
        }

        // mean
        let mean = this.setMean(arr);

        // median
        let median = this.setMedian(arr);

        // var rowsPind = [];
        // rowsPind.push(mean);
        // rowsPind.push(median);
        // this.setState({ rowsPind: rowsPind });

        // add mean, median to rowData
        arr.push(mean);
        arr.push(median);

        this.setState({
          rowData: arr,
          rowDataRemove: arr,
          loading: false,
          data: res,
        });

        // style mean, median
        let rows = Array.from(document.querySelectorAll('span.ag-cell-value'));
        rows.find((el) => el.innerText === 'Mean')?.classList.add('name-custom');
        rows.find((el) => el.innerText === 'Median')?.classList.add('name-custom');
      },
      (err) => {
        console.log(err);
        this.setState({ loading: false });
      }
    );
  }

  getMedian(arr) {
    arr = [...arr].sort((a, b) => a - b);
    let mid = arr.length >> 1;
    return arr.length % 2 ? arr[mid] : (arr[mid - 1] + arr[mid]) / 2;
  }

  setMedian(arr) {
    let median = {};
    arr.forEach((x) => {
      Object.keys(x).forEach((y) => {
        if (!median[y]) {
          median[y] = [];
        }
        x[y] !== null && median[y].push(x[y]);
      });
    });
    Object.keys(median).forEach((key) => {
      median[key] = this.getMedian(median[key]);
    });
    median.businessName = 'Median';
    median.businessId = 'Median';

    return median;
  }

  setMean(arr) {
    let avg = Array.from(
      arr.reduce(
        (acc, o) =>
          o &&
          Object.keys(o).reduce(
            (acc, key) =>
              typeof o[key] === 'number'
                ? acc.set(
                    key,
                    (([sum, count]) => [sum + o[key], count + 1])(acc.get(key) || [0, 0])
                  )
                : acc,
            acc
          ),
        new Map()
      ),
      ([name, [sum, count]]) => ({ name, average: sum / count })
    );
    let mean = avg.reduce((obj, item) => ((obj[item.name] = item.average), obj), {});
    mean.businessName = 'Mean';
    mean.businessId = 'Mean';

    return mean;
  }

  render() {
    const { loading, chitieus, disabled, heightLoading } = this.state;
    return (
      <React.Fragment>
        <div
          className="content content-PeerAnalysis"
          id="PeerAnalysis"
          style={{ padding: 0, marginTop: 0 }}
        >
          {loading ? (
            <CCol xs="12" lg="12" style={{ height: heightLoading }}>
              <RLoading loading={true} />
            </CCol>
          ) : (
            <React.Fragment>
              <RLoading loading={this.state.loadingNew} />

              <CRow>
                <CCol className="text-right">
                  <OverlayTrigger
                    placement="top"
                    overlay={<Tooltip id="button-tooltip">Select business to compare</Tooltip>}
                  >
                    <Button
                      style={{ margin: '0.8rem 0' }}
                      variant="info"
                      size="sm"
                      onClick={this.addNew.bind(this)}
                      disabled={disabled}
                    >
                      <span
                        className="iconify fa"
                        data-icon="fa-solid:plus"
                        data-inline="false"
                      ></span>
                      Select business
                    </Button>
                  </OverlayTrigger>
                </CCol>
              </CRow>
              <div className="dv-grid" style={{ height: 550 }}>
                <div style={{ height: '100%', boxSizing: 'border-box' }}>
                  <div
                    id="myGrid"
                    style={{
                      height: '100%',
                      width: '100%',
                    }}
                    className="ag-theme-alpine"
                  >
                    <AgGridReact
                      ref={(r) => (this.refgrid = r)}
                      columnDefs={this.state.columnDefs}
                      defaultColDef={this.state.defaultColDef}
                      animateRows={true}
                      onGridReady={this.onGridReady}
                      getRowNodeId={this.state.getRowNodeId}
                      rowData={this.state.rowData}
                      frameworkComponents={this.state.frameworkComponents}
                      pinnedBottomRowData={this.state.pinnedBottomRowData}
                      getRowNodeId={this.state.getRowNodeId}
                    />
                  </div>
                </div>
              </div>

              <Modal
                className="modal-middle"
                backdrop="static"
                show={this.state.show}
                onHide={() => this.setShow(false)}
              >
                <BusinessModalComponent
                  ids={this.state.ids}
                  maxSelect={10}
                  msg={'Can not show greater 10 Peer Analysis Business'}
                  onclose={(data) => {
                    this.setShow(data.show);
                    this.setState({ isInit: false }, () => {
                      this.getData(data.ids);
                    });
                  }}
                  {...this.props}
                ></BusinessModalComponent>
              </Modal>

              <Modal
                className="modal-middle"
                backdrop="static"
                show={this.state.showChiTieu}
                onHide={() => this.setState({ showChiTieu: false })}
              >
                <ChiTieuModalComponent
                  datas={chitieus}
                  onclose={(data) => {
                    this.setState({ showChiTieu: false });
                    this.setColumnDefs(data.datas);
                  }}
                  {...this.props}
                ></ChiTieuModalComponent>
              </Modal>
            </React.Fragment>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export { PeerAnalysis };
