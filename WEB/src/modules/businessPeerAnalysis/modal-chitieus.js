import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Col, FormCheck } from 'react-bootstrap';
import { Formik } from 'formik';
import { RLoading } from 'src/shared/component';
import { chitieus } from './service';

class ChiTieuModalComponent extends React.Component {
  static propTypes = {
    props: PropTypes.object,
    onclose: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      chitieus: [],
      datas: props.datas || [],
    };
    this.subscriptions = {};
  }
  Submit() {
    const data = {
      show: false,
      datas: this.state.datas,
    };
    this.props.onclose(data);
  }

  componentDidMount() {
    this.setState({ chitieus: chitieus });
  }

  checkCategory(key, checked) {
    let datas = this.state.datas || [];
    let obj = chitieus.find((x) => x.category === key);
    if (obj !== null) {
      let selected = obj.datas.map((x) => x.name);
      if (checked) {
        datas = datas.concat(selected);
      } else {
        datas = datas.filter((x) => !selected.includes(x));
      }
      this.setState({
        datas: datas,
      });
    }
  }
  handleCheckItem(key, checked) {
    let datas = this.state.datas;
    if (checked) {
      datas.push(key);
    } else {
      datas = datas.filter((x) => x !== key);
    }
    console.log(datas);
    this.setState({
      datas: datas,
    });
  }

  render() {
    let { datas, chitieus } = this.state;
    console.log('---', datas);
    return (
      <Formik
        onSubmit={(values) => {
          this.handleSubmit(values);
        }}
        enableReinitialize={true}
        initialValues={this.state.data}
      >
        {({ handleChange, handleSubmit, handleBlur, setFieldValue, values, touched, errors }) => (
          <Form
            noValidate
            onSubmit={handleSubmit}
            ref={() => {
              this.setFieldValue = setFieldValue;
            }}
          >
            <RLoading loading={this.state.loading} />
            <React.Fragment>
              <Modal.Header closeButton>
                <Modal.Title>Chọn chỉ tiêu</Modal.Title>
              </Modal.Header>
              <Modal.Body className="modalContent">
                {chitieus.map((x, i) => {
                  return (
                    <div key={i} className="chitieu-parent">
                      <Form.Row>
                        <FormCheck
                          className="category"
                          id={x.category}
                          value={x.category}
                          custom
                          type="checkbox"
                          label={x.category}
                          onChange={(ev) => {
                            this.checkCategory(ev.currentTarget.value, ev.currentTarget.checked);
                          }}
                        ></FormCheck>
                      </Form.Row>
                      <Form.Row className="data">
                        {x.datas.map((y, index) => {
                          return (
                            <Col lg={3} sm={12} key={index} className="col-chitieu">
                              <FormCheck
                                id={y.id}
                                value={y.name}
                                custom
                                type="checkbox"
                                label={y.name}
                                checked={datas.includes(y.name.toString())}
                                onChange={(ev) => {
                                  this.handleCheckItem(
                                    ev.currentTarget.value,
                                    ev.currentTarget.checked
                                  );
                                }}
                              ></FormCheck>
                            </Col>
                          );
                        })}
                      </Form.Row>
                    </div>
                  );
                })}
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={this.Submit.bind(this)} size="sm" variant="info">
                  <span className="iconify fa" data-icon="fa-check" data-inline="false"></span>
                  Select
                </Button>
              </Modal.Footer>
            </React.Fragment>
          </Form>
        )}
      </Formik>
    );
  }
}
export { ChiTieuModalComponent };
