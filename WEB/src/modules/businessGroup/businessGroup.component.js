import React from 'react';
import {
    CCol,
    CDataTable,
    CRow
} from '@coreui/react';
import './style.scss';
import { groupervice} from './service';
import { RCard } from 'src/shared/component';
import { formatDate } from 'src/shared/utils'
const SubsidiaryFields = [
    { key: 'items', label: 'No.', _style: { width: '5%' } },
    { key: 'name', label: 'Name', _style: { width: '20%' } },
    { key: 'foundDate', label: 'Incorporation date', _style: { width: '15%' } },
    { key: 'registrationNumber', label: 'Business ID', _style: { width: '15%' } },
    { key: 'sharedPercentage', label: 'Share Ownership', _style: { width: '15%' } },
    'address'
];
const RelatedFields = [
    { key: 'items', label: 'No.', _style: { width: '5%' } },
    { key: 'name', label: 'Name', _style: { width: '20%' } },
    { key: 'relationStatus', label: 'Relation status', _style: { width: '15%' } },
    { key: 'registrationNumber', label: 'Business ID', _style: { width: '15%' } },
    { key: 'operationStatus', label: 'Operation Status', _style: { width: '15%' } },
    'address'
]

class BusinessGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listDirectors: [],
            id: this.props.id,
            noItemMessageSubsidiary :'Loading... ',
            noItemMessageRelated : 'Loading... ',
        };
        this.subscriptions={};
    }

    componentDidMount() {
        this.getSubsidiaryCompany();
        this.getRelatedCompany();
    }
    componentWillUnmount() {

    }

    getSubsidiaryCompany() {
        let id = this.state.id;
        this.setState({ loadingSubsidiary: true });
        this.subscriptions['getSubsidiaryCompany'] = groupervice.GetSubsidiaryCompany(id).subscribe(res => {
            res.forEach(x => {
                x.name = x.name ? x.name : '';
                x.foundDate = x.foundDate ? formatDate(x.foundDate) : '';
                x.registrationNumber = x.registrationNumber ? x.registrationNumber : '';
                x.sharedPercentage = x.sharedPercentage ? x.sharedPercentage +'%' : '';
                x.address = x.address ? x.address : '';
            });

            this.setState({
                SubsidiaryCompany: res,
                loadingSubsidiary: false,
                noItemMessageSubsidiary: 'The company does not have Subsidiary company'
            })
        }, err => {
            console.log(err);
            this.setState({
                loadingSubsidiary: false
            })
        });
    }

    getRelatedCompany() {
        let id = this.state.id;
        this.setState({ loadingRelated: true });
        this.subscriptions['getRelatedCompany'] = groupervice.GetRelatedCompany(id).subscribe(res => {
            res.forEach(x => {
                x.name = x.name ? x.name : '';
                x.relationStatus = x.relationStatus ? x.relationStatus : '';
                x.registrationNumber = x.registrationNumber ? x.registrationNumber : '';
                x.operationStatus = x.operationStatus ? x.operationStatus : '';
                x.address = x.address ? x.address : '';
            });

            this.setState({
                RelatedCompany: res,
                loadingRelated: false,
                noItemMessageRelated: 'The company does not have Related company'
            })
        }, err => {
            console.log(err);
            this.setState({
                loadingRelated: false
            })
        });
    }


    render() {
        return (
            <div className="content-business-group">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Subsidiary company" className="card card-danger" loading={this.state.loadingSubsidiary}>
                            <CDataTable
                                items={this.state.SubsidiaryCompany}
                                fields={SubsidiaryFields}
                                pagination
                                itemsPerPage={15}
                                hover
                                scopedSlots = {{
                                    'items':
                                      (item, index)=>(
                                        <td>
                                          {index+1}
                                        </td>
                                      ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessageSubsidiary }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Related company" className="card card-danger" loading={this.state.loadingRelated}>
                            <CDataTable
                                items={this.state.RelatedCompany}
                                fields={RelatedFields}
                                pagination
                                itemsPerPage={15}
                                hover
                                scopedSlots = {{
                                    'items':
                                      (item, index)=>(
                                        <td>
                                          {index+1}
                                        </td>
                                      ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessageRelated }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
            </div>
        )
    }
}

export { BusinessGroup };