import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class Groupervice extends BaseService {
  constructor(props) {
    const _props = Object.assign({}, { url: 'api/vcis-business-shareholders' }, props);
    super(_props);
    this.sendToForm = new Subject();
  }

  GetSubsidiaryCompany(businessId) {
    return http.get('api/vcis-business-shareholders/subsidiary-company?businessId=' + businessId);
  }
  GetRelatedCompany(businessId) {
    return http.get('api/vcis-business-shareholders/related-company?businessId=' + businessId);
  }
}
const groupervice = new Groupervice();

export { groupervice, Groupervice };
