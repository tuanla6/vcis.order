import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button, Col, FormCheck } from 'react-bootstrap';
import { Formik } from 'formik';
import { CPagination } from '@coreui/react';
import { RLoading } from 'src/shared/component';
import { dupotAnalysisService } from './service';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class BusinessModalComponent extends React.Component {
  static propTypes = {
    props: PropTypes.object,
    onclose: PropTypes.func,
    maxSelect: PropTypes.number,
    msg: PropTypes.string,
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: {},
      page: 1,
      page_size: 15,
      sort: {},
      search: '',
      filter: {},
      selected: {},
      ids: props.ids || [],
      id: props.currentId,
      maxSelect: props.maxSelect || 6,
      msg: props.msg || 'Can not compare greater 5 business!',
    };
    this.subscriptions = {};
  }
  Submit() {
    const data = {
      show: false,
      ids: this.state.ids,
    };
    this.props.onclose(data);
  }

  componentDidMount() {
    this.getData();
  }
  handleCheckItem(key, checked) {
    let { ids, maxSelect, msg } = this.state;
    ids = ids.map((x) => x.toString());
    if (checked) {
      ids.push(key);
    } else {
      ids = ids.filter((x) => x !== key);
    }
    if (ids.length > maxSelect) {
      toast.warn(msg, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      ids = ids.filter((x) => x !== key);
    }
    this.setState({
      ids: ids,
    });
  }
  getData() {
    const meta = {
      page: this.state.page,
      page_size: this.state.page_size,
      sort: this.state.sort,
      search: this.state.search,
      filter: this.state.filter,
    };
    this.setState({ loading: true });
    this.subscriptions['getData'] = dupotAnalysisService.getMany(meta).subscribe(
      (res) => {
        let totalPage = Math.ceil(res.total / meta.page_size);
        this.setState({
          businessData: res.results,
          page: res.page,
          totalPage: totalPage,
          loading: false,
        });
      },
      (err) => {
        this.setState({
          loading: false,
        });
      }
    );
  }

  goToChangePage(page) {
    this.setState(
      {
        page: page,
      },
      () => {
        this.getData();
      }
    );
  }

  reset() {
    this.setFieldValue('crid', '');
    this.setFieldValue('tax_brn', '');
    this.setFieldValue('name', '');
    this.setState({ page: 1, filter: null, search: null }, () => {
      this.getData();
    });
  }

  handleSubmit(value) {
    this.setState(
      {
        page: 1,
        filter: value,
      },
      () => {
        this.getData();
      }
    );
  }

  render() {
    let { id, ids } = this.state;
    return (
      <Formik
        onSubmit={(values) => {
          this.handleSubmit(values);
        }}
        enableReinitialize={true}
        initialValues={this.state.data}
      >
        {({ handleChange, handleSubmit, handleBlur, setFieldValue, values, touched, errors }) => (
          <Form
            noValidate
            onSubmit={handleSubmit}
            ref={() => {
              this.setFieldValue = setFieldValue;
            }}
          >
            <RLoading loading={this.state.loading} />
            <React.Fragment>
              <Modal.Header closeButton>
                <Modal.Title>Search Business</Modal.Title>
              </Modal.Header>
              <Modal.Body className="modalContent">
                <Form.Row>
                  <Form.Group as={Col} md="4" sm="12" controlId="crid">
                    <Form.Label>CRID</Form.Label>
                    <React.Fragment>
                      <Form.Control
                        className="form-control-sm"
                        type="text"
                        name="crid"
                        value={values.crid || ''}
                        onChange={(val) => {
                          setFieldValue('crid', val.target.value);
                        }}
                        onBlur={handleBlur}
                        isInvalid={touched.crid && !!errors.crid}
                      />
                    </React.Fragment>
                  </Form.Group>
                  <Form.Group as={Col} md="4" sm="12" controlId="name">
                    <Form.Label>Name</Form.Label>
                    <React.Fragment>
                      <Form.Control
                        className="form-control-sm"
                        type="text"
                        name="name"
                        value={values.name || ''}
                        onChange={(val) => {
                          setFieldValue('name', val.target.value);
                        }}
                        onBlur={handleBlur}
                        isInvalid={touched.name && !!errors.name}
                      />
                    </React.Fragment>
                  </Form.Group>
                  <Form.Group as={Col} md="4" sm="12" controlId="tax_brn">
                    <Form.Label>Tax/BRN</Form.Label>
                    <React.Fragment>
                      <Form.Control
                        className="form-control-sm"
                        type="text"
                        name="tax_brn"
                        value={values.tax_brn || ''}
                        onChange={(val) => {
                          setFieldValue('tax_brn', val.target.value);
                        }}
                        onBlur={handleBlur}
                        isInvalid={touched.tax_brn && !!errors.tax_brn}
                      />
                    </React.Fragment>
                  </Form.Group>
                </Form.Row>
                <Form.Row style={{ justifyContent: 'center', marginBottom: 10 }}>
                  <Button variant="info" size="sm" onClick={handleSubmit}>
                    <span className="iconify fa" data-icon="fa-search" data-inline="false"></span>
                    Search
                  </Button>
                  <Button
                    style={{ marginLeft: 10 }}
                    variant="danger"
                    size="sm"
                    onClick={() => this.reset()}
                  >
                    <span className="iconify fa" data-icon="fa-refresh" data-inline="false"></span>
                    Reset
                  </Button>
                </Form.Row>
                <Form.Row>
                  <table className="table table-hover modal-business">
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">CRID</th>
                        <th scope="col">Business name</th>
                        <th scope="col">TaxCode</th>
                        <th scope="col">Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.businessData?.map((x) => {
                        return (
                          <tr key={x.id}>
                            <td className="grid-selected">
                              <FormCheck
                                id={x.id}
                                value={x.id}
                                custom
                                type="checkbox"
                                label=""
                                checked={ids.map((z) => z.toString()).includes(x.id.toString())}
                                disabled={id?.toString() === x.id.toString()}
                                onChange={(ev) => {
                                  this.handleCheckItem(
                                    ev.currentTarget.value,
                                    ev.currentTarget.checked
                                  );
                                }}
                              ></FormCheck>
                            </td>
                            <td>{x.crid}</td>
                            <td>{x.name}</td>
                            <td>{x.taxCode}</td>
                            <td>{x.defaultAddress}</td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </Form.Row>
                <Form.Row className="row-pagination">
                  {this.state.totalPage > 0 && (
                    <CPagination
                      activePage={this.state.page}
                      pages={this.state.totalPage}
                      onActivePageChange={(i) => this.goToChangePage(i)}
                    ></CPagination>
                  )}
                  <div>
                    <ToastContainer
                      position="top-right"
                      autoClose={5000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                    />
                  </div>
                </Form.Row>
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={this.Submit.bind(this)} size="sm" variant="info">
                  <span className="iconify fa" data-icon="fa-check" data-inline="false"></span>
                  Select
                </Button>
              </Modal.Footer>
            </React.Fragment>
          </Form>
        )}
      </Formik>
    );
  }
}
export { BusinessModalComponent };
