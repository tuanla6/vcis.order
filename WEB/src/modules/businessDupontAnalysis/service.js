import { BaseService } from 'src/shared/services';
import { http } from 'src/shared/utils';

class DupotAnalysisService extends BaseService {
  constructor(props) {
    const _props = Object.assign({}, { url: 'api/Businesses' }, props);
    super(_props);
  }

  getDupontAnalysis(obj) {
    return http.post('api/vcis-report/DupontAnalysis', obj);
  }
}
const dupotAnalysisService = new DupotAnalysisService();

export { dupotAnalysisService, DupotAnalysisService };
