import React from 'react';
import { Table, Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import './style.scss';
import { dupotAnalysisService } from './service';
import { currencyFormat, getDistanceToTop } from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
import { BusinessModalComponent } from './modal-business';

class DupontAnalysis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,

      data: [],
      header: {},
      numyear: null,

      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,

      show: false,
      ids: [],
      dataGroup: {},
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.setState({ heightLoading: getDistanceToTop('#Dupont-Analysis') });
    const ids = [this.state.id];
    this.getData(ids);
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getData(this.state.ids);
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getData(this.state.ids);
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getData(this.state.ids);
      });
    }
  }

  addNew() {
    this.setShow(true);
  }

  setShow(show) {
    this.setState({
      show: show,
    });
  }

  getData(ids) {
    this.setState({ ids: ids });
    let { id, type, year, quarter } = this.state;
    const obj = {
      ids: ids,
      FSYear: year,
      Quarter: quarter,
      Type: type,
    };
    this.setState({ loading: true });
    this.subscriptions['getDupontAnalysis'] = dupotAnalysisService.getDupontAnalysis(obj).subscribe(
      (res) => {
        let current = res[0] || {};
        if (current.datas?.length > 0) {
          let bid = current.datas[0].businessId;
          if (bid.toString() === id.toString()) {
            let header = current.header || {};
            this.setState({
              numyear: header.numyear,
              header: header,
            });
          }
        }
        let data = this.state.data || [];
        res.map((x) => {
          data = data.concat(x.datas);
        });
        let group = data.reduce((r, a) => {
          r[a.businessName] = [...(r[a.businessName] || []), a];
          return r;
        }, {});
        this.setState({
          loading: false,
          dataGroup: group,
        });
      },
      (err) => {
        console.log(err);
        this.setState({ loading: false });
      }
    );
  }

  del(x) {
    let ids = this.state.ids;
    let group = this.state.dataGroup;
    let id = group[x][0]?.businessId;
    delete group[x];
    ids = ids.filter((item) => item.toString() !== id?.toString());
    this.setState({
      dataGroup: group,
      ids: ids,
    });
  }

  render() {
    const { numyear, header, loading, heightLoading } = this.state;
    console.log(heightLoading);
    let group = Object.keys(this.state.dataGroup);
    return (
      <React.Fragment>
        <div
          className="content content-Dupont-Analysis"
          id="Dupont-Analysis"
          style={{ padding: 0, marginTop: 0 }}
        >
          {loading ? (
            <CCol xs="12" lg="12" style={{ height: this.state.heightLoading }}>
              <RLoading loading={true} />
            </CCol>
          ) : (
            <React.Fragment>
              <CRow>
                <CCol className="text-right">
                  <OverlayTrigger
                    placement="top"
                    overlay={<Tooltip id="button-tooltip">Select business to compare</Tooltip>}
                  >
                    <Button
                      style={{ margin: '0.8rem 0' }}
                      variant="info"
                      size="sm"
                      onClick={this.addNew.bind(this)}
                    >
                      <span
                        className="iconify fa"
                        data-icon="fa-solid:plus"
                        data-inline="false"
                      ></span>
                      Select business
                    </Button>
                  </OverlayTrigger>
                </CCol>
              </CRow>
              <Table bordered hover>
                <thead>
                  <tr>
                    <th className="text-center">Description</th>
                    <th className="text-center">
                      <p
                        dangerouslySetInnerHTML={{
                          __html: header.year1,
                        }}
                      />
                    </th>
                    {numyear >= 2 && (
                      <th className="text-center">
                        {' '}
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year2,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 3 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year3,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 4 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year4,
                          }}
                        />
                      </th>
                    )}
                    {numyear >= 5 && (
                      <th className="text-center">
                        <p
                          dangerouslySetInnerHTML={{
                            __html: header.year5,
                          }}
                        />
                      </th>
                    )}
                  </tr>
                </thead>
                <tbody>
                  {group.map((x, i) => {
                    let childs = this.state.dataGroup[x];
                    return (
                      <React.Fragment key={x + '_' + i}>
                        <tr className="business-name">
                          <td colSpan={i === 0 ? numyear + 1 : numyear} style={{ borderRight: 0 }}>
                            <span>{i + 1 + '. ' + x}</span>
                          </td>
                          {i !== 0 && (
                            <td style={{ borderLeft: 0 }} className="text-right">
                              <button className="transparent" size="sm" onClick={() => this.del(x)}>
                                <span
                                  style={{ cursor: 'pointer', color: 'red' }}
                                  className="iconify"
                                  data-icon="mdi:delete-circle"
                                  data-inline="false"
                                  data-width="25"
                                  data-height="25"
                                ></span>
                              </button>
                            </td>
                          )}
                        </tr>
                        {childs.map((y, j) => {
                          return (
                            <tr key={y + '_' + j}>
                              <td>
                                <span className="item-name">{y.name}</span>
                              </td>
                              <td className="text-right">{currencyFormat(y.year1)}</td>
                              {numyear >= 2 && (
                                <td className="text-right">{currencyFormat(y.year2)}</td>
                              )}
                              {numyear >= 3 && (
                                <td className="text-right">{currencyFormat(y.year3)}</td>
                              )}
                              {numyear >= 4 && (
                                <td className="text-right">{currencyFormat(y.year4)}</td>
                              )}
                              {numyear >= 5 && (
                                <td className="text-right">{currencyFormat(y.year5)}</td>
                              )}
                            </tr>
                          );
                        })}
                      </React.Fragment>
                    );
                  })}
                </tbody>
              </Table>
              <Modal
                className="modal-middle"
                backdrop="static"
                show={this.state.show}
                onHide={() => this.setShow(false)}
              >
                <BusinessModalComponent
                  currentId={this.state.id}
                  ids={this.state.ids}
                  onclose={(data) => {
                    this.setShow(data.show);
                    this.getData(data.ids);
                  }}
                  {...this.props}
                ></BusinessModalComponent>
              </Modal>
            </React.Fragment>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export { DupontAnalysis };
