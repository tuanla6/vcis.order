import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class BusinessInfoService extends BaseService {
  constructor(props) {
    const _props = Object.assign(
      {},
      { url: 'api/vcis-business' },
      props
    );
    super(_props);
    this.sendToForm = new Subject();
  }

  getCompanyProfile(id, year, quarter) {
    let url = 'api/vcis-business/' + id + '?year=';
    if (year) url += year + '&quarter=';
    else url += '0&quarter=';
    if (quarter) url += quarter;
    return http.get(url);
  }
  getCompetitor(id, year) {
    let url =
      'api/vcis-business/competitor?id=' +
      id +
      '&year=' +
      year;
    return http.get(url);
  }

  GetReportSummary(id, year) {
    let url =
      'api/vcis-report?businessId=' + id + '&year=' + year;
    return http.get(url);
  }

  getYearFS(id, type) {
    let url =
      'api/vcis-business/' + id + '/getyearfs?type=' + type;
    return new Promise((resolve, reject) => {
      http.get(url).subscribe(
        (res) => {
          resolve(res);
        },
        (err) => {
          reject(err);
          console.log(err);
        }
      );
    });
  }
}
const businessInfoService = new BusinessInfoService();
export { businessInfoService, BusinessInfoService };
