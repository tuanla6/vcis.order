import React from 'react';
import {
  Card,
  Row,
  Col,
  Form,
  Image,
} from 'react-bootstrap';
import GoogleMapReact from 'google-map-react';

import './style.scss';
import { CardToogle } from 'src/shared/component';
import {
  currencyFormat,
  formatDate,
} from 'src/shared/utils';
import ImgE from 'src/assets/img/e.png';
import { businessInfoService } from './service';
const AnyReactComponent = ({ text }) => <div>{text}</div>;

const defaultProps = {
  center: {
    lat: 20.9987400233186,
    lng: 105.79783290624619,
  },
  zoom: 11,
};

class BusinessInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listReportSummary: [],
      listCompetitor: [],
      id: props.id,
      profile: props.profile || {},
      year: props.year,
      loadingProfile: props.loadingProfile,
      loadingCompetitor: false,
      tyle: 1000000000,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.GetReportSummary();
    this.getCompetitor();
  }

  componentDidUpdate() {
    if (
      this.props.id !== this.state.id ||
      this.props.year !== this.state.year
    ) {
      this.setState(
        { id: this.props.id, year: this.props.year },
        () => {
          this.getCompetitor();
          this.GetReportSummary();
        }
      );
    }
    if (
      JSON.stringify(this.props.profile) !==
      JSON.stringify(this.state.profile)
    ) {
      this.setState({ profile: this.props.profile });
    }
    if (
      this.props.loadingProfile !==
      this.state.loadingProfile
    ) {
      this.setState({
        loadingProfile: this.props.loadingProfile,
      });
    }
  }
  componentWillUnmount() {
    Object.keys(this.subscriptions).forEach((key) => {
      this.subscriptions[key].unsubscribe();
    });
  }

  GetReportSummary() {
    let { id, year } = this.state;
    if (id && year) {
      this.setState({ loadingReportSummary: true });

      this.subscriptions['GetReportSummary'] =
        businessInfoService
          .GetReportSummary(id, year)
          .subscribe(
            (res) => {
              res.forEach((x) => {
                x.year1 = x.year1
                  ? (x.year1 / this.state.tyle).toFixed(2)
                  : null;
                x.percent = x.percent
                  ? (x.percent * 100).toFixed(2) + ' %'
                  : null;
              });
              this.setState({
                listReportSummary: res,
                loadingReportSummary: false,
              });
            },
            (err) => {
              console.log(err);
              this.setState({
                loadingReportSummary: false,
              });
            }
          );
    }
  }

  getCompetitor() {
    let { id, year } = this.state;
    if (id && year) {
      this.setState({ loadingCompetitor: true });
      this.subscriptions['getCompetitor'] =
        businessInfoService
          .getCompetitor(id, year)
          .subscribe(
            (res) => {
              this.setState({
                listCompetitor: res,
                loadingCompetitor: false,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingCompetitor: false });
            }
          );
    }
  }

  render() {
    return (
      <div className="content b-info">
        <div className="mb-2 slide scrollbar form-row">
          {this.state.listReportSummary.map(
            (item, index) => {
              return (
                <div
                  className="col-lg-2_5 item_slide"
                  key={index}
                >
                  <div className="card card-dashboard-two">
                    <div className="card-header">
                      <h6>
                        {item.year1}
                        {item.isup ? (
                          <span
                            className="iconify"
                            data-icon="mdi:trending-up"
                          ></span>
                        ) : (
                          <span
                            className="iconify"
                            data-icon="mdi:trending-down"
                          ></span>
                        )}
                        <small>{item.percent}</small>
                      </h6>
                      <p>{item.name}</p>
                    </div>
                    <div className="card-body">
                      <p className="type">VND Billion</p>
                    </div>
                  </div>
                </div>
              );
            }
          )}
        </div>

        <Form.Row>
          <Col
            sm={12}
            md={6}
            style={{ paddingBottom: '10px' }}
          >
            <CardToogle
              title="Company profile"
              defaultShow={true}
              className="card card-danger"
              loading={this.state.loadingProfile}
            >
              <Col xs={12}>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>English name</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.name}
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>
                      Vietnamese Name{' '}
                    </Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.vnname}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Business ID</Form.Label>
                    <p className="form-control-static">
                      {
                        this.state.profile
                          .registrationNumber
                      }
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>VNC ID</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.crid}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>
                      Incorporation date
                    </Form.Label>
                    <p className="form-control-static">
                      {formatDate(
                        this.state.profile.foundDate,
                        'dd MMM yyyy'
                      )}
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Legal form</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.typeName}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>
                      Operation status
                    </Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.statusName}
                    </p>
                  </Form.Group>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>
                      Ownership status
                    </Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.statusBonds}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Tax code</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.taxCode}
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>
                      Legal representative
                    </Form.Label>
                    <p className="form-control-static">
                      {
                        this.state.profile
                          .legalRepresentative
                      }
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Charter capital</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.currency !== null
                        ? this.state.profile.currency +
                          ' ' +
                          currencyFormat(
                            this.state.profile
                              .registeredCapital
                          )
                        : currencyFormat(
                            this.state.profile
                              .registeredCapital
                          )}
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Paip-up capital</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile
                        .currencyPaidUpCapital !== null
                        ? this.state.profile
                            .currencyPaidUpCapital +
                          ' ' +
                          currencyFormat(
                            this.state.profile.paidUpCapital
                          )
                        : currencyFormat(
                            this.state.profile.paidUpCapital
                          )}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Fiscal year end</Form.Label>
                    <p className="form-control-static">
                      {formatDate(
                        this.state.profile.fsYearEnd,
                        'dd MMM'
                      )}
                    </p>
                  </Form.Group>

                  <Form.Group
                    as={Col}
                    md="6"
                    controlId="ten"
                  >
                    <Form.Label>Audit status</Form.Label>
                    <p className="form-control-static">
                      {this.state.profile.auditStatus}
                    </p>
                  </Form.Group>
                </Form.Row>
                <Form.Row>
                  <Form.Group
                    as={Col}
                    md="12"
                    controlId="ten"
                  >
                    <Form.Label>
                      Industry classification
                    </Form.Label>
                    {this.state.profile.industrys !=
                    null ? (
                      <>
                        {this.state.profile.industrys
                          .vcis !== ' ' && (
                          <p className="form-control-static">
                            {'VCIS: ' +
                              this.state.profile.industrys
                                .vcis}
                          </p>
                        )}
                        {this.state.profile.industrys
                          .nace !== ' ' && (
                          <p className="form-control-static">
                            {'NACE: ' +
                              this.state.profile.industrys
                                .nace}
                          </p>
                        )}
                        {this.state.profile.industrys
                          .naics !== ' ' && (
                          <p className="form-control-static">
                            {'NAICS: ' +
                              this.state.profile.industrys
                                .naics}
                          </p>
                        )}
                      </>
                    ) : null}
                  </Form.Group>
                </Form.Row>
              </Col>
            </CardToogle>
          </Col>
          <Col
            sm={12}
            md={6}
            style={{ paddingBottom: '10px' }}
          >
            <CardToogle
              title="Quick rating"
              defaultShow={true}
              className="card card-warning"
            >
              <Card.Body>
                <Row>
                  <Col xs={4}>
                    <Image
                      style={{ width: '100%' }}
                      src={ImgE}
                      className="c-icon c-icon-custom-size c-sidebar-brand-full"
                    />
                  </Col>
                  <Col xs={8}>
                    <Row>
                      <Col xs={6} className="rating">
                        <div className="title">Scale</div>
                        <div className="body">
                          Extremly Large
                        </div>
                        <div className="more">
                          Show more
                        </div>
                      </Col>
                      <Col xs={6} className="rating">
                        <div className="title">
                          Credit limit
                        </div>
                        <div className="body">
                          USD 50,000
                        </div>
                        <div className="more">
                          Show more
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={6} className="rating">
                        <div className="title">
                          Net Sales ranking{' '}
                        </div>
                        <div className="body">#26</div>
                        <div className="more">
                          Industry name level 4
                        </div>
                      </Col>
                      <Col xs={6} className="rating">
                        <div className="title">
                          Market share
                        </div>
                        <div className="body">0.56%</div>
                        <div className="more">2021</div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Col xs={6}>
                    <div className="rating-detail">
                      18 Years in business
                    </div>
                  </Col>
                  <Col xs={6}>
                    <div className="rating-detail">
                      900 Employees in 2021
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <p className="line-detail">
                      Founded since 2008, the subject
                      specializes in manufacturing
                      electronic components for mobile
                      phones. Its products are supplied to
                      popular mobile phone manufacturers
                      like SAMSUNG... show more Công ty
                      không có bình luận thì show Detail
                      business line
                    </p>
                  </Col>
                </Row>
              </Card.Body>
            </CardToogle>
          </Col>
        </Form.Row>

        <Form.Row>
          <Col
            sm={12}
            md={6}
            style={{ paddingBottom: '10px' }}
          >
            <CardToogle
              title="Competitors"
              defaultShow={true}
              className="card card-danger"
              loading={this.state.loadingCompetitor}
            >
              <Col xs={12}>
                <ul
                  className="todo-list ui-sortable"
                  data-widget="todo-list"
                >
                  {this.state.listCompetitor.length > 0 ? (
                    this.state.listCompetitor.map(
                      (item, index) => {
                        return (
                          <li key={index}>
                            <span className="text">
                              {index + 1}. {item.name}
                            </span>
                            <small className="badge badge-warning">
                              {' '}
                              {currencyFormat(item.value)}
                            </small>
                          </li>
                        );
                      }
                    )
                  ) : (
                    <label className="no-record">
                      No record
                    </label>
                  )}
                </ul>
              </Col>
            </CardToogle>
          </Col>
          <Col
            sm={12}
            md={6}
            style={{ paddingBottom: '10px' }}
          >
            <CardToogle
              title="Contact address"
              defaultShow={true}
              className="card card-warning contact"
            >
              <Col xs={12}>
                <Form.Row>
                  <div className="address">
                    <span
                      className="iconify"
                      data-icon="mdi:map-marker-outline"
                    ></span>
                    No. 164, Khuat Duy Tien Street, Thanh
                    Xuan District, Hanoi, Vietnam
                  </div>
                </Form.Row>
                <Form.Row>
                  <Col xs={6}>
                    <div className="phone">
                      <span
                        className="iconify"
                        data-icon="mdi:phone"
                      ></span>
                      (84-222) 3617 501{' '}
                    </div>
                  </Col>
                  <Col xs={6}>
                    <div className="mail">
                      <span
                        className="iconify"
                        data-icon="mdi:email-check-outline"
                      ></span>
                      tieeingvietnam@gmail.com{' '}
                    </div>
                  </Col>
                </Form.Row>
                <div
                  style={{
                    height: 155,
                    width: '100%',
                    marginTop: 5,
                  }}
                >
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: '' }}
                    defaultCenter={defaultProps.center}
                    defaultZoom={defaultProps.zoom}
                  >
                    <AnyReactComponent
                      lat={20.9987400233186}
                      lng={105.79783290624619}
                      text="My Marker"
                    />
                  </GoogleMapReact>
                </div>
              </Col>
            </CardToogle>
          </Col>
        </Form.Row>
      </div>
    );
  }
}

export { BusinessInfo };
