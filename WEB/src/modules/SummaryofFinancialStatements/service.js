
import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class SummaryofFinancialStatementsService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-report' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
    getIncomeStatement(businessId, year, quater, type) {
      
    let url = 'api/vcis-report/incomestatement?businessId=' + businessId + '&year=' + year;
    if (quater) {
      url += '&q=' + quater;
    } else {
      url += '&q='
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type='
    }
    return http.get(url);
    }
}
const summaryofFinancialStatementsService = new SummaryofFinancialStatementsService();

export { summaryofFinancialStatementsService, SummaryofFinancialStatementsService };


