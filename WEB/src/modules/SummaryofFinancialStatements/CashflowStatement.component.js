import React from 'react';
import { Table } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import './style.scss';
import { summaryOFSService } from '../businessSummaryOFS/service';
import { currencyFormat, getDistanceToTop } from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class CashflowStatement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loadingInDirect: false,

            data: [],
            header: {},
            numyear: null,
            title: 'Items',

            headerInDirect: {},
            dataInDirect: [],
            numyearInDirect: null,
            titleInDirect: 'Items',

            id: props.id,
            type: props.type,
            year: props.year,
            quarter: props.quarter,
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.setState({ heightLoading: getDistanceToTop('#cashflow-statement') });
        this.getCashflowDirect();
        this.getCashflowInDirect();
    }
    componentWillUnmount() {

    }
    componentDidUpdate() {
        if (this.props.type !== this.state.type) {
            this.setState({ type: this.props.type }, () => {
                this.getCashflowDirect();
                this.getCashflowInDirect();
            })
        }
        if (this.props.year !== this.state.year) {
            this.setState({ year: this.props.year }, () => {
                this.getCashflowDirect();
                this.getCashflowInDirect();
            })
        }
        if (this.props.quarter !== this.state.quarter) {
            this.setState({ quarter: this.props.quarter }, () => {
                this.getCashflowDirect();
                this.getCashflowInDirect();
            })
        }
    }
    getCashflowDirect() {
        let { id, type, year, quarter } = this.state;
        this.setState({ loading: true });
        if (id && year) {
            this.subscriptions['getCashflowDirect'] = summaryOFSService.getCashflowDirect(id, year, quarter, type).subscribe(res => {
                let header = res.header || {};
                let hasdata = res.data.some(x => x.year1 !== null || x.year2 !== null || x.year3 !== null || x.year4 !== null || x.year5 !== null);

                this.setState({
                    loading: false,
                    data: hasdata ? res.data : [],
                    header: header,
                    numyear: header.numyear,
                    title: header.type === '2' ? 'Quarter over Quarter' : header.type === '3' ? 'Quarter on Quarter' : header.type === '4' ? 'Half-year' : 'Items',
                });
            },
                err => {
                    console.log(err);
                    this.setState({ loading: false });
                }
            );
        }
    }

    getCashflowInDirect() {
        let { id, type, year, quarter } = this.state;
        this.setState({ loadingInDirect: true });
        if (id && year) {
            this.subscriptions['getCashflowInDirect'] = summaryOFSService.getCashflowInDirect(id, year, quarter, type).subscribe(res => {

                let headerInDirect = res.header || {}
                let hasdata = res.data.some(x => x.year1 !== null || x.year2 !== null || x.year3 !== null || x.year4 !== null || x.year5 !== null);

                this.setState({
                    loadingInDirect: false,
                    dataInDirect: hasdata ? res.data : [],
                    headerInDirect: headerInDirect,
                    numyearInDirect: headerInDirect.numyear,
                    titleInDirect: headerInDirect.type === '2' ? 'Quarter over Quarter' : headerInDirect.type === '3' ? 'Quarter on Quarter' : headerInDirect.type === '4' ? 'Half-year' : 'Items',
                });
            },
                err => {
                    console.log(err);
                    this.setState({ loadingInDirect: false });
                }
            );
        }
    }


    render() {
        const { numyear, header, title, data, headerInDirect,
            dataInDirect, numyearInDirect, titleInDirect,
            loading, loadingInDirect } = this.state;
        return (
            <div className="content content-cashflow-statement" id='cashflow-statement'>
                {
                    (loading || loadingInDirect) ?
                    <CCol xs="12" lg="12" style={{ height: this.state.heightLoading }}>
                        <RLoading loading={true} />
                    </CCol> :
                    <React.Fragment>
                        {
                            data.length > 0 &&
                            <CRow>
                                <CCol xs="12" lg="12">
                                    <Table bordered className="table-balance-sheet">
                                        <tbody>
                                            <tr>
                                                <td colSpan={numyear + 1} className="table-title-center">CASH FLOW STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td colSpan={numyear + 1} className="table-title-center">(Indirect method)</td>
                                            </tr>
                                            <tr className="tr-header">
                                                <td className="bold">{title}</td>
                                                <td className="bold text-center">{header.year1}</td>
                                                {
                                                    numyear >= 2 && <td className="bold text-center">{header.year2}</td>
                                                }
                                                {
                                                    numyear >= 3 && <td className="bold text-center">{header.year3}</td>
                                                }
                                                {
                                                    numyear >= 4 && <td className="bold text-center">{header.year4}</td>
                                                }
                                                {
                                                    numyear >= 5 && <td className="bold text-center">{header.year5}</td>
                                                }
                                            </tr>

                                            {
                                                data.map((x, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td className={(x.level === 1 || x.level === 2) ? 'bold name' : 'name'}>{x.name}</td>
                                                            <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year1)}</td>
                                                            {
                                                                numyear >= 2 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year2)}</td>
                                                            }
                                                            {
                                                                numyear >= 3 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year3)}</td>
                                                            }
                                                            {
                                                                numyear >= 4 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year4)}</td>
                                                            }
                                                            {
                                                                numyear >= 5 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year5)}</td>
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }

                                        </tbody>
                                    </Table>
                                </CCol>
                            </CRow>
                        }

                        {
                            dataInDirect.length > 0 &&
                            <CRow>
                                <CCol xs="12" lg="12">
                                    <Table bordered className="table-balance-sheet">
                                        <tbody>
                                            <tr>
                                                <td colSpan={numyearInDirect + 1} className="table-title-center">CASH FLOW STATEMENT</td>
                                            </tr>
                                            <tr>
                                                <td colSpan={numyearInDirect + 1} className="table-title-center">(Indirect method)</td>
                                            </tr>
                                            <tr className="tr-header">
                                                <td className="bold">{titleInDirect}</td>
                                                <td className="bold text-center">{headerInDirect.year1}</td>
                                                {
                                                    numyearInDirect >= 2 && <td className="bold text-center">{headerInDirect.year2}</td>
                                                }
                                                {
                                                    numyearInDirect >= 3 && <td className="bold text-center">{headerInDirect.year3}</td>
                                                }
                                                {
                                                    numyearInDirect >= 4 && <td className="bold text-center">{headerInDirect.year4}</td>
                                                }
                                                {
                                                    numyearInDirect >= 5 && <td className="bold text-center">{headerInDirect.year5}</td>
                                                }
                                            </tr>

                                            {
                                                dataInDirect.map((x, i) => {
                                                    return (
                                                        <tr key={i}>
                                                            <td className={(x.level === 1 || x.level === 2) ? 'bold name' : 'name'}>{x.name}</td>
                                                            <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year1)}</td>
                                                            {
                                                                numyear >= 2 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year2)}</td>
                                                            }
                                                            {
                                                                numyear >= 3 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year3)}</td>
                                                            }
                                                            {
                                                                numyear >= 4 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year4)}</td>
                                                            }
                                                            {
                                                                numyear >= 5 && <td className={(x.level === 1 || x.level === 2) ? 'bold text-right' : 'text-right'}>{currencyFormat(x.year5)}</td>
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }

                                        </tbody>
                                    </Table>
                                </CCol>
                            </CRow>
                        }
                    </React.Fragment>
                }
            </div>
        )
    }
}

export { CashflowStatement };