import React from 'react';
import { Table } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import './style.scss';
import { summaryOFSService } from '../businessSummaryOFS/service';
import { currencyFormat, getDistanceToTop } from 'src/shared/utils';
import { RLoading } from 'src/shared/component';
class BalanceSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingBalanceSheet: false,
            data: [],
            header: {},
            numyear: null,
            title: 'FYE',
            yoy: 'yoy +/-',

            id: props.id,
            type: props.type,
            year: props.year,
            quarter: props.quarter,
            heightLoading: null
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.setState({ heightLoading: getDistanceToTop('#balancesheet') });
        this.getBalanceSheet();
    }
   
    componentDidUpdate() {
        if (this.props.type !== this.state.type) {
            this.setState({ type: this.props.type }, () => {
                this.getBalanceSheet();
            })
        }
        if (this.props.year !== this.state.year) {
            this.setState({ year: this.props.year }, () => {
                this.getBalanceSheet();
            })
        }
        if (this.props.quarter !== this.state.quarter) {
            this.setState({ quarter: this.props.quarter }, () => {
                this.getBalanceSheet();
            })
        }
    }
    getBalanceSheet() {
        let { id, type, year, quarter } = this.state;
        this.setState({ loadingBalanceSheet: true });
        if (id) {
            this.subscriptions['getBalanceSheet'] = summaryOFSService.getBalanceSheet(id, year, quarter, type).subscribe(res => {
                res.data.forEach(x => {
                    x.percent = x.percent ? (x.percent * 100).toFixed(2) + ' %' : '0 %';
                    x.percent2 = x.percent2 ? (x.percent2 * 100).toFixed(2) + ' %' : '0 %';
                    x.percent3 = x.percent3 ? (x.percent3 * 100).toFixed(2) + ' %' : '0 %';
                    x.addPercent1 = x.addPercent1 ? (x.addPercent1 * 100).toFixed(2) + ' %' : '0 %';
                    x.addPercent2 = x.addPercent2 ? (x.addPercent2 * 100).toFixed(2) + ' %' : '0 %';
                    x.addPercent3 = x.addPercent3 ? (x.addPercent3 * 100).toFixed(2) + ' %' : '0 %';
                });
                let header = res.header || {}
                this.setState({
                    loadingBalanceSheet: false,
                    data: res.data,
                    header: header,
                    numyear: header.numyear,
                    title: header.type === '2' ? 'Quarter over Quarter' : header.type === '3' ? 'Quarter on Quarter' : header.type === '4' ? 'Half-year' : 'FYE',
                    yoy: header.type !== '1' ? '+/-' : 'yoy +/-'
                });
            },
                err => {
                    console.log(err);
                    this.setState({ loadingBalanceSheet: false });
                }
            );
        }
    }

    render() {
        const { numyear, header, title, yoy, loadingBalanceSheet } = this.state;

        return (
            <div className="content content-balancesheet" id='balancesheet'>
                <CRow>
                    {
                        loadingBalanceSheet ?
                            <CCol xs="12" lg="12"   style={{ height: this.state.heightLoading }}>
                                <RLoading loading={true} />
                            </CCol> :
                            <CCol xs="12" lg="12">

                                <Table bordered className="table-balance-sheet">
                                    <tbody>
                                        <tr>
                                            <td colSpan="7" className="td-unit"><b>Unit: One VND</b></td>
                                        </tr>
                                        <tr className="tbl-header">
                                            <td className="bold">{title}</td>
                                            <td colSpan={numyear > 1 ? 2 : 1} className="text-center">{header.year1}</td>
                                            {
                                                numyear >= 2 &&
                                                <td colSpan={numyear > 2 ? 2 : 1} className="text-center">{header.year2}</td>
                                            }
                                            {
                                                numyear >= 3 &&
                                                <td colSpan="2" className="text-center">{header.year3}</td>
                                            }

                                        </tr>
                                        <tr className="tbl-header">
                                            <td className="bold">Consolidation status</td>
                                            <td colSpan={numyear > 1 ? 2 : 1} className="text-center">{header.consolidatedStatus1}</td>
                                            {
                                                numyear >= 2 &&
                                                <td colSpan={numyear > 2 ? 2 : 1} className="text-center">{header.consolidatedStatus2}</td>
                                            }
                                            {
                                                numyear >= 3 &&
                                                <td colSpan="2" className="text-center">{header.consolidatedStatus3}</td>
                                            }

                                        </tr>
                                        <tr className="tbl-header">
                                            <td className="bold">Audit status</td>
                                            <td colSpan={numyear > 1 ? 2 : 1} className="text-center">{header.auditStatus1}</td>
                                            {
                                                numyear >= 2 &&
                                                <td colSpan={numyear > 2 ? 2 : 1} className="text-center">{header.auditStatus2}</td>
                                            }
                                            {
                                                numyear >= 3 &&
                                                <td colSpan="2" className="text-center">{header.auditStatus3}</td>
                                            }

                                        </tr>
                                        <tr className="tbl-header">
                                            <td className="bold">Source</td>
                                            <td colSpan={numyear > 1 ? 2 : 1} className="text-center">{header.source1}</td>
                                            {
                                                numyear >= 2 &&
                                                <td colSpan={numyear > 2 ? 2 : 1} className="text-center">{header.source2}</td>
                                            }
                                            {
                                                numyear >= 3 &&
                                                <td colSpan="2" className="text-center">{header.source3}</td>
                                            }

                                        </tr>
                                        <tr className="tbl-header">
                                            <td className="bold">Items</td>
                                            <td className="bold text-center text-italic">Amount</td>
                                            {
                                                numyear >= 2 &&
                                                <>
                                                    <td className="bold text-center text-italic">{yoy}</td>
                                                    <td className="bold text-center text-italic">Amount</td>
                                                </>
                                            }
                                            {
                                                numyear >= 3 &&
                                                <>
                                                    <td className="bold text-center text-italic">{yoy}</td>
                                                    <td className="bold text-center text-italic">Amount</td>
                                                    <td className="bold text-center text-italic">{yoy}</td>
                                                </>
                                            }

                                        </tr>
                                        {
                                            this.state.data.map((x, i) => {
                                                return (
                                                    <React.Fragment key={i}>
                                                        <tr>
                                                            <td className={x.isBold ? 'bold name' : 'name'} rowSpan="2">{x.name}</td>
                                                            <td className={x.isBold ? 'bold value' : 'value'}>{currencyFormat(x.year1)}</td>
                                                            {
                                                                numyear >= 2 &&
                                                                <>
                                                                    <td className={x.isBold ? 'bold value' : 'value'} rowSpan="2">{x.percent}</td>
                                                                    <td className={x.isBold ? 'bold value' : 'value'} >{currencyFormat(x.year2)}</td>
                                                                </>
                                                            }
                                                            {
                                                                numyear >= 3 &&
                                                                <>
                                                                    <td className={x.isBold ? 'bold value' : 'value'} rowSpan="2">{x.percent2}</td>
                                                                    <td className={x.isBold ? 'bold value' : 'value'}>{currencyFormat(x.year3)}</td>
                                                                    <td className={x.isBold ? 'bold value' : 'value'} rowSpan="2">{x.percent3}</td>
                                                                </>
                                                            }

                                                        </tr>
                                                        <tr>
                                                            <td className={x.isBold ? 'bold value addper' : 'value addper'}>{x.addPercent1}</td>
                                                            {
                                                                numyear >= 2 &&
                                                                <td className={x.isBold ? 'bold value addper' : 'value addper'}>{x.addPercent2}</td>
                                                            }
                                                            {
                                                                numyear >= 3 &&
                                                                <td className={x.isBold ? 'bold value addper' : 'value addper'}>{x.addPercent3}</td>
                                                            }
                                                        </tr>
                                                    </React.Fragment>
                                                )
                                            })
                                        }

                                    </tbody>
                                </Table>
                            </CCol>
                    }


                </CRow>
            </div>
        )
    }
}

export { BalanceSheet };