
import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';

class ShareholderService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-shareholders' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getShareholder(businessId) {
        return http.get('api/vcis-business-shareholders?businessId='+businessId);
    }
}
const shareholderService = new ShareholderService();

export { shareholderService, ShareholderService };
