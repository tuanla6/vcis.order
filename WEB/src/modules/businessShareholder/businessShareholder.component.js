import React from 'react';
import {
    CCol,
    CDataTable,
    CRow
} from '@coreui/react';
import {
    Chart,
    Interval,
    Tooltip,
    Axis,
    Coordinate,
    Interaction
    // getTheme
} from 'bizcharts';

import './style.scss';
import { shareholderService } from './service';
import { RCard } from 'src/shared/component';
import { formatDate, currencyFormat } from 'src/shared/utils';
const parentFields = [
    { key: 'name', label: 'Name', _style: { width: '20%' } },
    { key: 'date', label: 'Incorporation date', _style: { width: '12%' } },
    { key: 'idNumber', label: 'Business ID/ Person ID', _style: { width: '12%' } },
    { key: 'sharedCapital', label: 'Capital', _style: { width: '15%' } },
    { key: 'sharedPercentage', label: 'Ownership percentage', _style: { width: '15%' } },
    'address'
];
let shareholderFields = [
    { key: 'name', label: 'Name', _style: { width: '20%' } },
    { key: 'date', label: 'Incorporation date/ DOB', _style: { width: '12%' } },
    { key: 'idNumber', label: 'Business ID/ Person ID', _style: { width: '12%' } },
    { key: 'sharedCapital', label: 'Capital', _style: { width: '15%' } },
    { key: 'sharedPercentage', label: 'Ownership percentage', _style: { width: '15%' } },
    'address'
];

const cols = {
    percent: {
        formatter: val => {
            val = val * 100 + '%';
            return val;
        },
    },
};


class BusinessShareholder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ParentCompany: [],
            Shareholders: [],
            dataTyLeCD: [],
            id: this.props.id,
            isMajor: false,
            isPie: false,
            isBar: false,
            registeredCapital: this.props.registeredCapital,
            noItemMessage: 'Loading... ',
            noItemMessageShareholder: 'Loading... ',
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.getShareholder();
    }
    componentDidUpdate() {
        if (this.props.registeredCapital !== this.state.registeredCapital) {
            this.setState({ registeredCapital: this.props.registeredCapital })
        }
    }
    componentWillUnmount() {

    }

    getShareholder() {
        let id = this.state.id;
        this.setState({ loadingShareholder: true });
        this.subscriptions['getShareholder'] = shareholderService.getShareholder(id).subscribe(res => {
            let sum = res.reduce((a, v) => a = a + v.sharedPercentage, 0);
            let business = res.map(x => x.type).every(v => v === 'Business');
            let person = res.map(x => x.type).every(v => v === 'Person');
            if (business) {
                shareholderFields = [
                    { key: 'name', label: 'Name', _style: { width: '20%' } },
                    { key: 'date', label: 'Incorporation date', _style: { width: '12%' } },
                    { key: 'idNumber', label: 'Business ID', _style: { width: '12%' } },
                    { key: 'sharedCapital', label: 'Capital', _style: { width: '15%' } },
                    { key: 'sharedPercentage', label: 'Ownership percentage', _style: { width: '15%' } },
                    'address'
                ];
            }
            if (person) {
                shareholderFields = [
                    { key: 'name', label: 'Name', _style: { width: '20%' } },
                    { key: 'date', label: 'DOB', _style: { width: '12%' } },
                    { key: 'idNumber', label: 'Person ID', _style: { width: '12%' } },
                    { key: 'sharedCapital', label: 'Capital', _style: { width: '15%' } },
                    { key: 'sharedPercentage', label: 'Ownership percentage', _style: { width: '15%' } },
                    'address'
                ];
            }

            if (sum < 100) {
                this.setState({ isMajor: true });
            }
            if (sum === 100) {
                this.setState({ isPie: true });
            }

            res.forEach(x => {
                x.percent = x.sharedPercentage ? x.sharedPercentage / 100 : 0
                x.capital = x.sharedCapital ? x.sharedCapital : 0;
                x.date = formatDate(x.date);
                x.sharedPercentage = x.sharedPercentage ? x.sharedPercentage + '%' : '';
                x.idNumber = x.idNumber ? x.idNumber : '';
                x.name = x.name ? x.name : '';
                x.address = x.address ? x.address : '';
                x.sharedCapital = x.sharedCapital ? currencyFormat(x.sharedCapital) + ' ' + x.currency : '';
            });

            let totalCapital = res.reduce((a, v) => a = a + v.capital, 0);
            if (totalCapital === this.state.registeredCapital) {
                this.setState({ isBar: true });
            }

            const dataTyLeCD = res.filter(x => x.percent > 0);
            const ParentCompany = res.filter(x => x.percent >= 51 && x.type !== 'Person',);
            this.setState({
                ParentCompany: ParentCompany,
                Shareholders: res,
                dataTyLeCD: dataTyLeCD,
                dataGiaTriCP: res,
                loadingShareholder: false,
                noItemMessageShareholder: 'The company does not have Shareholder',
                noItemMessage: 'The company does not have Parent'
            })
        }, err => {
            console.log(err);
            this.setState({
                loadingShareholder: false
            })
        });
    }


    render() {

        return (
            <div className="content-business-group">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Parent company" className="card card-danger" loading={this.state.loadingParentCompany}>
                            <CDataTable
                                items={this.state.ParentCompany}
                                fields={parentFields}
                                pagination
                                itemsPerPage={15}
                                hover
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Shareholder list" className="card card-danger" loading={this.state.loadingShareholder}>
                            {
                                this.state.isMajor &&
                                <CRow>
                                    <CCol xs="12" className="remark">
                                        * Remark: Major shareholder
                                    </CCol>
                                </CRow>
                            }
                            <CDataTable
                                items={this.state.Shareholders}
                                fields={shareholderFields}
                                pagination
                                itemsPerPage={15}
                                hover
                                noItemsView={{ noItems: this.state.noItemMessageShareholder }}
                            />
                            <CRow style={{ marginTop: 10 }}>
                                {
                                    this.state.isPie &&
                                    <CCol xs="12" lg={this.state.isBar ? 6 : 12}>
                                        <Chart height={300} data={this.state.dataTyLeCD} scale={cols} autoFit>
                                            <Coordinate type="theta" radius={0.75} />
                                            <Tooltip showTitle={false} />
                                            <Axis visible={false} />
                                            <Interval
                                                position="percent"
                                                adjust="stack"
                                                color="name"
                                                style={{
                                                    lineWidth: 1,
                                                    stroke: '#fff',
                                                }}
                                                label={['count', {
                                                    content: (data) => {
                                                        return `${data.name}: ${(data.percent*100).toFixed(2)}%`;
                                                    },
                                                }]}
                                            />
                                            <Interaction type='element-single-selected' />
                                        </Chart>
                                        <div className="chart-name">Biểu đồ tỷ lệ cổ đông</div>
                                    </CCol>
                                }
                                {
                                    this.state.isBar &&
                                    <CCol xs="12" lg="6" style={{ paddingRight: 30 }}>
                                        <Chart height={300} data={this.state.dataGiaTriCP} autoFit>
                                            <Coordinate transpose />
                                            <Axis
                                                name="capital"
                                                label={{ formatter: val => currencyFormat(val) }}
                                            />
                                            <Interval position="name*capital" tooltip={
                                                [
                                                    'name*capital*currency',
                                                    (x, y, z) => ({
                                                        name: 'Share capital',
                                                        value: currencyFormat(y) + ' ' + z,
                                                    }),
                                                ]
                                            } />
                                        </Chart>
                                        <div className="chart-name">Biểu đồ giá trị cổ phần</div>
                                    </CCol>
                                }
                            </CRow>

                        </RCard>
                    </CCol>

                </CRow>
            </div>
        )
    }
}

export { BusinessShareholder };