import React from 'react';
import { CCol, CRow } from '@coreui/react';
import { Chart, Interval, Axis } from 'bizcharts';

import './style.scss';
import { negativePaymentService } from './service';
import { RCard } from 'src/shared/component';
import { currencyFormat } from 'src/shared/utils';
class BusinessNP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      outstandingTitle: '',
      overduePaymentTitle: '',
      otherTitle: '',
      outstandingData: [],
      socialinsuranceData: [],
      supplierData: [],
      id: this.props.id,
      loadingNegativePayment: false,
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.getNegativePayment();
  }

  componentWillUnmount() {}

  getNegativePayment() {
    let id = this.state.id;
    this.setState({ loadingNegativePayment: true });
    if (id) {
      this.subscriptions['getNegativePayment'] = negativePaymentService
        .getNegativePayment(id)
        .subscribe(
          (res) => {
            let itemOutstanding = res.find((x) => x.type === 1);
            if (itemOutstanding !== undefined && itemOutstanding !== 'undefined') {
              this.setState({
                outstandingData: itemOutstanding.listData,
                outstandingTitle: itemOutstanding.title,
              });
            }
            let itemSocialinsurance = res.find((x) => x.type === 2);
            if (itemSocialinsurance !== undefined && itemSocialinsurance !== 'undefined') {
              this.setState({
                socialinsuranceData: itemSocialinsurance.listData,
                overduePaymentTitle: itemSocialinsurance.title,
              });
            }
            let itemsupplier = res.find((x) => x.type === 3);
            if (itemsupplier !== undefined && itemsupplier !== 'undefined') {
              this.setState({
                supplierData: itemsupplier.listData,
                otherTitle: itemsupplier.title,
              });
            }
            this.setState({ loadingNegativePayment: false });
          },
          (err) => {
            console.log(err);
            this.setState({ loadingNegativePayment: false });
          }
        );
    }
    this.setState({
      outstandingData: [],
      SocialinsuranceData: [],
      supplierData: [],
    });
  }

  render() {
    return (
      <div className="content-business-pay">
        <CRow>
          <CCol xs="12" lg="6">
            {this.state.outstandingTitle === '' ? (
              <RCard
                loading={this.state.loadingNegativePayment}
                title="Total overdue payment to Social insurance"
                className="card card-danger"
              >
                <span>No record</span>
              </RCard>
            ) : (
              <RCard
                loading={this.state.loadingNegativePayment}
                title={this.state.outstandingTitle}
                className="card card-danger"
              >
                <Chart
                  height={400}
                  autoFit
                  data={this.state.outstandingData}
                  interactions={['active-region']}
                >
                  {/* <Interval position="dateShow*value" opacity={0.3} fill={{
                                        fillOpacity: 0.8
                                    }} style={['sales', (val) => {
                                        return {
                                            lineWidth: 1,
                                            strokeOpacity: 1,
                                            fillOpacity: 0.3,
                                            opacity: 0.65,
                                        };
                                    }]} />
                                    <Tooltip shared /> */}
                  <Axis name="value" label={{ formatter: (val) => currencyFormat(val) }} />
                  <Interval
                    position="dateShow*value"
                    opacity={0.3}
                    size={15}
                    tooltip={[
                      'dateShow*value',
                      (x, y) => ({
                        name: 'Value',
                        value: currencyFormat(y),
                      }),
                    ]}
                  />
                </Chart>
              </RCard>
            )}
          </CCol>
          <CCol xs="12" lg="6">
            {this.state.overduePaymentTitle === '' ? (
              <RCard
                loading={this.state.loadingNegativePayment}
                title="Total overdue payment to Social insurance"
                className="card card-danger"
              >
                <span>No record</span>
              </RCard>
            ) : (
              <RCard
                loading={this.state.loadingNegativePayment}
                title={this.state.overduePaymentTitle}
                className="card card-danger"
              >
                <Chart
                  height={400}
                  autoFit
                  data={this.state.socialinsuranceData}
                  interactions={['active-region']}
                >
                  <Axis name="value" label={{ formatter: (val) => currencyFormat(val) }} />
                  <Interval
                    position="dateShow*value"
                    size={15}
                    opacity={0.3}
                    tooltip={[
                      'dateShow*value',
                      (x, y) => ({
                        name: 'Value',
                        value: currencyFormat(y),
                      }),
                    ]}
                  />
                </Chart>
              </RCard>
            )}
          </CCol>
        </CRow>
        <CRow>
          <CCol xs="12" lg="12">
            {this.state.otherTitle === '' ? (
              <RCard
                loading={this.state.loadingNegativePayment}
                title="Total overdue payment to supplier and other"
                className="card card-danger"
              >
                <span>No record</span>
              </RCard>
            ) : (
              <RCard
                loading={this.state.loadingNegativePayment}
                title={this.state.otherTitle}
                className="card card-danger"
              >
                <Chart
                  height={400}
                  autoFit
                  data={this.state.supplierData}
                  interactions={['active-region']}
                >
                  <Axis name="value" label={{ formatter: (val) => currencyFormat(val) }} />
                  <Interval
                    position="dateShow*value"
                    size={15}
                    opacity={0.3}
                    tooltip={[
                      'dateShow*value',
                      (x, y) => ({
                        name: 'Value',
                        value: currencyFormat(y),
                      }),
                    ]}
                  />
                </Chart>
              </RCard>
            )}
          </CCol>
        </CRow>
      </div>
    );
  }
}

export { BusinessNP };
