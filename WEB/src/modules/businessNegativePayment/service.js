import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class NegativePaymentService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-negativepayment' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
    getNegativePayment(id) {
        let url = 'api/vcis-business-negativepayment/getnegativepayment?id=' + id;
        return http.get(url);
    }
}
const negativePaymentService = new NegativePaymentService();
export { negativePaymentService, NegativePaymentService };