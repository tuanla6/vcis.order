import React from 'react';
import PropTypes from 'prop-types';
import {
    CCard,
    CCol,
    CRow,
    // CPagination
} from '@coreui/react';
import { Row, Col, Table } from 'react-bootstrap';
import { Chart, Line, Point, Tooltip, Interaction, Axis, Area, Coordinate } from "bizcharts";
import DataSet from '@antv/data-set';
import './style.scss';

class LiquidityRatios extends React.Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    };
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    year: "2020",
                    reportName: "Current Ratio",
                    value: 1.15,
                },
                {
                    year: "2020",
                    reportName: "Quick Ratio",
                    value: 1.05,
                },
                {
                    year: "2020",
                    reportName: "Cash Ratio",
                    value: 0.77,
                },
                {
                    year: "2019",
                    reportName: "Current Ratio",
                    value: 1.18,
                },
                {
                    year: "2019",
                    reportName: "Quick Ratio",
                    value: 1.04,
                },
                {
                    year: "2019",
                    reportName: "Cash Ratio",
                    value: 0.63,
                },
                {
                    year: "2018",
                    reportName: "Current Ratio",
                    value: 1.27,
                },
                {
                    year: "2018",
                    reportName: "Quick Ratio",
                    value: 1.10,
                },
                {
                    year: "2018",
                    reportName: "Cash Ratio",
                    value: 0.66,
                },
                {
                    year: "2017",
                    reportName: "Current Ratio",
                    value: 1.45,
                },
                {
                    year: "2017",
                    reportName: "Quick Ratio",
                    value: 1.26,
                },
                {
                    year: "2017",
                    reportName: "Cash Ratio",
                    value: 0.71,
                },
                {
                    year: "2016",
                    reportName: "Current Ratio",
                    value: 1.26,
                },
                {
                    year: "2016",
                    reportName: "Quick Ratio",
                    value: 0.93,
                },
                {
                    year: "2016",
                    reportName: "Cash Ratio",
                    value: 0.54,
                }
            ],
            data1: [
                { item: 'Current Ratio', CurrentRatio: 70, QuickRatio: 30, CashRatio: 50 },
                { item: 'Quick Ratio', CurrentRatio: 60, QuickRatio: 80, CashRatio: 40 },
                { item: 'Cash Ratio', CurrentRatio: 50, QuickRatio: 60, CashRatio: 30 },
            ],
            dv: null
        };
        const { DataView } = DataSet;
        this.state.dv = new DataView().source(this.state.data1);
        this.state.dv.transform({
            type: 'fold',
            fields: ['CurrentRatio', 'QuickRatio', 'CashRatio'],
            key: 'user',
            value: 'score',
        });

    }

    componentDidMount() {
    }
    componentWillUnmount() {

    }

    render() {
        return (
            <CRow className="ratio">
                <CCol xl={12}>
                    <CCard className="p30">
                        <Row>
                            <Col>
                                <Chart scale={{ value: { type: 'linear-strict' } }} autoFit height={220} data={this.state.data}>
                                    <Line shape="smooth" position="year*value" color="reportName" />
                                    <Point position="year*value" color="reportName" />
                                    <Tooltip shared={true} showCrosshairs />
                                    <Interaction type="legend-filter" />
                                </Chart>
                            </Col>

                            <Col>
                                <Chart height={220}
                                    data={this.state.dv.rows}
                                    autoFit 
                                    // scale={{
                                    //     score: {
                                    //         min: 0,
                                    //         max: 80,
                                    //     }
                                    // }}
                                    interactions={['legend-highlight']}
                                >
                                    <Coordinate type="polar" radius={0.8} />
                                    <Tooltip shared />
                                    <Point
                                        position="item*score"
                                        color="user"
                                        shape="circle"
                                    />
                                    <Line
                                        position="item*score"
                                        color="user"
                                        size="2"
                                    />
                                    <Area
                                        position="item*score"
                                        color="user"
                                    />
                                    <Axis name="score" grid={{ line: { type: 'line' } }} />
                                    <Axis name="item" line={false} />
                                </Chart>
                            </Col>
                            <Col>
                                <Chart scale={{ value: { type: 'linear-strict' } }} autoFit height={220} data={this.state.data}>
                                    <Line shape="smooth" position="year*value" color="reportName" />
                                    <Point position="year*value" color="reportName" />
                                    <Tooltip shared={true} showCrosshairs />
                                    <Interaction type="legend-filter" />
                                </Chart>
                            </Col>
                        </Row>
                        <Row className="content-data-detail">
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th className="text-center">Description</th>
                                        <th className="text-center">Industry Average</th>
                                        <th className="text-center">2021</th>
                                        <th className="text-center">2020</th>
                                        <th className="text-center">2019</th>
                                        <th className="text-center">2018</th>
                                        <th className="text-center">2017</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span>
                                                Current Ratio
                                            </span>
                                            <p className="data-comment">
                                                (Current Assets/Current Liabilities)
                                            </p>
                                        </td>
                                        <td className='text-right'>1.86</td>
                                        <td className='text-right'>1.19</td>
                                        <td className='text-right'>1.15</td>
                                        <td className='text-right'>1.18</td>
                                        <td className='text-right'>1.27</td>
                                        <td className='text-right'>1.45</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                Current Ratio
                                            </span>
                                            <p className="data-comment">
                                                (Current Assets/Current Liabilities)
                                            </p>
                                        </td>
                                        <td className='text-right'>1.86</td>
                                        <td className='text-right'>1.19</td>
                                        <td className='text-right'>1.15</td>
                                        <td className='text-right'>1.18</td>
                                        <td className='text-right'>1.27</td>
                                        <td className='text-right'>1.45</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                Current Ratio
                                            </span>
                                            <p className="data-comment">
                                                (Current Assets/Current Liabilities)
                                            </p>
                                        </td>
                                        <td className='text-right'>1.86</td>
                                        <td className='text-right'>1.19</td>
                                        <td className='text-right'>1.15</td>
                                        <td className='text-right'>1.18</td>
                                        <td className='text-right'>1.27</td>
                                        <td className='text-right'>1.45</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>
                                                Current Ratio
                                            </span>
                                            <p className="data-comment">
                                                (Current Assets/Current Liabilities)
                                            </p>
                                        </td>
                                        <td className='text-right'>1.86</td>
                                        <td className='text-right'>1.19</td>
                                        <td className='text-right'>1.15</td>
                                        <td className='text-right'>1.18</td>
                                        <td className='text-right'>1.27</td>
                                        <td className='text-right'>1.45</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Row>
                    </CCard>
                </CCol>
            </CRow>
        )
    }
}

export { LiquidityRatios };