import React from 'react';
import PropTypes from 'prop-types';
import {
  CCard,
  CCol,
  CRow,
  // CPagination
} from '@coreui/react';
import { Button } from 'react-bootstrap';
import { Row, Col, Nav, Tabs, Tab } from 'react-bootstrap';
import { BusinessInfo } from 'src/modules/businessInfo/businessInfo.component';
import { HistoricalTimeLine } from 'src/modules/HistoricalTimeLine/historicalTimeLine.component';
import { BusinessBOD } from 'src/modules/businessBoardOfDirector/businessBOD.component';
import { BusinessAddress } from 'src/modules/businessAddress/address.component';
import { BusinessShareholder } from 'src/modules/businessShareholder/businessShareholder.component';
import { BusinessBranch } from 'src/modules/businessBranch/businessBranch.component';
import { BusinessGroup } from 'src/modules/businessGroup/businessGroup.component';
import { BusinessNP } from 'src/modules/businessNegativePayment/businessNegativePayment.component';
import { BusinessNews } from 'src/modules/businessNews/businessNews.component';
import { BusinessLigion } from 'src/modules/businessLigion/businessLigion.component';
import { LineofBusiness } from 'src/modules/LineofBusiness/lineofBusiness.component';
import { ImportExportActivity } from 'src/modules/ImportExportActivity/importExportActivity.component';
import { Banker } from 'src/modules/banker/banker.component';
import { BusinessSummaryOFS } from 'src/modules/businessSummaryOFS/businessSummaryOFS.component';
import { BusinessFinancialRatios } from 'src/modules/businessFinancialRatios/businessFinancialRatios.component';
import { BusinessFinancialAnalysis } from 'src/modules/businessFinancialAnalysis/homeFA.component';
import { businessInfoService } from 'src/modules/businessInfo/service';
import { RSelect } from 'src/shared/component';

import './style.scss';
class BusinessDetail extends React.Component {
  static propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    setData: PropTypes.func,
    setMeta: PropTypes.func,
    data: PropTypes.array,
    meta: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      menus: [
        { id: 'Overview', value: 'Overview' },
        { id: 'Address', value: 'Address' },
        { id: 'Shareholder', value: 'Shareholder' },
        {
          id: 'NegativePayment',
          value: 'Negative payment',
        },
        {
          id: 'IndustryInformation',
          value: 'Industry Information',
        },
        {
          id: 'Report',
          value: 'Summary of Financial Statements',
        },
      ],
      loai: 'Overview',
      id: props.match.params.id,
      loadingProfile: false,
      profile: {},
      type: '1',
      ds_type: [
        { value: '1', label: 'Latest F.S' },
        { value: '2', label: 'Quarter over Quarter' },
        { value: '3', label: 'Quarter on Quarter' },
        { value: '4', label: 'Half-year' },
      ],
      ds_year: [],
      year: null,
      quarter: null,
    };
    this.subscriptions = {};
  }
  async componentDidMount() {
    await this.getYearFS();
    this.getCompanyProfile();
  }

  componentWillUnmount() {}

  getCompanyProfile() {
    let { id, year } = this.state;
    this.setState({ loadingProfile: true });
    if (id) {
      this.subscriptions['getCompanyProfile'] = businessInfoService
        .getCompanyProfile(id, year, null)
        .subscribe(
          (res) => {
            this.setState({
              profile: res,
              loadingProfile: false,
            });
          },
          (err) => {
            console.log(err);
            this.setState({ loadingProfile: false });
          }
        );
    }
  }

  async getYearFS() {
    let { id, type } = this.state;
    if (id && type) {
      const yearFS = await businessInfoService.getYearFS(id, type);
      let ds_year = [];
      let ds_quarter = [];
      yearFS.map((x) => {
        ds_year.push({
          label: x.year.toString(),
          value: x.year.toString(),
        });
      });
      ds_year = ds_year.filter((x, i, arr) => arr.findIndex((t) => t.value === x.value) === i);
      if (ds_year.length > 0) {
        const year_default = this.state.year ?? ds_year[0].value;
        this.setState({ year: year_default });
        if (this.state.type === '2' || this.state.type === '3') {
          yearFS
            .filter((z) => z.year.toString() === year_default.toString())
            .map((x) => {
              ds_quarter.push({
                label: 'Q' + x.quarter.toString(),
                value: x.quarter.toString(),
              });
            });
          if (ds_quarter.length > 0) {
            const quarter_default = this.state.quarter ?? ds_quarter[0].value;
            this.setState({ quarter: quarter_default });
          }
        }
      }
      this.setState({
        ds_year: ds_year,
        ds_quarter: ds_quarter,
      });
    }
  }
  changeType(v) {
    const type = v.target.value;
    this.setState({ type: type, year: null }, () => {
      this.getYearFS();
    });
  }
  changeYear(v) {
    const year = v.target.value;
    this.setState({ year: year, quarter: null }, () => {
      this.getYearFS();
      if (this.state.loai === 'Overview') {
        this.getCompanyProfile();
      }
    });
  }
  changeQuarter(v) {
    const quarter = v.target.value;
    this.setState({ quarter: quarter }, () => {
      this.getYearFS();
    });
  }

  render() {
    let css = 'col-lg-8 col-md-8';
    if (this.state.loai === 'Report') {
      css =
        this.state.type === '2' || this.state.type === '3'
          ? 'col-lg-4 col-md-4'
          : 'col-lg-6 col-md-6';
    }

    return (
      <CRow>
        <CCol xl={12}>
          <CCard className="p30 header-info">
            <div className="az-dashboard-one-title">
              <div className={css}>
                <h2 className="az-dashboard-title">
                  <span
                    style={{
                      marginRight: 10,
                      marginBottom: 5,
                    }}
                    className="iconify"
                    data-icon="mdi:domain"
                    data-inline="false"
                    data-width="25"
                    data-height="25"
                  ></span>
                  {this.state.profile?.name}
                </h2>
                <p className="az-dashboard-text" style={{ paddingLeft: 10 }}>
                  {this.state.profile?.address && (
                    <>
                      <span
                        className="iconify"
                        data-icon="mdi:map-marker-outline"
                        data-inline="false"
                        data-width="15"
                        data-height="15"
                      ></span>
                      <span>{this.state.profile?.address}</span>
                    </>
                  )}
                </p>
              </div>
              {this.state.loai === 'Report' && (
                <Col lg={2} md={2} className="report-select">
                  <b className="select-text">Select type:</b>
                  <RSelect
                    size="sm"
                    name="loai_id"
                    data={this.state.ds_type}
                    value={this.state.type}
                    onChange={(v) => this.changeType(v)}
                    mode="simpleSelect"
                  ></RSelect>
                </Col>
              )}

              <Col lg={2} md={2} className="report-select">
                <b className="select-text">Select year:</b>
                <RSelect
                  size="sm"
                  name="loai_id"
                  data={this.state.ds_year}
                  value={this.state.year}
                  onChange={(v) => this.changeYear(v)}
                  mode="simpleSelect"
                ></RSelect>
              </Col>

              {(this.state.type === '2' || this.state.type === '3') && (
                <Col lg={2} md={2} className="report-select">
                  <b className="select-text">Select quarter:</b>
                  <RSelect
                    size="sm"
                    name="loai_id"
                    data={this.state.ds_quarter}
                    value={this.state.quarter}
                    onChange={(v) => this.changeQuarter(v)}
                    mode="simpleSelect"
                  ></RSelect>
                </Col>
              )}

              <Col lg={2} md={2} className="download-report">
                <Button variant="outline-success">
                  <span
                    style={{
                      marginRight: 5,
                      marginBottom: 2,
                    }}
                    className="iconify"
                    data-icon="mdi:download"
                    data-inline="false"
                    data-width="20"
                    data-height="20"
                  ></span>
                  Download report
                </Button>
              </Col>
            </div>
          </CCard>
          <CCard className="card-info">
            <Row className="business-datail">
              <Col lg={10} md={12}>
                {this.state.loai === 'Overview' && (
                  <Tabs
                    id="controlled-tab-example"
                    defaultActiveKey="Information"
                    mountOnEnter={true}
                    unmountOnExit={true}
                  >
                    <Tab
                      eventKey="Information"
                      title={<React.Fragment> Summary Information</React.Fragment>}
                    >
                      <BusinessInfo
                        id={this.state.id}
                        profile={this.state.profile}
                        loadingProfile={this.state.loadingProfile}
                        year={this.state.year}
                      />
                    </Tab>
                    <Tab
                      eventKey="Historical"
                      title={<React.Fragment> Historical timeline</React.Fragment>}
                    >
                      <HistoricalTimeLine id={this.state.id} />
                    </Tab>
                    <Tab
                      eventKey="Management"
                      title={<React.Fragment> Management board</React.Fragment>}
                    >
                      <BusinessBOD id={this.state.id} />
                    </Tab>
                  </Tabs>
                )}
                {this.state.loai === 'Address' && <BusinessAddress id={this.state.id} />}
                {this.state.loai === 'Shareholder' && (
                  <Tabs
                    id="controlled-tab-example"
                    defaultActiveKey="Shareholder"
                    mountOnEnter={true}
                    unmountOnExit={true}
                  >
                    <Tab
                      eventKey="Shareholder"
                      title={<React.Fragment> Shareholder(s)</React.Fragment>}
                    >
                      <BusinessShareholder
                        id={this.state.id}
                        registeredCapital={this.state.profile?.registeredCapital}
                      />
                    </Tab>
                    <Tab eventKey="Branch" title={<React.Fragment> Branch</React.Fragment>}>
                      <BusinessBranch id={this.state.id} />
                    </Tab>
                    <Tab eventKey="Group" title={<React.Fragment> Group</React.Fragment>}>
                      <BusinessGroup id={this.state.id} />
                    </Tab>
                  </Tabs>
                )}
                {this.state.loai === 'NegativePayment' && (
                  <Tabs
                    id="controlled-tab-example"
                    defaultActiveKey="NegativePayment"
                    mountOnEnter={true}
                    unmountOnExit={true}
                  >
                    <Tab
                      eventKey="NegativePayment"
                      title={<React.Fragment> Negative payment</React.Fragment>}
                    >
                      <BusinessNP id={this.state.id} />
                    </Tab>
                    <Tab eventKey="News" title={<React.Fragment> News</React.Fragment>}>
                      <BusinessNews id={this.state.id} />
                    </Tab>
                    <Tab eventKey="Litigation" title={<React.Fragment> Litigation</React.Fragment>}>
                      <BusinessLigion id={this.state.id} />
                    </Tab>
                  </Tabs>
                )}
                {this.state.loai === 'IndustryInformation' && (
                  <Tabs
                    id="controlled-tab-example"
                    defaultActiveKey="LineofBusiness"
                    mountOnEnter={true}
                    unmountOnExit={true}
                  >
                    <Tab
                      eventKey="LineofBusiness"
                      title={<React.Fragment> Line of business</React.Fragment>}
                    >
                      <LineofBusiness id={this.state.id} />
                    </Tab>
                    <Tab
                      eventKey="ImportExportActivity"
                      title={<React.Fragment> Import & Export activity</React.Fragment>}
                    >
                      <ImportExportActivity id={this.state.id} />
                    </Tab>
                    <Tab eventKey="Banker" title={<React.Fragment> Banker</React.Fragment>}>
                      <Banker id={this.state.id} />
                    </Tab>
                  </Tabs>
                )}
                {this.state.loai === 'Report' && (
                  <Tabs
                    id="controlled-tab-example"
                    defaultActiveKey="SummaryOFS"
                    mountOnEnter={true}
                    unmountOnExit={true}
                  >
                    <Tab
                      eventKey="SummaryOFS"
                      title={<React.Fragment> Summary of Financial Statements</React.Fragment>}
                    >
                      <BusinessSummaryOFS
                        id={this.state.id}
                        type={this.state.type}
                        year={this.state.year}
                        quarter={this.state.quarter}
                      />
                    </Tab>
                    <Tab eventKey="FR" title={<React.Fragment> Financial Ratios</React.Fragment>}>
                      <BusinessFinancialRatios
                        id={this.state.id}
                        type={this.state.type}
                        year={this.state.year}
                        quarter={this.state.quarter}
                      />
                    </Tab>
                    <Tab eventKey="FA" title={<React.Fragment> Financial Analysis</React.Fragment>}>
                      <BusinessFinancialAnalysis
                        id={this.state.id}
                        type={this.state.type}
                        year={this.state.year}
                        quarter={this.state.quarter}
                      />
                    </Tab>
                  </Tabs>
                )}
              </Col>
              <Col lg={2} md={12}>
                <Nav
                  variant="pills"
                  className="flex-column menu-right"
                  defaultActiveKey={this.state.menus[0].id}
                >
                  {this.state.menus.map((item) => {
                    return (
                      <Nav.Item
                        className="nav-tab-detail"
                        as="li"
                        key={item.id}
                        onClick={() => this.setState({ loai: item.id })}
                      >
                        <Nav.Link eventKey={item.id}>{item.value}</Nav.Link>
                      </Nav.Item>
                    );
                  })}
                </Nav>
              </Col>
            </Row>
          </CCard>
        </CCol>
      </CRow>
    );
  }
}

export { BusinessDetail };
