import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BusinessDetail } from './detail.component';

class BusinessDetailModule extends React.Component {
    static propTypes = {
        match: PropTypes.object,
    }
    render() {
        return (
            <Switch>                       
                <Route path={'/business-detail/:id'} render={(props) => <BusinessDetail {...props} />} />
            </Switch>
        );
    }
}

export { BusinessDetailModule };