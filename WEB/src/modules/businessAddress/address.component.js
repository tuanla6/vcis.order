import React from 'react';
import { Card } from 'react-bootstrap';
import { CCol, CRow, } from '@coreui/react';
import { RCard } from 'src/shared/component';
import './style.scss';
import { addressService } from './service';

class BusinessAddress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listBusinessAddress: [],
            id: this.props.id,
            loadingBusinessAddress: false,
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.getBusinessAddress();
    }
    componentWillUnmount() {
    }
    getBusinessAddress() {
        let id = this.state.id;
        this.setState({ loadingBusinessAddress: true });
        if (id) {
            this.subscriptions['getBusinessAddress'] = addressService.getAddress(id).subscribe(res => {
                this.setState({
                    listBusinessAddress: res,
                    loadingBusinessAddress: false
                });
            },
                err => {
                    console.log(err);
                    this.setState({ loadingBusinessAddress: false });
                }
            );
        }
    }

    render() {
        return (
            <div className="content-business-address">
                <CRow>
                    {
                        this.state.listBusinessAddress.map((item, index) => {
                            return (
                                <CCol xs="12" lg="6" md="6" key={index}>
                                    <CRow>
                                        <CCol xs="12" lg="12">
                                            <RCard title={item.addressTypeName} className="card card-danger">
                                                <CRow className="row-one">
                                                    <CCol xs="12" md="2" lg="2">
                                                        <Card.Text className="card-body-title">
                                                            Address:
                                                        </Card.Text>
                                                    </CCol>
                                                    <CCol xs="12" md="10" lg="10">
                                                        <Card.Text>
                                                            {item.fullAddress}
                                                        </Card.Text>
                                                    </CCol>
                                                </CRow>
                                                <CRow>
                                                    <CCol xs="12" md="2" lg="2">
                                                        <Card.Text className="card-body-title">
                                                            Telephone:
                                                        </Card.Text>
                                                    </CCol>
                                                    <CCol xs="12" md="10" lg="10">
                                                        <Card.Text>
                                                            {item.phone}
                                                        </Card.Text>
                                                    </CCol>
                                                </CRow>
                                                <CRow className="row-one">
                                                    <CCol xs="12" md="2" lg="2">
                                                        <Card.Text className="card-body-title">
                                                            Email:
                                                        </Card.Text>
                                                    </CCol>
                                                    <CCol xs="12" md="10" lg="10">
                                                        <Card.Text>
                                                            {item.email}
                                                        </Card.Text>
                                                    </CCol>
                                                </CRow>
                                            </RCard>

                                        </CCol>
                                    </CRow>
                                </CCol>
                            );
                        })
                    }
                </CRow>
            </div>
        )
    }
}

export { BusinessAddress };