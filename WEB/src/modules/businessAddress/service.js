import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class AddressService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
    getAddress(id) {
        let url = 'api/vcis-business/businessaddress?id=' + id;
        return http.get(url);
    }
}
const addressService = new AddressService();
export { addressService, AddressService };