import React from 'react';
import {
    CCol,
    CRow,
    CDataTable
} from '@coreui/react';

import './style.scss';
import { newsService } from './service';
import { RCard } from 'src/shared/component';
import { formatDate } from 'src/shared/utils'
const fields = [
    { key: 'items', label: 'No.', _style: { width: '8%' } },
    { key: 'content', label: 'Content' },
    { key: 'dateOfNews', label: 'Date Of News', _style: { width: '15%' } },
];

class BusinessNews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            NegativeNnews: [],
            PositiveNews: [],
            NeutralNews: [],
            id: this.props.id,
            noItemMessage: 'Loading... '
        };
        this.subscriptions = {}
    }

    componentDidMount() {
        this.getNews();
    }

    componentWillUnmount() {

    }

    getNews() {
        let id = this.state.id;
        this.setState({ loading: true });
        this.subscriptions['getParentCompany'] = newsService.getNews(id).subscribe(res => {
            res.forEach(x => {
                x.content = x.content ? x.content : '';
                x.dateOfNews = x.dateOfNews ? formatDate(x.dateOfNews) : ''
            });

            let NegativeNnews = res.filter(x => x.type === 1);
            let PositiveNews = res.filter(x => x.type === 2);
            let NeutralNews = res.filter(x => x.type === 3);

            this.setState({
                NegativeNnews: NegativeNnews,
                PositiveNews: PositiveNews,
                NeutralNews: NeutralNews,
                loading: false,
                noItemMessage: 'No records'
            })
        }, err => {
            console.log(err);
            this.setState({
                loading: false
            })
        });
    }


    render() {
        return (
            <div className="content-business-news">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Negative news" className="card card-danger" loading={this.state.loading}>
                            <CDataTable
                                items={this.state.NegativeNnews}
                                fields={fields}
                                itemsPerPage={20}
                                pagination
                                hover
                                scopedSlots={{
                                    'items':
                                        (item, index) => (
                                            <td>
                                                {index + 1}
                                            </td>
                                        ),
                                    'content':
                                        (item, index) => (
                                            <td>
                                                <p dangerouslySetInnerHTML={{ __html: item.content }} />
                                            </td>
                                        ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>

                    <CCol xs="12" lg="12">
                        <RCard title="Positive news" className="card card-danger" loading={this.state.loading}>
                            <CDataTable
                                items={this.state.PositiveNews}
                                fields={fields}
                                itemsPerPage={20}
                                pagination
                                hover
                                scopedSlots={{
                                    'items':
                                        (item, index) => (
                                            <td>
                                                {index + 1}
                                            </td>
                                        ),
                                    'content':
                                        (item, index) => (
                                            <td>
                                                <p dangerouslySetInnerHTML={{ __html: item.content }} />
                                            </td>
                                        ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Neutral news" className="card card-danger" loading={this.state.loading}>
                            <CDataTable
                                items={this.state.NeutralNews}
                                fields={fields}
                                itemsPerPage={20}
                                pagination
                                hover
                                scopedSlots={{
                                    'items':
                                        (item, index) => (
                                            <td>
                                                {index + 1}
                                            </td>
                                        ),
                                    'content':
                                        (item, index) => (
                                            <td>
                                                <p dangerouslySetInnerHTML={{ __html: item.content }} />
                                            </td>
                                        ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>

                </CRow>
            </div>
        )
    }
}

export { BusinessNews };