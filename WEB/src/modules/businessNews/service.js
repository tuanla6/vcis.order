import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class NewsService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-negativenew' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getNews(businessId) {
      return http.get('api/vcis-business-negativenew?businessId='+businessId);
  }
}
const newsService = new NewsService();

export { newsService, NewsService };
