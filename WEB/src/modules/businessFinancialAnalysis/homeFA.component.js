import React from 'react';
import PropTypes from 'prop-types';
import {
  CCard,
  CCol,
  CRow,
  // CPagination
} from '@coreui/react';
import { Row, Col, Tabs, Tab } from 'react-bootstrap';
import './style.scss';
import { DupontAnalysis } from '../businessDupontAnalysis/DupontAnalysis.component';
import { BusinessFA } from './businessFA.component';
import { PeerAnalysis } from '../businessPeerAnalysis/PeerAnalysis.component';

class BusinessFinancialAnalysis extends React.Component {
  static propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
  };
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
    };
  }
  componentDidMount() {}
  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter });
    }
  }
  componentWillUnmount() {}

  render() {
    return (
      <CRow>
        <CCol xl={12}>
          <CCard className="p30">
            <Row>
              <Col lg={12} md={12} className="info-analysis">
                <Tabs
                  id="controlled-tab-example"
                  defaultActiveKey="Overview"
                  mountOnEnter={true}
                  unmountOnExit={true}
                >
                  <Tab eventKey="Overview" title={<React.Fragment> Overview</React.Fragment>}>
                    <BusinessFA
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                  <Tab eventKey="Dupont" title={<React.Fragment> Dupont Analysis</React.Fragment>}>
                    <DupontAnalysis
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                  <Tab eventKey="Peer" title={<React.Fragment> Peer Analysis</React.Fragment>}>
                    <PeerAnalysis
                      id={this.state.id}
                      type={this.state.type}
                      year={this.state.year}
                      quarter={this.state.quarter}
                    />
                  </Tab>
                </Tabs>
              </Col>
            </Row>
          </CCard>
        </CCol>
      </CRow>
    );
  }
}

export { BusinessFinancialAnalysis };
