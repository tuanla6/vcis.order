import React from 'react';
import { Col, Form } from 'react-bootstrap';
import { RCard } from 'src/shared/component';
import { Chart, Interval, Axis, Tooltip, Coordinate, Legend, Line, View } from 'bizcharts';
import DataSet from '@antv/data-set';
import { financialAnalysisServiceService } from './service';
import { currencyFormatBilion } from 'src/shared/utils';
import GaugeChart from 'react-gauge-chart';

class BusinessFA extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingBusinessFA: false,
      data: [],
      header: {},
      numyear: null,
      chartData1: [],
      chartData2: [],
      chartData3: [],
      chartData4: [],
      id: props.id,
      type: props.type,
      year: props.year,
      quarter: props.quarter,
      listYear: [],
      dataDSO: [],
      dataDOH: [],
      dataDPO: [],
      dataCCC: [],
      chartData5: [],
      gaugeGrossProfitMargin: [],
      gaugeOperatingIncomeMargin: [],
      gaugeEBITMargin: [],
      gaugeEBTMargin: [],
    };
    this.subscriptions = {};
  }

  componentDidMount() {
    this.getFinancialAnalysis();
    this.getCashConversionCycle();
    this.getDataMargin();
  }

  componentWillUnmount() {}

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({ type: this.props.type }, () => {
        this.getFinancialAnalysis();
        this.getCashConversionCycle();
        this.getDataMargin();
      });
    }
    if (this.props.year !== this.state.year) {
      this.setState({ year: this.props.year }, () => {
        this.getFinancialAnalysis();
        this.getCashConversionCycle();
        this.getDataMargin();
      });
    }
    if (this.props.quarter !== this.state.quarter) {
      this.setState({ quarter: this.props.quarter }, () => {
        this.getFinancialAnalysis();
        this.getCashConversionCycle();
        this.getDataMargin();
      });
    }
  }
  getFinancialAnalysis() {
    let { id, type, year, quarter } = this.state;

    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      if (id) {
        this.subscriptions['getFinancialAnalysis'] = financialAnalysisServiceService
          .getFinancialAnalysis(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let listYear = [];
              let chartDataNew3 = [];
              for (let i = 0; i < res.data.length; i++) {
                for (let j = 1; j <= header.numyear; j++) {
                  if (res.data[i].industryAverageGroupId === 3) {
                    var dataNew4 = {};
                    dataNew4['name'] = res.data[i].name;
                    dataNew4['year'] = res.header['year' + j];
                    dataNew4['value'] = res.data[i]['year' + j];
                    chartDataNew3.push(dataNew4);
                  }
                  res.data[i][header['year' + j]] = res.data[i]['year' + j];
                  if (res.data[i].industryAverageGroupId === 4) {
                    res.data[i][header['year' + j]] = res.data[i]['year' + j] / 1000000000;
                  }
                }
              }
              for (let j = 1; j <= header.numyear; j++) {
                listYear.push(header['year' + j]);
              }
              this.setState({ listYear: listYear });
              let chartData1 = res.data.filter((x) => x.industryAverageGroupId === 1);
              if (chartData1 !== undefined && chartData1 !== 'undefined') {
                this.setState({ chartData1: chartData1 });
              }
              let chartData2 = res.data.filter((x) => x.industryAverageGroupId === 2);
              if (chartData2 !== undefined && chartData2 !== 'undefined') {
                this.setState({ chartData2: chartData2 });
              }
              this.setState({ chartData3: chartDataNew3 });

              let chartData4 = res.data.filter((x) => x.industryAverageGroupId === 4);
              if (chartData4 !== undefined && chartData4 !== 'undefined') {
                this.setState({ chartData4: chartData4 });
              }
              this.setState({
                loading: false,
                data: res.data,
                header: header,
                numyear: header.numyear,
                chartData1: chartData1,
                chartData2: chartData2,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBusinessFA: false });
            }
          );
      }
    }
  }
  getCashConversionCycle() {
    let { id, type, year, quarter } = this.state;
    this.setState({ loadingBusinessFA: true });
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      if (id) {
        this.subscriptions['getCashConversionCycle'] = financialAnalysisServiceService
          .getCashConversionCycle(id, year, quarter, type)
          .subscribe(
            (res) => {
              var dataDSO = [];
              var dataDOH = [];
              var dataDPO = [];
              var dataCCC = [];
              let header = res.header || {};
              const getDataDSO = res.data.filter((x) => x.index === 1);
              const getDataDOH = res.data.filter((x) => x.index === 2);
              const getDataDPO = res.data.filter((x) => x.index === 3);
              const getDataCCC = res.data.filter((x) => x.index === 4);
              for (let j = 1; j <= header.numyear; j++) {
                if (getDataDSO[0].index === 1) {
                  var dso = {};
                  dso['name'] = res.header['year' + j];
                  dso['DSO'] = getDataDSO[0]['year' + j];
                  dataDSO.push(dso);
                }
                if (getDataDOH[0].index === 2) {
                  var doh = {};
                  doh['name'] = res.header['year' + j];
                  doh['DOH'] = getDataDOH[0]['year' + j];
                  dataDOH.push(doh);
                }
                if (getDataDPO[0].index === 3) {
                  var dpo = {};
                  dpo['name'] = res.header['year' + j];
                  dpo['DPO'] = getDataDPO[0]['year' + j];
                  dataDPO.push(dpo);
                }
                if (getDataCCC[0].index === 4) {
                  var ccc = {};
                  ccc['name'] = res.header['year' + j];
                  ccc['CCC'] = getDataCCC[0]['year' + j];
                  dataCCC.push(ccc);
                }
              }
              this.setState({
                dataDSO: dataDSO,
                dataDOH: dataDOH,
                dataDPO: dataDPO,
                dataCCC: dataCCC,
              });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBusinessFA: false });
            }
          );
      }
    }
  }
  getDataMargin() {
    let { id, type, year, quarter } = this.state;
    let check = id && year;
    if (type === '2' || type === '3') {
      check = check && quarter;
    }
    if (check) {
      if (id) {
        this.subscriptions['getDataMargin'] = financialAnalysisServiceService
          .getDataMargin(id, year, quarter, type)
          .subscribe(
            (res) => {
              let header = res.header || {};
              let chartDataNew5 = [];
              for (let i = 0; i < res.data.length; i++) {
                for (let j = 1; j <= header.numyear; j++) {
                  var dataNew5 = {};
                  dataNew5['name'] = res.data[i].name;
                  dataNew5['year'] = res.header['year' + j];
                  dataNew5['value'] = res.data[i]['year' + j];
                  dataNew5['averageValue'] = res.data[i].averageValue;
                  dataNew5['industryAverageGroupId'] = res.data[i].industryAverageGroupId;
                  chartDataNew5.push(dataNew5);
                }
              }
              let chartDataGrossProfitMargin = chartDataNew5.filter(
                (x) => x.name === 'Gross Profit Margin (%)'
              );
              if (chartDataGrossProfitMargin != null) {
                const dataGrossProfitMargin = this.getConvertDataMargin(
                  chartDataGrossProfitMargin[0]
                );
                this.setState({ gaugeGrossProfitMargin: dataGrossProfitMargin });
              }
              let chartDataOperatingIncomeMargin = chartDataNew5.filter(
                (x) => x.name === 'Operating Income Margin (%)'
              );
              if (chartDataOperatingIncomeMargin != null) {
                const dataOperatingIncomeMargin = this.getConvertDataMargin(
                  chartDataOperatingIncomeMargin[0]
                );
                this.setState({ gaugeOperatingIncomeMargin: dataOperatingIncomeMargin });
              }
              let chartDataEBITMargin = chartDataNew5.filter((x) => x.name === 'EBIT Margin (%)');
              if (chartDataEBITMargin != null) {
                const dataGrossEBITMargin = this.getConvertDataMargin(chartDataEBITMargin[0]);
                this.setState({ gaugeEBITMargin: dataGrossEBITMargin });
              }
              let chartDataEBTMargin = chartDataNew5.filter((x) => x.name === 'EBT Margin (%)');
              if (chartDataEBTMargin != null) {
                const dataEBTMargin = this.getConvertDataMargin(chartDataEBTMargin[0]);
                this.setState({ gaugeEBTMargin: dataEBTMargin });
              }
              this.setState({ chartData5: chartDataNew5 });
            },
            (err) => {
              console.log(err);
              this.setState({ loadingBusinessFA: false });
            }
          );
      }
    }
  }
  getConvertDataMargin(obj) {
    if (obj === 'undefined' || obj === undefined || obj.value > 100 || obj.value < 0) {
      return {};
    }
    return {
      name: obj.name,
      averageValue: obj.averageValue,
      valueBusiness: obj.value != null ? obj.value / 100 : 0,
      valueTotal: obj.averageValue != null ? 100 - obj.averageValue : 0,
    };
  }
  render() {
    const ds = new DataSet();
    const dv = ds.createView().source(this.state.chartData1);
    dv.transform({
      type: 'fold',
      fields: this.state.listYear,
      key: 'type',
      value: 'value',
    });

    const dv2 = ds.createView().source(this.state.chartData2);
    dv2.transform({
      type: 'fold',
      fields: this.state.listYear,
      key: 'type',
      value: 'value',
    });

    const dv4 = ds.createView().source(this.state.chartData4);
    dv4.transform({
      type: 'fold',
      fields: this.state.listYear,
      key: 'type',
      value: 'value',
    });

    const dvDSO = ds.createView().source(this.state.dataDSO);
    dvDSO.transform({
      type: 'fold',
      fields: ['DSO'],
      key: 'type',
      value: 'DSO',
    });

    const dvDOH = ds.createView().source(this.state.dataDOH);
    dvDOH.transform({
      type: 'fold',
      fields: ['DOH'],
      key: 'type',
      value: 'DOH',
    });

    const dvDPO = ds.createView().source(this.state.dataDPO);
    dvDPO.transform({
      type: 'fold',
      fields: ['DPO'],
      key: 'type',
      value: 'DPO',
    });

    const dvCCC = ds.createView().source(this.state.dataCCC);
    dvCCC.transform({
      type: 'fold',
      fields: ['CCC'],
      key: 'type',
      value: 'CCC',
    });
    const dataEBIT = this.state.chartData3
      .filter((x) => x.name === 'EBIT')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueEBIT: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });
    const dataEBT = this.state.chartData3
      .filter((x) => x.name === 'EBT')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueEBT: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });
    const dataNetIncome = this.state.chartData3
      .filter((x) => x.name === 'Net Income')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueNetIncome: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });
    const chartData3 = this.state.chartData3
      .filter((x) => x.name !== 'EBIT' && x.name !== 'EBT' && x.name !== 'Net Income')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          Billions: Number((key.value / 1000000000).toFixed(2)),
          labelData: currencyFormatBilion(key.value),
        };
      });

    const chartDataGrossProfit = this.state.chartData5
      .filter((x) => x.name === 'Gross profit')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          Billions: Number((key.value / 1000000000).toFixed(2)),
          labelData: currencyFormatBilion(key.value),
        };
      });

    const dataGrossProfitMargin = this.state.chartData5
      .filter((x) => x.name === 'Gross Profit Margin (%)')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueGPM: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });
    const chartDataOperating = this.state.chartData5
      .filter((x) => x.name === 'Operating Income')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          Billions: Number((key.value / 1000000000).toFixed(2)),
          labelData: currencyFormatBilion(key.value),
        };
      });

    const dataOIM = this.state.chartData5
      .filter((x) => x.name === 'Operating Income Margin (%)')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueOIM: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });

    const chartDataEBIT = this.state.chartData5
      .filter((x) => x.name === 'EBIT')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          Billions: Number((key.value / 1000000000).toFixed(2)),
          labelData: currencyFormatBilion(key.value),
        };
      });

    const dataEBITMargin = this.state.chartData5
      .filter((x) => x.name === 'EBIT Margin (%)')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueEBITMargin: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });

    const chartDataEBT = this.state.chartData5
      .filter((x) => x.name === 'EBT')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          Billions: Number((key.value / 1000000000).toFixed(2)),
          labelData: currencyFormatBilion(key.value),
        };
      });

    const dataEBTMargin = this.state.chartData5
      .filter((x) => x.name === 'EBT Margin (%)')
      .map((key) => {
        return {
          name: key.name,
          year: key.year,
          valueEBTMargin: key.value != null ? Number(key.value.toFixed(2)) : null,
        };
      });
    const scale = {
      valueEBIT: {
        alias: 'EBIT',
      },
      valueEBT: {
        alias: 'EBT',
      },
      valueNetIncome: {
        alias: 'Net Income',
      },
      valueGPM: {
        alias: 'Gross Profit Margin',
      },
      valueOIM: {
        alias: 'Operating Income Margin',
      },
      valueEBITMargin: {
        alias: 'EBIT Margin',
      },
      valueEBTMargin: {
        alias: 'EBT Margin',
      },
    };

    const colors = ['#069', '#17a2b8', '#28a745', '#c30', '#ffc107', '#6c757d'];
    const axisLabel = {
      formatter(text, item, index) {
        return text + '%';
      },
    };

    const titleChart4 = {
      position: 'end',
      text: 'Billions',
    };
    return (
      <div className="content b-info">
        <Form.Row>
          <Col sm={12} md={6}>
            <RCard title="Assets" className="card card-danger">
              <Chart height={300} data={dv} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="name"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis
                  name="value"
                  label={{
                    formatter: (val) => {
                      return (val * 100).toFixed(0) + '%';
                    },
                  }}
                  position={'left'}
                />
                <Tooltip />
                <Interval
                  position="name*value"
                  color={'type'}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={6}>
            <RCard title="Resources" className="card card-danger">
              <Chart height={300} data={dv2} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="name"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis
                  name="value"
                  label={{
                    formatter: (val) => {
                      return (val * 100).toFixed(0) + '%';
                    },
                  }}
                  position={'left'}
                />
                <Tooltip />
                <Interval
                  position="name*value"
                  color={'type'}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col xs="12" md={6}>
            <RCard title="Profitability" className="card card-danger">
              <Chart height={300} padding={[50, 60]} data={chartData3} autoFit>
                <Tooltip shared></Tooltip>
                <Interval
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 0,
                    },
                  ]}
                  color={['name', colors]}
                  position="year*Billions"
                  size={15}
                />
                <Axis name="Billions" position="left" title grid={false} />
                <View data={dataEBIT} scale={scale} padding={0}>
                  <Axis visible={true} name="valueEBIT" label={axisLabel} position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueEBIT',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueEBIT"
                    color={colors[3]}
                  />
                </View>
                <View data={dataEBT} scale={scale} padding={0}>
                  <Axis visible={false} name="valueEBT" position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueEBT',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueEBT"
                    color={colors[4]}
                  />
                </View>
                <View data={dataNetIncome} scale={scale} padding={0}>
                  <Axis visible={false} name="valueNetIncome" position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueNetIncome',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueNetIncome"
                    color={colors[5]}
                  />
                </View>
                <Legend
                  name="Legend1"
                  custom={true}
                  items={[
                    {
                      name: 'Net Sales',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[0] },
                      },
                    },
                    {
                      name: 'Gross profit',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[1] },
                      },
                    },
                    {
                      name: 'EBITDA',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[2] },
                      },
                    },
                    {
                      name: 'EBIT',
                      marker: {
                        symbol: 'hyphen',
                        style: {
                          stroke: colors[3],
                          lineWidth: 2,
                        },
                      },
                    },
                    {
                      name: 'EBT',
                      marker: {
                        symbol: 'hyphen',
                        style: {
                          stroke: colors[4],
                          lineWidth: 2,
                        },
                      },
                    },
                    {
                      name: 'Net Income',
                      marker: {
                        symbol: 'hyphen',
                        style: {
                          stroke: colors[5],
                          lineWidth: 2,
                        },
                      },
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={6}>
            <RCard title="Cashflow" className="card card-danger">
              <Chart height={300} data={dv4} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="name"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis name="value" position={'left'} title={titleChart4} />
                <Tooltip />
                <Interval
                  position="name*value"
                  color={'type'}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col xs="12" md={3}>
            <RCard title="DSO" className="card card-danger">
              <Chart height={300} padding={[50, 60]} data={dvDSO} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="label"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis name="value" position={'right'} />
                <Tooltip />
                <Interval
                  position="name*DSO"
                  color={colors[0]}
                  size={15}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={3}>
            <RCard title="DOH" className="card card-danger">
              <Chart height={300} padding={[50, 60]} data={dvDOH} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="label"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis name="value" position={'right'} />
                <Tooltip />
                <Interval
                  position="name*DOH"
                  color={colors[1]}
                  size={15}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={3}>
            <RCard title="DPO" className="card card-danger">
              <Chart height={300} padding={[50, 60]} data={dvDPO} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="label"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis name="value" position={'right'} />
                <Tooltip />
                <Interval
                  position="name*DPO"
                  size={15}
                  color={colors[2]}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={3}>
            <RCard title="CCC = DSO + DOH - DPO" className="card card-danger">
              <Chart height={300} padding={[50, 60]} data={dvCCC} autoFit>
                <Legend />
                <Coordinate transpose scale={[1, -1]} />
                <Axis
                  name="label"
                  label={{
                    offset: 12,
                  }}
                />
                <Axis name="value" position={'right'} />
                <Tooltip />
                <Interval
                  position="name*CCC"
                  size={15}
                  color={colors[5]}
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 1 / 32,
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col xs="12" md={6}>
            <RCard title="Gross Profit Margin" className="card card-danger">
              <GaugeChart
                id="gauge-chart5"
                nrOfLevels={420}
                arcsLength={[
                  this.state.gaugeGrossProfitMargin.averageValue,
                  this.state.gaugeGrossProfitMargin.valueTotal,
                ]}
                colors={['#58a6ff', '#ddd']}
                percent={this.state.gaugeGrossProfitMargin.valueBusiness}
                arcPadding={0.02}
                arcWidth={0.2}
                // formatTextValue={(value) => value + ''}
                textColor={'#cecece'}
                cornerRadius={0}
                style={{
                  height: '50%',
                  width: '50%',
                }}
              />
              <div className="box-note-chart">
                <span className="box-avg-ind">&nbsp; &nbsp; &nbsp;</span>
                <span>Avg.Ind</span>
                <span className="box-avg-business">&nbsp; &nbsp; &nbsp;</span>
                <span>Gross Profit Margin</span>
              </div>
              <Chart height={300} padding={[50, 60]} data={chartDataGrossProfit} autoFit>
                <Tooltip shared />
                <Interval
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 0,
                    },
                  ]}
                  color={['name', colors[2]]}
                  position="year*Billions"
                  size={15}
                />
                <Axis visible={true} name="Billions" position="left" title grid={false} />
                <View data={dataGrossProfitMargin} scale={scale} padding={0}>
                  <Axis visible={true} name="valueGPM" label={axisLabel} position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueGPM',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueGPM"
                    color={colors[4]}
                  />
                </View>
                <Legend
                  name="Legend2"
                  custom={true}
                  items={[
                    {
                      name: 'Gross profit',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[2] },
                      },
                    },
                    {
                      name: 'Gross Profit Margin (%)',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[4] },
                      },
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={6}>
            <RCard title="Operating Income Margin" className="card card-danger">
              <GaugeChart
                id="gauge-chart5"
                nrOfLevels={420}
                arcsLength={[
                  this.state.gaugeOperatingIncomeMargin.averageValue,
                  this.state.gaugeOperatingIncomeMargin.valueTotal,
                ]}
                colors={['#58a6ff', '#ddd']}
                percent={this.state.gaugeOperatingIncomeMargin.valueBusiness}
                arcPadding={0.02}
                arcWidth={0.2}
                // formatTextValue={(value) => value + ''}
                textColor={'#cecece'}
                cornerRadius={0}
                style={{
                  height: '50%',
                  width: '50%',
                }}
              />
              <div className="box-note-chart">
                <span className="box-avg-ind">&nbsp; &nbsp; &nbsp;</span>
                <span>Avg.Ind</span>
                <span className="box-avg-business">&nbsp; &nbsp; &nbsp;</span>
                <span>Operating Income Margin</span>
              </div>
              <Chart height={300} padding={[50, 60]} data={chartDataOperating} autoFit>
                <Tooltip shared />
                <Interval
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 0,
                    },
                  ]}
                  color={['name', colors[1]]}
                  position="year*Billions"
                  size={15}
                />
                <Axis visible={true} name="Billions" position="left" title grid={false} />
                <View data={dataOIM} scale={scale} padding={0}>
                  <Axis visible={true} name="valueOIM" label={axisLabel} position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueOIM',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueOIM"
                    color={colors[3]}
                  />
                </View>
                <Legend
                  name="Legend3"
                  custom={true}
                  items={[
                    {
                      name: 'Operatin income',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[1] },
                      },
                    },
                    {
                      name: 'Operating Income Margin (%)',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[3] },
                      },
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={6}>
            <RCard title="EBIT Margin" className="card card-danger">
              <GaugeChart
                id="gauge-chart5"
                nrOfLevels={420}
                arcsLength={[
                  this.state.gaugeEBITMargin.averageValue,
                  this.state.gaugeEBITMargin.valueTotal,
                ]}
                colors={['#58a6ff', '#ddd']}
                percent={this.state.gaugeEBITMargin.valueBusiness}
                arcPadding={0.02}
                arcWidth={0.2}
                // formatTextValue={(value) => value + ''}
                textColor={'#cecece'}
                cornerRadius={0}
                style={{
                  height: '50%',
                  width: '50%',
                }}
              />
              <div className="box-note-chart">
                <span className="box-avg-ind">&nbsp; &nbsp; &nbsp;</span>
                <span>Avg.Ind</span>
                <span className="box-avg-business">&nbsp; &nbsp; &nbsp;</span>
                <span>EBIT Margin</span>
              </div>
              <Chart height={300} padding={[50, 60]} data={chartDataEBIT} autoFit>
                <Tooltip shared />
                <Interval
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 0,
                    },
                  ]}
                  color={['name', colors[4]]}
                  position="year*Billions"
                  size={15}
                />
                <Axis visible={true} name="Billions" position="left" title grid={false} />
                <View data={dataEBITMargin} scale={scale} padding={0}>
                  <Axis visible={true} name="valueEBITMargin" label={axisLabel} position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueEBITMargin',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueEBITMargin"
                    color={colors[2]}
                  />
                </View>
                <Legend
                  name="Legend4"
                  custom={true}
                  items={[
                    {
                      name: 'EBIT',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[4] },
                      },
                    },
                    {
                      name: 'EBIT Margin (%)',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[2] },
                      },
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
          <Col sm={12} md={6}>
            <RCard title="EBT Margin" className="card card-danger">
              <GaugeChart
                id="gauge-chart5"
                nrOfLevels={420}
                arcsLength={[
                  this.state.gaugeEBTMargin.averageValue,
                  this.state.gaugeEBTMargin.valueTotal,
                ]}
                colors={['#58a6ff', '#ddd']}
                percent={this.state.gaugeEBTMargin.valueBusiness}
                arcPadding={0.02}
                arcWidth={0.2}
                // formatTextValue={(value) => value + ''}
                textColor={'#cecece'}
                cornerRadius={0}
                style={{
                  height: '50%',
                  width: '50%',
                }}
              />
              <div className="box-note-chart">
                <span className="box-avg-ind">&nbsp; &nbsp; &nbsp;</span>
                <span>Avg.Ind</span>
                <span className="box-avg-business">&nbsp; &nbsp; &nbsp;</span>
                <span>EBT Margin</span>
              </div>
              <Chart height={300} padding={[50, 60]} data={chartDataEBT} autoFit>
                <Tooltip shared />
                <Interval
                  adjust={[
                    {
                      type: 'dodge',
                      marginRatio: 0,
                    },
                  ]}
                  color={['name', colors[5]]}
                  position="year*Billions"
                  size={15}
                />
                <Axis visible={true} name="Billions" position="left" title grid={false} />
                <View data={dataEBTMargin} scale={scale} padding={0}>
                  <Axis visible={true} name="valueEBTMargin" label={axisLabel} position="right" />
                  <Line
                    shape="smooth"
                    tooltip={[
                      'name*valueEBTMargin',
                      (name, value) => {
                        return {
                          name: name,
                          value: value + ' %',
                        };
                      },
                    ]}
                    position="year*valueEBTMargin"
                    color={colors[1]}
                  />
                </View>
                <Legend
                  name="Legend5"
                  custom={true}
                  items={[
                    {
                      name: 'EBT',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[5] },
                      },
                    },
                    {
                      name: 'EBT Margin (%)',
                      marker: {
                        symbol: 'square',
                        style: { fill: colors[1] },
                      },
                    },
                  ]}
                />
              </Chart>
            </RCard>
          </Col>
        </Form.Row>
      </div>
    );
  }
}

export { BusinessFA };
