import { http } from 'src/shared/utils';

class FinancialAnalysisService {
  getFinancialAnalysis(businessId, year, quarter, type) {
    let url = 'api/vcis-report/financial-analysis?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q=';
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type=';
    }
    return http.get(url);
  }
  getCashConversionCycle(businessId, year, quarter, type) {
    let url = 'api/vcis-report/cash-conversion-cycle?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q=';
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type=';
    }
    return http.get(url);
  }
  getDataMargin(businessId, year, quarter, type) {
    let url = 'api/vcis-report/data-margin?businessId=' + businessId + '&year=' + year;
    if (quarter) {
      url += '&q=' + quarter;
    } else {
      url += '&q=';
    }
    if (type) {
      url += '&type=' + type;
    } else {
      url += '&type=';
    }
    return http.get(url);
  }
}
const financialAnalysisServiceService = new FinancialAnalysisService();
export { financialAnalysisServiceService, FinancialAnalysisService };

const AssetsData = [
  {
    label: 'Cash and cash equivalents',
    series1: 2800,
    series2: 2260,
    series3: 1300,
    series4: 200,
    series5: 800,
  },
  {
    label: 'Short-term marketable investments',
    series1: 1800,
    series2: 1300,
    series3: 1000,
    series4: 3000,
    series5: 400,
  },
  {
    label: 'Trade debts',
    series1: 950,
    series2: 900,
    series3: 1200,
    series4: 500,
    series5: 600,
  },
  {
    label: 'Inventories',
    series1: 500,
    series2: 390,
    series3: 100,
    series4: 900,
    series5: 1600,
  },
  {
    label: 'Other Current Assets',
    series1: 170,
    series2: 100,
    series3: 300,
    series4: 1600,
    series5: 800,
  },
  {
    label: 'Property, plant, adequipment',
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
  {
    label: 'Investment property',
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
  {
    label: 'Long-term investment',
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
  {
    label: 'Other Nor-Current Assets',
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
];
const ResourcesData = [
  {
    label: 'Trade credt',
    series1: 2800,
    series2: 2260,
    series3: 1300,
    series4: 200,
    series5: 800,
  },
  {
    label: 'Short-term loans',
    series1: 1800,
    series2: 1300,
    series3: 1000,
    series4: 3000,
    series5: 400,
  },
  {
    label: 'Other current liabilities',
    series1: 950,
    series2: 900,
    series3: 1200,
    series4: 500,
    series5: 600,
  },
  {
    label: 'Long-term loans',
    series1: 500,
    series2: 390,
    series3: 100,
    series4: 900,
    series5: 1600,
  },
  {
    label: 'Other non-current liabilities',
    series1: 170,
    series2: 100,
    series3: 300,
    series4: 1600,
    series5: 800,
  },
  {
    label: "Owner's investment capital",
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
  {
    label: '.....',
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
  {
    label: "Other owner's equity",
    series1: 600,
    series2: 500,
    series3: 1500,
    series4: 2100,
    series5: 1100,
  },
];
export { AssetsData, ResourcesData };
