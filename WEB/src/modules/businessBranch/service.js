import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class BranchService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-branch' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getBranch(businessId) {
      return http.get('api/vcis-business-branch?businessId='+businessId);
  }
}
const branchService = new BranchService();

export { branchService, BranchService };

