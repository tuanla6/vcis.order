import React from 'react';
import {
    CCol,
    CDataTable,
    CRow
} from '@coreui/react';
import './style.scss';
import { branchService } from './service';
import { RCard } from 'src/shared/component';
const fields = [
    { key: 'items', label: 'Items', _style: { width: '8%' } },
    { key: 'name', label: 'Name', _style: { width: '35%' } },
    { key: 'brn', label: 'Business ID', _style: { width: '20%' } },
    'address'
];

class BusinessBranch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Branch: [],
            id: this.props.id,
            noItemMessage:'Loading... '
        };
        this.subscriptions = {}
    }

    componentDidMount() {
        this.getBranch();
    }
    componentWillUnmount() {

    }

    getBranch() {
        let id = this.state.id;
        this.setState({ loading: true });
        this.subscriptions['getParentCompany'] = branchService.getBranch(id).subscribe(res => {
            res.forEach(x => {
                x.name = x.name ? x.name : '';
                x.brn = x.brn ? x.brn : '';
                x.address = x.address ? x.address : '';
            });

            this.setState({
                Branch: res,
                loading: false,
                noItemMessage: 'The company does not have branch'
            })
        }, err => {
            console.log(err);
            this.setState({
                loading: false
            })
        });
    }

    render() {
        return (
            <div className="content-business-branch">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Branch(es)" className="card card-danger" loading={this.state.loading}>
                            <CDataTable
                                items={this.state.Branch}
                                fields={fields}
                                itemsPerPage={20}
                                pagination
                                hover
                                scopedSlots = {{
                                    'items':
                                      (item, index)=>(
                                        <td>
                                          {index+1}
                                        </td>
                                      ),
                                }}
                                noItemsView={{ noItems: this.state.noItemMessage }}
                            />
                        </RCard>
                    </CCol>
                </CRow>
            </div>
        )
    }
}

export { BusinessBranch };