
import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';

class ImportExportService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-importexport' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getImportexport(businessId) {
        return http.get('api/vcis-business-importexport?businessId='+businessId);
    }
}
const importExportService = new ImportExportService();

export { importExportService, ImportExportService };
