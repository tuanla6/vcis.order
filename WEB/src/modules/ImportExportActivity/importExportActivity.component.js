import React from 'react';
import { Table } from 'react-bootstrap';
import { CCol, CRow, CDataTable } from '@coreui/react';
import { RCard } from 'src/shared/component';
import './style.scss';
import { importExportService } from './service';
const fieldSuppliers = [
    { key: 'items', label: 'No.', _style: { width: '8%' } },
    { key: 'businessName', label: 'Suppliers' },
    { key: 'country', label: 'Country', _style: { width: '20%' } }
];
const fieldClients = [
    { key: 'items', label: 'No.', _style: { width: '8%' } },
    { key: 'businessName', label: 'Clients' },
    { key: 'country', label: 'Country', _style: { width: '20%' } }
];

class ImportExportActivity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            import: {},
            export: {},
            Suppliers: [],
            Clients: [],
            id: this.props.id,
        };
        this.subscriptions = {}
    }

    componentDidMount() {
        this.getImportexport();
    }
    componentWillUnmount() {

    }
    getImportexport() {
        let id = this.state.id;
        this.setState({ loading: true });
        this.subscriptions['getImportexport'] = importExportService.getImportexport(id).subscribe(res => {
            let _import = res.importExports.find(x => x.category === 'Import') || {};
            let _export = res.importExports.find(x => x.category === 'Export') || {};
            this.setState({
                import: _import,
                export: _export,
                Suppliers: res.majorSuppliers,
                Clients: res.majorClients,
                loading: false,
            })
        }, err => {
            console.log(err);
            this.setState({
                loading: false
            })
        });
    }
    render() {
        return (
            <div className="content-import-export-activity">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Import activity" className="card card-danger" loading={this.state.loading}>
                            {
                                Object.keys(this.state.import).length !== 0 &&
                                <Table className="custom-table">
                                    <tbody>
                                        <tr>
                                            <td>Products</td>
                                            <td>{this.state.import.typesOfProducts} </td>
                                        </tr>
                                        <tr>
                                            <td>Market </td>
                                            <td>{this.state.import.market} </td>
                                        </tr>
                                        <tr>
                                            <td>Ratio</td>
                                            <td>{this.state.import.ratio} </td>
                                        </tr>
                                        <tr>
                                            <td>Payment </td>
                                            <td>{this.state.import.modeOfPayment} </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            }
                            {
                                this.state.Suppliers.length > 0 &&
                                <CDataTable
                                    items={this.state.Suppliers}
                                    fields={fieldSuppliers}
                                    itemsPerPage={15}
                                    pagination
                                    hover
                                    scopedSlots={{
                                        'items':
                                            (item, index) => (
                                                <td>
                                                    {index + 1}
                                                </td>
                                            ),
                                    }}
                                />
                            }

                        </RCard>
                    </CCol>
                    <CCol xs="12" lg="12">
                        <RCard title="Export activity" className="card card-danger">
                            {
                                Object.keys(this.state.export).length !== 0 &&
                                <Table className="custom-table">
                                    <tbody>
                                        <tr>
                                            <td>Products</td>
                                            <td>{this.state.export.typesOfProducts} </td>
                                        </tr>
                                        <tr>
                                            <td>Market </td>
                                            <td>{this.state.export.market} </td>
                                        </tr>
                                        <tr>
                                            <td>Ratio</td>
                                            <td>{this.state.export.ratio} </td>
                                        </tr>
                                        <tr>
                                            <td>Payment </td>
                                            <td>{this.state.export.modeOfPayment} </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            }

                            {
                                this.state.Clients.length > 0 &&
                                <CDataTable
                                    items={this.state.Clients}
                                    fields={fieldClients}
                                    itemsPerPage={15}
                                    pagination
                                    hover
                                    scopedSlots={{
                                        'items':
                                            (item, index) => (
                                                <td>
                                                    {index + 1}
                                                </td>
                                            ),
                                    }}
                                />
                            }
                        </RCard>
                    </CCol>

                </CRow>
            </div>
        )
    }
}

export { ImportExportActivity };