import React from 'react';
import { Col, Card, FormControl, InputGroup, Container, Row, Button } from 'react-bootstrap';
import CIcon from '@coreui/icons-react';
import { CPagination } from '@coreui/react';
import './style.scss';
import { freeSet } from '@coreui/icons';
import { homeService } from './service';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.goToSearch = this.goToSearch.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
        this.goToChangePage = this.goToChangePage.bind(this);

        this.state = {
            businessData: [],
            inputSearch: "",
            page: null,
            totalPage: null
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.getListAll();
    }
    componentWillUnmount() {
    }
    getListAll() {
        const meta = {
            page: 1,
            page_size: 20,
            sort: {},
            search: '',
            filter: {}
        };
        this.subscriptions['getManyBusiness'] = homeService.getMany(meta).subscribe(res => {
            let totalPage = Math.ceil(res.total / meta.page_size);
            this.setState({ businessData: res.results, page: res.page, totalPage: totalPage});
        });
    }
    goToSearch() {
        const meta = {
            page: 1,
            page_size: 20,
            sort: {},
            search: this.state.inputSearch,
            filter: {}
        };
        this.subscriptions['getManyBusiness'] = homeService.getMany(meta).subscribe(res => {
            let totalPage = Math.ceil(res.total / meta.page_size);
            this.setState({ businessData: res.results, page: res.page, totalPage: totalPage});
        });
    }
    onKeyUp(event) {
        if (event.key === 'Enter') {
            this.goToSearch();
        }
    }
    goToDetail(id) {
        this.props.history.push({ pathname: `/business-detail/${id}` });
    }
    goToChangePage(page) {
        const meta = {
            page: page,
            page_size: 20,
            sort: {},
            search: this.state.inputSearch,
            filter: {}
        };
        this.subscriptions['getManyBusiness'] = homeService.getMany(meta).subscribe(res => {
            let totalPage = Math.ceil(res.total / meta.page_size);
            this.setState({ businessData: res.results, page: res.page, totalPage: totalPage});
        });
    }
    render() {
        return (
            <div className="home-content">
                <Container className="home-container">
                    <Row>
                        <Col sm={12} md={12} className="home-header">
                            <Col sm={6} md={6} className="home-header-search" >
                                <InputGroup className="mb-3">
                                    <FormControl
                                        className="input-search"
                                        placeholder="Type the Name of Company or the Tax Code"
                                        aria-label="Type the Name of Company or the Tax Code"
                                        aria-describedby="basic-addon2"
                                        name="inputSearch"
                                        value={this.state.inputSearch}
                                        onKeyPress={this.onKeyUp}
                                        onChange={e => this.setState({ inputSearch: e.target.value })}
                                    />
                                    <Button variant="outline-secondary" type="submit" className="btn-search" id="button-addon1" onClick={() => { this.goToSearch(); }}>
                                        <CIcon content={freeSet.cilSearch} name="cil-search" className="icon-search" />
                                    </Button>
                                </InputGroup>
                            </Col>
                            <hr className="home-header-hr" />
                        </Col>
                    </Row>
                    <Row>
                        {
                            this.state.businessData.map((item) => {
                                return (
                                    <Col sm={12} md={6} key={item.id}>
                                        <Card className="body-card">
                                            <Card.Body>
                                                <Card.Title onClick={() => { this.goToDetail(item.id); }}>{item.name}</Card.Title>
                                                <Card.Text>{item.defaultAddress}</Card.Text>
                                                <Card.Text> Tax code: {item.taxCode} </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                );
                            })
                        }
                    </Row>
                    <Row className="row-pagination">
                        <CPagination
                            activePage={this.state.page}
                            pages={this.state.totalPage}
                            onActivePageChange={(i) => this.goToChangePage(i)}
                        >
                        </CPagination>
                    </Row>
                </Container>
            </div>
        )
    }
}

export { Home };