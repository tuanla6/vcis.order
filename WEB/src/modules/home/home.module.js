import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Home } from './home.component';
class HomeModule extends React.Component {
    static propTypes = {
        match: PropTypes.object,
    }
    render() {
        return (
            <Switch>                       
                <Route path={'/home'} render={(props) => <Home {...props} />} />
            </Switch>
        );
    }
}

export { HomeModule };