import { BaseService} from 'src/shared/services';
import { Subject } from 'rxjs';
class HomeService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/Businesses' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
}
const homeService = new HomeService();
export { homeService, HomeService };