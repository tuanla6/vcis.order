import { BaseService } from 'src/shared/services';
import { Subject } from 'rxjs';
import { http } from 'src/shared/utils';
class LineofBusinessService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/vcis-business-lineofbusiness' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getBusinessActivities(businessId) {
      return http.get('api/vcis-business-lineofbusiness?id='+businessId);
  }
}
const lineofBusinessService = new LineofBusinessService();

export { lineofBusinessService, LineofBusinessService };
