import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { CCol, CRow } from '@coreui/react';
import { RCard } from 'src/shared/component';
import './style.scss';
import { lineofBusinessService } from './service';
class LineofBusiness extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.id,
            loadingBusinessActivities: false,
            mainBusiness: [],
            listRegisteredBusiness: []
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.getBusinessActivities();
    }
    componentWillUnmount() {

    }
    getBusinessActivities() {
        let id = this.state.id;
        this.setState({ loadingBusinessActivities: true });
        if (id) {
            this.subscriptions['getBusinessActivities'] = lineofBusinessService.getBusinessActivities(id).subscribe(res => {
                this.setState({ mainBusiness: res.mainBusiness, listRegisteredBusiness: res.listRegisteredBusiness, loadingBusinessActivities: false });
            },
                err => {
                    console.log(err);
                    this.setState({ loadingBusinessActivities: false });
                }
            );
        }
    }
    render() {
        const listMain = this.state.mainBusiness?.listMainBusiness || [];
        return (
            <div className="content-business-activities">
                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Main business activities" className="card card-danger"  loading={this.state.loadingBusinessActivities}>
                            <div className="VSIC">
                                <div className="famres-title">VSIC</div>
                                <Container>
                                    <Row className="famres-row-header">
                                        <Col md={2}>Industry</Col>
                                        <Col md={2}>Sector</Col>
                                        <Col md={2}>Sub-sector</Col>
                                        <Col md={6}>Business activity</Col>
                                    </Row>
                                    <Row className="famres-row-content">
                                        <Col md={{ span: 12 }}>
                                            {
                                                listMain.filter((e) => e.businessLevel === 1)?.map((item, index) => {
                                                    return (
                                                        <span key={index}> {item.businessLineCode}: {item.businessName} </span>
                                                    );
                                                })
                                            }
                                        </Col>
                                    </Row>
                                    <Row className="famres-row-content">
                                        <Col md={{ span: 10, offset: 2 }}>
                                            {
                                                listMain.filter((e) => e.businessLevel === 2)?.map((item, index) => {
                                                    return (
                                                        <span key={index}> {item.businessLineCode}: {item.businessName} </span>
                                                    );
                                                })
                                            }
                                        </Col>
                                    </Row>
                                    <Row className="famres-row-content">
                                        <Col md={{ span: 8, offset: 4 }}>
                                            {
                                                listMain.filter((e) => e.businessLevel === 3)?.map((item, index) => {
                                                    return (
                                                        <span key={index}> {item.businessLineCode}: {item.businessName} </span>
                                                    );
                                                })
                                            } </Col>
                                    </Row>
                                    <Row className="famres-row-content">
                                        <Col md={{ span: 6, offset: 6 }}>
                                            {
                                                listMain.filter((e) => e.businessLevel === 4 || e.businessLevel === 5)?.map((item, index) => {
                                                    return (
                                                        <span key={index}> {item.businessLineCode}: {item.businessName} </span>
                                                    );
                                                })
                                            }
                                        </Col>
                                    </Row>
                                </Container>
                            </div>
                            <div className="VSIC NACE">
                                <div className="famres-title">NACE</div>
                                <Container>
                                    {this.state.mainBusiness != null ? (<Row className="famres-row-content">
                                        <Col md={{ span: 12 }}>{this.state.mainBusiness.nace}</Col>
                                    </Row>) : ("")
                                    }

                                </Container>
                            </div>

                            <div className="VSIC NAICS">
                                <div className="famres-title">NAICS</div>
                                <Container>
                                    {this.state.mainBusiness != null ? (<Row className="famres-row-content">
                                        <Col md={{ span: 12 }}>{this.state.mainBusiness.naics}</Col>
                                    </Row>) : ("")
                                    }
                                </Container>
                            </div>
                            <div className="VSIC NAICS">
                                <div className="products-and-services">Products and services</div>
                                <Container>
                                    {this.state.mainBusiness != null ? (<Row className="famres-row-content">
                                        <Col md={{ span: 12 }}><p dangerouslySetInnerHTML={{ __html: this.state.mainBusiness.productsAndServices }} /></Col>
                                    </Row>) : ("")
                                    }
                                </Container>
                            </div>
                        </RCard>
                    </CCol>
                </CRow>

                <CRow>
                    <CCol xs="12" lg="12">
                        <RCard title="Registered business activities" className="card card-danger"  loading={this.state.loadingBusinessActivities}>

                            <div className="VSIC NAICS">
                                <div className="famres-title">VSIC</div>
                                <Container className="container-registered-business-activities">
                                    {
                                        this.state.listRegisteredBusiness.map((item, index) => {
                                            return (
                                                <Col md={{ span: 12 }} key={index} className="naics-content">{item.businessLineCode}: {item.businessName}</Col>
                                            );
                                        })
                                    }
                                </Container>
                            </div>
                        </RCard>
                    </CCol>
                </CRow>
            </div>
        )
    }
}

export { LineofBusiness };