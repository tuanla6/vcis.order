import React from 'react';

const Tables = React.lazy(() => import('./views/base/tables/Tables'));
const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));
const BusinessDetail = React.lazy(() => import('./modules/businessDetail'));
const Home = React.lazy(() => import('./modules/home'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/business-detail/:id', exact: true, name: 'BusinessDetail', component: BusinessDetail },
  { path: '/home', exact: true, name: 'Home', component: Home },
];

export default routes;
