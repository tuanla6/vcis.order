import { OAUTH_SET_TOKEN, OAUTH_REMOVE_TOKEN } from './oauthConstant';
import jwt_decode from 'jwt-decode';
const tokenDefault = {
    id_token: null,
    access_token: null,
    expires_in: null,
    token_type: null,
    refresh_token: null,
    permissions: [],
    profiles: null,
};
function processToken(token) {
    const tokenInfo = jwt_decode(token.access_token);
    console.log('tokenInfo=', tokenInfo);
    let result = {
        profiles: {
            id: tokenInfo.id,
            email: tokenInfo.email,
            tai_khoan: tokenInfo.tai_khoan,
            ten: tokenInfo.ten
        },
        permissions: tokenInfo.permission ? JSON.parse(tokenInfo.permission) : [],
    };
    result = Object.assign({}, token, result);

    return result;
}
const oauthReducer = (state = tokenDefault, action) => {
    switch (action.type) {
    case OAUTH_SET_TOKEN:
        return Object.assign({}, state, processToken(action.token));
    case OAUTH_REMOVE_TOKEN:
        return Object.assign({}, state, tokenDefault);
    default:
        return state;
    }
};

export { oauthReducer };