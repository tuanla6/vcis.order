import { combineReducers } from 'redux';
import { oauthReducer } from './oauth/oauthReducer';

const reducer = combineReducers({
    oauth: oauthReducer
});
export default reducer;

