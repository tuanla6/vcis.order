import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'
import { persistor, store } from './redux/store';
import { PersistGate } from 'redux-persist/lib/integration/react';
import './scss/style.scss';
import { TheLayout } from './containers/TheLayout';
import Login from './views/pages/login/Login';
import { RouteGuardComponent as RouteGuard } from 'src/shared/component';
import OauthCallback from 'src/modules/oauth-callback/OauthCallback';

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)


class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={loading} persistor={persistor}>
            <Router>
              <Switch>
                <Route path="/oauth-callback" component={OauthCallback} />
                <Route exact path="/login" name="Login Page" component={Login} />
                <RouteGuard path="/" component={TheLayout} />
              </Switch>
            </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
