const _nav =  [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['User']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Business Deatail',
    to: '/business-detail',
    icon: 'cil-list-rich',
  },
  
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Business Logs']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Business Logs',
    to: '/base',
    icon: 'cil-settings',
  },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Download History',
  //   to: '/buttons',
  //   icon: 'cil-cloud-download',
  // },
  // //
  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['REQUEST'],
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Requests',
  //   to: '/pages',
  //   icon: 'cil-pencil',
  // },
  // //
  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['ECOMMERCE'],
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Customers',
  //   to: '/pages',
  //   icon: 'cil-user',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Orders',
  //   to: '/pages',
  //   icon: 'cil-list-rich',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Invoices',
  //   to: '/pages',
  //   icon: 'cil-star',
  // },

  // {
  //   _tag: 'CSidebarNavTitle',
  //   _children: ['SETTING'],
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Elastic Search',
  //   to: '/pages',
  //   icon: 'cil-layers',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Report Types',
  //   to: '/pages',
  //   icon: 'cil-star',
  // },
  // {
  //   _tag: 'CSidebarNavItem',
  //   name: 'Settings',
  //   to: '/pages',
  //   icon: 'cil-settings',
  // },
]

export default _nav
