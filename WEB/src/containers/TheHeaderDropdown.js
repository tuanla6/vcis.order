import React from 'react'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { authService } from 'src/shared/services';
import { connect } from 'react-redux';

class TheHeaderDropdownComponent extends React.Component {

  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);
    var profiles = this.props.user.profiles;
    console.log('profiles', profiles);
    this.state = {
      user_info: profiles
    };
    sessionStorage.setItem('user_info', JSON.stringify(profiles));
  }

  logout() {
    authService.logout();
  }

  render() {
    return (
      <CDropdown
        inNav
        className="c-header-nav-items mx-2"
        direction="down"
      >
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <div className="c-avatar">
            <CImg
              src={'avatars/6.jpg'}
              className="c-avatar-img"
              alt="admin@bootstrapmaster.com"
            />
          </div>
        </CDropdownToggle>
        <CDropdownMenu className="pt-0" placement="bottom-end">
          <CDropdownItem>
            <CIcon name="cil-user" className="mfe-2" />Profile
          </CDropdownItem>
          <CDropdownItem divider />
          <CDropdownItem onClick={this.logout}>
            <CIcon name="cil-lock-locked" className="mfe-2" />
            Logout
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.oauth
  };
};
const TheHeaderDropdown = connect(mapStateToProps, {})(TheHeaderDropdownComponent);
export default TheHeaderDropdown