import React from 'react'
import {
  TheContent,
  TheSidebar,
  TheFooter,
  TheHeader
} from './index'

class TheLayout extends React.Component {
  componentDidMount() {
    document.querySelector('.c-sidebar-fixed')?.classList.add('c-sidebar-minimized');
  }
  render() {
    return (
      <div className="c-app c-default-layout">
        <TheSidebar />
        <div className="c-wrapper">
          <TheHeader />
          <div className="c-body">
            <TheContent />
          </div>
          <TheFooter />
        </div>
      </div>
    )
  };
}

export { TheLayout }
