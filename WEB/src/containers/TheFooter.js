import React from 'react'
import { CFooter } from '@coreui/react'

class TheFooter extends React.Component {
  render() {
    return (
      <CFooter fixed={false}>
        <div>
          <span className="ml-1">&copy; Copyright © 2021 </span>
          <a href="https://vietnamcredit.com.vn/" target="_blank" rel="noopener noreferrer">VietnamCredit</a>
          <span className="ml-1">&copy; . All rights reserved.</span>
        </div>
        <div className="mfs-auto">
          <span className="mr-1">Version 3.0.0</span>

        </div>
      </CFooter>
    )
  }
}
export { TheFooter }
